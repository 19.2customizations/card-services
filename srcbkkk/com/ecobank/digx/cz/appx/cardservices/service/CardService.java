package com.ecobank.digx.cz.appx.cardservices.service;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import com.ecobank.digx.cz.app.cardservices.dto.CardActivityDTO;
import com.ecobank.digx.cz.app.cardservices.dto.CardActivityListResponse;
import com.ecobank.digx.cz.app.cardservices.dto.CardBalanceDTO;
import com.ecobank.digx.cz.app.cardservices.dto.CardBalanceResponse;
import com.ecobank.digx.cz.app.cardservices.dto.CardListResponse;
import com.ecobank.digx.cz.app.cardservices.dto.CardRequestListResponse;
import com.ecobank.digx.cz.app.cardservices.dto.CardServiceCreateRequestDTO;
import com.ecobank.digx.cz.app.cardservices.dto.CardServiceCreateResponseDTO;
import com.ecobank.digx.cz.app.cardservices.dto.CardServiceDTO;
import com.ecobank.digx.cz.app.cardservices.dto.CardServiceResponse;
import com.ecobank.digx.cz.app.cardservices.dto.CardServiceResponseDTO;
import com.ecobank.digx.cz.app.cardservices.dto.CardServiceUpdateRequestDTO;
import com.ecobank.digx.cz.extxface.mule.dto.ESBCardResponseDTO;
import com.ofss.digx.app.context.ChannelContext;
import com.ofss.digx.app.core.ChannelInteraction;
import com.ofss.digx.app.messages.Status;
import com.ofss.digx.appx.AbstractRESTApplication;
import com.ofss.digx.appx.PATCH;
import com.ofss.digx.infra.exceptions.Exception;
import com.ofss.digx.service.response.BaseResponseObject;
import com.ofss.fc.infra.log.impl.MultiEntityLogger;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

@Path("/cardService")
public class CardService extends AbstractRESTApplication implements ICardService {

	private static final String THIS_COMPONENT_NAME = CardService.class.getName();
	private static transient Logger logger = MultiEntityLogger.getUniqueInstance().getLogger(THIS_COMPONENT_NAME);
	private static transient MultiEntityLogger formatter = MultiEntityLogger.getUniqueInstance();

	@Override
	public Response read(String accNo) {
		// TODO Auto-generated method stub
		return null;
	}

	@POST
	@Produces({ "application/json" })
	@Consumes({ "application/json" })
	@Operation(summary = "card services", description = "This API allows to fund a virtual and prepaid card", tags = {
			"CardService" }, operationId = "com.ofss.digx.appx.payment.service.transfer.SelfTransfer.create")
	@ApiResponses({
			@io.swagger.v3.oas.annotations.responses.ApiResponse(responseCode = "201", description = "New Virtual or Prepaid Card Created", content = {
					@io.swagger.v3.oas.annotations.media.Content(mediaType = "application/json", schema = @Schema(implementation = CardServiceCreateResponseDTO.class)) }),
			@io.swagger.v3.oas.annotations.responses.ApiResponse(responseCode = "400", description = "Validation Failure", content = {
					@io.swagger.v3.oas.annotations.media.Content(mediaType = "application/json", schema = @Schema(implementation = com.ofss.digx.app.messages.Status.class)) }),
			@io.swagger.v3.oas.annotations.responses.ApiResponse(responseCode = "500", description = "Internal Server Error", content = {
					@io.swagger.v3.oas.annotations.media.Content(mediaType = "application/json", schema = @Schema(implementation = com.ofss.digx.app.messages.Status.class)) }) })

	public Response createCard(
			@RequestBody(description = "Card Generation & Physical card Request", required = true, content = {
					@io.swagger.v3.oas.annotations.media.Content(schema = @Schema(implementation = CardServiceDTO.class)) }) CardServiceDTO cardServiceDTO) {

		System.out.println("Amount    : " + cardServiceDTO.getAmount());
		System.out.println("MobileNo  : " + cardServiceDTO.getMobileNo());
		System.out.println("NameOnCard: " + cardServiceDTO.getNameOnCard());

		String sAcc = cardServiceDTO.getSourceAccountId().getValue();
		System.out.println("Source Account: " + sAcc);
		// C35@~0022069204
		int x = sAcc.indexOf("~");
		String sourceAccount = sAcc.substring(x + 1);
		cardServiceDTO.setSourceAccountNo(sourceAccount);
		cardServiceDTO.setCustomerNo(sourceAccount);

		if (logger.isLoggable(Level.FINE)) {
			logger.log(Level.FINE,
					formatter.formatMessage("Entered into create of Virtual/Prepaid card REST Service  Input: %s",
							new Object[] { cardServiceDTO }));
		}
		Response response = null;
		ChannelInteraction channelInteraction = null;
		ChannelContext channelContext = null;
		CardServiceCreateResponseDTO cardServiceResponse = null;
		try {
			channelContext = super.getChannelContext();
			channelInteraction = ChannelInteraction.getInstance();
			channelInteraction.begin(channelContext);

			String userId = channelContext.getSessionContext().getUserId();
			cardServiceDTO.setUserId(userId);

			CardServiceCreateRequestDTO cardServiceCreateRequestDTO = new CardServiceCreateRequestDTO();
			cardServiceCreateRequestDTO.setCardServiceDetails(cardServiceDTO);

			com.ecobank.digx.cz.app.cardservices.service.CardServices cardServices = new com.ecobank.digx.cz.app.cardservices.service.CardServices();
			cardServiceResponse = cardServices.create(channelContext.getSessionContext(), cardServiceCreateRequestDTO);

			response = buildResponse(cardServiceResponse, Response.Status.CREATED);

		} catch (Exception e) {
			logger.log(Level.SEVERE,
					formatter.formatMessage(
							"Exception encountered while invoking the create service for AccountTransferRequestDTO=%s",
							new Object[] { channelContext }),
					e);

			response = buildResponse(e, Response.Status.BAD_REQUEST);
		} catch (java.lang.Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				channelInteraction.close(channelContext);
			} catch (Exception e) {
				logger.log(Level.SEVERE, formatter.formatMessage("Error encountered while closing channelContext %s",
						new Object[] { channelContext }), e);

				response = buildResponse(e, Response.Status.INTERNAL_SERVER_ERROR);
			}
		}
		logger.log(Level.FINE, formatter.formatMessage("Exiting from create() : AccountTransferCreateResponse=%s",
				new Object[] { cardServiceResponse }));

		return response;
	}

	@POST
	@Path("/createcardactivity")
	@Produces({ "application/json" })
	@Consumes({ "application/json" })
	@Operation(summary = "card activity", description = "This API allows you to perform operation or activity on the card", tags = {
			"CardService" }, operationId = "com.ecobank.digx.cz.appx.cardservices.service.CardService.createCardActivity")
	@ApiResponses({
			@io.swagger.v3.oas.annotations.responses.ApiResponse(responseCode = "201", description = "Perform card activity", content = {
					@io.swagger.v3.oas.annotations.media.Content(mediaType = "application/json", schema = @Schema(implementation = CardServiceCreateResponseDTO.class)) }),
			@io.swagger.v3.oas.annotations.responses.ApiResponse(responseCode = "400", description = "Validation Failure", content = {
					@io.swagger.v3.oas.annotations.media.Content(mediaType = "application/json", schema = @Schema(implementation = com.ofss.digx.app.messages.Status.class)) }),
			@io.swagger.v3.oas.annotations.responses.ApiResponse(responseCode = "500", description = "Internal Server Error", content = {
					@io.swagger.v3.oas.annotations.media.Content(mediaType = "application/json", schema = @Schema(implementation = com.ofss.digx.app.messages.Status.class)) }) })

	public Response createCardActivity(@RequestBody(description = "Create card activity", required = true, content = {
			@io.swagger.v3.oas.annotations.media.Content(schema = @Schema(implementation = CardActivityDTO.class)) }) CardActivityDTO cardServiceDTO) {

		System.out.println("Card Id    : " + cardServiceDTO.getCardId());

		if (logger.isLoggable(Level.FINE)) {
			logger.log(Level.FINE, formatter.formatMessage("Entered into create of Virtual/Prepaid card REST Service  Input: %s", new Object[] { cardServiceDTO }));
		}
		Response response = null;
		ChannelInteraction channelInteraction = null;
		ChannelContext channelContext = null;
		CardServiceResponseDTO cardServiceResponse = null;
		try {
			channelContext = super.getChannelContext();
			channelInteraction = ChannelInteraction.getInstance();
			channelInteraction.begin(channelContext);

			if (cardServiceDTO.getSourceAccountId() != null) {
				String sAcc = cardServiceDTO.getSourceAccountId().getValue();
				System.out.println("Source Account: " + sAcc);
				int x = sAcc.indexOf("~");
				String sourceAccount = sAcc.substring(x + 1);
				cardServiceDTO.setSourceAccountNo(sourceAccount);
			}

			String userId = channelContext.getSessionContext().getUserId();
			cardServiceDTO.setUserId(userId);

			com.ecobank.digx.cz.app.cardservices.service.CardServices cardServices = new com.ecobank.digx.cz.app.cardservices.service.CardServices();
			cardServiceResponse = cardServices.createCardActivity(channelContext.getSessionContext(), cardServiceDTO);

			response = buildResponse(cardServiceResponse, Response.Status.CREATED);

		} catch (Exception e) {
			logger.log(Level.SEVERE,
					formatter.formatMessage(
							"Exception encountered while invoking the create service for createCardActivity=%s",
							new Object[] { channelContext }),
					e);

			response = buildResponse(e, Response.Status.BAD_REQUEST);
		} catch (java.lang.Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				channelInteraction.close(channelContext);
			} catch (Exception e) {
				logger.log(Level.SEVERE,
						formatter.formatMessage("Error encountered while createCardActivity channelContext %s",
								new Object[] { channelContext }),
						e);

				response = buildResponse(e, Response.Status.INTERNAL_SERVER_ERROR);
			}
		}
		logger.log(Level.FINE, formatter.formatMessage("Exiting from create() : createCardActivity=%s",
				new Object[] { cardServiceResponse }));

		return response;
	}

	@PATCH
	@Path("/{referenceId}")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	@Operation(summary = "Update or process card action or activity or request", description = "Updates card action or activity", tags = {
			"Payments" }, responses = {
					@io.swagger.v3.oas.annotations.responses.ApiResponse(responseCode = "200", description = "Card Details updated sucessfully", content = {
							@io.swagger.v3.oas.annotations.media.Content(mediaType = "application/json", schema = @Schema(implementation = CardServiceResponse.class)) }),
					@io.swagger.v3.oas.annotations.responses.ApiResponse(responseCode = "400", description = "Validation Failure", content = {
							@io.swagger.v3.oas.annotations.media.Content(mediaType = "application/json", schema = @Schema(implementation = com.ofss.digx.app.messages.Status.class)) }),
					@io.swagger.v3.oas.annotations.responses.ApiResponse(responseCode = "500", description = "Internal Server Error", content = {
							@io.swagger.v3.oas.annotations.media.Content(mediaType = "application/json", schema = @Schema(implementation = com.ofss.digx.app.messages.Status.class)) }) }, operationId = "com.ofss.digx.appx.payment.service.transfer.SelfTransfer.updateStatus")
	public Response updateCard(
			@Parameter(in = ParameterIn.PATH, name = "referenceId", description = "Card-Service Reference Id", required = true, schema = @Schema(type = "string", description = "unique id of the card request")) @PathParam("referenceId") String referenceId,
			@Parameter(in = ParameterIn.QUERY, description = "Activity or Action code", required = true, name = "actionCode", schema = @Schema(type = "String")) @QueryParam("actionCode") String actionCode) {
		if (logger.isLoggable(Level.FINE)) {
			logger.log(Level.FINE,
					formatter.formatMessage("Entering updateStatus of Card Service Rest service, reference Id: %s",
							new Object[] { referenceId }));
		}
		Response response = null;
		ChannelContext channelContext = null;
		ChannelInteraction channelInteraction = null;
		CardServiceUpdateRequestDTO cardServiceUpdateRequestDTO = null;
		CardServiceResponseDTO cardServiceResponse = null;
		try {
			channelContext = super.getChannelContext();
			channelInteraction = ChannelInteraction.getInstance();
			channelInteraction.begin(channelContext);
			System.out.println("referenceId ::: " + referenceId);
			cardServiceUpdateRequestDTO = new CardServiceUpdateRequestDTO();
			cardServiceUpdateRequestDTO.setPaymentId(referenceId);
			cardServiceUpdateRequestDTO.setActionCode(actionCode);
			cardServiceResponse = new CardServiceResponseDTO();
			com.ecobank.digx.cz.app.cardservices.service.CardServices cardServices = new com.ecobank.digx.cz.app.cardservices.service.CardServices();
			cardServiceResponse = cardServices.proocessCardRequest(channelContext.getSessionContext(),
					cardServiceUpdateRequestDTO);

			response = buildResponse(cardServiceResponse, Response.Status.OK);

		} catch (com.ofss.digx.infra.exceptions.Exception e) {
			logger.log(Level.SEVERE,
					formatter.formatMessage(
							"Exception encountered while invoking the updateStatus service for reference Id: %s",
							new Object[] { referenceId }),
					e);

			response = buildResponse(e, Response.Status.BAD_REQUEST);
		} finally {
			try {
				channelInteraction.close(channelContext);
			} catch (com.ofss.digx.infra.exceptions.Exception e) {
				// logger.log(Level.SEVERE, formatter.formatMessage("Error encountered while
				// closing channelContext %s",
				// new Object[] { channelContext }), e);

				response = buildResponse(e, Response.Status.INTERNAL_SERVER_ERROR);
			}
		}
		logger.log(Level.FINE, formatter.formatMessage("Exiting from updateStatus() : CardServiceResponse=%s",
				new Object[] { cardServiceResponse }));

		return response;
	}
	
	
	@PATCH
	@Path("/updatecardrequest/{referenceId}")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	@Operation(summary = "Update new cardrequest", description = "Updates new card request", tags = {
			"Payments" }, responses = {
					@io.swagger.v3.oas.annotations.responses.ApiResponse(responseCode = "200", description = "Card Details updated sucessfully", content = {
							@io.swagger.v3.oas.annotations.media.Content(mediaType = "application/json", schema = @Schema(implementation = CardServiceResponse.class)) }),
					@io.swagger.v3.oas.annotations.responses.ApiResponse(responseCode = "400", description = "Validation Failure", content = {
							@io.swagger.v3.oas.annotations.media.Content(mediaType = "application/json", schema = @Schema(implementation = com.ofss.digx.app.messages.Status.class)) }),
					@io.swagger.v3.oas.annotations.responses.ApiResponse(responseCode = "500", description = "Internal Server Error", content = {
							@io.swagger.v3.oas.annotations.media.Content(mediaType = "application/json", schema = @Schema(implementation = com.ofss.digx.app.messages.Status.class)) }) }, operationId = "com.ecobank.digx.cz.appx.cardservices.service.CardService.updateCardRequest")
	public Response updateCardRequest(
			@Parameter(in = ParameterIn.PATH, name = "referenceId", description = "Card-Service Reference Id", required = true, schema = @Schema(type = "string", description = "unique id of the card request")) @PathParam("referenceId") String referenceId,
			@Parameter(in = ParameterIn.QUERY, description = "Activity or Action code", required = true, name = "actionCode", schema = @Schema(type = "String")) @QueryParam("actionCode") String actionCode) {
		if (logger.isLoggable(Level.FINE)) {
			logger.log(Level.FINE,
					formatter.formatMessage("Entering updateCardRequest of Card Service Rest service, reference Id: %s",
							new Object[] { referenceId }));
		}
		Response response = null;
		ChannelContext channelContext = null;
		ChannelInteraction channelInteraction = null;
		CardServiceUpdateRequestDTO cardServiceUpdateRequestDTO = null;
		CardServiceResponseDTO cardServiceResponse = null;
		try {
			channelContext = super.getChannelContext();
			channelInteraction = ChannelInteraction.getInstance();
			channelInteraction.begin(channelContext);
			System.out.println("referenceId ::: " + referenceId);
			cardServiceUpdateRequestDTO = new CardServiceUpdateRequestDTO();
			cardServiceUpdateRequestDTO.setPaymentId(referenceId);
			cardServiceUpdateRequestDTO.setActionCode(actionCode);
			cardServiceResponse = new CardServiceResponseDTO();
			com.ecobank.digx.cz.app.cardservices.service.CardServices cardServices = new com.ecobank.digx.cz.app.cardservices.service.CardServices();
			cardServiceResponse = cardServices.updateCardRequest(channelContext.getSessionContext(),
					cardServiceUpdateRequestDTO);

			response = buildResponse(cardServiceResponse, Response.Status.OK);

		} catch (com.ofss.digx.infra.exceptions.Exception e) {
			logger.log(Level.SEVERE,
					formatter.formatMessage(
							"Exception encountered while invoking the updateStatus service for reference Id: %s",
							new Object[] { referenceId }),
					e);

			response = buildResponse(e, Response.Status.BAD_REQUEST);
		} finally {
			try {
				channelInteraction.close(channelContext);
			} catch (com.ofss.digx.infra.exceptions.Exception e) {
				// logger.log(Level.SEVERE, formatter.formatMessage("Error encountered while
				// closing channelContext %s",
				// new Object[] { channelContext }), e);

				response = buildResponse(e, Response.Status.INTERNAL_SERVER_ERROR);
			}
		}
		logger.log(Level.FINE, formatter.formatMessage("Exiting from updateCardRequest() : CardServiceResponse=%s",
				new Object[] { cardServiceResponse }));

		return response;
	}

	@GET
	@Path("/blockCard")
	@Produces({ "application/json" })
	@Consumes({ "application/json" })
	@Operation(summary = "Block Card", description = "This API allows to Block Card", tags = {
			"ESBCardRequestDTO" }, operationId = "com.ecobank.digx.cz.appx.cardservices.service.CardService.blockCard")
	@ApiResponses({
			@io.swagger.v3.oas.annotations.responses.ApiResponse(responseCode = "201", description = "Block Card", content = {
					@io.swagger.v3.oas.annotations.media.Content(mediaType = "application/json", schema = @Schema(implementation = ESBCardResponseDTO.class)) }),
			@io.swagger.v3.oas.annotations.responses.ApiResponse(responseCode = "400", description = "Validation Failure", content = {
					@io.swagger.v3.oas.annotations.media.Content(mediaType = "application/json", schema = @Schema(implementation = com.ofss.digx.app.messages.Status.class)) }),
			@io.swagger.v3.oas.annotations.responses.ApiResponse(responseCode = "500", description = "Internal Server Error", content = {
					@io.swagger.v3.oas.annotations.media.Content(mediaType = "application/json", schema = @Schema(implementation = com.ofss.digx.app.messages.Status.class)) }) })
	public Response blockCard(
			@Parameter(in = ParameterIn.QUERY, description = "card Id", required = true, name = "cardId", schema = @Schema(type = "String")) @QueryParam("cardId") String cardId) {

		System.out.println("cardId 22  : " + cardId);

		Response response = null;
		ChannelInteraction channelInteraction = null;
		ChannelContext channelContext = null;
		CardServiceResponseDTO cardServiceResponseDTO = null;
		try {
			channelContext = super.getChannelContext();
			channelInteraction = ChannelInteraction.getInstance();
			channelInteraction.begin(channelContext);
			com.ecobank.digx.cz.app.cardservices.service.CardServices cardServices = new com.ecobank.digx.cz.app.cardservices.service.CardServices();
			cardServiceResponseDTO = cardServices.blockCard(channelContext.getSessionContext(), cardId);

			response = buildResponse(cardServiceResponseDTO, Response.Status.OK);

		} catch (Exception e) {

			response = buildResponse(e, Response.Status.BAD_REQUEST);
		} finally {
			try {
				channelInteraction.close(channelContext);
			} catch (Exception e) {
				logger.log(Level.SEVERE, formatter.formatMessage("Error encountered while closing channelContext %s",
						new Object[] { channelContext }), e);

				response = buildResponse(e, Response.Status.INTERNAL_SERVER_ERROR);
			}
		}
		logger.log(Level.FINE, formatter.formatMessage("Exiting from create() : CardServiceCreateResponse=%s",
				new Object[] { cardServiceResponseDTO }));

		return response;
	}

	@GET
	@Path("/unblockCard")
	@Produces({ "application/json" })
	@Consumes({ "application/json" })
	@Operation(summary = "unBlock Card", description = "This API allows to unblock Card", tags = {
			"ESBCardRequestDTO" }, operationId = "com.ecobank.digx.cz.appx.cardservices.service.CardService.unblockCard")
	@ApiResponses({
			@io.swagger.v3.oas.annotations.responses.ApiResponse(responseCode = "201", description = "unBlock Card", content = {
					@io.swagger.v3.oas.annotations.media.Content(mediaType = "application/json", schema = @Schema(implementation = ESBCardResponseDTO.class)) }),
			@io.swagger.v3.oas.annotations.responses.ApiResponse(responseCode = "400", description = "Validation Failure", content = {
					@io.swagger.v3.oas.annotations.media.Content(mediaType = "application/json", schema = @Schema(implementation = com.ofss.digx.app.messages.Status.class)) }),
			@io.swagger.v3.oas.annotations.responses.ApiResponse(responseCode = "500", description = "Internal Server Error", content = {
					@io.swagger.v3.oas.annotations.media.Content(mediaType = "application/json", schema = @Schema(implementation = com.ofss.digx.app.messages.Status.class)) }) })
	public Response unblockCard(
			@Parameter(in = ParameterIn.QUERY, description = "card Id", required = true, name = "cardId", schema = @Schema(type = "String")) @QueryParam("cardId") String cardId) {

		System.out.println("cardId  : " + cardId);

		Response response = null;
		ChannelInteraction channelInteraction = null;
		ChannelContext channelContext = null;
		CardServiceResponseDTO cardServiceResponseDTO = null;
		try {
			channelContext = super.getChannelContext();
			channelInteraction = ChannelInteraction.getInstance();
			channelInteraction.begin(channelContext);
			com.ecobank.digx.cz.app.cardservices.service.CardServices cardServices = new com.ecobank.digx.cz.app.cardservices.service.CardServices();
			cardServiceResponseDTO = cardServices.unblockCard(channelContext.getSessionContext(), cardId);

			response = buildResponse(cardServiceResponseDTO, Response.Status.OK);

		} catch (Exception e) {

			response = buildResponse(e, Response.Status.BAD_REQUEST);
		} finally {
			try {
				channelInteraction.close(channelContext);
			} catch (Exception e) {
				logger.log(Level.SEVERE, formatter.formatMessage("Error encountered while closing channelContext %s",
						new Object[] { channelContext }), e);

				response = buildResponse(e, Response.Status.INTERNAL_SERVER_ERROR);
			}
		}
		logger.log(Level.FINE, formatter.formatMessage("Exiting from create() : CardServiceCreateResponse=%s",
				new Object[] { cardServiceResponseDTO }));

		return response;
	}

	@Path("/listcardactivity/{cardId}")
	@GET
	@Produces({ "application/json" })
	@Operation(summary = "fetch list of card action or activity per card", description = "fetch card activity list", tags = {
			"Fetch card activity" }, operationId = "com.ecobank.digx.cz.appx.cardservices.service.CardService.listCardActivityById")
	@ApiResponses({ @ApiResponse(responseCode = "200", description = "Fetch Successful", content = {
			@Content(mediaType = "application/json", schema = @Schema(implementation = CardActivityListResponse.class)) }),
			@ApiResponse(responseCode = "400", description = "Validation Failure", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = Status.class)) }),
			@ApiResponse(responseCode = "500", description = "Internal Server Error", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = Status.class)) }) })
	public Response listCardActivityById(
			@Parameter(in = ParameterIn.PATH, description = "card id", required = true, name = "cardId", schema = @Schema(type = "String")) @PathParam("cardId") String cardId) {

		System.out.println("listCardActivityById-REST: " + cardId);

		if (logger.isLoggable(Level.FINE))
			logger.log(Level.FINE, this.formatter.formatMessage(" Receive card id " + cardId));

		CardActivityListResponse responseDTO = new CardActivityListResponse();

		Response response = null;
		ChannelInteraction channelInteraction = null;
		ChannelContext channelContext = null;
		try {
			channelContext = getChannelContext();
			channelInteraction = ChannelInteraction.getInstance();
			channelInteraction.begin(channelContext);

			String bankCode = channelContext.getSessionContext().getBankCode();

			String affCode = channelContext.getSessionContext().getTargetUnit();
			String userId = channelContext.getSessionContext().getUserId();

			com.ecobank.digx.cz.app.cardservices.service.CardServices cardServices = new com.ecobank.digx.cz.app.cardservices.service.CardServices();
			responseDTO = cardServices.listCardActivityById(channelContext.getSessionContext(), cardId); // .read(channelContext.getSessionContext(),
																											// request);

			response = buildResponse((BaseResponseObject) responseDTO, Response.Status.OK);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Error-REST: " + e.getMessage());

			response = buildResponse(e, Response.Status.BAD_REQUEST);
		} finally {
			try {
				channelInteraction.close(channelContext);
			} catch (Exception e) {
				// logger.log(Level.SEVERE, this.formatter.formatMessage("Exception while
				// closing the channel interaction", new Object[] { AccessPoint.class
				// .getName() }), (Throwable)e);
				e.printStackTrace();
				response = buildResponse(e, Response.Status.INTERNAL_SERVER_ERROR);
			}
		}
		// if (logger.isLoggable(Level.FINE))
		/// logger.log(Level.FINE, this.formatter
		// .formatMessage("Exiting read AccessPoint, AccessPointResponse: %s", new
		// Object[] { accessPointResponse }));

		return response;
	}

	@Path("/getcardbalance/{cardId}")
	@GET
	@Produces({ "application/json" })
	@Operation(summary = "get card balance", description = "get card balance", tags = {
			"get card balance" }, operationId = "com.ecobank.digx.cz.appx.cardservices.service.CardService.getCardBalance")
	@ApiResponses({ @ApiResponse(responseCode = "200", description = "Fetch Successful", content = {
			@Content(mediaType = "application/json", schema = @Schema(implementation = CardActivityListResponse.class)) }),
			@ApiResponse(responseCode = "400", description = "Validation Failure", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = Status.class)) }),
			@ApiResponse(responseCode = "500", description = "Internal Server Error", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = Status.class)) }) })
	public Response getCardBalance(
			@Parameter(in = ParameterIn.PATH, description = "card id", required = true, name = "cardId", schema = @Schema(type = "String")) @PathParam("cardId") String cardId,
			@Parameter(in = ParameterIn.QUERY, description = "card Category", required = true, name = "cardCategory", schema = @Schema(type = "String")) @QueryParam("cardCategory") String cardCategory) {
		
		System.out.println("get card balance: " + cardId + " cardCategory: " + cardCategory);

		if (logger.isLoggable(Level.FINE))
			logger.log(Level.FINE, this.formatter.formatMessage(" Receive card balance card id " + cardId));

		CardBalanceResponse responseDTO = new CardBalanceResponse();

		Response response = null;
		ChannelInteraction channelInteraction = null;
		ChannelContext channelContext = null;
		try {
			channelContext = getChannelContext();
			channelInteraction = ChannelInteraction.getInstance();
			channelInteraction.begin(channelContext);

			String bankCode = channelContext.getSessionContext().getBankCode();

			String affCode = channelContext.getSessionContext().getTargetUnit();
			String userId = channelContext.getSessionContext().getUserId();

			System.out.println("bankCode: " + bankCode + " affCode: " + affCode + " userId: " + userId);

			CardBalanceDTO requestDTO = new CardBalanceDTO();

			requestDTO.setCardId(cardId);
			requestDTO.setCardCategory(cardCategory);

			com.ecobank.digx.cz.app.cardservices.service.CardServices cardServices = new com.ecobank.digx.cz.app.cardservices.service.CardServices();
			responseDTO = cardServices.balanceCheck(channelContext.getSessionContext(), requestDTO);
			System.out.println("name: " + responseDTO.getNameOnCard() + " balance: " + responseDTO.getBalance());
			response = buildResponse((BaseResponseObject) responseDTO, Response.Status.OK);

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Error-REST: " + e.getMessage());

			response = buildResponse(e, Response.Status.BAD_REQUEST);
		} finally {
			try {
				channelInteraction.close(channelContext);
			} catch (Exception e) {
				// logger.log(Level.SEVERE, this.formatter.formatMessage("Exception while
				// closing the channel interaction", new Object[] { AccessPoint.class
				// .getName() }), (Throwable)e);
				e.printStackTrace();
				response = buildResponse(e, Response.Status.INTERNAL_SERVER_ERROR);
			}
		}
		// if (logger.isLoggable(Level.FINE))
		/// logger.log(Level.FINE, this.formatter
		// .formatMessage("Exiting read AccessPoint, AccessPointResponse: %s", new
		// Object[] { accessPointResponse }));

		return response;
	}

	@Path("/listcardsbycustomerId/{customerId}")
	@GET
	@Produces({ "application/json" })
	@Operation(summary = "fetch list of cards for customer", description = "fetch list of cards for customer", tags = {
			"fetch list of cards for customer" }, operationId = "com.ecobank.digx.cz.appx.cardservices.service.CardService.listCardsByCustomerId")
	@ApiResponses({ @ApiResponse(responseCode = "200", description = "Fetch Successful", content = {
			@Content(mediaType = "application/json", schema = @Schema(implementation = CardActivityListResponse.class)) }),
			@ApiResponse(responseCode = "400", description = "Validation Failure", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = Status.class)) }),
			@ApiResponse(responseCode = "500", description = "Internal Server Error", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = Status.class)) }) })
	public Response listCardsByCustomerId(
			@Parameter(in = ParameterIn.PATH, description = "customer id", required = true, name = "customerId", schema = @Schema(type = "String")) @PathParam("customerId") String customerId) {

		System.out.println("listcardsbycustomerId-REST: " + customerId);

		if (logger.isLoggable(Level.FINE))
			logger.log(Level.FINE, this.formatter.formatMessage(" Receive list of cards " + customerId));

		CardListResponse responseDTO = new CardListResponse();

		Response response = null;
		ChannelInteraction channelInteraction = null;
		ChannelContext channelContext = null;
		try {
			channelContext = getChannelContext();
			channelInteraction = ChannelInteraction.getInstance();
			channelInteraction.begin(channelContext);

			String bankCode = channelContext.getSessionContext().getBankCode();

			String affCode = channelContext.getSessionContext().getTargetUnit();
			String userId = channelContext.getSessionContext().getUserId();

			com.ecobank.digx.cz.app.cardservices.service.CardServices cardServices = new com.ecobank.digx.cz.app.cardservices.service.CardServices();
			responseDTO = cardServices.listCardsByCustomerId(channelContext.getSessionContext(), customerId);

			response = buildResponse((BaseResponseObject) responseDTO, Response.Status.OK);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Error-REST: " + e.getMessage());

			response = buildResponse(e, Response.Status.BAD_REQUEST);
		} finally {
			try {
				channelInteraction.close(channelContext);
			} catch (Exception e) {
				// logger.log(Level.SEVERE, this.formatter.formatMessage("Exception while
				// closing the channel interaction", new Object[] { AccessPoint.class
				// .getName() }), (Throwable)e);
				e.printStackTrace();
				response = buildResponse(e, Response.Status.INTERNAL_SERVER_ERROR);
			}
		}

		return response;
	}

	@Path("/listcardrequestbycustomerId/{customerId}")
	@GET
	@Produces({ "application/json" })
	@Operation(summary = "fetch list of cards for customer", description = "fetch list of cards for customer", tags = {
			"fetch list of cards for customer" }, operationId = "com.ecobank.digx.cz.appx.cardservices.service.CardService.listCardsByCustomerId")
	@ApiResponses({ @ApiResponse(responseCode = "200", description = "Fetch Successful", content = {
			@Content(mediaType = "application/json", schema = @Schema(implementation = CardActivityListResponse.class)) }),
			@ApiResponse(responseCode = "400", description = "Validation Failure", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = Status.class)) }),
			@ApiResponse(responseCode = "500", description = "Internal Server Error", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = Status.class)) }) })
	public Response listCardRequestsByCustomerId(
			@Parameter(in = ParameterIn.PATH, description = "customer id", required = true, name = "customerId", schema = @Schema(type = "String")) @PathParam("customerId") String customerId) {

		System.out.println("listcardsbycustomerId-request-REST: " + customerId);

		if (logger.isLoggable(Level.FINE))
			logger.log(Level.FINE,
					this.formatter.formatMessage(" Receive list of cards request by customer Id " + customerId));

		CardRequestListResponse responseDTO = new CardRequestListResponse();

		Response response = null;
		ChannelInteraction channelInteraction = null;
		ChannelContext channelContext = null;
		try {
			channelContext = getChannelContext();
			channelInteraction = ChannelInteraction.getInstance();
			channelInteraction.begin(channelContext);

			// String bankCode = channelContext.getSessionContext().getBankCode();

			// String affCode = channelContext.getSessionContext().getTargetUnit();
			// String userId = channelContext.getSessionContext().getUserId();

			com.ecobank.digx.cz.app.cardservices.service.CardServices cardServices = new com.ecobank.digx.cz.app.cardservices.service.CardServices();
			responseDTO = cardServices.listCardRequestsByCustomerId(channelContext.getSessionContext(), customerId);

			response = buildResponse((BaseResponseObject) responseDTO, Response.Status.OK);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Error-REST: " + e.getMessage());

			response = buildResponse(e, Response.Status.BAD_REQUEST);
		} finally {
			try {
				channelInteraction.close(channelContext);
			} catch (Exception e) {
				// logger.log(Level.SEVERE, this.formatter.formatMessage("Exception while
				// closing the channel interaction", new Object[] { AccessPoint.class
				// .getName() }), (Throwable)e);
				e.printStackTrace();
				response = buildResponse(e, Response.Status.INTERNAL_SERVER_ERROR);
			}
		}

		return response;
	}

	@Override
	public Response inquiry(String customerNo, String last4Digits) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Response fundCard(String amount, String customerNo, String currency, String last4Digits) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Response balanceCheck(String customerNo) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Response miniStatement(String customerNo, String startDate, String endDate, String numberOfTrans) {
		// TODO Auto-generated method stub
		return null;
	}

}
