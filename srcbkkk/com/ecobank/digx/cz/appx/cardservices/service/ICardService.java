package com.ecobank.digx.cz.appx.cardservices.service;

import javax.ws.rs.core.Response;


import com.ecobank.digx.cz.app.cardservices.dto.CardServiceDTO;



public interface ICardService {
	
    Response read(String accNo );
    Response createCard(CardServiceDTO cardServiceDTO);
	Response updateCard(String referenceId, String actionCode);
	
	Response updateCardRequest(String referenceId, String actionCode);
	
	Response inquiry(String customerNo, String last4Digits);
    Response fundCard(String amount, String customerNo, String currency, String last4Digits);
    Response balanceCheck(String customerNo);
    Response miniStatement(String customerNo, String startDate, String endDate, String numberOfTrans);
    Response blockCard(String cardId);
    Response unblockCard(String cardId);
    
    public Response listCardActivityById(String cardId) ;
	
	public Response listCardRequestsByCustomerId( String customerId);
	
	public Response listCardsByCustomerId( String customerId);
	
	public Response getCardBalance( String cardId, String cardCategory);
	
	
			
	
}
