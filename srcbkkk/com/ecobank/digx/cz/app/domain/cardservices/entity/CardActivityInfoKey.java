package com.ecobank.digx.cz.app.domain.cardservices.entity;

import com.ofss.fc.framework.domain.AbstractDomainObjectKey;

public class CardActivityInfoKey extends AbstractDomainObjectKey {
	private static final long serialVersionUID = -4865985769527653277L;
	private String refId;
	private String determinantValue;
	

	@Override
	public String keyAsString() {
		return getRefId();
	}

	public String getRefId() {
		return refId;
	}

	public void setRefId(String refId) {
		this.refId = refId;
	}


	public String getDeterminantValue() {
		return determinantValue;
	}

	public void setDeterminantValue(String determinantValue) {
		this.determinantValue = determinantValue;
	}
}