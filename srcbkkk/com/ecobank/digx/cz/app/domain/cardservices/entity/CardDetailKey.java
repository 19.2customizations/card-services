package com.ecobank.digx.cz.app.domain.cardservices.entity;

import com.ofss.fc.framework.domain.AbstractDomainObjectKey;

public class CardDetailKey extends AbstractDomainObjectKey {
	private static final long serialVersionUID = -4865985769527653277L;
	private String cardId;
	private String determinantValue;
	

	@Override
	public String keyAsString() {
		return getCardId();
	}

	public String getCardId() {
		return cardId;
	}

	public void setCardId(String cardId) {
		this.cardId = cardId;
	}

	public String getDeterminantValue() {
		return determinantValue;
	}

	public void setDeterminantValue(String determinantValue) {
		this.determinantValue = determinantValue;
	}
}