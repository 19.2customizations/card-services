package com.ecobank.digx.cz.app.domain.cardservices.entity.repository.adapter;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;


import com.ecobank.digx.cz.app.domain.cardservices.entity.CardRequest;
import com.ecobank.digx.cz.app.domain.cardservices.entity.CardRequestKey;
import com.ofss.digx.framework.determinant.DeterminantResolver;
import com.ofss.digx.framework.domain.repository.adapter.AbstractLocalRepositoryAdapter;
import com.ofss.digx.infra.exceptions.Exception;

public class LocalCardServiceRepositoryAdapter extends AbstractLocalRepositoryAdapter<CardRequest>
implements ICardServiceRepositoryAdapter
{

	  private static LocalCardServiceRepositoryAdapter singletonInstance;
	  
	  public static LocalCardServiceRepositoryAdapter getInstance()
	  {
	    if (singletonInstance == null) {
	      synchronized (LocalCardServiceRepositoryAdapter.class)
	      {
	        if (singletonInstance == null) {
	          singletonInstance = new LocalCardServiceRepositoryAdapter();
	        }
	      }
	    }
	    return singletonInstance;
	  }
	  
	  
	        public void create(CardRequest object)
			    throws Exception
			  {
			    if (object.getRefKey() != null) {
			     /* object.getPassportNumber().setDeterminantValue(
			        DeterminantResolver.getInstance().fetchDeterminantValue(AccountTransfer.class.getName()));*/
			    }
			    super.insert(object);
			  }

	  public void update(CardRequest object) throws Exception {
		  System.out.println("Update Card Request:: " + object.getRefKey().getExternalRefNo());
          super.update(object);
          System.out.println("End-Update Card Request:: " + object.getRefKey().getExternalRefNo());
		
	  }

	@Override
	public CardRequest read(CardRequestKey key) throws Exception {
		key.setDeterminantValue(
				DeterminantResolver.getInstance().fetchDeterminantValue(CardRequest.class.getName()));
		return (CardRequest) get(CardRequest.class, (Serializable) key);
	}
	
	 public List<CardRequest> listCardRequestByCustomerId(String customerId) throws Exception {
	    HashMap<String, Object> parameters = null;
	    List<CardRequest> list = null;
	    parameters = new HashMap<>();
	    parameters.put("customerId", customerId);
        list = executeNamedQuery("ListCustomerCardRequest", parameters);
	    return list;
	  }


	@Override
	public void delete(CardRequest arg0) throws Exception {
		// TODO Auto-generated method stub
		
	}


	
			  
}


