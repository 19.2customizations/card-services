package com.ecobank.digx.cz.app.domain.cardservices.entity.repository.adapter;

import java.util.List;

import com.ecobank.digx.cz.app.cardservices.dto.CardServiceResponse;
import com.ecobank.digx.cz.app.domain.cardservices.entity.CardRequest;
import com.ecobank.digx.cz.app.domain.cardservices.entity.CardRequestKey;
import com.ofss.digx.framework.domain.repository.IRepositoryAdapter;
import com.ofss.digx.infra.exceptions.Exception;

public interface ICardServiceRepositoryAdapter extends IRepositoryAdapter<CardRequest, CardRequestKey> {
	
	public abstract void create(CardRequest cardRequest) throws Exception;
	
	public CardRequest read(CardRequestKey key) throws Exception;
	
	public List<CardRequest> listCardRequestByCustomerId(String customerId) throws Exception ;
	
	public abstract void update(CardRequest object) throws Exception ;
	


}
