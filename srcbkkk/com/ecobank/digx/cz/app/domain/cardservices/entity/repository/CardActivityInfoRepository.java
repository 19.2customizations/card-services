package com.ecobank.digx.cz.app.domain.cardservices.entity.repository;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Logger;
import java.util.prefs.Preferences;

import com.ecobank.digx.cz.app.cardservices.dto.CardFundingRequestDTO;
import com.ecobank.digx.cz.app.cardservices.dto.CardServiceResponseDTO;
import com.ecobank.digx.cz.app.domain.cardservices.entity.CardActivityInfo;
import com.ecobank.digx.cz.app.domain.cardservices.entity.CardActivityInfoKey;
import com.ecobank.digx.cz.app.domain.cardservices.entity.CardDetail;
import com.ecobank.digx.cz.app.domain.cardservices.entity.repository.adapter.ICardActivityInfoRepositoryAdapter;
import com.ecobank.digx.cz.extxface.fc12if.adapter.ubs124.dto.SingleTransactionRequestDTO;
import com.ecobank.digx.cz.extxface.fc12if.adapter.ubs124.dto.TransactionFetchRequestDTO;
import com.ecobank.digx.cz.extxface.fc12if.adapter.ubs124.dto.TransactionFetchResponseDTO;
import com.ecobank.digx.cz.extxface.fc12if.adapter.ubs124.impl.RemoteFlexIFPostingAdapter;
import com.ecobank.digx.cz.extxface.mule.adapter.client.CreditCardServicesRepositoryAdapter;
import com.ecobank.digx.cz.extxface.mule.adapter.client.RemoteCardServicesRepositoryAdapter;
import com.ofss.digx.framework.domain.repository.AbstractDomainObjectRepository;
import com.ofss.digx.framework.domain.repository.RepositoryAdapterFactory;
import com.ofss.digx.infra.exceptions.Exception;
import com.ofss.fc.infra.config.ConfigurationFactory;
import com.ofss.fc.infra.log.impl.MultiEntityLogger;
import com.thoughtworks.xstream.XStream;

public class CardActivityInfoRepository extends AbstractDomainObjectRepository<CardActivityInfo, CardActivityInfoKey> {
	private static CardActivityInfoRepository singletonInstance;
	private Preferences digx_consulting = ConfigurationFactory.getInstance().getConfigurations("DIGXCONSULTING");
	private static final String THIS_COMPONENT_NAME = CardActivityInfoRepository.class.getName();
	private static transient Logger logger = MultiEntityLogger.getUniqueInstance().getLogger(THIS_COMPONENT_NAME);


	public static CardActivityInfoRepository getInstance() {
		if (singletonInstance == null) {
			synchronized (CardActivityInfoRepository.class) {
				if (singletonInstance == null) {
					singletonInstance = new CardActivityInfoRepository();
				}
			}
		}
		return singletonInstance;
	}

	public void create(CardActivityInfo object) throws Exception {
		ICardActivityInfoRepositoryAdapter repositoryAdapter = (ICardActivityInfoRepositoryAdapter) 
				RepositoryAdapterFactory.getInstance().getRepositoryAdapter("CARD_ACTIVITY_INFO_REPOSITORY_ADAPTER");
		repositoryAdapter.create(object);
	}
	
	@Override
	public void update(CardActivityInfo object) throws Exception {
		ICardActivityInfoRepositoryAdapter repositoryAdapter = (ICardActivityInfoRepositoryAdapter) RepositoryAdapterFactory
				.getInstance().getRepositoryAdapter("CARD_ACTIVITY_INFO_REPOSITORY_ADAPTER");
		repositoryAdapter.update(object);
		
	}


	@Override
	public void delete(CardActivityInfo arg0) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public CardActivityInfo read(CardActivityInfoKey key) throws Exception {
		System.out.println("[[KEY]]   ::: "+ key);
		ICardActivityInfoRepositoryAdapter repositoryAdapter = (ICardActivityInfoRepositoryAdapter) RepositoryAdapterFactory
				.getInstance().getRepositoryAdapter("CARD_ACTIVITY_INFO_REPOSITORY_ADAPTER");
		return repositoryAdapter.read(key);
	}
	
	public List<CardActivityInfo> listCardActivityById(String cardId) throws Exception 
	{
		System.out.println("[[KEY]]   ::: "+ cardId);
		ICardActivityInfoRepositoryAdapter repositoryAdapter = (ICardActivityInfoRepositoryAdapter) RepositoryAdapterFactory
				.getInstance().getRepositoryAdapter("CARD_ACTIVITY_INFO_REPOSITORY_ADAPTER");
		return repositoryAdapter.listCardActivityById(cardId);
	}
	
	public CardServiceResponseDTO processTopupCard(String affCode, CardActivityInfo cRequest, CardDetail cardDetail) throws Exception {
		System.out.println("[[CardRequest-REPOSITORY22]]   ::: " );
		CardServiceResponseDTO responseDTO = new CardServiceResponseDTO();
		
		
		//IRemoteFlexIFPostingAdapter repositoryAdapter = (IRemoteFlexIFPostingAdapter) RepositoryAdapterFactory
		//		.getInstance().getRepositoryAdapter("CZ_FLEXCUBE_IF_REPOSITORY_ADAPTER");
		
		//String externalRefNo,String affCode,
		RemoteFlexIFPostingAdapter repositoryAdapter = new RemoteFlexIFPostingAdapter();
		/*CardActivityInfoKey key = new CardActivityInfoKey();
		key.setDeterminantValue(externalRefNo);
		key.setRefId(externalRefNo);
		CardActivityInfo cRequest = read( key);*/
		
		
		XStream xs = new XStream();
		
		String maskedPan = cardDetail.getMaskedPan();
		String debitAccount = cRequest.getSourceAccount();
		String externalRefNo = cRequest.getRefKey().getRefId();
		
		SingleTransactionRequestDTO requestT = new SingleTransactionRequestDTO();
		requestT.setAffiliateCode(affCode);
		requestT.setAmount(Double.parseDouble(cRequest.getFee()));
		String crAcc = digx_consulting.get("OBDX_FEE_ACCOUNT", "");
		requestT.setCreditAccountNo(crAcc);
		requestT.setDebitAccountNo(debitAccount);
		requestT.setExternalRefNo(externalRefNo);
		requestT.setNarration("TOPUP CARD " +  maskedPan  + cRequest.getActivityDescription());
		requestT.setPostingBranchCode("");
		String sourceCode = digx_consulting.get("DIGX_FLEX_SOURCE_CODE", "");
		requestT.setSourceCode(sourceCode);
		requestT.setTransactionCode("U07");
		System.out.println("Post Single Tran:::" + xs.toXML(requestT)); // requestT.getSourceCode());
		
		TransactionFetchResponseDTO response = repositoryAdapter.processSingleTransactionPosting(requestT);
		
		responseDTO.setCbaReferenceNo(response.getCbaReferenceNo());
		responseDTO.setExternalRefNo(cRequest.getRefKey().getRefId());
		
		cRequest.setCbaRspCode(response.getResponseCode());
		cRequest.setCbaRspMsg(response.getResponseMessage());
		//cRequest.setTranRefNo(response.getCbaReferenceNo());
		cRequest.setCbaReferenceNo(response.getCbaReferenceNo());
		update(cRequest);
		
		if(response.getResponseCode().equals("000"))
		{
			RemoteCardServicesRepositoryAdapter rAdapter = new RemoteCardServicesRepositoryAdapter();
			CreditCardServicesRepositoryAdapter ccAdaptor = new CreditCardServicesRepositoryAdapter();
			CardFundingRequestDTO fundRequest = new CardFundingRequestDTO();
			fundRequest.setAffiliateCode(affCode);
			fundRequest.setAmount(new BigDecimal(cRequest.getFee()));
			fundRequest.setCardCustomerId(cardDetail.getCardCustomerId());
			fundRequest.setMobileNo(cardDetail.getMobileNo());
			fundRequest.setNote(cRequest.getActivityDescription());
			fundRequest.setMaskedPan(cardDetail.getMaskedPan());
			fundRequest.setExternalRefNo(externalRefNo);
					
			logger.info(":: Name On Card ::" + cardDetail.getNameOnCard());
			fundRequest.setNameOnCard(cardDetail.getNameOnCard());
			
			CardServiceResponseDTO respf = null;
			
			if(cardDetail.getCardCategory().equals("VIRTUAL"))
			{
				respf = rAdapter.fundCard(fundRequest);
			}
			else if(cardDetail.getCardCategory().equals("CREDIT"))
			{
				logger.info(":: Credit Card Processing ::");
				respf = ccAdaptor.fundCard(fundRequest);
			}
			cRequest.setResponseCode(respf.getResponseCode());
			cRequest.setResponseMessage(respf.getResponseMessage());
			
			responseDTO.setResponseCode(respf.getResponseCode());
			responseDTO.setResponseMessage(respf.getResponseMessage());
			
			if(respf.getResponseCode().equals("000"))
			{
				cRequest.setTranRefNo(respf.getTransactionRefNo());
			}
			
			update(cRequest);
		}
		
		
		TransactionFetchRequestDTO requestF = new TransactionFetchRequestDTO();
		requestF.setAffiliateCode(affCode);
		requestF.setDebitAccountNo(debitAccount);
		requestF.setExternalRefNo(externalRefNo);
		requestF.setSourceCode(sourceCode);
		response = repositoryAdapter.fetchTransactionStatus(requestF);
		System.out.println("TFETCH:::" + response.getCbaReferenceNo());
		
		
		
		return responseDTO;
	}
}
