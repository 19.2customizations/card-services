package com.ecobank.digx.cz.app.domain.cardservices.entity;

import java.util.List;

import com.ecobank.digx.cz.app.domain.cardservices.entity.repository.CardActivityInfoRepository;
import com.ofss.digx.infra.exceptions.Exception;
import com.ofss.fc.datatype.Date;
import com.ofss.fc.framework.domain.IPersistenceObject;
import com.ofss.fc.infra.das.exception.PersistenceException;

public class CardActivityInfo implements IPersistenceObject{
	
	private static final long serialVersionUID = 222786759021659990L;
	
	private String cardId;
	private Date activityDate;
	private String activityType;
	private String activityDescription;
	private String status;
	private String responseCode;
	private String responseMessage;
	private String tranRefNo;
	private String fee;
	private String userId;
	private CardActivityInfoKey refKey;
	
	private String affiliateCode;
	private String sourceAccount;
	private String cbaRspCode;
	private String cbaRspMsg;
	private String cbaReferenceNo;
	
	
	public void create(CardActivityInfo cardActivityInfo) throws Exception {
		System.out.println("create refID ::: " + cardActivityInfo.getRefKey().getRefId());
		CardActivityInfoRepository.getInstance().create(cardActivityInfo);
	}
	
	public void update(CardActivityInfo cardActivityInfo) throws Exception {
		CardActivityInfoRepository.getInstance().update(cardActivityInfo);
	}
	
	public CardActivityInfo read(CardActivityInfoKey key) throws Exception {
		System.out.println("refID ::: " + key.getRefId());
		return CardActivityInfoRepository.getInstance().read(key);
	}
	
	public List<CardActivityInfo> listCardActivityById(String cardId ) throws Exception {
		System.out.println("refID ::: " + cardId);
		return CardActivityInfoRepository.getInstance().listCardActivityById(cardId);
	}
	
	@Override
	public boolean isEntityReadOnly() {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public void markReadOnly(boolean arg0) throws PersistenceException {
		// TODO Auto-generated method stub
		
	}

	

	public String getCardId() {
		return cardId;
	}

	public void setCardId(String cardId) {
		this.cardId = cardId;
	}

	public Date getActivityDate() {
		return activityDate;
	}

	public void setActivityDate(Date activityDate) {
		this.activityDate = activityDate;
	}

	public String getActivityType() {
		return activityType;
	}

	public void setActivityType(String activityType) {
		this.activityType = activityType;
	}

	public String getActivityDescription() {
		return activityDescription;
	}

	public void setActivityDescription(String activityDescription) {
		this.activityDescription = activityDescription;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public String getTranRefNo() {
		return tranRefNo;
	}

	public void setTranRefNo(String tranRefNo) {
		this.tranRefNo = tranRefNo;
	}

	public String getFee() {
		return fee;
	}

	public void setFee(String fee) {
		this.fee = fee;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public CardActivityInfoKey getRefKey() {
		return refKey;
	}

	public void setRefKey(CardActivityInfoKey refKey) {
		this.refKey = refKey;
	}

	public String getAffiliateCode() {
		return affiliateCode;
	}

	public void setAffiliateCode(String affiliateCode) {
		this.affiliateCode = affiliateCode;
	}

	public String getSourceAccount() {
		return sourceAccount;
	}

	public void setSourceAccount(String sourceAccount) {
		this.sourceAccount = sourceAccount;
	}

	public String getCbaRspCode() {
		return cbaRspCode;
	}

	public void setCbaRspCode(String cbaRspCode) {
		this.cbaRspCode = cbaRspCode;
	}

	public String getCbaRspMsg() {
		return cbaRspMsg;
	}

	public void setCbaRspMsg(String cbaRspMsg) {
		this.cbaRspMsg = cbaRspMsg;
	}

	public String getCbaReferenceNo() {
		return cbaReferenceNo;
	}

	public void setCbaReferenceNo(String cbaReferenceNo) {
		this.cbaReferenceNo = cbaReferenceNo;
	}
	
	
	
}
