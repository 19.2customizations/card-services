package com.ecobank.digx.cz.app.domain.cardservices.entity.repository.adapter;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import com.ecobank.digx.cz.app.domain.cardservices.entity.CardDetail;
import com.ecobank.digx.cz.app.domain.cardservices.entity.CardDetailKey;
import com.ofss.digx.framework.determinant.DeterminantResolver;
import com.ofss.digx.framework.domain.repository.adapter.AbstractLocalRepositoryAdapter;
import com.ofss.digx.infra.exceptions.Exception;

public class LocalCardDetailRepositoryAdapter extends AbstractLocalRepositoryAdapter<CardDetail>
implements ICardDetailRepositoryAdapter
{

	  private static LocalCardDetailRepositoryAdapter singletonInstance;
	  
	  public static LocalCardDetailRepositoryAdapter getInstance()
	  {
	    if (singletonInstance == null) {
	      synchronized (LocalCardDetailRepositoryAdapter.class)
	      {
	        if (singletonInstance == null) {
	          singletonInstance = new LocalCardDetailRepositoryAdapter();
	        }
	      }
	    }
	    return singletonInstance;
	  }
	  
	  public void create(CardDetail object)
			    throws Exception
			  {
		  System.out.println("card Account  ::: " + object.getCardAccount());
		  System.out.println("card ccy      ::: " + object.getCardCcy());
		  System.out.println("card expiry   ::: " + object.getExpiryDate());
		  System.out.println("customerId    ::: " + object.getCustomerId());
		  System.out.println("sourceAccount ::: " + object.getSourceAccount());
		  System.out.println("refKey 22       ::: " + object.getRefKey().getCardId());
		  //System.out.println("refId         ::: " + object.getRefId());
			    if (object.getRefKey() != null) {
			     /* object.getPassportNumber().setDeterminantValue(
			        DeterminantResolver.getInstance().fetchDeterminantValue(AccountTransfer.class.getName()));*/
			    }
			    super.insert(object);
			  }

	  public void update(CardDetail object) throws Exception {
          super.update(object);
		
	  }
	  
	@Override
	public CardDetail read(CardDetailKey key) throws Exception {
		System.out.println("inside local-Repo:  " + key.getCardId());
		key.setDeterminantValue(
				DeterminantResolver.getInstance().fetchDeterminantValue(CardDetail.class.getName()));
		return (CardDetail) get(CardDetail.class, (Serializable) key);
	}
	
	public List<CardDetail> listCardsByCustomerId(String customerId) throws Exception {
	    HashMap<String, Object> parameters = null;
	    List<CardDetail> list = null;
	    parameters = new HashMap<>();
	    parameters.put("customerId", customerId);
        list = executeNamedQuery("ListCardsByCustomerId", parameters);
	    return list;
	  }
	
	public CardDetail getCardDetailByMaskedPANandExpiryDate(String customerId,String cardType, String maskedPAN, String expiryDate) throws Exception {
	    HashMap<String, Object> parameters = null;
	    List<CardDetail> list = null;
	    CardDetail cardDetail = null;
	    parameters = new HashMap<>();
	    parameters.put("customerId", customerId);
	    parameters.put("cardType", cardType);
	    parameters.put("maskedPAN", maskedPAN);
	    parameters.put("expiryDate", expiryDate);
        list = executeNamedQuery("fetchCardsByMaskedPANandExpiryDate", parameters);
        if(list != null && list.size() > 0)
        	cardDetail = list.get(0);
	    return cardDetail;
	  }
			  
}


