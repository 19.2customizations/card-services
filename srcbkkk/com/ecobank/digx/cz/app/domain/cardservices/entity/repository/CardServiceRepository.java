package com.ecobank.digx.cz.app.domain.cardservices.entity.repository;

import java.util.List;
import java.util.prefs.Preferences;

import com.ecobank.digx.cz.app.domain.cardservices.entity.CardRequest;
import com.ecobank.digx.cz.app.domain.cardservices.entity.CardRequestKey;
import com.ecobank.digx.cz.app.domain.cardservices.entity.repository.adapter.ICardServiceRepositoryAdapter;
import com.ofss.digx.framework.domain.repository.AbstractDomainObjectRepository;
import com.ofss.digx.framework.domain.repository.RepositoryAdapterFactory;
import com.ofss.digx.infra.exceptions.Exception;
import com.ofss.fc.infra.config.ConfigurationFactory;

public class CardServiceRepository extends AbstractDomainObjectRepository<CardRequest, CardRequestKey> {
	private static CardServiceRepository singletonInstance;
	private Preferences digx_consulting = ConfigurationFactory.getInstance().getConfigurations("DIGXCONSULTING");

	public static CardServiceRepository getInstance() {
		if (singletonInstance == null) {
			synchronized (CardServiceRepository.class) {
				if (singletonInstance == null) {
					singletonInstance = new CardServiceRepository();
				}
			}
		}
		return singletonInstance;
	}

	public void create(CardRequest object) throws Exception {
		ICardServiceRepositoryAdapter repositoryAdapter = (ICardServiceRepositoryAdapter) 
				RepositoryAdapterFactory.getInstance().getRepositoryAdapter("CARD_SERVICES_REPOSITORY_ADAPTER");
		repositoryAdapter.create(object);
	}
	
	
	public void update(CardRequest object) throws Exception {
		ICardServiceRepositoryAdapter repositoryAdapter = (ICardServiceRepositoryAdapter) RepositoryAdapterFactory
				.getInstance().getRepositoryAdapter("CARD_SERVICES_REPOSITORY_ADAPTER");
		repositoryAdapter.update(object);
		
	}


	@Override
	public void delete(CardRequest arg0) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public CardRequest read(CardRequestKey key) throws Exception {
		System.out.println("[[KEY]]   ::: "+ key);
		ICardServiceRepositoryAdapter repositoryAdapter = (ICardServiceRepositoryAdapter) RepositoryAdapterFactory
				.getInstance().getRepositoryAdapter("CARD_SERVICES_REPOSITORY_ADAPTER");
		return repositoryAdapter.read(key);
	}
	

	
	
	
	public List<CardRequest> listCardRequestByCustomerId(String customerId) throws Exception {
		System.out.println("[[FETCH-CARD-REQUEST-CUSTOMER]]   ::: "+ customerId);
		ICardServiceRepositoryAdapter repositoryAdapter = (ICardServiceRepositoryAdapter) RepositoryAdapterFactory
				.getInstance().getRepositoryAdapter("CARD_SERVICES_REPOSITORY_ADAPTER");
		return repositoryAdapter.listCardRequestByCustomerId(customerId);
	}
}
