package com.ecobank.digx.cz.app.cardservices.dto;

import java.util.List;

import com.ofss.digx.service.response.BaseResponseObject;

public class CardListResponse extends BaseResponseObject{
	
	private static final long serialVersionUID = 1L;
	 
	
	private List<CardDetailDTO> cards;


	public List<CardDetailDTO> getCards() {
		return cards;
	}


	public void setCards(List<CardDetailDTO> cards) {
		this.cards = cards;
	}


	
	
	
	
	
}
