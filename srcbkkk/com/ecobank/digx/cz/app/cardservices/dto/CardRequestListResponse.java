package com.ecobank.digx.cz.app.cardservices.dto;

import java.util.List;

import com.ofss.digx.service.response.BaseResponseObject;

public class CardRequestListResponse extends BaseResponseObject{
	
	private static final long serialVersionUID = 1L;
	 
	
	private List<CardServiceDTO> requests;


	public List<CardServiceDTO> getRequests() {
		return requests;
	}


	public void setRequests(List<CardServiceDTO> requests) {
		this.requests = requests;
	}


	

	
	
	
	
	
}
