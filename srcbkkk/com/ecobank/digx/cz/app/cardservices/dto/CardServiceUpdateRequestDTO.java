package com.ecobank.digx.cz.app.cardservices.dto;

import com.ofss.digx.app.common.dto.DataTransferObject;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(title = "CardServiceUpdateRequestDTO")
public class CardServiceUpdateRequestDTO extends DataTransferObject {
	private static final long serialVersionUID = -5025245192478218954L;
	private String paymentId;
	private String actionCode;

	public String getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}

	public String toString() {
		return "SelfTransferUpdateRequestDTO [paymentId=" + paymentId + " ]";
	}

	public String getActionCode() {
		return actionCode;
	}

	public void setActionCode(String actionCode) {
		this.actionCode = actionCode;
	}
	
	
}