package com.ecobank.digx.cz.app.cardservices.dto;

import java.util.logging.Logger;

import com.ofss.digx.app.common.dto.DataTransferObject;
import com.ofss.fc.infra.log.impl.MultiEntityLogger;

public class CardDetailCreateRequestDTO extends DataTransferObject {
	
	private static final long serialVersionUID = 1L;
	private static final String THIS_COMPONENT_NAME = CardDetailCreateRequestDTO.class.getName();
	private transient MultiEntityLogger formatter;

	public CardDetailCreateRequestDTO() {
		formatter = MultiEntityLogger.getUniqueInstance();
	}
	
	private transient Logger logger = MultiEntityLogger.getUniqueInstance().getLogger(THIS_COMPONENT_NAME);
	private CardDetailDTO cardDetailDTO;
	private String refId;

	public String getRefId() {
		return refId;
	}

	public void setRefId(String refId) {
		this.refId = refId;
	}

	public CardDetailDTO getCardDetailDTO() {
		return cardDetailDTO;
	}

	public void setCardDetailDTO(CardDetailDTO cardDetailDTO) {
		this.cardDetailDTO = cardDetailDTO;
	}

	public String toString() {
		return "CardDetailDTO [Card Detailss=" + cardDetailDTO + " ]";
	}
}