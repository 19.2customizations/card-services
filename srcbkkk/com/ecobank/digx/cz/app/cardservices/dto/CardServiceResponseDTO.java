package com.ecobank.digx.cz.app.cardservices.dto;

import com.ofss.digx.service.response.BaseResponseObject;

public class CardServiceResponseDTO extends BaseResponseObject {
	
	
	private static final long serialVersionUID = 1L;
	
	private String cbaReferenceNo;
	private String responseCode;
	private String responseMessage;
	private String transactionRefNo;
	private String externalRefNo;
	private String customerNo;
	private String cardId;
	private String last4Digits;
	
	
	public String getCbaReferenceNo() {
		return cbaReferenceNo;
	}
	public void setCbaReferenceNo(String cbaReferenceNo) {
		this.cbaReferenceNo = cbaReferenceNo;
	}
	public String getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	public String getResponseMessage() {
		return responseMessage;
	}
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}
	public String getTransactionRefNo() {
		return transactionRefNo;
	}
	public void setTransactionRefNo(String transactionRefNo) {
		this.transactionRefNo = transactionRefNo;
	}
	public String getCustomerNo() {
		return customerNo;
	}
	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}
	public String getLast4Digits() {
		return last4Digits;
	}
	public void setLast4Digits(String last4Digits) {
		this.last4Digits = last4Digits;
	}
	public String getExternalRefNo() {
		return externalRefNo;
	}
	public void setExternalRefNo(String externalRefNo) {
		this.externalRefNo = externalRefNo;
	}
	public String getCardId() {
		return cardId;
	}
	public void setCardId(String cardId) {
		this.cardId = cardId;
	}
	
	
}
