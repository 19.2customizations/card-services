package com.ecobank.digx.cz.app.cardservices.service;

import com.ecobank.digx.cz.app.cardservices.dto.CardActivityDTO;
import com.ecobank.digx.cz.app.cardservices.dto.CardActivityListResponse;
import com.ecobank.digx.cz.app.cardservices.dto.CardBalanceDTO;
import com.ecobank.digx.cz.app.cardservices.dto.CardBalanceResponse;
import com.ecobank.digx.cz.app.cardservices.dto.CardListResponse;
import com.ecobank.digx.cz.app.cardservices.dto.CardRequestListResponse;
import com.ecobank.digx.cz.app.cardservices.dto.CardServiceCreateRequestDTO;
import com.ecobank.digx.cz.app.cardservices.dto.CardServiceCreateResponseDTO;
import com.ecobank.digx.cz.app.cardservices.dto.CardServiceResponseDTO;
import com.ecobank.digx.cz.app.cardservices.dto.CardServiceUpdateRequestDTO;
import com.ofss.fc.app.context.SessionContext;

public interface ICardServices {
	public abstract CardServiceCreateResponseDTO read(SessionContext sessionContext, CardServiceCreateRequestDTO cardServiceRequestDTO) 
			throws Exception;

	public CardServiceCreateResponseDTO create(SessionContext sessionContext, CardServiceCreateRequestDTO requestDTO)
			throws Exception;
	
//	public CardServiceCreateResponseDTO saveCardDetails(SessionContext sessionContext, CardServiceCreateRequestDTO requestDTO)
//			throws Exception;
	
	public CardServiceResponseDTO proocessCardRequest(SessionContext sessionContext, CardServiceUpdateRequestDTO cardServiceUpdateRequestDTO)
			throws Exception;
	
	public CardServiceResponseDTO updateCardRequest(SessionContext sessionContext, CardServiceUpdateRequestDTO cardServiceUpdateRequestDTO)
			throws Exception;
			

	public CardServiceCreateResponseDTO fundCard(SessionContext sessionContext, CardServiceCreateRequestDTO requestDTO)
			throws Exception;

	public CardServiceResponseDTO blockCard(SessionContext sessionContext, String cardId)
			throws Exception;
	
	public CardServiceResponseDTO unblockCard(SessionContext sessionContext, String cardId)
			throws Exception;

	public CardBalanceResponse balanceCheck(SessionContext sessionContext, CardBalanceDTO requestDTO)
			throws Exception;

	public CardServiceCreateResponseDTO miniStatement(SessionContext sessionContext, CardServiceCreateRequestDTO requestDTO)
			throws Exception;

	
	public CardServiceCreateResponseDTO inquiry(SessionContext sessionContext, CardServiceCreateRequestDTO requestDTO)
			throws Exception;

	public CardActivityListResponse listCardActivityById(SessionContext sessionContext, String cardId)
			throws Exception;
	
	public CardRequestListResponse listCardRequestsByCustomerId(SessionContext sessionContext, String customerId)
			throws Exception ;
	
	public CardListResponse listCardsByCustomerId(SessionContext sessionContext, String customerId)
			throws Exception;
	
	public CardServiceResponseDTO createCardActivity(SessionContext sessionContext, CardActivityDTO cardActivity)
			throws Exception;
	
}
