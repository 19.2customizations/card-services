 package com.ecobank.digx.cz.extxface.mule.adapter.client;

import java.math.BigDecimal;
import java.util.Random;
import java.util.logging.Logger;
import java.util.prefs.Preferences;

import com.ecobank.digx.cz.app.cardservices.dto.CardActivityDTO;
import com.ecobank.digx.cz.app.cardservices.dto.CardBalanceResponse;
import com.ecobank.digx.cz.app.cardservices.dto.CardFundingRequestDTO;
import com.ecobank.digx.cz.app.cardservices.dto.CardServiceResponseDTO;
import com.ecobank.digx.cz.app.domain.cardservices.entity.CardDetail;
import com.ecobank.digx.cz.app.domain.cardservices.entity.CardRequest;
import com.ecobank.digx.cz.extxface.mule.dto.ESBCardRequestDTO;
import com.ecobank.digx.cz.extxface.mule.dto.ESBCardResponseDTO;
import com.ecobank.digx.cz.extxface.mule.dto.HostHeaderInfo;
import com.ofss.digx.app.core.AdapterInteraction;
import com.ofss.digx.framework.rest.client.RESTClientFactory;
import com.ofss.digx.framework.rest.client.RESTClientRequestBuilder;
import com.ofss.digx.framework.rest.client.RESTClientRequestConfig;
import com.ofss.digx.framework.rest.client.impl.IRESTClient;
import com.ofss.digx.infra.exceptions.Exception;
import com.ofss.fc.infra.config.ConfigurationFactory;
import com.ofss.fc.infra.log.impl.MultiEntityLogger;
import com.thoughtworks.xstream.XStream;

public class RemoteCardServicesRepositoryAdapter {
	private static RemoteCardServicesRepositoryAdapter singletonInstance;

	private Preferences digx_consulting = ConfigurationFactory.getInstance().getConfigurations("DIGXCONSULTING");

	private static final String THIS_COMPONENT_NAME = RemoteCardServicesRepositoryAdapter.class.getName();

	private final transient MultiEntityLogger formatter = MultiEntityLogger.getUniqueInstance();

	private static transient Logger logger = MultiEntityLogger.getUniqueInstance().getLogger(THIS_COMPONENT_NAME);
	
	public static RemoteCardServicesRepositoryAdapter getInstance() {
		if (singletonInstance == null)
			synchronized (RemoteCardServicesRepositoryAdapter.class) {
				if (singletonInstance == null)
					singletonInstance = new RemoteCardServicesRepositoryAdapter();
			}
		return singletonInstance;
	}
	
	public CardServiceResponseDTO generateCard(CardRequest cardRequest, String[] personalData) throws Exception
	{
		CardServiceResponseDTO response = new CardServiceResponseDTO();
		response.setResponseCode("E04");
		response.setResponseMessage("Failed at interface level");
		AdapterInteraction.begin();
		
		String address = personalData[0];
		String city = personalData[1];
		String country = personalData[2];
		String dob = personalData[3];
		
		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESB_VIRTUAL_CARD_CREATE");
		RESTClientFactory clientFactory = RESTClientFactory.getInstance();
		IRESTClient restClient = clientFactory.getRESTClientInstance();
		
		String url = digx_consulting.get("ESB_VIRTUAL_CARD_URL", "");
		String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String ipx = digx_consulting.get("ESB_OBDX_IP", "");
		//String sourceChannel = digx_consulting.get("ESB_OBDX_SOURCE_CHANNEL", "");
		
		
		//String fullName = cardRequest.getNameOnCard();
		String fullName = cardRequest.getNameOnCard();
		String firstName = fullName.split(" ")[0];
		logger.info("firstName: " + firstName);
		int fName = fullName.split(" ")[0].length();
		String lastName = fullName.substring(fName).isEmpty() ? firstName : fullName.substring(fName);
		logger.info("lastName: " + lastName);
		
		ESBCardRequestDTO esbRequestDTO = new ESBCardRequestDTO();
		HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
		hostHeaderInfo.setAffiliateCode(cardRequest.getAffiliateCode());
		hostHeaderInfo.setIpAddress(ipx);
		hostHeaderInfo.setRequestId(cardRequest.getRefKey().getExternalRefNo());
		hostHeaderInfo.setRequestToken("");
		hostHeaderInfo.setRequestType("CREATECARD");
		hostHeaderInfo.setSourceChannelId("MOBILE");
		hostHeaderInfo.setSourceCode(sourceCode);
		esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);
		esbRequestDTO.setCustomerNo(cardRequest.getMobileNo());
		esbRequestDTO.setFirstName(firstName);
		esbRequestDTO.setLastName(lastName);
		esbRequestDTO.setAddress(address);
		esbRequestDTO.setCity(city);
		esbRequestDTO.setCountry("GH");
		esbRequestDTO.setPostalCode("00233");
		esbRequestDTO.setState("AA");
		esbRequestDTO.setDob(dob);
		esbRequestDTO.setIdType("2");
		esbRequestDTO.setIdValue(cardRequest.getMobileNo());
		esbRequestDTO.setMobileNo(cardRequest.getMobileNo());
		esbRequestDTO.setEmail(cardRequest.getEmail());
		esbRequestDTO.setExpirationDate(cardRequest.getExpiryDate());
		esbRequestDTO.setNotificationMethod("SMS");
		
		ESBCardResponseDTO resp = null;

		try {
			XStream xs = new XStream();
			logger.info("CARDSERVICES-Request : " + xs.toXML(esbRequestDTO));
			logger.info("CARDSERVICES-URL     : " + url);
			logger.info("CARDSERVICES-Source Code : " + sourceCode);
			
			resp = (ESBCardResponseDTO) restClient.post(prop, ESBCardResponseDTO.class, esbRequestDTO);
			logger.info("Generate Last4Digits    : " + resp.getLast4Digits());
			logger.info("Generate CustomerNo     : " + resp.getCustomerNo());
			logger.info("Generate Card-Response  : " + xs.toXML(resp));
			logger.info("getResponseCode         : " + resp.getHostHeaderInfo().getResponseCode());
			logger.info("getResponseMessage      : " + resp.getHostHeaderInfo().getResponseMessage());
			if(resp != null && resp.getHostHeaderInfo().getResponseCode().equals("000"))
			{
				response.setResponseCode("000");
				response.setResponseMessage(resp.getHostHeaderInfo().getResponseMessage());
				response.setTransactionRefNo(resp.getHostHeaderInfo().getRequestId());
				response.setCustomerNo(resp.getCustomerNo());
				response.setLast4Digits(resp.getLast4Digits());
			}
		} finally {
		}
		AdapterInteraction.close();
	    return response;
	}
	
	
	
	
	public ESBCardResponseDTO inquiry(CardRequest cardRequest, CardServiceResponseDTO cardServiceResponseDTO) throws Exception {
		AdapterInteraction.begin();

		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESB_VIRTUAL_CARD_INQUIRY");
		RESTClientFactory clientFactory = RESTClientFactory.getInstance();
		IRESTClient restClient = clientFactory.getRESTClientInstance();

		String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String ipx = digx_consulting.get("ESB_OBDX_IP", "");

		ESBCardRequestDTO esbRequestDTO = new ESBCardRequestDTO();
		HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
		hostHeaderInfo.setAffiliateCode(cardRequest.getAffiliateCode());
		hostHeaderInfo.setIpAddress(ipx);
		hostHeaderInfo.setRequestId("122233300");
		hostHeaderInfo.setRequestToken("3322344667809876");
		hostHeaderInfo.setRequestType("CARDINQUIRY");
		hostHeaderInfo.setSourceChannelId("MOBILE");
		hostHeaderInfo.setSourceCode(sourceCode);
		esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);
		esbRequestDTO.setCustomerNo(cardServiceResponseDTO.getCustomerNo());
		esbRequestDTO.setLast4Digits(cardServiceResponseDTO.getLast4Digits());

		ESBCardResponseDTO resp = null;

		try {
			XStream xs = new XStream();
			logger.info("CARDSERVICES-Request : " + xs.toXML(esbRequestDTO));
			logger.info("CARDSERVICES-Source Code : " + sourceCode);

			resp = (ESBCardResponseDTO) restClient.post(prop, ESBCardResponseDTO.class, esbRequestDTO);
			logger.info("Generate CustomerNo     : " + resp.getCustomerNo());
			logger.info("Generate Card-Response  : " + xs.toXML(resp));
			logger.info("getResponseCode         : " + resp.getHostHeaderInfo().getResponseCode());
			logger.info("getResponseMessage      : " + resp.getHostHeaderInfo().getResponseMessage());
		} finally {
		}
		AdapterInteraction.close();

		return resp;
	}
	
	
	public CardActivityDTO blockCard(CardDetail cardDetail) throws Exception {
		CardActivityDTO response = new CardActivityDTO();
		response.setResponseCode("E04");
		response.setResponseMessage("Failed at interface level");
		AdapterInteraction.begin();

		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESB_VIRTUAL_CARD_BLOCK");
		RESTClientFactory clientFactory = RESTClientFactory.getInstance();
		IRESTClient restClient = clientFactory.getRESTClientInstance();

		String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String ipx = digx_consulting.get("ESB_OBDX_IP", "");
		
		String genRef = GetRefNumber("BC", 12);
		ESBCardRequestDTO esbRequestDTO = new ESBCardRequestDTO();
		HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
		hostHeaderInfo.setAffiliateCode("EGH"); //temporarily hardcoded
		hostHeaderInfo.setIpAddress(ipx);
		hostHeaderInfo.setRequestId(genRef);
		hostHeaderInfo.setRequestToken("3322344667809876");
		hostHeaderInfo.setRequestType("BLOCKCARD");
		hostHeaderInfo.setSourceChannelId("MOBILE");
		hostHeaderInfo.setSourceCode(sourceCode);
		esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);
		esbRequestDTO.setCustomerNo(cardDetail.getCardCustomerId());
		esbRequestDTO.setMobileNo(cardDetail.getMobileNo());
		esbRequestDTO.setLast4Digits(cardDetail.getMaskedPan().substring(12));

		ESBCardResponseDTO resp = null;

		try {
			XStream xs = new XStream();
			logger.info("Card Block Request : " + xs.toXML(esbRequestDTO));
			logger.info("Card Block Source Code : " + sourceCode);

			resp = (ESBCardResponseDTO) restClient.post(prop, ESBCardResponseDTO.class, esbRequestDTO);
			logger.info("Generate CustomerNo     : " + resp.getCustomerNo());
			logger.info("Generate Card-Response  : " + xs.toXML(resp));
			logger.info("getResponseCode         : " + resp.getHostHeaderInfo().getResponseCode());
			logger.info("getResponseMessage      : " + resp.getHostHeaderInfo().getResponseMessage());
			if (resp != null && resp.getHostHeaderInfo().getResponseCode().equals("000")) {
				response.setResponseCode("000");
				response.setResponseMessage(resp.getHostHeaderInfo().getResponseMessage());
				response.setActivityDescription("BLOCK CARD");
				response.setCardId(cardDetail.getRefKey().getCardId());
				response.setRefId(genRef);
			}
		} finally {
		}
		AdapterInteraction.close();

		return response;
	}
	
	
	public CardActivityDTO unblockCard(CardDetail cardDetail) throws Exception {
		CardActivityDTO response = new CardActivityDTO();
		response.setResponseCode("E04");
		response.setResponseMessage("Failed at interface level");
		AdapterInteraction.begin();

		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESB_VIRTUAL_CARD_UNBLOCK");
		RESTClientFactory clientFactory = RESTClientFactory.getInstance();
		IRESTClient restClient = clientFactory.getRESTClientInstance();

		String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String ipx = digx_consulting.get("ESB_OBDX_IP", "");
		
		String genRef = GetRefNumber("UBC", 12);
		ESBCardRequestDTO esbRequestDTO = new ESBCardRequestDTO();
		HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
		hostHeaderInfo.setAffiliateCode("EGH"); //temporarily hardcoded
		hostHeaderInfo.setIpAddress(ipx);
		hostHeaderInfo.setRequestId(genRef);
		hostHeaderInfo.setRequestToken("3322344667809876");
		hostHeaderInfo.setRequestType("UNBLOCKCARD");
		hostHeaderInfo.setSourceChannelId("MOBILE");
		hostHeaderInfo.setSourceCode(sourceCode);
		esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);
		esbRequestDTO.setCustomerNo(cardDetail.getCardCustomerId());
		esbRequestDTO.setMobileNo(cardDetail.getMobileNo());
		esbRequestDTO.setLast4Digits(cardDetail.getMaskedPan().substring(12));

		ESBCardResponseDTO resp = null;

		try {
			XStream xs = new XStream();
			logger.info("Card UnBlock Request : " + xs.toXML(esbRequestDTO));
			logger.info("Card UnBlock Source Code : " + sourceCode);

			resp = (ESBCardResponseDTO) restClient.post(prop, ESBCardResponseDTO.class, esbRequestDTO);
			logger.info("Generate CustomerNo     : " + resp.getCustomerNo());
			logger.info("Generate Card-Response  : " + xs.toXML(resp));
			logger.info("getResponseCode         : " + resp.getHostHeaderInfo().getResponseCode());
			logger.info("getResponseMessage      : " + resp.getHostHeaderInfo().getResponseMessage());
			if (resp != null && resp.getHostHeaderInfo().getResponseCode().equals("000")) {
				response.setResponseCode("000");
				response.setResponseMessage(resp.getHostHeaderInfo().getResponseMessage());
				response.setActivityDescription("UNBLOCK CARD");
				response.setCardId(cardDetail.getRefKey().getCardId());
				response.setRefId(genRef);
			}
		} finally {
		}
		AdapterInteraction.close();

		return response;
	}
	
	
	public CardBalanceResponse balanceCheck(String cardCustomerId, String mobile) throws Exception {
		CardBalanceResponse response = new CardBalanceResponse();
		response.setResponseCode("E04");
		response.setResponseMessage("Failed at interface level");
		AdapterInteraction.begin();

		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESB_VIRTUAL_CARD_BALANCE");
		RESTClientFactory clientFactory = RESTClientFactory.getInstance();
		IRESTClient restClient = clientFactory.getRESTClientInstance();

		String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String ipx = digx_consulting.get("ESB_OBDX_IP", "");
		
		String genRef = GetRefNumber("BC", 12);
		ESBCardRequestDTO esbRequestDTO = new ESBCardRequestDTO();
		HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
		hostHeaderInfo.setAffiliateCode("EGH"); //temporarily hardcoded
		hostHeaderInfo.setIpAddress(ipx);
		hostHeaderInfo.setRequestId(genRef);
		hostHeaderInfo.setRequestToken("3322344667809876");
		hostHeaderInfo.setRequestType("GETBAL");
		hostHeaderInfo.setSourceChannelId("MOBILE");
		hostHeaderInfo.setSourceCode(sourceCode);
		esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);
		esbRequestDTO.setCustomerNo(cardCustomerId);
		esbRequestDTO.setMobileNo(mobile);

		ESBCardResponseDTO resp = null;

		try {
			XStream xs = new XStream();
			logger.info("Card Balance Request : " + xs.toXML(esbRequestDTO));
			logger.info("Card Balance Source Code : " + sourceCode);

			resp = (ESBCardResponseDTO) restClient.post(prop, ESBCardResponseDTO.class, esbRequestDTO);
			logger.info("Generate CustomerNo     : " + resp.getCustomerNo());
			logger.info("Generate Card-Response  : " + xs.toXML(resp));
			logger.info("getResponseCode         : " + resp.getHostHeaderInfo().getResponseCode());
			logger.info("getResponseMessage      : " + resp.getHostHeaderInfo().getResponseMessage());
			logger.info("getBalance Check        : " + resp.getBalance());
			logger.info("getBalance ccy          : " + resp.getCcy());
			if (resp != null && resp.getHostHeaderInfo().getResponseCode().equals("000")) {
				response.setResponseCode("000");
				response.setResponseMessage(resp.getHostHeaderInfo().getResponseMessage());
				response.setBalance(new BigDecimal(resp.getBalance()));
				response.setCcyCode(resp.getCcy());
				response.setNameOnCard(resp.getName());
				response.setCardType("VIRTUAL");
				System.out.println("PAN  22:::   ");
				if(resp.getPan() != null)
				{
					String pan = resp.getPan();
					if(pan != null && pan.length() > 4)
					   response.setMaskedPan(pan.substring(0,6) + "****" + pan.substring(pan.length() - 4));
				}
				
			}
		} finally {
		}
		AdapterInteraction.close();

		return response;
	}
	
	
	public CardServiceResponseDTO fundCard(CardFundingRequestDTO requestAct) throws Exception {
		CardServiceResponseDTO response = new CardServiceResponseDTO();
		response.setResponseCode("E04");
		response.setResponseMessage("Failed at interface level");
		AdapterInteraction.begin();

		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESB_VIRTUAL_CARD_FUND");
		RESTClientFactory clientFactory = RESTClientFactory.getInstance();
		IRESTClient restClient = clientFactory.getRESTClientInstance();

		String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String ipx = digx_consulting.get("ESB_OBDX_IP", "");
		
		String genRef = requestAct.getExternalRefNo(); // GetRefNumber("BC", 12);
		ESBCardRequestDTO esbRequestDTO = new ESBCardRequestDTO();
		HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
		hostHeaderInfo.setAffiliateCode("EGH"); //temporarily hardcoded
		hostHeaderInfo.setIpAddress(ipx);
		hostHeaderInfo.setRequestId(genRef);
		hostHeaderInfo.setRequestToken(genRef);
		hostHeaderInfo.setRequestType("FUNDCARD");
		hostHeaderInfo.setSourceChannelId("MOBILE");
		hostHeaderInfo.setSourceCode(sourceCode);
		esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);
		esbRequestDTO.setCustomerNo(requestAct.getCardCustomerId());
		esbRequestDTO.setMobileNo(requestAct.getMobileNo());
		String mPan = requestAct.getMaskedPan();
		mPan = mPan.substring(mPan.length() - 4);
		esbRequestDTO.setLast4Digits(mPan);
		esbRequestDTO.setTransferType("WalletToCard");
		esbRequestDTO.setCcy("XOF"); //XOF is the only allowed currency on UAT
		esbRequestDTO.setReferenceMemo(requestAct.getNote());
		esbRequestDTO.setAmount(requestAct.getAmount().toString());
		ESBCardResponseDTO resp = null;

		try {
			XStream xs = new XStream();
			logger.info("Fund Card Request : " + xs.toXML(esbRequestDTO));
			logger.info("Fund Card Source Code : " + sourceCode);

			resp = (ESBCardResponseDTO) restClient.post(prop, ESBCardResponseDTO.class, esbRequestDTO);
			logger.info("Generate CustomerNo     : " + resp.getCustomerNo());
			logger.info("Generate Card-Response  : " + xs.toXML(resp));
			logger.info("getResponseCode         : " + resp.getHostHeaderInfo().getResponseCode());
			logger.info("getResponseMessage      : " + resp.getHostHeaderInfo().getResponseMessage());
			if (resp != null && resp.getHostHeaderInfo().getResponseCode().equals("000")) {
				response.setResponseCode("000");
				response.setResponseMessage(resp.getHostHeaderInfo().getResponseMessage());
				//response.setActivityDescription("FUND CARD");
				response.setTransactionRefNo(resp.getTransRefNo());
			}
		} finally {
		}
		AdapterInteraction.close();

		return response;
	}
	
	
	public CardActivityDTO miniStatement(CardDetail cardDetail) throws Exception {
		CardActivityDTO response = new CardActivityDTO();
		response.setResponseCode("E04");
		response.setResponseMessage("Failed at interface level");
		AdapterInteraction.begin();

		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESB_VIRTUAL_CARD_MINISTATEMENT");
		RESTClientFactory clientFactory = RESTClientFactory.getInstance();
		IRESTClient restClient = clientFactory.getRESTClientInstance();

		String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String ipx = digx_consulting.get("ESB_OBDX_IP", "");
		
		String genRef = GetRefNumber("BC", 12);
		ESBCardRequestDTO esbRequestDTO = new ESBCardRequestDTO();
		HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
		hostHeaderInfo.setAffiliateCode("EGH"); //temporarily hardcoded
		hostHeaderInfo.setIpAddress(ipx);
		hostHeaderInfo.setRequestId(genRef);
		hostHeaderInfo.setRequestToken("3322344667809876");
		hostHeaderInfo.setRequestType("MINISTATEMENT");
		hostHeaderInfo.setSourceChannelId("MOBILE");
		hostHeaderInfo.setSourceCode(sourceCode);
		esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);
		esbRequestDTO.setCustomerNo(cardDetail.getCardCustomerId());
		esbRequestDTO.setStartDate("");
		esbRequestDTO.setEndDate("");
		esbRequestDTO.setNumberOfTrans("");

		ESBCardResponseDTO resp = null;

		try {
			XStream xs = new XStream();
			logger.info("Mini Statement Request : " + xs.toXML(esbRequestDTO));
			logger.info("Mini Statement Source Code : " + sourceCode);

			resp = (ESBCardResponseDTO) restClient.post(prop, ESBCardResponseDTO.class, esbRequestDTO);
			logger.info("Generate CustomerNo     : " + resp.getCustomerNo());
			logger.info("Generate Card-Response  : " + xs.toXML(resp));
			logger.info("getResponseCode         : " + resp.getHostHeaderInfo().getResponseCode());
			logger.info("getResponseMessage      : " + resp.getHostHeaderInfo().getResponseMessage());
			if (resp != null && resp.getHostHeaderInfo().getResponseCode().equals("000")) {
				response.setResponseCode("000");
				response.setResponseMessage(resp.getHostHeaderInfo().getResponseMessage());
				response.setActivityDescription("MINISTATEMENT");
			}
		} finally {
		}
		AdapterInteraction.close();

		return response;
	}
	
	
	public  String GetRefNumber(String type, int len) {

        String finalString = "";
        int x = 0;
        char[] stringChars = new char[len];
        for (int i = 0; i < len; i++) //4
        {
            Random random = new Random();
            x = random.nextInt(9);

            stringChars[i] = Integer.toString(x).toCharArray()[0];
        }


        finalString = new String(stringChars);
        finalString = type + finalString;
        return finalString.trim();
    }
}
