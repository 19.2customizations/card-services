 package com.ecobank.digx.cz.extxface.mule.adapter.client;

import java.math.BigDecimal;
import java.util.Random;
import java.util.logging.Logger;
import java.util.prefs.Preferences;

import com.ecobank.digx.cz.app.cardservices.dto.CardActivityDTO;
import com.ecobank.digx.cz.app.cardservices.dto.CardBalanceResponse;
import com.ecobank.digx.cz.app.cardservices.dto.CardFundingRequestDTO;
import com.ecobank.digx.cz.app.cardservices.dto.CardServiceResponseDTO;
import com.ecobank.digx.cz.app.cardservices.dto.CardServiceUpdateRequestDTO;
import com.ecobank.digx.cz.app.domain.cardservices.entity.CardDetail;
import com.ecobank.digx.cz.app.domain.cardservices.entity.CardRequest;
import com.ecobank.digx.cz.extxface.mule.dto.ESBCardRequestDTO;
import com.ecobank.digx.cz.extxface.mule.dto.ESBCardResponseDTO;
import com.ecobank.digx.cz.extxface.mule.dto.HostHeaderInfo;
import com.ofss.digx.app.core.AdapterInteraction;
import com.ofss.digx.framework.rest.client.RESTClientFactory;
import com.ofss.digx.framework.rest.client.RESTClientRequestBuilder;
import com.ofss.digx.framework.rest.client.RESTClientRequestConfig;
import com.ofss.digx.framework.rest.client.impl.IRESTClient;
import com.ofss.digx.infra.exceptions.Exception;
import com.ofss.fc.infra.config.ConfigurationFactory;
import com.ofss.fc.infra.log.impl.MultiEntityLogger;
import com.thoughtworks.xstream.XStream;

public class CreditCardServicesRepositoryAdapter {
	private static CreditCardServicesRepositoryAdapter singletonInstance;

	private Preferences digx_consulting = ConfigurationFactory.getInstance().getConfigurations("DIGXCONSULTING");

	private static final String THIS_COMPONENT_NAME = CreditCardServicesRepositoryAdapter.class.getName();
	private static transient Logger logger = MultiEntityLogger.getUniqueInstance().getLogger(THIS_COMPONENT_NAME);
	
	public static CreditCardServicesRepositoryAdapter getInstance() {
		if (singletonInstance == null)
			synchronized (CreditCardServicesRepositoryAdapter.class) {
				if (singletonInstance == null)
					singletonInstance = new CreditCardServicesRepositoryAdapter();
			}
		return singletonInstance;
	}
	
	public CardBalanceResponse balanceCheck(String cardId,String affCode) throws Exception {
		CardBalanceResponse response = new CardBalanceResponse();
		response.setResponseCode("E04");
		response.setResponseMessage("Failed at interface level");
		AdapterInteraction.begin();

		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESB_CREDIT_CARD_NAME");
		RESTClientFactory clientFactory = RESTClientFactory.getInstance();
		IRESTClient restClient = clientFactory.getRESTClientInstance();

		String requestId = GetRefNumber("BC", 12);
		String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String pwd = digx_consulting.get("ESB_OBDX_PWD", "");
		String requestToken = JEncrypt.GenerateTransactionToken(sourceCode, requestId, pwd);
		String ipx = digx_consulting.get("ESB_OBDX_IP", "");
		
		
		ESBCardRequestDTO esbRequestDTO = new ESBCardRequestDTO();
		HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
		hostHeaderInfo.setAffiliateCode("EGH"); //temporarily hardcoded
		hostHeaderInfo.setIpAddress(ipx);
		hostHeaderInfo.setRequestId(requestId);
		hostHeaderInfo.setRequestToken(requestToken);
		hostHeaderInfo.setRequestType("GETCARDBALANCE");
		hostHeaderInfo.setSourceChannelId("WEB");
		hostHeaderInfo.setSourceCode(sourceCode);
		esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);
		esbRequestDTO.setCardNo(cardId);

		ESBCardResponseDTO resp = null;

		try {
			XStream xs = new XStream();
			logger.info("Card Balance Request : " + xs.toXML(esbRequestDTO));
			logger.info("Card Balance Source Code : " + sourceCode);

			resp = (ESBCardResponseDTO) restClient.post(prop, ESBCardResponseDTO.class, esbRequestDTO);
			logger.info("Generate Name           : " + resp.getName());
			logger.info("Generate Card-Response  : " + xs.toXML(resp));
			logger.info("getResponseCode         : " + resp.getHostHeaderInfo().getResponseCode());
			logger.info("getResponseMessage      : " + resp.getHostHeaderInfo().getResponseMessage());
			
			if (resp != null && resp.getHostHeaderInfo().getResponseCode().equals("000")) {
				
				CardBalanceResponse cardBalanceResponse = getCardBalance(cardId);
				logger.info("credit card balance : " + cardBalanceResponse.getBalance());
				logger.info("credit card account : " + cardBalanceResponse.getCardAccount());
				
				response.setResponseCode("000");
				response.setResponseMessage(resp.getHostHeaderInfo().getResponseMessage());
				response.setBalance(cardBalanceResponse.getBalance());
				if(cardBalanceResponse.getExpiry() != null)
				  response.setExpiry(cardBalanceResponse.getExpiry());
				else
					response.setExpiry("");
				
				String ccyCode = digx_consulting.get(affCode + "_LCY", "");
				
				if( resp.getCcy() != null)
				   response.setCcyCode(resp.getCcy());
				else
					response.setCcyCode(ccyCode);
				
				response.setNameOnCard(resp.getName());
				response.setCardAccount(cardBalanceResponse.getCardAccount());
				String pan = cardBalanceResponse.getCardAccount();
				//To be replaced with Pseudo PAN or Masked PAN in next release
				String maskedPan = pan.substring(0,6) + "****" + pan.substring(pan.length() - 4);
				
				response.setMaskedPan(maskedPan);
				response.setSourceAccount(cardBalanceResponse.getSourceAccount());
				response.setCardType("CREDIT");
			}
		} finally {
		}
		AdapterInteraction.close();

		return response;
	}
	
	
	public CardBalanceResponse getCardBalance(String cardId) throws Exception {
		CardBalanceResponse cardBalanceResponse = new CardBalanceResponse();
		AdapterInteraction.begin();

		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESB_CREDIT_CARD_BALANCE");
		RESTClientFactory clientFactory = RESTClientFactory.getInstance();
		IRESTClient restClient = clientFactory.getRESTClientInstance();

		String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String ipx = digx_consulting.get("ESB_OBDX_IP", "");
		
		String genRef = GetRefNumber("BC", 12);
		ESBCardRequestDTO esbRequestDTO = new ESBCardRequestDTO();
		HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
		hostHeaderInfo.setAffiliateCode("EGH"); //temporarily hardcoded
		hostHeaderInfo.setIpAddress(ipx);
		hostHeaderInfo.setRequestId(genRef);
		hostHeaderInfo.setRequestToken("3322344667809876");
		hostHeaderInfo.setRequestType("POSTBILL");
		hostHeaderInfo.setSourceChannelId("WEB");
		hostHeaderInfo.setSourceCode(sourceCode);
		esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);
		esbRequestDTO.setCardAccount(cardId);

		ESBCardResponseDTO resp = null;

		try {
			XStream xs = new XStream();
			logger.info("Card Balance Request : " + xs.toXML(esbRequestDTO));
			logger.info("Card Balance Source Code : " + sourceCode);

			resp = (ESBCardResponseDTO) restClient.post(prop, ESBCardResponseDTO.class, esbRequestDTO);
			logger.info("Available Balance            : " + resp.getAvailableAmount());
			logger.info("Generate Card-Response  : " + xs.toXML(resp));
			logger.info("getResponseCode         : " + resp.getHostHeaderInfo().getResponseCode());
			logger.info("getResponseMessage      : " + resp.getHostHeaderInfo().getResponseMessage());
			if (resp != null && resp.getHostHeaderInfo().getResponseCode().equals("000")) {
				//balance = resp.getAvailableAmount();
				cardBalanceResponse.setBalance(new BigDecimal(resp.getAvailableAmount()));
				cardBalanceResponse.setCardAccount(resp.getCardAccount());
				cardBalanceResponse.setExpiry(resp.getExpiry());
				cardBalanceResponse.setSourceAccount(resp.getAccountNo());
			}
		} finally {
		}
		AdapterInteraction.close();

		return cardBalanceResponse;
	}
	
	
	//credit card funding
	public CardServiceResponseDTO fundCard(CardFundingRequestDTO requestAct) throws Exception {
		CardServiceResponseDTO response = new CardServiceResponseDTO();
		response.setResponseCode("E04");
		response.setResponseMessage("Failed at interface level");
		AdapterInteraction.begin();

		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESB_CREDIT_CARD_FUND");
		RESTClientFactory clientFactory = RESTClientFactory.getInstance();
		IRESTClient restClient = clientFactory.getRESTClientInstance();

		String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String ipx = digx_consulting.get("ESB_OBDX_IP", "");
		
		String genRef = requestAct.getExternalRefNo(); // GetRefNumber("BC", 12);
		ESBCardRequestDTO esbRequestDTO = new ESBCardRequestDTO();
		HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
		hostHeaderInfo.setAffiliateCode("EGH"); //temporarily hardcoded
		hostHeaderInfo.setIpAddress(ipx);
		hostHeaderInfo.setRequestId(genRef);
		hostHeaderInfo.setRequestToken(genRef);
		hostHeaderInfo.setRequestType("FUNDCARD");
		hostHeaderInfo.setSourceChannelId("MOBILE");
		hostHeaderInfo.setSourceCode(sourceCode);
		esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);
		//
		esbRequestDTO.setCardActNo(requestAct.getCardAccount());
		esbRequestDTO.setRecipientName(requestAct.getNameOnCard());
		esbRequestDTO.setAmount(requestAct.getAmount().toString());
		esbRequestDTO.setCcy("GHS"); //temporary
		esbRequestDTO.setAccountNumber(requestAct.getAccountNumber());
		esbRequestDTO.setAccountBranch(requestAct.getAccountBranch());
		esbRequestDTO.setNarration(requestAct.getNote());
		
		ESBCardResponseDTO resp = null;

		try {
			XStream xs = new XStream();
			logger.info("Fund Credit Card Request : " + xs.toXML(esbRequestDTO));

			resp = (ESBCardResponseDTO) restClient.post(prop, ESBCardResponseDTO.class, esbRequestDTO);
			logger.info("Generate CustomerNo     : " + resp.getCustomerNo());
			logger.info("cc Card-Response           : " + xs.toXML(resp));
			logger.info("cc getResponseCode         : " + resp.getHostHeaderInfo().getResponseCode());
			logger.info("cc getResponseMessage      : " + resp.getHostHeaderInfo().getResponseMessage());
			logger.info("cc internalRefNo           : " + resp.getInternalRefNo());
			if (resp != null && resp.getHostHeaderInfo().getResponseCode().equals("000")) {
				response.setResponseCode("000");
				response.setResponseMessage(resp.getHostHeaderInfo().getResponseMessage());
				response.setTransactionRefNo(resp.getTransRefNo());
			}
		} finally {
		}
		AdapterInteraction.close();

		return response;
	}
	
	
	public CardActivityDTO blockCard(CardRequest cardRequest) throws Exception {
		CardActivityDTO response = new CardActivityDTO();
		response.setResponseCode("E04");
		response.setResponseMessage("Failed at interface level");
		AdapterInteraction.begin();

		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESB_CREDIT_CARD_BLOCK");
		RESTClientFactory clientFactory = RESTClientFactory.getInstance();
		IRESTClient restClient = clientFactory.getRESTClientInstance();

		String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String ipx = digx_consulting.get("ESB_OBDX_IP", "");
		
		String genRef = GetRefNumber("BC", 12);
		ESBCardRequestDTO esbRequestDTO = new ESBCardRequestDTO();
		HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
		hostHeaderInfo.setAffiliateCode("EGH"); //temporarily hardcoded
		hostHeaderInfo.setIpAddress(ipx);
		hostHeaderInfo.setRequestId(genRef);
		hostHeaderInfo.setRequestToken("3322344667809876");
		hostHeaderInfo.setRequestType("BLOCKCARD");
		hostHeaderInfo.setSourceChannelId("MOBILE");
		hostHeaderInfo.setSourceCode(sourceCode);
		esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);
		esbRequestDTO.setReasonCode(cardRequest.getNote());
		esbRequestDTO.setCardNo(cardRequest.getCardAccountNo());

		ESBCardResponseDTO resp = null;

		try {
			XStream xs = new XStream();
			logger.info("Card Block Request : " + xs.toXML(esbRequestDTO));
			logger.info("Card Block Source Code : " + sourceCode);

			resp = (ESBCardResponseDTO) restClient.post(prop, ESBCardResponseDTO.class, esbRequestDTO);
			logger.info("Generate CustomerNo     : " + resp.getCustomerNo());
			logger.info("Generate Card-Response  : " + xs.toXML(resp));
			logger.info("getResponseCode         : " + resp.getHostHeaderInfo().getResponseCode());
			logger.info("getResponseMessage      : " + resp.getHostHeaderInfo().getResponseMessage());
			if (resp != null && resp.getHostHeaderInfo().getResponseCode().equals("000")) {
				response.setResponseCode("000");
				response.setResponseMessage(resp.getHostHeaderInfo().getResponseMessage());
				response.setActivityDescription("BLOCK CARD");
				response.setCardId("");
				response.setRefId(genRef);
			}
		} finally {
		}
		AdapterInteraction.close();

		return response;
	}
	
	
	public CardActivityDTO unblockCard(CardRequest cardRequest) throws Exception {
		CardActivityDTO response = new CardActivityDTO();
		response.setResponseCode("E04");
		response.setResponseMessage("Failed at interface level");
		AdapterInteraction.begin();

		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESB_CREDIT_CARD_UNBLOCK");
		RESTClientFactory clientFactory = RESTClientFactory.getInstance();
		IRESTClient restClient = clientFactory.getRESTClientInstance();

		String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String ipx = digx_consulting.get("ESB_OBDX_IP", "");
		
		String genRef = GetRefNumber("UBC", 12);
		ESBCardRequestDTO esbRequestDTO = new ESBCardRequestDTO();
		HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
		hostHeaderInfo.setAffiliateCode("EGH"); //temporarily hardcoded
		hostHeaderInfo.setIpAddress(ipx);
		hostHeaderInfo.setRequestId(genRef);
		hostHeaderInfo.setRequestToken("3322344667809876");
		hostHeaderInfo.setRequestType("UNBLOCK");
		hostHeaderInfo.setSourceChannelId("MOBILE");
		hostHeaderInfo.setSourceCode(sourceCode);
		esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);
		esbRequestDTO.setReasonCode(cardRequest.getNote());
		esbRequestDTO.setCardNo(cardRequest.getCardAccountNo());

		ESBCardResponseDTO resp = null;

		try {
			XStream xs = new XStream();
			logger.info("Card UnBlock Request : " + xs.toXML(esbRequestDTO));
			logger.info("Card UnBlock Source Code : " + sourceCode);

			resp = (ESBCardResponseDTO) restClient.post(prop, ESBCardResponseDTO.class, esbRequestDTO);
			logger.info("Generate CustomerNo     : " + resp.getCustomerNo());
			logger.info("Generate Card-Response  : " + xs.toXML(resp));
			logger.info("getResponseCode         : " + resp.getHostHeaderInfo().getResponseCode());
			logger.info("getResponseMessage      : " + resp.getHostHeaderInfo().getResponseMessage());
			if (resp != null && resp.getHostHeaderInfo().getResponseCode().equals("000")) {
				response.setResponseCode("000");
				response.setResponseMessage(resp.getHostHeaderInfo().getResponseMessage());
				response.setActivityDescription("UNBLOCK CARD");
				response.setCardId("");
				response.setRefId(genRef);
			}
		} finally {
		}
		AdapterInteraction.close();

		return response;
	}
	
	
	
	public  String GetRefNumber(String type, int len) {

        String finalString = "";
        int x = 0;
        char[] stringChars = new char[len];
        for (int i = 0; i < len; i++) //4
        {
            Random random = new Random();
            x = random.nextInt(9);

            stringChars[i] = Integer.toString(x).toCharArray()[0];
        }


        finalString = new String(stringChars);
        finalString = type + finalString;
        return finalString.trim();
    }
}
