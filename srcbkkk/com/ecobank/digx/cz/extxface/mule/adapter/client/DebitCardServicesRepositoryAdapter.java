 package com.ecobank.digx.cz.extxface.mule.adapter.client;

import java.math.BigDecimal;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.Random;
import java.util.logging.Logger;
import java.util.prefs.Preferences;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.ws.rs.client.Client;

import com.ecobank.digx.cz.app.cardservices.dto.CardActivityDTO;
import com.ecobank.digx.cz.app.cardservices.dto.CardBalanceResponse;
import com.ecobank.digx.cz.app.cardservices.dto.CardFundingRequestDTO;
import com.ecobank.digx.cz.app.cardservices.dto.CardServiceResponseDTO;
import com.ecobank.digx.cz.app.domain.cardservices.entity.CardDetail;
import com.ecobank.digx.cz.app.domain.cardservices.entity.CardRequest;
import com.ecobank.digx.cz.extxface.mule.dto.ESBCardRequestDTO;
import com.ecobank.digx.cz.extxface.mule.dto.ESBCardResponseDTO;
import com.ecobank.digx.cz.extxface.mule.dto.HostHeaderInfo;
import com.ofss.digx.app.core.AdapterInteraction;
import com.ofss.digx.framework.rest.client.RESTClientFactory;
import com.ofss.digx.framework.rest.client.RESTClientRequestBuilder;
import com.ofss.digx.framework.rest.client.RESTClientRequestConfig;
import com.ofss.digx.framework.rest.client.impl.IRESTClient;
import com.ofss.digx.infra.exceptions.Exception;
import com.ofss.fc.infra.config.ConfigurationFactory;
import com.ofss.fc.infra.log.impl.MultiEntityLogger;
import com.thoughtworks.xstream.XStream;

public class DebitCardServicesRepositoryAdapter {
	private static DebitCardServicesRepositoryAdapter singletonInstance;

	private Preferences digx_consulting = ConfigurationFactory.getInstance().getConfigurations("DIGXCONSULTING");

	private static final String THIS_COMPONENT_NAME = DebitCardServicesRepositoryAdapter.class.getName();
	private static transient Logger logger = MultiEntityLogger.getUniqueInstance().getLogger(THIS_COMPONENT_NAME);
	
	public static DebitCardServicesRepositoryAdapter getInstance() {
		if (singletonInstance == null)
			synchronized (DebitCardServicesRepositoryAdapter.class) {
				if (singletonInstance == null)
					singletonInstance = new DebitCardServicesRepositoryAdapter();
			}
		return singletonInstance;
	}
	
	public CardServiceResponseDTO generateCard(CardRequest cardRequest, String branchCode, String deliveryBranch) throws Exception
	{
		CardServiceResponseDTO response = new CardServiceResponseDTO();
		response.setResponseCode("E04");
		response.setResponseMessage("Failed at interface level");
		AdapterInteraction.begin();
		
		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESB_DEBIT_CARD_CREATE");
		enableSSL();
		RESTClientFactory clientFactory = RESTClientFactory.getInstance();
		IRESTClient restClient = clientFactory.getRESTClientInstance();
		
		String url = digx_consulting.get("ESB_DEBIT_CARD_URL", "");
		String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String ipx = digx_consulting.get("ESB_OBDX_IP", "");
		
		String requestId = cardRequest.getRefKey().getExternalRefNo();
		
		String pwd = digx_consulting.get("ESB_OBDX_PWD", "");
		String requestToken = JEncrypt.GenerateTransactionToken(sourceCode, requestId, pwd);
		
		
		
		SimpleDateFormat dff4 = new SimpleDateFormat("yyyy-MM-dd");
		java.util.Date dt = new java.util.Date();
		String dateCurrent = dff4.format(dt);
		
		
		ESBCardRequestDTO esbRequestDTO = new ESBCardRequestDTO();
		HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
		hostHeaderInfo.setAffiliateCode(cardRequest.getAffiliateCode());
		hostHeaderInfo.setIpAddress(ipx);
		hostHeaderInfo.setRequestId(requestId);
		hostHeaderInfo.setRequestToken(requestToken);
		hostHeaderInfo.setRequestType("DEBITCARDREQUEST");
		hostHeaderInfo.setSourceChannelId("WEB");
		hostHeaderInfo.setSourceCode(sourceCode);
		esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);
		
		esbRequestDTO.setAccountNo(cardRequest.getCardAccountNo());
		esbRequestDTO.setCardScheme(cardRequest.getSchemeType());
		esbRequestDTO.setCardProduct("1");
		esbRequestDTO.setRequestBranch(branchCode);
		esbRequestDTO.setCollectionBranch(deliveryBranch);
		esbRequestDTO.setPersonalized("Y");
		esbRequestDTO.setCreatedBy("OBDX");
		esbRequestDTO.setCreatedDate(dateCurrent);
		esbRequestDTO.setVerifiedBy("OBDX");
		esbRequestDTO.setVerifiedDate(dateCurrent);
		esbRequestDTO.setAbridgedCardNo("");
		
		ESBCardResponseDTO resp = null;

		try {
			XStream xs = new XStream();
			logger.info("CARDSERVICES-Request : " + xs.toXML(esbRequestDTO));
			logger.info("CARDSERVICES-URL     : " + url);
			logger.info("CARDSERVICES-Source Code : " + sourceCode);
			
			enableSSL();
			resp = (ESBCardResponseDTO) restClient.post(prop, ESBCardResponseDTO.class, esbRequestDTO);
			//logger.info("Generate Last4Digits    : " + resp.getLast4Digits());
			//logger.info("Generate CustomerNo     : " + resp.getCustomerNo());
			logger.info("Generate Card-Response  : " + xs.toXML(resp));
			logger.info("getResponseCode         : " + resp.getHostHeaderInfo().getResponseCode());
			logger.info("getResponseMessage      : " + resp.getHostHeaderInfo().getResponseMessage());
			if(resp != null && resp.getHostHeaderInfo().getResponseCode().equals("000"))
			{
				response.setResponseCode("000");
				response.setResponseMessage(resp.getHostHeaderInfo().getResponseMessage());
				response.setTransactionRefNo(resp.getHostHeaderInfo().getRequestId());
				//response.setCustomerNo(resp.getCustomerNo());
				//response.setLast4Digits(resp.getLast4Digits());
			}
		} finally {
		}
		AdapterInteraction.close();
	    return response;
	}
	
	
	
	
	public ESBCardResponseDTO inquiry(CardRequest cardRequest, CardServiceResponseDTO cardServiceResponseDTO) throws Exception {
		AdapterInteraction.begin();

		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESB_VIRTUAL_CARD_INQUIRY");
		RESTClientFactory clientFactory = RESTClientFactory.getInstance();
		IRESTClient restClient = clientFactory.getRESTClientInstance();

		String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String ipx = digx_consulting.get("ESB_OBDX_IP", "");

		ESBCardRequestDTO esbRequestDTO = new ESBCardRequestDTO();
		HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
		hostHeaderInfo.setAffiliateCode(cardRequest.getAffiliateCode());
		hostHeaderInfo.setIpAddress(ipx);
		hostHeaderInfo.setRequestId("122233300");
		hostHeaderInfo.setRequestToken("3322344667809876");
		hostHeaderInfo.setRequestType("CARDINQUIRY");
		hostHeaderInfo.setSourceChannelId("MOBILE");
		hostHeaderInfo.setSourceCode(sourceCode);
		esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);
		esbRequestDTO.setCustomerNo(cardServiceResponseDTO.getCustomerNo());
		esbRequestDTO.setLast4Digits(cardServiceResponseDTO.getLast4Digits());

		ESBCardResponseDTO resp = null;

		try {
			XStream xs = new XStream();
			logger.info("CARDSERVICES-Request : " + xs.toXML(esbRequestDTO));
			logger.info("CARDSERVICES-Source Code : " + sourceCode);

			resp = (ESBCardResponseDTO) restClient.post(prop, ESBCardResponseDTO.class, esbRequestDTO);
			logger.info("Generate CustomerNo     : " + resp.getCustomerNo());
			logger.info("Generate Card-Response  : " + xs.toXML(resp));
			logger.info("getResponseCode         : " + resp.getHostHeaderInfo().getResponseCode());
			logger.info("getResponseMessage      : " + resp.getHostHeaderInfo().getResponseMessage());
		} finally {
		}
		AdapterInteraction.close();

		return resp;
	}
	
	
	public CardActivityDTO blockCard(CardDetail cardDetail) throws Exception {
		CardActivityDTO response = new CardActivityDTO();
		response.setResponseCode("E04");
		response.setResponseMessage("Failed at interface level");
		AdapterInteraction.begin();

		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESB_VIRTUAL_CARD_BLOCK");
		RESTClientFactory clientFactory = RESTClientFactory.getInstance();
		IRESTClient restClient = clientFactory.getRESTClientInstance();

		String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String ipx = digx_consulting.get("ESB_OBDX_IP", "");
		
		String genRef = GetRefNumber("BC", 12);
		ESBCardRequestDTO esbRequestDTO = new ESBCardRequestDTO();
		HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
		hostHeaderInfo.setAffiliateCode("EGH"); //temporarily hardcoded
		hostHeaderInfo.setIpAddress(ipx);
		hostHeaderInfo.setRequestId(genRef);
		hostHeaderInfo.setRequestToken("3322344667809876");
		hostHeaderInfo.setRequestType("BLOCKCARD");
		hostHeaderInfo.setSourceChannelId("MOBILE");
		hostHeaderInfo.setSourceCode(sourceCode);
		esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);
		esbRequestDTO.setCustomerNo(cardDetail.getCardCustomerId());
		esbRequestDTO.setMobileNo(cardDetail.getMobileNo());
		esbRequestDTO.setLast4Digits(cardDetail.getMaskedPan().substring(12));

		ESBCardResponseDTO resp = null;

		try {
			XStream xs = new XStream();
			logger.info("Card Block Request : " + xs.toXML(esbRequestDTO));
			logger.info("Card Block Source Code : " + sourceCode);

			resp = (ESBCardResponseDTO) restClient.post(prop, ESBCardResponseDTO.class, esbRequestDTO);
			logger.info("Generate CustomerNo     : " + resp.getCustomerNo());
			logger.info("Generate Card-Response  : " + xs.toXML(resp));
			logger.info("getResponseCode         : " + resp.getHostHeaderInfo().getResponseCode());
			logger.info("getResponseMessage      : " + resp.getHostHeaderInfo().getResponseMessage());
			if (resp != null && resp.getHostHeaderInfo().getResponseCode().equals("000")) {
				response.setResponseCode("000");
				response.setResponseMessage(resp.getHostHeaderInfo().getResponseMessage());
				response.setActivityDescription("BLOCK CARD");
				response.setCardId(cardDetail.getRefKey().getCardId());
				response.setRefId(genRef);
			}
		} finally {
		}
		AdapterInteraction.close();

		return response;
	}
	
	
	public CardActivityDTO unblockCard(CardDetail cardDetail) throws Exception {
		CardActivityDTO response = new CardActivityDTO();
		response.setResponseCode("E04");
		response.setResponseMessage("Failed at interface level");
		AdapterInteraction.begin();

		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESB_VIRTUAL_CARD_UNBLOCK");
		RESTClientFactory clientFactory = RESTClientFactory.getInstance();
		IRESTClient restClient = clientFactory.getRESTClientInstance();

		String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String ipx = digx_consulting.get("ESB_OBDX_IP", "");
		
		String genRef = GetRefNumber("UBC", 12);
		ESBCardRequestDTO esbRequestDTO = new ESBCardRequestDTO();
		HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
		hostHeaderInfo.setAffiliateCode("EGH"); //temporarily hardcoded
		hostHeaderInfo.setIpAddress(ipx);
		hostHeaderInfo.setRequestId(genRef);
		hostHeaderInfo.setRequestToken("3322344667809876");
		hostHeaderInfo.setRequestType("UNBLOCKCARD");
		hostHeaderInfo.setSourceChannelId("MOBILE");
		hostHeaderInfo.setSourceCode(sourceCode);
		esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);
		esbRequestDTO.setCustomerNo(cardDetail.getCardCustomerId());
		esbRequestDTO.setMobileNo(cardDetail.getMobileNo());
		esbRequestDTO.setLast4Digits(cardDetail.getMaskedPan().substring(12));

		ESBCardResponseDTO resp = null;

		try {
			XStream xs = new XStream();
			logger.info("Card UnBlock Request : " + xs.toXML(esbRequestDTO));
			logger.info("Card UnBlock Source Code : " + sourceCode);

			resp = (ESBCardResponseDTO) restClient.post(prop, ESBCardResponseDTO.class, esbRequestDTO);
			logger.info("Generate CustomerNo     : " + resp.getCustomerNo());
			logger.info("Generate Card-Response  : " + xs.toXML(resp));
			logger.info("getResponseCode         : " + resp.getHostHeaderInfo().getResponseCode());
			logger.info("getResponseMessage      : " + resp.getHostHeaderInfo().getResponseMessage());
			if (resp != null && resp.getHostHeaderInfo().getResponseCode().equals("000")) {
				response.setResponseCode("000");
				response.setResponseMessage(resp.getHostHeaderInfo().getResponseMessage());
				response.setActivityDescription("UNBLOCK CARD");
				response.setCardId(cardDetail.getRefKey().getCardId());
				response.setRefId(genRef);
			}
		} finally {
		}
		AdapterInteraction.close();

		return response;
	}
	
	
	public CardBalanceResponse balanceCheck(String cardId) throws Exception {
		CardBalanceResponse response = new CardBalanceResponse();
		response.setResponseCode("E04");
		response.setResponseMessage("Failed at interface level");
		AdapterInteraction.begin();

		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESB_CREDIT_CARD_BALANCE");
		RESTClientFactory clientFactory = RESTClientFactory.getInstance();
		IRESTClient restClient = clientFactory.getRESTClientInstance();

		String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String ipx = digx_consulting.get("ESB_OBDX_IP", "");
		
		String genRef = GetRefNumber("BC", 12);
		ESBCardRequestDTO esbRequestDTO = new ESBCardRequestDTO();
		HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
		hostHeaderInfo.setAffiliateCode("EGH"); //temporarily hardcoded
		hostHeaderInfo.setIpAddress(ipx);
		hostHeaderInfo.setRequestId(genRef);
		hostHeaderInfo.setRequestToken("3322344667809876");
		hostHeaderInfo.setRequestType("BALANCEINQUIRY");
		hostHeaderInfo.setSourceChannelId("WEB");
		hostHeaderInfo.setSourceCode(sourceCode);
		esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);
		esbRequestDTO.setCardNo(cardId);

		ESBCardResponseDTO resp = null;

		try {
			XStream xs = new XStream();
			logger.info("Card Balance Request : " + xs.toXML(esbRequestDTO));
			logger.info("Card Balance Source Code : " + sourceCode);

			resp = (ESBCardResponseDTO) restClient.post(prop, ESBCardResponseDTO.class, esbRequestDTO);
			logger.info("Generate Name           : " + resp.getName());
			logger.info("Generate Card-Response  : " + xs.toXML(resp));
			logger.info("getResponseCode         : " + resp.getHostHeaderInfo().getResponseCode());
			logger.info("getResponseMessage      : " + resp.getHostHeaderInfo().getResponseMessage());
			logger.info("getBalance Check        : " + resp.getBalance());
			logger.info("getBalance ccy          : " + resp.getCcy());
			if (resp != null && resp.getHostHeaderInfo().getResponseCode().equals("000")) {
				response.setResponseCode("000");
				response.setResponseMessage(resp.getHostHeaderInfo().getResponseMessage());
				response.setBalance(new BigDecimal(resp.getBalance()));
				response.setCcyCode(resp.getCcy());
				response.setNameOnCard(resp.getName());
				response.setCardType("DEBIT");
			}
		} finally {
		}
		AdapterInteraction.close();

		return response;
	}
	
	
	//credit card funding
	public CardServiceResponseDTO fundCard(CardFundingRequestDTO requestAct) throws Exception {
		CardServiceResponseDTO response = new CardServiceResponseDTO();
		response.setResponseCode("E04");
		response.setResponseMessage("Failed at interface level");
		AdapterInteraction.begin();

		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESB_CREDIT_CARD_FUND");
		RESTClientFactory clientFactory = RESTClientFactory.getInstance();
		IRESTClient restClient = clientFactory.getRESTClientInstance();

		String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String ipx = digx_consulting.get("ESB_OBDX_IP", "");
		
		String genRef = requestAct.getExternalRefNo(); // GetRefNumber("BC", 12);
		ESBCardRequestDTO esbRequestDTO = new ESBCardRequestDTO();
		HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
		hostHeaderInfo.setAffiliateCode("EGH"); //temporarily hardcoded
		hostHeaderInfo.setIpAddress(ipx);
		hostHeaderInfo.setRequestId(genRef);
		hostHeaderInfo.setRequestToken(genRef);
		hostHeaderInfo.setRequestType("FUNDCARD");
		hostHeaderInfo.setSourceChannelId("MOBILE");
		hostHeaderInfo.setSourceCode(sourceCode);
		esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);
		//
		esbRequestDTO.setCardActNo(requestAct.getCardAccount());
		esbRequestDTO.setRecipientName(requestAct.getNameOnCard());
		esbRequestDTO.setAmount(requestAct.getAmount().toString());
		esbRequestDTO.setCcy("GHS"); //temporary
		//esbRequestDTO.setAccountNumber(requestAct.get);
		//esbRequestDTO.setAccountBranch(requestAct.get);
		//esbRequestDTO.setNarration(requestAct.getNote());
		
		ESBCardResponseDTO resp = null;

		try {
			XStream xs = new XStream();
			logger.info("Fund Credit Card Request : " + xs.toXML(esbRequestDTO));

			resp = (ESBCardResponseDTO) restClient.post(prop, ESBCardResponseDTO.class, esbRequestDTO);
			logger.info("Generate CustomerNo     : " + resp.getCustomerNo());
			logger.info("cc Card-Response           : " + xs.toXML(resp));
			logger.info("cc getResponseCode         : " + resp.getHostHeaderInfo().getResponseCode());
			logger.info("cc getResponseMessage      : " + resp.getHostHeaderInfo().getResponseMessage());
			logger.info("cc internalRefNo           : " + resp.getInternalRefNo());
			if (resp != null && resp.getHostHeaderInfo().getResponseCode().equals("000")) {
				response.setResponseCode("000");
				response.setResponseMessage(resp.getHostHeaderInfo().getResponseMessage());
				response.setTransactionRefNo(resp.getTransRefNo());
			}
		} finally {
		}
		AdapterInteraction.close();

		return response;
	}
	
	
	public CardActivityDTO miniStatement(CardDetail cardDetail) throws Exception {
		CardActivityDTO response = new CardActivityDTO();
		response.setResponseCode("E04");
		response.setResponseMessage("Failed at interface level");
		AdapterInteraction.begin();

		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESB_VIRTUAL_CARD_MINISTATEMENT");
		RESTClientFactory clientFactory = RESTClientFactory.getInstance();
		IRESTClient restClient = clientFactory.getRESTClientInstance();

		String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String ipx = digx_consulting.get("ESB_OBDX_IP", "");
		
		String genRef = GetRefNumber("BC", 12);
		ESBCardRequestDTO esbRequestDTO = new ESBCardRequestDTO();
		HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
		hostHeaderInfo.setAffiliateCode("EGH"); //temporarily hardcoded
		hostHeaderInfo.setIpAddress(ipx);
		hostHeaderInfo.setRequestId(genRef);
		hostHeaderInfo.setRequestToken("3322344667809876");
		hostHeaderInfo.setRequestType("MINISTATEMENT");
		hostHeaderInfo.setSourceChannelId("MOBILE");
		hostHeaderInfo.setSourceCode(sourceCode);
		esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);
		esbRequestDTO.setCustomerNo(cardDetail.getCardCustomerId());
		esbRequestDTO.setStartDate("");
		esbRequestDTO.setEndDate("");
		esbRequestDTO.setNumberOfTrans("");

		ESBCardResponseDTO resp = null;

		try {
			XStream xs = new XStream();
			logger.info("Mini Statement Request : " + xs.toXML(esbRequestDTO));
			logger.info("Mini Statement Source Code : " + sourceCode);

			resp = (ESBCardResponseDTO) restClient.post(prop, ESBCardResponseDTO.class, esbRequestDTO);
			logger.info("Generate CustomerNo     : " + resp.getCustomerNo());
			logger.info("Generate Card-Response  : " + xs.toXML(resp));
			logger.info("getResponseCode         : " + resp.getHostHeaderInfo().getResponseCode());
			logger.info("getResponseMessage      : " + resp.getHostHeaderInfo().getResponseMessage());
			if (resp != null && resp.getHostHeaderInfo().getResponseCode().equals("000")) {
				response.setResponseCode("000");
				response.setResponseMessage(resp.getHostHeaderInfo().getResponseMessage());
				response.setActivityDescription("MINISTATEMENT");
			}
		} finally {
		}
		AdapterInteraction.close();

		return response;
	}
	
	
public static void enableSSL() {
		
		
		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			public void checkClientTrusted(
					java.security.cert.X509Certificate[] certs, String authType) {
			}

			public void checkServerTrusted(
					java.security.cert.X509Certificate[] certs, String authType) {
			}
		} };

		try {
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
			
			 //HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

		        // Create all-trusting host name verifier
		        HostnameVerifier allHostsValid = new HostnameVerifier() {
		            public boolean verify(String hostname, SSLSession session) {
		                return true;
		            }
		        };

		        // Install the all-trusting host verifier
		        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
		        
		} catch (java.lang.Exception e) {
		}
	}
	
	
	/*public Client hostIgnoringClient() {
	    try
	    {
	        SSLContext sslcontext = SSLContext.getInstance( "TLS" );
	        sslcontext.init( null, null, null );
	        DefaultClientConfig config = new DefaultClientConfig();
	        Map<String, Object> properties = config.getProperties();
	        HTTPSProperties httpsProperties = new HTTPSProperties(
	                new HostnameVerifier()
	                {
	                    @Override
	                    public boolean verify( String s, SSLSession sslSession )
	                    {
	                        return true;
	                    }
	                }, sslcontext
	        );
	        properties.put( HTTPSProperties.PROPERTY_HTTPS_PROPERTIES, httpsProperties );
	        config.getClasses().add( JacksonJsonProvider.class );
	        return Client.create( config );
	    }
	    catch ( KeyManagementException | NoSuchAlgorithmException e )
	    {
	        throw new RuntimeException( e );
	    }
	}*/
	
	
	public  String GetRefNumber(String type, int len) {

        String finalString = "";
        int x = 0;
        char[] stringChars = new char[len];
        for (int i = 0; i < len; i++) //4
        {
            Random random = new Random();
            x = random.nextInt(9);

            stringChars[i] = Integer.toString(x).toCharArray()[0];
        }


        finalString = new String(stringChars);
        finalString = type + finalString;
        return finalString.trim();
    }
}
