package com.ecobank.digx.cz.extxface.mule.dto;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"hostHeaderInfo",
"customerNo",
"last4Digits",
"pan",
"expiryDate",
"cvv",
"name",
"transRefNo",
"ccy",
"balance",
"status",
"internalRefNo",
"availableAmount",
"cardAccount",
"accountNo",
"expiry"
})
public class ESBCardResponseDTO {

@JsonProperty("hostHeaderInfo")
private HostHeaderInfo hostHeaderInfo;
@JsonProperty("customerNo")
private String customerNo;
@JsonProperty("last4Digits")
private String last4Digits;
@JsonProperty("pan")
private String pan;
@JsonProperty("expiryDate")
private String expiryDate;
@JsonProperty("cvv")
private String cvv;
@JsonProperty("name")
private String name;
@JsonProperty("transRefNo")
private String transRefNo;
@JsonProperty("ccy")
private String ccy;
@JsonProperty("balance")
private String balance;
@JsonProperty("status")
private String status;
@JsonProperty("internalRefNo")
private String internalRefNo;
@JsonProperty("availableAmount")
private String availableAmount;
@JsonProperty("cardAccount")
private String cardAccount;
@JsonProperty("accountNo")
private String accountNo;
@JsonProperty("expiry")
private String expiry;

@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

@JsonProperty("hostHeaderInfo")
public HostHeaderInfo getHostHeaderInfo() {
return hostHeaderInfo;
}

@JsonProperty("hostHeaderInfo")
public void setHostHeaderInfo(HostHeaderInfo hostHeaderInfo) {
this.hostHeaderInfo = hostHeaderInfo;
}

@JsonProperty("customerNo")
public String getCustomerNo() {
return customerNo;
}

@JsonProperty("customerNo")
public void setCustomerNo(String customerNo) {
this.customerNo = customerNo;
}

@JsonProperty("last4Digits")
public String getLast4Digits() {
return last4Digits;
}

@JsonProperty("last4Digits")
public void setLast4Digits(String last4Digits) {
this.last4Digits = last4Digits;
}

@JsonProperty("pan")
public String getPan() {
return pan;
}

@JsonProperty("pan")
public void setPan(String pan) {
this.pan = pan;
}

@JsonProperty("expiryDate")
public String getExpiryDate() {
return expiryDate;
}

@JsonProperty("expiryDate")
public void setExpiryDate(String expiryDate) {
this.expiryDate = expiryDate;
}

@JsonProperty("cvv")
public String getCvv() {
return cvv;
}

@JsonProperty("cvv")
public void setCvv(String cvv) {
this.cvv = cvv;
}

@JsonProperty("name")
public String getName() {
return name;
}

@JsonProperty("name")
public void setName(String name) {
this.name = name;
}

@JsonProperty("transRefNo")
public String getTransRefNo() {
return transRefNo;
}

@JsonProperty("transRefNo")
public void setTransRefNo(String transRefNo) {
this.transRefNo = transRefNo;
}

@JsonProperty("ccy")
public String getCcy() {
return ccy;
}

@JsonProperty("ccy")
public void setCcy(String ccy) {
this.ccy = ccy;
}

@JsonProperty("balance")
public String getBalance() {
return balance;
}

@JsonProperty("balance")
public void setBalance(String balance) {
this.balance = balance;
}

@JsonProperty("status")
public String getStatus() {
return status;
}

@JsonProperty("status")
public void setStatus(String status) {
this.status = status;
}
@JsonProperty("internalRefNo")
public String getInternalRefNo() {
	return internalRefNo;
}
@JsonProperty("internalRefNo")
public void setInternalRefNo(String internalRefNo) {
	this.internalRefNo = internalRefNo;
}

public String getAvailableAmount() {
	return availableAmount;
}

public void setAvailableAmount(String availableAmount) {
	this.availableAmount = availableAmount;
}

public String getCardAccount() {
	return cardAccount;
}

public void setCardAccount(String cardAccount) {
	this.cardAccount = cardAccount;
}

public String getAccountNo() {
	return accountNo;
}

public void setAccountNo(String accountNo) {
	this.accountNo = accountNo;
}

public String getExpiry() {
	return expiry;
}

public void setExpiry(String expiry) {
	this.expiry = expiry;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}
