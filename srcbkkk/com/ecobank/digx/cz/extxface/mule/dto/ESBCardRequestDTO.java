package com.ecobank.digx.cz.extxface.mule.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"hostHeaderInfo",
"customerNo",
"firstName",
"lastName",
"address",
"city",
"country",
"postalCode",
"state",
"dob",
"idType",
"idValue",
"mobileNo",
"email",
"expirationDate",
"notificationMethod",
"last4Digits",
"transferType",
"referenceMemo",
"amount",
"ccy",
"startDate",
"endDate",
"numberOfTrans",
"accountNumber",
"accountNo",
"accountBranch",
"narration",
"cardNo",
"cardAccount",
"cardScheme",
"reasonCode"
})
public class ESBCardRequestDTO {

@JsonProperty("hostHeaderInfo")
private HostHeaderInfo hostHeaderInfo;
@JsonProperty("customerNo")
private String customerNo;
@JsonProperty("firstName")
private String firstName;
@JsonProperty("lastName")
private String lastName;
@JsonProperty("address")
private String address;
@JsonProperty("city")
private String city;
@JsonProperty("country")
private String country;
@JsonProperty("postalCode")
private String postalCode;
@JsonProperty("state")
private String state;
@JsonProperty("dob")
private String dob;
@JsonProperty("idType")
private String idType;
@JsonProperty("idValue")
private String idValue;
@JsonProperty("mobileNo")
private String mobileNo;
@JsonProperty("email")
private String email;
@JsonProperty("expirationDate")
private String expirationDate;
@JsonProperty("notificationMethod")
private String notificationMethod;
@JsonProperty("last4Digits")
private String last4Digits;
@JsonProperty("transferType")
private String transferType;
@JsonProperty("referenceMemo")
private String referenceMemo;
@JsonProperty("amount")
private String amount;
@JsonProperty("ccy")
private String ccy;
@JsonProperty("startDate")
private String startDate;
@JsonProperty("endDate")
private String endDate;
@JsonProperty("numberOfTrans")
private String numberOfTrans;
@JsonProperty("cardActNo")
private String cardActNo;
@JsonProperty("recipientName")
private String recipientName;
@JsonProperty("accountNumber")
private String accountNumber;
@JsonProperty("accountNo")
private String accountNo;
@JsonProperty("accountBranch")
private String accountBranch;
@JsonProperty("narration")
private String narration;
@JsonProperty("cardNo")
private String cardNo;
@JsonProperty("cardScheme")
private String cardScheme;
@JsonProperty("cardProduct")
private String cardProduct;
@JsonProperty("requestBranch")
private String requestBranch;
@JsonProperty("collectionBranch")
private String collectionBranch;
@JsonProperty("personalized")
private String personalized;
@JsonProperty("createdBy")
private String createdBy;
@JsonProperty("createdDate")
private String createdDate;
@JsonProperty("verifiedBy")
private String verifiedBy;
@JsonProperty("verifiedDate")
private String verifiedDate;
@JsonProperty("abridgedCardNo")
private String abridgedCardNo; 
@JsonProperty("cardAccount")
private String cardAccount;
@JsonProperty("reasonCode")
private String reasonCode;

@JsonProperty("hostHeaderInfo")
public HostHeaderInfo getHostHeaderInfo() {
return hostHeaderInfo;
}

@JsonProperty("hostHeaderInfo")
public void setHostHeaderInfo(HostHeaderInfo hostHeaderInfo) {
this.hostHeaderInfo = hostHeaderInfo;
}

@JsonProperty("customerNo")
public String getCustomerNo() {
return customerNo;
}

@JsonProperty("customerNo")
public void setCustomerNo(String customerNo) {
this.customerNo = customerNo;
}

@JsonProperty("firstName")
public String getFirstName() {
return firstName;
}

@JsonProperty("firstName")
public void setFirstName(String firstName) {
this.firstName = firstName;
}

@JsonProperty("lastName")
public String getLastName() {
return lastName;
}

@JsonProperty("lastName")
public void setLastName(String lastName) {
this.lastName = lastName;
}

@JsonProperty("address")
public String getAddress() {
return address;
}

@JsonProperty("address")
public void setAddress(String address) {
this.address = address;
}

@JsonProperty("city")
public String getCity() {
return city;
}

@JsonProperty("city")
public void setCity(String city) {
this.city = city;
}

@JsonProperty("country")
public String getCountry() {
return country;
}

@JsonProperty("country")
public void setCountry(String country) {
this.country = country;
}

@JsonProperty("postalCode")
public String getPostalCode() {
return postalCode;
}

@JsonProperty("postalCode")
public void setPostalCode(String postalCode) {
this.postalCode = postalCode;
}

@JsonProperty("state")
public String getState() {
return state;
}

@JsonProperty("state")
public void setState(String state) {
this.state = state;
}

@JsonProperty("dob")
public String getDob() {
return dob;
}

@JsonProperty("dob")
public void setDob(String dob) {
this.dob = dob;
}

@JsonProperty("idType")
public String getIdType() {
return idType;
}

@JsonProperty("idType")
public void setIdType(String idType) {
this.idType = idType;
}

@JsonProperty("idValue")
public String getIdValue() {
return idValue;
}

@JsonProperty("idValue")
public void setIdValue(String idValue) {
this.idValue = idValue;
}

@JsonProperty("mobileNo")
public String getMobileNo() {
return mobileNo;
}

@JsonProperty("mobileNo")
public void setMobileNo(String mobileNo) {
this.mobileNo = mobileNo;
}

@JsonProperty("email")
public String getEmail() {
return email;
}

@JsonProperty("email")
public void setEmail(String email) {
this.email = email;
}

@JsonProperty("expirationDate")
public String getExpirationDate() {
return expirationDate;
}

@JsonProperty("expirationDate")
public void setExpirationDate(String expirationDate) {
this.expirationDate = expirationDate;
}

@JsonProperty("notificationMethod")
public String getNotificationMethod() {
return notificationMethod;
}

@JsonProperty("notificationMethod")
public void setNotificationMethod(String notificationMethod) {
this.notificationMethod = notificationMethod;
}

@JsonProperty("last4Digits")
public String getLast4Digits() {
	return last4Digits;
}

@JsonProperty("last4Digits")
public void setLast4Digits(String last4Digits) {
	this.last4Digits = last4Digits;
}

@JsonProperty("transferType")
public String getTransferType() {
	return transferType;
}

@JsonProperty("transferType")
public void setTransferType(String transferType) {
	this.transferType = transferType;
}

@JsonProperty("referenceMemo")
public String getReferenceMemo() {
	return referenceMemo;
}

@JsonProperty("referenceMemo")
public void setReferenceMemo(String referenceMemo) {
	this.referenceMemo = referenceMemo;
}

@JsonProperty("amount")
public String getAmount() {
	return amount;
}

@JsonProperty("amount")
public void setAmount(String amount) {
	this.amount = amount;
}

@JsonProperty("ccy")
public String getCcy() {
	return ccy;
}
@JsonProperty("ccy")
public void setCcy(String ccy) {
	this.ccy = ccy;
}
@JsonProperty("startDate")
public String getStartDate() {
	return startDate;
}
@JsonProperty("startDate")
public void setStartDate(String startDate) {
	this.startDate = startDate;
}
@JsonProperty("endDate")
public String getEndDate() {
	return endDate;
}
@JsonProperty("endDate")
public void setEndDate(String endDate) {
	this.endDate = endDate;
}
@JsonProperty("numberOfTrans")
public String getNumberOfTrans() {
	return numberOfTrans;
}
@JsonProperty("numberOfTrans")
public void setNumberOfTrans(String numberOfTrans) {
	this.numberOfTrans = numberOfTrans;
}
@JsonProperty("accountNumber")
public String getAccountNumber() {
	return accountNumber;
}
@JsonProperty("accountNumber")
public void setAccountNumber(String accountNumber) {
	this.accountNumber = accountNumber;
}
@JsonProperty("accountBranch")
public String getAccountBranch() {
	return accountBranch;
}
@JsonProperty("accountBranch")
public void setAccountBranch(String accountBranch) {
	this.accountBranch = accountBranch;
}
@JsonProperty("narration")
public String getNarration() {
	return narration;
}
@JsonProperty("narration")
public void setNarration(String narration) {
	this.narration = narration;
}
@JsonProperty("cardActNo")
public String getCardActNo() {
	return cardActNo;
}
@JsonProperty("cardActNo")
public void setCardActNo(String cardActNo) {
	this.cardActNo = cardActNo;
}
@JsonProperty("recipientName")
public String getRecipientName() {
	return recipientName;
}
@JsonProperty("recipientName")
public void setRecipientName(String recipientName) {
	this.recipientName = recipientName;
}
@JsonProperty("cardNo")
public String getCardNo() {
	return cardNo;
}
@JsonProperty("cardNo")
public void setCardNo(String cardNo) {
	this.cardNo = cardNo;
}
@JsonProperty("accountNo")
public String getAccountNo() {
	return accountNo;
}
@JsonProperty("accountNo")
public void setAccountNo(String accountNo) {
	this.accountNo = accountNo;
}
@JsonProperty("cardScheme")
public String getCardScheme() {
	return cardScheme;
}
@JsonProperty("cardScheme")
public void setCardScheme(String cardScheme) {
	this.cardScheme = cardScheme;
}
@JsonProperty("cardProduct")
public String getCardProduct() {
	return cardProduct;
}
@JsonProperty("cardProduct")
public void setCardProduct(String cardProduct) {
	this.cardProduct = cardProduct;
}
@JsonProperty("requestBranch")
public String getRequestBranch() {
	return requestBranch;
}
@JsonProperty("requestBranch")
public void setRequestBranch(String requestBranch) {
	this.requestBranch = requestBranch;
}
@JsonProperty("collectionBranch")
public String getCollectionBranch() {
	return collectionBranch;
}
@JsonProperty("collectionBranch")
public void setCollectionBranch(String collectionBranch) {
	this.collectionBranch = collectionBranch;
}
@JsonProperty("personalized")
public String getPersonalized() {
	return personalized;
}
@JsonProperty("personalized")
public void setPersonalized(String personalized) {
	this.personalized = personalized;
}
@JsonProperty("createdBy")
public String getCreatedBy() {
	return createdBy;
}
@JsonProperty("createdBy")
public void setCreatedBy(String createdBy) {
	this.createdBy = createdBy;
}
@JsonProperty("createdDate")
public String getCreatedDate() {
	return createdDate;
}
@JsonProperty("createdDate")
public void setCreatedDate(String createdDate) {
	this.createdDate = createdDate;
}
@JsonProperty("verifiedBy")
public String getVerifiedBy() {
	return verifiedBy;
}
@JsonProperty("verifiedBy")
public void setVerifiedBy(String verifiedBy) {
	this.verifiedBy = verifiedBy;
}
@JsonProperty("verifiedDate")
public String getVerifiedDate() {
	return verifiedDate;
}
@JsonProperty("verifiedDate")
public void setVerifiedDate(String verifiedDate) {
	this.verifiedDate = verifiedDate;
}
@JsonProperty("abridgedCardNo")
public String getAbridgedCardNo() {
	return abridgedCardNo;
}
@JsonProperty("abridgedCardNo")
public void setAbridgedCardNo(String abridgedCardNo) {
	this.abridgedCardNo = abridgedCardNo;
}
@JsonProperty("cardAccount")
public String getCardAccount() {
	return cardAccount;
}
@JsonProperty("cardAccount")
public void setCardAccount(String cardAccount) {
	this.cardAccount = cardAccount;
}
@JsonProperty("reasonCode")
public String getReasonCode() {
	return reasonCode;
}
@JsonProperty("reasonCode")
public void setReasonCode(String reasonCode) {
	this.reasonCode = reasonCode;
}


}