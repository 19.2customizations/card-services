package com.ecobank.digx.cz.extxface.mule.dto;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"sourceCode",
"requestId",
"requestToken",
"requestType",
"affiliateCode",
"ipAddress",
"sourceChannelId",
"responseCode",
"responseMessage"
})
public class HostHeaderInfo {

@JsonProperty("sourceCode")
private String sourceCode;
@JsonProperty("requestId")
private String requestId;
@JsonProperty("requestToken")
private String requestToken;
@JsonProperty("requestType")
private String requestType;
@JsonProperty("affiliateCode")
private String affiliateCode;
@JsonProperty("ipAddress")
private String ipAddress;
@JsonProperty("sourceChannelId")
private String sourceChannelId;
@JsonProperty("responseCode")
private String responseCode;
@JsonProperty("responseMessage")
private String responseMessage;

@JsonProperty("sourceCode")
public String getSourceCode() {
return sourceCode;
}

@JsonProperty("sourceCode")
public void setSourceCode(String sourceCode) {
this.sourceCode = sourceCode;
}

@JsonProperty("requestId")
public String getRequestId() {
return requestId;
}

@JsonProperty("requestId")
public void setRequestId(String requestId) {
this.requestId = requestId;
}

@JsonProperty("requestToken")
public String getRequestToken() {
return requestToken;
}

@JsonProperty("requestToken")
public void setRequestToken(String requestToken) {
this.requestToken = requestToken;
}

@JsonProperty("requestType")
public String getRequestType() {
return requestType;
}

@JsonProperty("requestType")
public void setRequestType(String requestType) {
this.requestType = requestType;
}

@JsonProperty("affiliateCode")
public String getAffiliateCode() {
return affiliateCode;
}

@JsonProperty("affiliateCode")
public void setAffiliateCode(String affiliateCode) {
this.affiliateCode = affiliateCode;
}

@JsonProperty("ipAddress")
public String getIpAddress() {
return ipAddress;
}

@JsonProperty("ipAddress")
public void setIpAddress(String ipAddress) {
this.ipAddress = ipAddress;
}

@JsonProperty("sourceChannelId")
public String getSourceChannelId() {
return sourceChannelId;
}

@JsonProperty("sourceChannelId")
public void setSourceChannelId(String sourceChannelId) {
this.sourceChannelId = sourceChannelId;
}

@JsonProperty("responseCode")
public String getResponseCode() {
	return responseCode;
}

@JsonProperty("responseCode")
public void setResponseCode(String responseCode) {
	this.responseCode = responseCode;
}

@JsonProperty("responseMessage")
public String getResponseMessage() {
	return responseMessage;
}

@JsonProperty("responseMessage")
public void setResponseMessage(String responseMessage) {
	this.responseMessage = responseMessage;
}

}