package com.ecobank.digx.cz.appx.cardservices.service;

import javax.ws.rs.core.Response;

import com.ecobank.digx.cz.app.cardservices.dto.CardListResponse;
import com.ecobank.digx.cz.app.cardservices.dto.CardServiceDTO;

import com.ecobank.digx.cz.app.cardservices.dto.FetchCardFeeRequestDTO;
import com.ecobank.digx.cz.app.cardservices.dto.TravelNotificationRequestDTO;

import com.ofss.fc.app.context.SessionContext;

import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Schema;


public interface ICardService {

	Response read(String accNo);

	Response createCard(CardServiceDTO cardServiceDTO);

	Response updateCard(String referenceId, String actionCode);

	Response updateCardRequest(String referenceId, String actionCode);

	Response inquiry(String customerNo, String last4Digits);

	Response fundCard(String amount, String customerNo, String currency, String last4Digits);

	Response balanceCheck(String customerNo);

	Response miniStatement(String customerNo, String startDate, String endDate, String numberOfTrans);

	Response fetchCardLimit(String cardId, String cardCategory,String cardAccountCustNo);
	
	Response fetchCardByMsisdn(String cardType,String msisdn);
	
	 Response getCardStatement(
			 String cardId,
			String cardCategory,
			 String startDate,
			String endDate,
			String msisdn) ;
		

	public Response listCardActivityById(String cardId);

	public Response listCardRequestsByCustomerId(String customerId);
	
	public Response listCardRequestFees(String cardCategory);
	public Response listCardLimits(String cardCategory);

	public Response listCardsByCustomerId(String customerId);

	public Response getCardBalance(String cardId, String cardCategory,String expiryDate,String cvv, String maskedPan,String cardType, String mobileNo);
	

	public Response travelNotification(TravelNotificationRequestDTO travelNotificationRequestDTO);
	
	public Response fetchCardFee(FetchCardFeeRequestDTO fetchCardFeeRequestDTO);
}
