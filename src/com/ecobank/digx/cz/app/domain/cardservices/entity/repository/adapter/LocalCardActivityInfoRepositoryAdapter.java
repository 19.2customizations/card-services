package com.ecobank.digx.cz.app.domain.cardservices.entity.repository.adapter;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import com.ecobank.digx.cz.app.domain.cardservices.entity.CardActivityInfo;
import com.ecobank.digx.cz.app.domain.cardservices.entity.CardActivityInfoKey;
import com.ofss.digx.framework.determinant.DeterminantResolver;
import com.ofss.digx.framework.domain.repository.adapter.AbstractLocalRepositoryAdapter;
import com.ofss.digx.infra.exceptions.Exception;

public class LocalCardActivityInfoRepositoryAdapter extends AbstractLocalRepositoryAdapter<CardActivityInfo>
		implements ICardActivityInfoRepositoryAdapter {

	private static LocalCardActivityInfoRepositoryAdapter singletonInstance;

	public static LocalCardActivityInfoRepositoryAdapter getInstance() {
		if (singletonInstance == null) {
			synchronized (LocalCardActivityInfoRepositoryAdapter.class) {
				if (singletonInstance == null) {
					singletonInstance = new LocalCardActivityInfoRepositoryAdapter();
				}
			}
		}
		return singletonInstance;
	}

	public void create(CardActivityInfo cardActivityInfo) throws Exception {
		System.out.println("ActivityType  ::: " + cardActivityInfo.getActivityType());
		System.out.println("refId         ::: " + cardActivityInfo.getRefKey().getRefId());
		if (cardActivityInfo.getRefKey() != null) {
			/*
			 * object.getPassportNumber().setDeterminantValue(
			 * DeterminantResolver.getInstance().fetchDeterminantValue(AccountTransfer.class
			 * .getName()));
			 */
		}
		
		System.out.println("refId-card-activity::" + cardActivityInfo.isEntityReadOnly());
		super.insert(cardActivityInfo);
	}

	public void update(CardActivityInfo object) throws Exception {
		super.update(object);

	}

	@Override
	public CardActivityInfo read(CardActivityInfoKey key) throws Exception {
		System.out.println("inside local-Repo:  " + key);
		key.setDeterminantValue(
				DeterminantResolver.getInstance().fetchDeterminantValue(CardActivityInfo.class.getName()));
		return (CardActivityInfo) get(CardActivityInfo.class, (Serializable) key);
	}

	public List<CardActivityInfo> listCardActivityById(String cardId) throws Exception {
		HashMap<String, Object> parameters = null;
		List<CardActivityInfo> list = null;
		parameters = new HashMap<>();
		parameters.put("cardId", cardId);
		list = executeNamedQuery("ListCardActivityById", parameters);
		return list;
	}

}
