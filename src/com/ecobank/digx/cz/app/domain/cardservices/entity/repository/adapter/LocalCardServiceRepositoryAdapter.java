package com.ecobank.digx.cz.app.domain.cardservices.entity.repository.adapter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.ecobank.digx.cz.app.cardservices.dto.CardLimitDTO;
import com.ecobank.digx.cz.app.cardservices.dto.CardLimitResponseDTO;
import com.ecobank.digx.cz.app.cardservices.dto.CardRequestFeeDTO;
import com.ecobank.digx.cz.app.cardservices.dto.CardRequestFeeListResponse;
import com.ecobank.digx.cz.app.domain.cardservices.entity.CardRequest;
import com.ecobank.digx.cz.app.domain.cardservices.entity.CardRequestKey;
import com.ofss.digx.framework.determinant.DeterminantResolver;
import com.ofss.digx.framework.domain.repository.adapter.AbstractLocalRepositoryAdapter;
import com.ofss.digx.infra.exceptions.Exception;

public class LocalCardServiceRepositoryAdapter extends AbstractLocalRepositoryAdapter<CardRequest>
implements ICardServiceRepositoryAdapter
{

	  private static LocalCardServiceRepositoryAdapter singletonInstance;
	  
	  public static LocalCardServiceRepositoryAdapter getInstance()
	  {
	    if (singletonInstance == null) {
	      synchronized (LocalCardServiceRepositoryAdapter.class)
	      {
	        if (singletonInstance == null) {
	          singletonInstance = new LocalCardServiceRepositoryAdapter();
	        }
	      }
	    }
	    return singletonInstance;
	  }
	  
	  
	        public void create(CardRequest object)
			    throws Exception
			  {
			    if (object.getRefKey() != null) {
			     /* object.getPassportNumber().setDeterminantValue(
			        DeterminantResolver.getInstance().fetchDeterminantValue(AccountTransfer.class.getName()));*/
			    }
			    super.insert(object);
			  }

	  public void update(CardRequest object) throws Exception {
		  System.out.println("Update Card Request:: " + object.getRefKey().getExternalRefNo());
          super.update(object);
          System.out.println("End-Update Card Request:: " + object.getRefKey().getExternalRefNo());
		
	  }

	@Override
	public CardRequest read(CardRequestKey key) throws Exception {
		key.setDeterminantValue(
				DeterminantResolver.getInstance().fetchDeterminantValue(CardRequest.class.getName()));
		return (CardRequest) get(CardRequest.class, (Serializable) key);
	}
	
	 public List<CardRequest> listCardRequestByCustomerId(String customerId, String cardCategory) throws Exception {
		 System.out.println("listCardRequestByCustomerId customerId  : " + customerId);
		 System.out.println("listCardRequestByCustomerId cardCategory: " + cardCategory);
		 
	    HashMap<String, Object> parameters = null;
	    List<CardRequest> list = null;
	    parameters = new HashMap<>();
	    
	    parameters.put("customerId", customerId);
	    if(cardCategory != null && !cardCategory.isEmpty()  ) {
	    	  parameters.put("cardCategory", cardCategory);
	    	  list = executeNamedQuery("ListCustomerCardRequestAndCardCategory", parameters);
	    } else {
	    	  list = executeNamedQuery("ListCustomerCardRequest", parameters);
	    }
	    
	    return list;
	  }


	@Override
	public void delete(CardRequest arg0) throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	
	public CardRequestFeeListResponse listCardRequestFees(String affCode , String cardType) throws Exception {
		CardRequestFeeListResponse response = new CardRequestFeeListResponse();
		response.setResponseCode("E99");
		List<CardRequestFeeDTO> list = new ArrayList<CardRequestFeeDTO>();
		
	    HashMap<String, Object> parameters = new HashMap<>();
	    parameters.put("affCode", affCode);
	    //parameters.put("determinantValue", 
	     //   DeterminantResolver.getInstance().fetchDeterminantValue(PayeeCountLimit.class.getName()));
	    List<Object[]> objectList = executeNamedQuery("fetchAllCardRequestFees", parameters);
	    if(objectList != null)
	    {
		    for (Object[] object : objectList) {
		      CardRequestFeeDTO fee = new CardRequestFeeDTO();
		      
		      fee.setCardCategory((String)object[0]);
		      fee.setCardType((String)object[1]);
		      fee.setCcyCode((String)object[2]);
		      fee.setFee((BigDecimal)object[3]);
		      fee.setDeliveryFee((BigDecimal)object[4]);
		      fee.setNote((String)object[5]);
		      fee.setSupplementaryCardfee((BigDecimal)object[6]);
		      fee.setReplacementCardfee((BigDecimal)object[7]);
		      fee.setMonthlyCardfee((BigDecimal)object[8]);
		      
		      list.add(fee);
		     
		    }
		    response.setFees(list);
		    response.setResponseCode("000");
	    }
	    return response;
	  }
	
	public CardRequestFeeDTO getCardRequestFeeByScheme(String affCode , String cardCategory, String cardType) throws Exception {
		
		CardRequestFeeDTO fee = null;
		
	    HashMap<String, Object> parameters = new HashMap<>();
	    parameters.put("affCode", affCode);
	    parameters.put("cardCategory", cardCategory);
	    parameters.put("cardType", cardType);
	    //parameters.put("determinantValue", 
	     //   DeterminantResolver.getInstance().fetchDeterminantValue(PayeeCountLimit.class.getName()));
	    List<Object[]> objectList = executeNamedQuery("fetchCardRequestFeeByScheme", parameters);
	    if(objectList != null)
	    {
		    for (Object[] object : objectList) {
		       fee = new CardRequestFeeDTO();
		      
		      fee.setCardCategory((String)object[0]);
		      fee.setCardType((String)object[1]);
		      fee.setCcyCode((String)object[2]);
		      fee.setFee((BigDecimal)object[3]);
		      fee.setDeliveryFee((BigDecimal)object[4]);
		      fee.setNote((String)object[5]);
		      fee.setSupplementaryCardfee((BigDecimal)object[6]);
		      fee.setReplacementCardfee((BigDecimal)object[7]);
		      fee.setMonthlyCardfee((BigDecimal)object[8]);
		      
		      fee.setIncomeAccount((String)object[9]);
		      fee.setIncomeAccountBranch((String)object[10]);
		      fee.setCardProduct((String)object[11]);
		      
		      fee.setCardProductDesc((String)object[12]);
		      fee.setCardFeeDescription((String)object[13]);
		      break;
		    }
		    
	    }
	    return fee;
	  }
	
	
	public CardLimitResponseDTO listCardLimits(String affCode , String cardType) throws Exception {
		CardLimitResponseDTO response = new CardLimitResponseDTO();
		response.setResponseCode("E99");
		List<CardLimitDTO> list = new ArrayList<CardLimitDTO>();
		
	    HashMap<String, Object> parameters = new HashMap<>();
	    parameters.put("affCode", affCode);
	    //parameters.put("determinantValue", 
	     //   DeterminantResolver.getInstance().fetchDeterminantValue(PayeeCountLimit.class.getName()));
	    List<Object[]> objectList = executeNamedQuery("fetchCardLimits", parameters);
	    if(objectList != null)
	    {
		    for (Object[] object : objectList) {
		    	CardLimitDTO cDto = new CardLimitDTO();
		      
		    	cDto.setSchemeType((String)object[0]);
		    	cDto.setCardType((String)object[1]);
		    	cDto.setCcy((String)object[2]);
		    	cDto.setOnlinePurchaseLimit((BigDecimal)object[3]);
		    	cDto.setAtmCashLimit((BigDecimal)object[4]);
		    	cDto.setPurchaseLimit((BigDecimal)object[5]);
		    	
		      list.add(cDto);
		     
		    }
		    response.setLimits(list);
		    response.setResponseCode("000");
	    }
	    return response;
	  }


	
			  
}


