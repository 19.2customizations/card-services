package com.ecobank.digx.cz.app.domain.cardservices.entity.repository.adapter;

import java.util.List;

import com.ecobank.digx.cz.app.domain.cardservices.entity.CardDetail;
import com.ecobank.digx.cz.app.domain.cardservices.entity.CardDetailKey;
import com.ecobank.digx.cz.app.domain.cardservices.entity.CardRequest;
import com.ofss.digx.framework.domain.repository.IRepositoryAdapter;
import com.ofss.digx.infra.exceptions.Exception;

public interface ICardDetailRepositoryAdapter extends IRepositoryAdapter<CardDetail, CardDetailKey> {
	
	public abstract void create(CardDetail cardDetail) throws Exception;
	
	public CardDetail read(CardDetailKey key) throws Exception;
	
	public List<CardDetail> listCardsByCustomerId(String customerId, String cardCategory) throws Exception ;
	
	public void update(CardDetail object) throws Exception ;
	public CardDetail getCardDetailByMaskedPANandExpiryDate(String customerId,String cardType, String maskedPAN, String expiryDate) throws Exception ;

}
