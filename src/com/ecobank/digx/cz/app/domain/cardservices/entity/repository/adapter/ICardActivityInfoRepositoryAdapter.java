package com.ecobank.digx.cz.app.domain.cardservices.entity.repository.adapter;

import java.util.List;

import com.ecobank.digx.cz.app.domain.cardservices.entity.CardActivityInfo;
import com.ecobank.digx.cz.app.domain.cardservices.entity.CardActivityInfoKey;
import com.ofss.digx.framework.domain.repository.IRepositoryAdapter;
import com.ofss.digx.infra.exceptions.Exception;

public interface ICardActivityInfoRepositoryAdapter extends IRepositoryAdapter<CardActivityInfo, CardActivityInfoKey> {
	
	public abstract void create(CardActivityInfo cardActivityInfo) throws Exception;
	
	public CardActivityInfo read(CardActivityInfoKey key) throws Exception;
	
	public List<CardActivityInfo> listCardActivityById(String cardId) throws Exception;
	
	public void update(CardActivityInfo object) throws Exception ;

}
