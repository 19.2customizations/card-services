package com.ecobank.digx.cz.app.domain.cardservices.entity.policy;

import com.ofss.digx.app.config.dto.workingwindow.WorkingWindowCheckResponse;
import com.ofss.fc.framework.domain.policy.IBusinessPolicyDTO;

public class CardServiceBusinessPolicyData implements IBusinessPolicyDTO {
	
	
	private WorkingWindowCheckResponse workingWindowCheckResponse;
	
	  private String senderAccountNo;
	  private String affiliateCode;
	  private String cardNo;
	  private String cardType;
	  private String cardCategory;
	 
	  private String responseCode;
	 
	  private String activityCode;
	  private String responseMessage;
	  
	  
	  
	public WorkingWindowCheckResponse getWorkingWindowCheckResponse() {
		return workingWindowCheckResponse;
	}
	public void setWorkingWindowCheckResponse(WorkingWindowCheckResponse workingWindowCheckResponse) {
		this.workingWindowCheckResponse = workingWindowCheckResponse;
	}
	public String getSenderAccountNo() {
		return senderAccountNo;
	}
	public void setSenderAccountNo(String senderAccountNo) {
		this.senderAccountNo = senderAccountNo;
	}
	public String getAffiliateCode() {
		return affiliateCode;
	}
	public void setAffiliateCode(String affiliateCode) {
		this.affiliateCode = affiliateCode;
	}
	public String getCardNo() {
		return cardNo;
	}
	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	public String getCardCategory() {
		return cardCategory;
	}
	public void setCardCategory(String cardCategory) {
		this.cardCategory = cardCategory;
	}
	public String getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	public String getActivityCode() {
		return activityCode;
	}
	public void setActivityCode(String activityCode) {
		this.activityCode = activityCode;
	}
	public String getResponseMessage() {
		return responseMessage;
	}
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}
	
	  
	  

}
