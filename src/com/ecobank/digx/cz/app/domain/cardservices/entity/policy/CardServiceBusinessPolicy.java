package com.ecobank.digx.cz.app.domain.cardservices.entity.policy;

import java.util.prefs.Preferences;

import com.ecobank.digx.cz.app.domain.accountinquiry.entity.TransferCurrencyAllow;
import com.ofss.digx.domain.payment.entity.policy.AbstractPaymentBusinessPolicy;
import com.ofss.fc.framework.domain.policy.IBusinessPolicyDTO;
import com.ofss.fc.infra.config.ConfigurationFactory;
import com.ofss.fc.infra.log.impl.MultiEntityLogger;
import com.ofss.fc.infra.validation.error.ValidationError;

public class CardServiceBusinessPolicy extends AbstractPaymentBusinessPolicy {
	  private static final String THIS_COMPONENT_NAME = CardServiceBusinessPolicy.class.getName();
	  
	  private static final MultiEntityLogger FORMATTER = MultiEntityLogger.getUniqueInstance();
	  
	  //private static final Logger logger = FORMATTER.getLogger(THIS_COMPONENT_NAME);
	  
	 // private Preferences digx_consulting = ConfigurationFactory.getInstance().getConfigurations("DIGXCONSULTING");
	  
	  private CardServiceBusinessPolicyData policyData;
	  
	  public CardServiceBusinessPolicy() {}
	  
	  public CardServiceBusinessPolicy(IBusinessPolicyDTO iBusinessPolicyDTO) {
	    if (iBusinessPolicyDTO instanceof CardServiceBusinessPolicyData) {
	      this.policyData = (CardServiceBusinessPolicyData)iBusinessPolicyDTO;
	      
	      
	    } 
	  }
	  
	  public void validatePolicy() {
		  
		  
		  //validateRegEx((PaymentBusinessPolicyData)this.paymentBusinessPolicyData, null);
	     // validatePayment((PaymentBusinessPolicyData)this.paymentBusinessPolicyData);
		  System.out.println("CardServiceBusinessPolicy222  : " + policyData.getResponseCode() );
		  
		  String rspCode = policyData.getResponseCode();
		 
		 
		  /*if(rspCode != null && !rspCode.equals("000"))
		  {
			 addValidationError(new ValidationError("CardServiceBusinessPolicy", "", null, "DIGX_CZ_" + rspCode, new String[] { "" }));
		     return;
		  }*/
		  
		  if(rspCode != null)
          {
			 if (!rspCode.equals("000"))
			 {
	
			    addValidationError(new ValidationError("CardServiceBusinessPolicy", "responseCode", (String) null, "DIGX_CZ_" + rspCode, new String[] { "" }));
	
			  return;
			 }

		  }
		 
		  
	  }

}
