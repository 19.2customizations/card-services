package com.ecobank.digx.cz.app.cardservices.assembler;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.ecobank.digx.cz.app.cardservices.dto.CardActivityDTO;
import com.ecobank.digx.cz.app.cardservices.dto.CardActivityListResponse;
import com.ecobank.digx.cz.app.cardservices.dto.CardDetailCreateRequestDTO;
import com.ecobank.digx.cz.app.cardservices.dto.CardDetailDTO;
import com.ecobank.digx.cz.app.cardservices.dto.CardListResponse;
import com.ecobank.digx.cz.app.cardservices.dto.CardServiceResponseDTO;
import com.ecobank.digx.cz.app.domain.cardservices.entity.CardActivityInfo;
import com.ecobank.digx.cz.app.domain.cardservices.entity.CardActivityInfoKey;
import com.ecobank.digx.cz.app.domain.cardservices.entity.CardDetail;
import com.ecobank.digx.cz.app.domain.cardservices.entity.CardDetailKey;
import com.ecobank.digx.cz.app.domain.cardservices.entity.CardRequest;
import com.ecobank.digx.cz.extxface.mule.adapter.client.JEncrypt;
import com.ecobank.digx.cz.extxface.mule.dto.ESBCardResponseDTO;
import com.ofss.fc.datatype.Date;
import com.ofss.fc.framework.domain.IAbstractDomainObject;
import com.ofss.fc.framework.domain.assembler.AbstractAssembler;
import com.ofss.fc.framework.domain.common.dto.DomainObjectDTO;
import com.ofss.fc.infra.exception.FatalException;
import com.ofss.fc.infra.log.impl.MultiEntityLogger;

public class CardDetailAssembler extends AbstractAssembler {
	private static final String THIS_COMPONENT_NAME = CardDetailAssembler.class.getName();
	private transient Logger logger = MultiEntityLogger.getUniqueInstance().getLogger(THIS_COMPONENT_NAME);

	public DomainObjectDTO fromDomainObject(IAbstractDomainObject arg0) throws FatalException {
		return null;
	}

	public IAbstractDomainObject toDomainObject(DomainObjectDTO arg0) throws FatalException {
		return null;
	}

	public CardDetail toCardDetailDomainObjectCreate(CardDetailCreateRequestDTO cardDetailCreateRequestDTO) {
		CardDetail cardDetailDomain = null;
		if (cardDetailCreateRequestDTO != null) {

			populateDataTransferObjectDTOMap("com.ecobank.digx.cz.app.cardservices.dto.CardDetailRequestDTO",
					cardDetailCreateRequestDTO);
			try {
				if (retrieveDataTransferObjectDTOMapElement(
						"com.ecobank.digx.cz.app.cardservices.dto.CardDetailRequestDTO").getDictionaryArray() == null) {
					cardDetailDomain = new CardDetail();
				} else {
					cardDetailDomain = (CardDetail) getCustomizedDomainObject(retrieveDataTransferObjectDTOMapElement(
							"com.ecobank.digx.cz.app.cardservices.dto.CardDetailRequestDTO"));
				}
			} catch (Exception e) {
				this.logger.log(Level.WARNING,
						"Customized Domain Object failed to be instantiated from com.ecobank.digx.cz.app.cardservices.dto.CardDetailRequestDTO",
						e);

				cardDetailDomain = new CardDetail();
			}

			cardDetailDomain = new CardDetail();
			CardDetailKey key = new CardDetailKey();

			cardDetailDomain.setCardAccount(cardDetailCreateRequestDTO.getCardDetailDTO().getCardAccount());
			cardDetailDomain.setCardCategory(cardDetailCreateRequestDTO.getCardDetailDTO().getCardType());
			cardDetailDomain.setCardCcy(cardDetailCreateRequestDTO.getCardDetailDTO().getCardCcy());
			cardDetailDomain.setCardCustomerId(cardDetailCreateRequestDTO.getCardDetailDTO().getCardCustomerId());
			// cardDetailDomain.setCardId(cardDetailCreateRequestDTO.getCardDetailDTO().getCardId());
			cardDetailDomain.setCardLimit(cardDetailCreateRequestDTO.getCardDetailDTO().getCardLimit());
			cardDetailDomain.setCardStatus(cardDetailCreateRequestDTO.getCardDetailDTO().getCardStatus());
			cardDetailDomain.setCardType(cardDetailCreateRequestDTO.getCardDetailDTO().getCardType());
			cardDetailDomain.setCreatedBy(cardDetailCreateRequestDTO.getCardDetailDTO().getCreatedBy());
			cardDetailDomain.setCreatedDate(cardDetailCreateRequestDTO.getCardDetailDTO().getCreatedDate());
			cardDetailDomain.setCustomerId(cardDetailCreateRequestDTO.getCardDetailDTO().getCustomerId());
			cardDetailDomain.setMobileNo(cardDetailCreateRequestDTO.getCardDetailDTO().getMobileNo());
			cardDetailDomain.setExpiryDate(cardDetailCreateRequestDTO.getCardDetailDTO().getExpiryDate());
			cardDetailDomain.setLinkAccounts(cardDetailCreateRequestDTO.getCardDetailDTO().getSourceAccount());
			cardDetailDomain.setMaskedPan(cardDetailCreateRequestDTO.getCardDetailDTO().getMaskedPan());

			cardDetailDomain.setModifiedBy(cardDetailCreateRequestDTO.getCardDetailDTO().getModifiedBy());
			cardDetailDomain.setModifiedDate(cardDetailCreateRequestDTO.getCardDetailDTO().getModifiedDate());
			cardDetailDomain.setNameOnCard(cardDetailCreateRequestDTO.getCardDetailDTO().getNameOnCard());
			// cardDetailDomain.setRefId(cardDetailCreateRequestDTO.getCardDetailDTO().getRefId());
			cardDetailDomain.setSchemeType(cardDetailCreateRequestDTO.getCardDetailDTO().getSchemeType());
			cardDetailDomain.setSourceAccount(cardDetailCreateRequestDTO.getCardDetailDTO().getSourceAccount());
			cardDetailDomain.setUserId(cardDetailCreateRequestDTO.getCardDetailDTO().getUserId());
			key.setCardId(cardDetailCreateRequestDTO.getCardDetailDTO().getCardId());
			cardDetailDomain.setRefKey(key);
		}
		return cardDetailDomain;
	}

	public CardDetailDTO fromCardDetailDomainToDTO(CardDetail domain) {
		CardDetailDTO dto = new CardDetailDTO();
		dto.setCardId(domain.getRefKey().getCardId());
		// dto.setRefId(domain.getRefId());
		dto.setMaskedPan(domain.getMaskedPan());
		dto.setExpiryDate(domain.getExpiryDate());
		dto.setNameOnCard(domain.getNameOnCard());
		dto.setSchemeType(domain.getSchemeType());
		dto.setSourceAccount(domain.getSourceAccount());
		dto.setCardCategory(domain.getCardCategory());
		dto.setCardType(domain.getCardType());
		dto.setCustomerId(domain.getCustomerId());
		dto.setCardStatus(domain.getCardStatus());

		return dto;
	}

	public CardListResponse fromCardDetailDomainToDTOList(List<CardDetail> list) {
		CardListResponse cards = new CardListResponse();
		List<CardDetailDTO> listC = new ArrayList<CardDetailDTO>();
		if (list != null && list.size() > 0) {
			for (CardDetail cardDetail : list) {
				listC.add(fromCardDetailDomainToDTO(cardDetail));
			}

			cards.setCards(listC);
		}
		else
		{
			//MOCK CARD /****** MUST REMOVE*******/
			/*CardDetailDTO cdto = new CardDetailDTO();
			cdto.setBalance(BigDecimal.ZERO);
			cdto.setNameOnCard("KWAME JOHN");
			cdto.setExpiryDate("10/22");
			cdto.setCardCategory("DEBIT");
			cdto.setCardId("0");
			cdto.setCardCcy("USD");
			cdto.setCardCustomerId("TEST");
			cdto.setCardStatus("Active");
			cdto.setCardType("DEBIT");
			cdto.setMaskedPan("506333****1111");
			cdto.setSchemeType("DEBIT");
			cdto.setUserId("");
			listC.add(cdto);
			cdto = new CardDetailDTO();
			cdto.setBalance(BigDecimal.ZERO);
			cdto.setNameOnCard("JOHN OBI");
			cdto.setExpiryDate("10/22");
			cdto.setCardCategory("CREDIT");
			cdto.setCardId("0");
			cdto.setCardCcy("USD");
			cdto.setCardCustomerId("TEST");
			cdto.setCardStatus("Active");
			cdto.setCardType("CREDIT");
			cdto.setMaskedPan("546333****1111");
			cdto.setSchemeType("CREDIT");
			cdto.setUserId("");
			listC.add(cdto);
			
			cdto = new CardDetailDTO();
			cdto.setBalance(BigDecimal.ZERO);
			cdto.setNameOnCard("KWAME INACTIVE");
			cdto.setExpiryDate("10/22");
			cdto.setCardCategory("DEBIT");
			cdto.setCardId("0");
			cdto.setCardCcy("USD");
			cdto.setCardCustomerId("TEST");
			cdto.setCardStatus("Inactive");
			cdto.setCardType("DEBIT");
			cdto.setMaskedPan("506333****1111");
			cdto.setSchemeType("DEBIT");
			cdto.setUserId("");
			listC.add(cdto);
			
			cdto = new CardDetailDTO();
			cdto.setBalance(BigDecimal.ZERO);
			cdto.setNameOnCard("JOHN INACTIVE");
			cdto.setExpiryDate("10/22");
			cdto.setCardCategory("CREDIT");
			cdto.setCardId("0");
			cdto.setCardCcy("USD");
			cdto.setCardCustomerId("TEST");
			cdto.setCardStatus("Inactive");
			cdto.setCardType("CREDIT");
			cdto.setMaskedPan("546333****1100");
			cdto.setSchemeType("CREDIT");
			cdto.setUserId("");
			listC.add(cdto);
			cards.setCards(listC);*/
			
		}

		return cards;
	}

	public CardActivityDTO fromCardActivityDomainToDTO(CardActivityInfo domain) {
		CardActivityDTO dto = new CardActivityDTO();
		dto.setCardId(domain.getCardId());
		dto.setRefId(domain.getRefKey().getRefId());
		dto.setActivityType(domain.getActivityType());
		dto.setActivityDate(domain.getActivityDate());
		dto.setActivityDescription(domain.getActivityDescription());
		dto.setResponseCode(domain.getResponseCode());
		dto.setResponseMessage(domain.getResponseMessage());
		dto.setStatus(domain.getStatus());
		return dto;
	}

	public CardActivityListResponse fromCardActivityDomainToDTOList(List<CardActivityInfo> list) {
		CardActivityListResponse response = new CardActivityListResponse();
		List<CardActivityDTO> listC = new ArrayList<CardActivityDTO>();
		if (list != null && list.size() > 0) {
			for (CardActivityInfo cardDetail : list) {
				listC.add(fromCardActivityDomainToDTO(cardDetail));
			}

			response.setActivities(listC);
		}

		return response;
	}

	public CardActivityInfo fromCardActivityDTOToDomainObject(CardActivityDTO cardActivityDTO, String userId,
			String affCode) {
		CardActivityInfoKey cardActivityInfoKey = new CardActivityInfoKey();

		CardActivityInfo cardActivityInfo = new CardActivityInfo();
		cardActivityInfo.setActivityDate(new com.ofss.fc.datatype.Date());
		cardActivityInfo.setActivityDescription(cardActivityDTO.getActivityDescription());
		cardActivityInfo.setActivityType(cardActivityDTO.getActivityType());
		cardActivityInfo.setStatus("SUCCESS");
		String refId = GetRefNumber("CD", 12);
		cardActivityInfo.setCardId(cardActivityDTO.getCardId());
		cardActivityInfo.setTranRefNo(refId);
		cardActivityInfo.setFee(new BigDecimal(cardActivityDTO.getFee()));
		cardActivityInfo.setAmount(cardActivityDTO.getAmount());

		cardActivityInfo.setCbaRspCode("PP");
		cardActivityInfo.setSourceAccount(cardActivityDTO.getSourceAccountNo());
		cardActivityInfo.setAffiliateCode(affCode);

		cardActivityInfoKey.setRefId(refId);
		// cardActivityInfo.setRefId(refId);
		cardActivityInfo.setRefKey(cardActivityInfoKey);
		cardActivityInfo.setResponseCode("PP");
		cardActivityInfo.setResponseMessage("");
		cardActivityInfo.setUserId(userId);
		
		cardActivityInfo.setCardCategory(cardActivityDTO.getCardType());
		cardActivityInfo.setCardAccount(cardActivityDTO.getCardAccount());
		cardActivityInfo.setMaskedPan(cardActivityDTO.getMaskedPan());
		String cdx = cardActivityDTO.getEncCardData();
				
				//JEncrypt
		System.out.println("card-data [REFKEY] " + cdx);
		if(cdx != null && !cdx.equals(""))
		{
			cardActivityInfo.setCardDat(JEncrypt.encrypt(cdx));
			
		}
		//cardActivityInfo.setCardDat(cardActivityDTO.getEncCardData() != null ? cardActivityDTO.getEncCardData() : "");
		cardActivityInfo.setUdf1(cardActivityDTO.getUdf1());
		cardActivityInfo.setUdf2(cardActivityDTO.getUdf2());
		cardActivityInfo.setUdf3(cardActivityDTO.getUdf3());
		
		cardActivityInfo.setEmail(cardActivityDTO.getEmail());
		cardActivityInfo.setMobileNo(cardActivityDTO.getMobileNo());
		cardActivityInfo.setCardCustomerId(cardActivityDTO.getCardCustomerId());
		cardActivityInfo.setName(cardActivityDTO.getNameOnCard());
		cardActivityInfo.setCardType(cardActivityDTO.getCardType());
		cardActivityInfo.setBranchCode("");
		cardActivityInfo.setCcy(cardActivityDTO.getCcyCode());

		// cardActivityInfo.create(cardActivityInfo);
		System.out.println("GetExternalRefNo [REFKEY]  ::: " + cardActivityInfo.getRefKey().getRefId());

		return cardActivityInfo;

	}
	
	public CardActivityInfo toCardActivityFromCardRequest(CardRequest cardRequest2, 
			String userId, CardServiceResponseDTO serviceResponseDTO,
			CardActivityInfo cardActivityInfo,String rspCode, String rspMsg)
		{
		CardActivityInfoKey cardActivityInfoKey = new CardActivityInfoKey();

		
		cardActivityInfo.setActivityDate(new com.ofss.fc.datatype.Date());
		cardActivityInfo.setActivityDescription("TOPUP_CARD");
		cardActivityInfo.setActivityType("TOPUP_CARD");
		cardActivityInfo.setStatus("SUCCESS");
		String refId = GetRefNumber("CD", 12);
		//cardActivityInfo.setCardId("CA" +);
		cardActivityInfo.setTranRefNo(refId);
		
		cardActivityInfo.setCbaRspCode(rspCode);
		cardActivityInfo.setCbaRspMsg(rspMsg);
		

		cardActivityInfoKey.setRefId(refId);
		// cardActivityInfo.setRefId(refId);
		cardActivityInfo.setRefKey(cardActivityInfoKey);
		
		cardActivityInfo.setUserId(userId);
		
		cardActivityInfo.setCardCategory(cardRequest2.getTypeOfCard());
		
		cardActivityInfo.setMaskedPan(serviceResponseDTO.getMaskedPan());
		String cData = serviceResponseDTO.getCardData();
		cardActivityInfo.setCardDat(cData);
		cardActivityInfo.setUdf1(serviceResponseDTO.getPseudoPAN());
		cardActivityInfo.setUdf2(serviceResponseDTO.getExpiryDate());
		cardActivityInfo.setUdf3("");
		
		cardActivityInfo.setEmail(cardRequest2.getEmail());
		cardActivityInfo.setCardCustomerId(cardRequest2.getCustomerNo());
		cardActivityInfo.setName(serviceResponseDTO.getNameOnCard());
		cardActivityInfo.setCardType(cardRequest2.getCardType());
		cardActivityInfo.setBranchCode("");
		

		// cardActivityInfo.create(cardActivityInfo);
		System.out.println("GetExternalRefNo [REFKEY]  ::: " + cardActivityInfo.getRefKey().getRefId());

		return cardActivityInfo;

	}

	public CardDetailDTO fromCardRequestDTOToDomainCardDetail(CardRequest cardRequest2,
			ESBCardResponseDTO esbCardResponseDTO, String cardCustomerId, String maskedPan) {
		// CardDetailCreateRequestDTO cardDetailCreateRequestDTO = new
		// CardDetailCreateRequestDTO();
		CardDetailDTO cardDetailDTO = new CardDetailDTO();
		cardDetailDTO.setCardAccount(cardRequest2.getCardAccountNo());
		cardDetailDTO.setCardCategory(cardRequest2.getCardType());
		cardDetailDTO.setCardCcy(cardRequest2.getCcy());
		cardDetailDTO.setCardCustomerId(cardCustomerId);
		String cardIdGen = GetRefNumber("CRD", 12) + maskedPan.substring(maskedPan.length() - 4);
		cardDetailDTO.setCardId(cardIdGen);
		cardDetailDTO.setCardLimit(0);
		cardDetailDTO.setCardStatus(cardRequest2.getStatus());
		cardDetailDTO.setCardType(cardRequest2.getCardType());
		cardDetailDTO.setCreatedBy(cardRequest2.getUserId());
		cardDetailDTO.setCreatedDate(new com.ofss.fc.datatype.Date());
		cardDetailDTO.setCustomerId(cardRequest2.getCustomerNo());
		cardDetailDTO.setMobileNo(cardRequest2.getMobileNo());
		cardDetailDTO.setExpiryDate(esbCardResponseDTO.getExpiryDate());
		cardDetailDTO.setLinkAccounts(cardRequest2.getSourceAccountNo());
		cardDetailDTO.setMaskedPan(maskedPan);
		cardDetailDTO.setModifiedBy(cardRequest2.getUserId());
		cardDetailDTO.setModifiedDate(new com.ofss.fc.datatype.Date());
		cardDetailDTO.setNameOnCard(esbCardResponseDTO.getName());
		cardDetailDTO.setRefId(cardRequest2.getRefKey().getExternalRefNo());
		cardDetailDTO.setSchemeType(cardRequest2.getSchemeType());
		cardDetailDTO.setSourceAccount(cardRequest2.getSourceAccountNo());
		cardDetailDTO.setUserId(cardRequest2.getUserId());

		return cardDetailDTO;

	}
	

	public CardDetail toCardDetailDomainObject(
			String cardType, String cardCategory, String maskedPAN, String accountNo,
			String customerId, String expiryDate, String nameOnCard,String status,
			String ccy,String username,String fullPan, String cardCustomerId, String cardAccount, String cardSchemeType) {
		    CardDetail cardDetailDomain = null;
		

			cardDetailDomain = new CardDetail();
			CardDetailKey key = new CardDetailKey();
			
			System.out.println("maskedPAN 222: "+ maskedPAN + " " + customerId);
			if(maskedPAN != null && !maskedPAN.equals(""))
			{
				String refx = maskedPAN.replace("*", "").trim() + this.GetRefNumber("CRD", 20);
				key.setCardId(refx);
			}
			else
			{
				maskedPAN = customerId;
				String refx = customerId + this.GetRefNumber("CRD", 20);
				key.setCardId(refx);
			}

			cardDetailDomain.setCardAccount(cardAccount);
			cardDetailDomain.setCardCategory(cardCategory);
			cardDetailDomain.setCardCcy(ccy);
			cardDetailDomain.setCardCustomerId(cardCustomerId);
			
			cardDetailDomain.setCardLimit(0);
			cardDetailDomain.setCardStatus(status);
			cardDetailDomain.setCardType(cardSchemeType);
			cardDetailDomain.setCreatedBy(username);
			cardDetailDomain.setCreatedDate(new Date());
			cardDetailDomain.setCustomerId(customerId);
			cardDetailDomain.setMobileNo("");
			cardDetailDomain.setExpiryDate(expiryDate);
			cardDetailDomain.setLinkAccounts(accountNo);
			cardDetailDomain.setMaskedPan(maskedPAN);
			if(fullPan != null && !fullPan.equals(""))
				cardDetailDomain.setFullPan(JEncrypt.encrypt(fullPan));
			cardDetailDomain.setModifiedBy(username);
			cardDetailDomain.setModifiedDate(new Date());
			cardDetailDomain.setNameOnCard(nameOnCard);
			// cardDetailDomain.setRefId(cardDetailCreateRequestDTO.getCardDetailDTO().getRefId());
			cardDetailDomain.setSchemeType(cardType);
			
			cardDetailDomain.setSourceAccount(accountNo);
			cardDetailDomain.setUserId(username);
			
			cardDetailDomain.setRefKey(key);
		
		return cardDetailDomain;
	}

	public String GetRefNumber(String type, int len) {

		String finalString = "";
		int x = 0;
		char[] stringChars = new char[len];
		for (int i = 0; i < len; i++) // 4
		{
			Random random = new Random();
			x = random.nextInt(9);

			stringChars[i] = Integer.toString(x).toCharArray()[0];
		}

		finalString = new String(stringChars);
		finalString = type + finalString;
		return finalString.trim();
	}

	public String maskString(String strText, int start, int end, char maskChar) throws Exception {

		if (strText == null || strText.equals(""))
			return "";

		if (start < 0)
			start = 0;

		if (end > strText.length())
			end = strText.length();

		if (start > end)
			throw new Exception("End index cannot be greater than start index");

		int maskLength = end - start;

		if (maskLength == 0)
			return strText;

		StringBuilder sbMaskString = new StringBuilder(maskLength);

		for (int i = 0; i < maskLength; i++) {
			sbMaskString.append(maskChar);
		}

		return strText.substring(0, start) + sbMaskString.toString() + strText.substring(start + maskLength);
	}

}