package com.ecobank.digx.cz.app.cardservices.assembler;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.ecobank.digx.cz.app.cardservices.dto.CardActivityListResponse;
import com.ecobank.digx.cz.app.cardservices.dto.CardRequestListResponse;
import com.ecobank.digx.cz.app.cardservices.dto.CardServiceCreateRequestDTO;
import com.ecobank.digx.cz.app.cardservices.dto.CardServiceDTO;
import com.ecobank.digx.cz.app.domain.cardservices.entity.CardActivityInfo;
import com.ecobank.digx.cz.app.domain.cardservices.entity.CardRequest;
import com.ecobank.digx.cz.app.domain.cardservices.entity.CardRequestKey;
import com.ofss.fc.framework.domain.IAbstractDomainObject;
import com.ofss.fc.framework.domain.assembler.AbstractAssembler;
import com.ofss.fc.framework.domain.common.dto.DomainObjectDTO;
import com.ofss.fc.infra.exception.FatalException;
import com.ofss.fc.infra.log.impl.MultiEntityLogger;

public class CardServiceAssembler extends AbstractAssembler {
	private static final String THIS_COMPONENT_NAME = CardServiceAssembler.class.getName();
	private transient Logger logger = MultiEntityLogger.getUniqueInstance().getLogger(THIS_COMPONENT_NAME);

	public DomainObjectDTO fromDomainObject(IAbstractDomainObject arg0) throws FatalException {
		return null;
	}

	public IAbstractDomainObject toDomainObject(DomainObjectDTO arg0) throws FatalException {
		return null;
	}

	public CardRequest toCardRequestDomainObjectCreate(CardServiceCreateRequestDTO requestDTO)
			throws com.ofss.digx.infra.exceptions.Exception {
		CardRequest cardServiceDomain = null;
		if (requestDTO != null) {
			populateDataTransferObjectDTOMap("com.ecobank.digx.cz.app.cardservices.dto.CardServiceRequestDTO",
					requestDTO);
			try {
				if (retrieveDataTransferObjectDTOMapElement(
						"com.ecobank.digx.cz.app.cardservices.dto.CardServiceRequestDTO")
								.getDictionaryArray() == null) {
					cardServiceDomain = new CardRequest();
				} else {
					cardServiceDomain = (CardRequest) getCustomizedDomainObject(
							retrieveDataTransferObjectDTOMapElement(
									"com.ecobank.digx.cz.app.cardservices.dto.CardServiceRequestDTO"));
				}
			} catch (Exception e) {
				this.logger.log(Level.WARNING,
						"Customized Domain Object failed to be instantiated from com.ecobank.digx.cz.app.cardservices.dto.CardServiceRequestDTO",
						e);

				cardServiceDomain = new CardRequest();
			}
			
			System.out.println("nameOnCard ::: " + requestDTO.getCardServiceDetails().getNameOnCard());
			System.out.println("customerNo ::: " + requestDTO.getCardServiceDetails().getCustomerNo());
			CardRequestKey key = new CardRequestKey();
			
			cardServiceDomain.setAffiliateCode(requestDTO.getCardServiceDetails().getAffiliateCode());
			cardServiceDomain.setAmount(requestDTO.getCardServiceDetails().getAmount());
			cardServiceDomain.setCardAccountNo(requestDTO.getCardServiceDetails().getCardAccountNo());
			cardServiceDomain.setCardType(requestDTO.getCardServiceDetails().getCardType());
			cardServiceDomain.setCbaReferenceNo(requestDTO.getCardServiceDetails().getCbaReferenceNo());
			cardServiceDomain.setCcy(requestDTO.getCardServiceDetails().getCcy());
			String email = requestDTO.getCardServiceDetails().getEmail();
			if(email == null)
				email ="";
			cardServiceDomain.setEmail(email);
			cardServiceDomain.setUserId(requestDTO.getCardServiceDetails().getUserId());
			cardServiceDomain.setExpiryDate(requestDTO.getCardServiceDetails().getExpiryDate());
			cardServiceDomain.setMaskedCardNo(requestDTO.getCardServiceDetails().getMaskedCardNo());
			cardServiceDomain.setMobileNo(requestDTO.getCardServiceDetails().getMobileNo());
			cardServiceDomain.setNameOnCard(requestDTO.getCardServiceDetails().getNameOnCard());
			cardServiceDomain.setNote(requestDTO.getCardServiceDetails().getNote());
			cardServiceDomain.setPostedDate(requestDTO.getCardServiceDetails().getPostedDate());
			cardServiceDomain.setRequestDate(requestDTO.getCardServiceDetails().getRequestDate());
			cardServiceDomain.setResponseCode("PP");
			cardServiceDomain.setResponseMessage("");
			cardServiceDomain.setSchemeType(requestDTO.getCardServiceDetails().getSchemeType());
			cardServiceDomain.setSourceAccountNo(requestDTO.getCardServiceDetails().getSourceAccountNo());
			cardServiceDomain.setStatus(requestDTO.getCardServiceDetails().getStatus());
			cardServiceDomain.setTypeOfCard(requestDTO.getCardServiceDetails().getTypeOfCard());
			cardServiceDomain.setCity(requestDTO.getCardServiceDetails().getCity());
			
			cardServiceDomain.setAddress(requestDTO.getCardServiceDetails().getAddress());
			cardServiceDomain.setDeliveryLocationType(requestDTO.getCardServiceDetails().getDeliveryLocationType());
			cardServiceDomain.setBranchCode(requestDTO.getCardServiceDetails().getBranchCode());
			cardServiceDomain.setFee(requestDTO.getCardServiceDetails().getFee());
			cardServiceDomain.setCardDetail("");
			key.setExternalRefNo(GetRefNumber("DIG",12));
			cardServiceDomain.setRefKey(key);
			cardServiceDomain.setCustomerNo(requestDTO.getCardServiceDetails().getCustomerNo());
		}
		return cardServiceDomain;
	}
	
	public CardServiceDTO fromCardRequestDomainToDTO(CardRequest domain)
	{
		CardServiceDTO dto = new CardServiceDTO();
		dto.setAddress(domain.getAddress());
		dto.setAmount(domain.getAmount());
		dto.setCardType(domain.getCardType());
		dto.setTypeOfCard(domain.getTypeOfCard());
		dto.setCity(domain.getCity());
		dto.setDeliveryLocationType(domain.getDeliveryLocationType());
		dto.setEmail(domain.getEmail());
		dto.setMaskedCardNo(domain.getMaskedCardNo());
		dto.setMobileNo(domain.getMobileNo());
		dto.setExternalRefNo(domain.getRefKey().getExternalRefNo());
		dto.setRequestDate(domain.getRequestDate());
		dto.setNameOnCard(domain.getNameOnCard());
		dto.setNote(domain.getNote());
        dto.setSchemeType(domain.getSchemeType());
        dto.setSourceAccountNo(domain.getSourceAccountNo());
        dto.setStatus(domain.getStatus());
		dto.setResponseCode(domain.getResponseCode());
		dto.setResponseMessage(domain.getResponseMessage());
		dto.setStatus(domain.getStatus());
		return dto;
	}
	
	public CardServiceDTO fromCardActivityAndDetailDomainToFundingDTO(CardRequest domain)
	{
		CardServiceDTO dto = new CardServiceDTO();
		dto.setAddress(domain.getAddress());
		dto.setAmount(domain.getAmount());
		dto.setCardType(domain.getCardType());
		dto.setTypeOfCard(domain.getTypeOfCard());
		dto.setCity(domain.getCity());
		dto.setDeliveryLocationType(domain.getDeliveryLocationType());
		dto.setEmail(domain.getEmail());
		dto.setMaskedCardNo(domain.getMaskedCardNo());
		dto.setMobileNo(domain.getMobileNo());
		dto.setExternalRefNo(domain.getRefKey().getExternalRefNo());
		dto.setRequestDate(domain.getRequestDate());
		dto.setNameOnCard(domain.getNameOnCard());
		dto.setNote(domain.getNote());
        dto.setSchemeType(domain.getSchemeType());
        dto.setSourceAccountNo(domain.getSourceAccountNo());
        dto.setStatus(domain.getStatus());
		dto.setResponseCode(domain.getResponseCode());
		dto.setResponseMessage(domain.getResponseMessage());
		dto.setStatus(domain.getStatus());
		return dto;
	}
	
	public CardRequestListResponse fromCardRequestDomainToDTOList(List<CardRequest> list)
	{
		
		System.out.println("checking size 1: " + list.size());
		
		CardRequestListResponse response = new CardRequestListResponse();
		List<CardServiceDTO> listC = new ArrayList<CardServiceDTO>();
		if(list != null && list.size() > 0)
		{
			for(CardRequest cardDetail : list)
			{
				listC.add(fromCardRequestDomainToDTO(cardDetail));
			}
			
			response.setRequests(listC);
		}
		
		//System.out.println("checking size 2: " + response.getRequests().size());
		return response;
	}
	
	
	public  String GetRefNumber(String type, int len) {

        String finalString = "";
        int x = 0;
        char[] stringChars = new char[len];
        for (int i = 0; i < len; i++) //4
        {
            Random random = new Random();
            x = random.nextInt(9);

            stringChars[i] = Integer.toString(x).toCharArray()[0];
        }


        finalString = new String(stringChars);
        finalString = type + finalString;
        return finalString.trim();
    }

}