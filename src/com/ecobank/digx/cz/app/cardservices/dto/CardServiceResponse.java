package com.ecobank.digx.cz.app.cardservices.dto;

import com.ofss.digx.service.response.BaseResponseObject;

public class CardServiceResponse extends BaseResponseObject {
	private static final long serialVersionUID = 2695633089263589147L;
	private String externalReferenceId;
	
	private String responseCode;
	private String responseMessage;

	public String getExternalReferenceId() {
		return externalReferenceId;
	}

	public void setExternalReferenceId(String externalReferenceId) {
		this.externalReferenceId = externalReferenceId;
	}

	public String toString() {
		return "CardServiceResponse [external Reference Id=" + externalReferenceId + " ]";
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}
}