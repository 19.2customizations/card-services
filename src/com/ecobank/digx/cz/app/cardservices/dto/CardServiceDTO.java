package com.ecobank.digx.cz.app.cardservices.dto;

import com.ofss.digx.app.common.dto.DataTransferObject;
import com.ofss.digx.datatype.complex.Account;
import com.ofss.fc.datatype.Date;
import com.ofss.fc.infra.log.impl.MultiEntityLogger;

import io.swagger.v3.oas.annotations.media.Schema;

public class CardServiceDTO extends DataTransferObject {
	
	private static final long serialVersionUID = 1L;
	//private static final String THIS_COMPONENT_NAME = CardServiceDTO.class.getName();
	//private transient MultiEntityLogger formatter;

	public CardServiceDTO() {
		//formatter = MultiEntityLogger.getUniqueInstance();
	}
	
	  private String cardType;
	  private String affiliateCode;
	  private String schemeType;
	  @Schema(description = "Amount must be greater than zero. ", type = "Double", required = true)
	  private double amount;
	  private String typeOfCard;
	  private String last4Digits;
	  private String customerNo;
	  private String mobileNo;
	  private String userId;
	  private String email;
	  private String note;
	  private String nameOnCard;
	  private String deliveryLocationType;
	  private String city;
	  private String address;
	  private String branchCode;
	  
	  private String expiryDate;
	  private String status;
	  private String maskedCardNo;
	  private String cardAccountNo;
	  @Schema(description = "Source Account", type = "String", required = true)
	  private String sourceAccountNo;
	  @Schema(description = "Source Account Id", type = "Account", required = true)
	  private Account sourceAccountId;
	  private String responseCode;
	  private String responseMessage;
	  private String ccy;
	  private Date requestDate;
	  private Date postedDate;
	  private String cbaReferenceNo;
	  private String externalRefNo;
	  
	  private double fee;
	  private double deliveryFee;
	  

	/*public MultiEntityLogger getFormatter() {
		return formatter;
	}
	public void setFormatter(MultiEntityLogger formatter) {
		this.formatter = formatter;
	}*/
	  
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	public String getAffiliateCode() {
		return affiliateCode;
	}
	public void setAffiliateCode(String affiliateCode) {
		this.affiliateCode = affiliateCode;
	}
	public String getSchemeType() {
		return schemeType;
	}
	public void setSchemeType(String schemeType) {
		this.schemeType = schemeType;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getTypeOfCard() {
		return typeOfCard;
	}
	public void setTypeOfCard(String typeOfCard) {
		this.typeOfCard = typeOfCard;
	}
	public String getCustomerNo() {
		return customerNo;
	}
	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getNameOnCard() {
		return nameOnCard;
	}
	public void setNameOnCard(String nameOnCard) {
		this.nameOnCard = nameOnCard;
	}
	public String getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMaskedCardNo() {
		return maskedCardNo;
	}
	public void setMaskedCardNo(String maskedCardNo) {
		this.maskedCardNo = maskedCardNo;
	}
	public String getCardAccountNo() {
		return cardAccountNo;
	}
	public void setCardAccountNo(String cardAccountNo) {
		this.cardAccountNo = cardAccountNo;
	}
	public String getSourceAccountNo() {
		return sourceAccountNo;
	}
	public void setSourceAccountNo(String sourceAccountNo) {
		this.sourceAccountNo = sourceAccountNo;
	}
	public String getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	public String getResponseMessage() {
		return responseMessage;
	}
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	public Date getRequestDate() {
		return requestDate;
	}
	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}
	public Date getPostedDate() {
		return postedDate;
	}
	public void setPostedDate(Date postedDate) {
		this.postedDate = postedDate;
	}
	public String getCbaReferenceNo() {
		return cbaReferenceNo;
	}
	public void setCbaReferenceNo(String cbaReferenceNo) {
		this.cbaReferenceNo = cbaReferenceNo;
	}
	public String getExternalRefNo() {
		return externalRefNo;
	}
	public void setExternalRefNo(String externalRefNo) {
		this.externalRefNo = externalRefNo;
	}
	public String getLast4Digits() {
		return last4Digits;
	}
	public void setLast4Digits(String last4Digits) {
		this.last4Digits = last4Digits;
	}
	public Account getSourceAccountId() {
		return sourceAccountId;
	}
	public void setSourceAccountId(Account sourceAccountId) {
		this.sourceAccountId = sourceAccountId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getDeliveryLocationType() {
		return deliveryLocationType;
	}
	public void setDeliveryLocationType(String deliveryLocationType) {
		this.deliveryLocationType = deliveryLocationType;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public double getFee() {
		return fee;
	}
	public void setFee(double fee) {
		this.fee = fee;
	}
	public double getDeliveryFee() {
		return deliveryFee;
	}
	public void setDeliveryFee(double deliveryFee) {
		this.deliveryFee = deliveryFee;
	}	
	
}
