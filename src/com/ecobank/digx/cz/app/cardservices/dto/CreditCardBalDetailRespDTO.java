package com.ecobank.digx.cz.app.cardservices.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.ofss.digx.service.response.BaseResponseObject;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "responseCode", "responseMessage", "accountNo", "availableAmount", "balance", "bankC", "cardAcct", "crd", "minPayable", "otb" })
public class CreditCardBalDetailRespDTO  extends BaseResponseObject {

	
	private static final long serialVersionUID = 1L;
	@JsonProperty("responseCode")
	private String responseCode;
	@JsonProperty("responseMessage")
	private String responseMessage;
	@JsonProperty("accountNo")
	private String accountNo;
	@JsonProperty("availableAmount")
	private Integer availableAmount;
	@JsonProperty("balance")
	private Integer balance;
	@JsonProperty("bankC")
	private String bankC;
	@JsonProperty("cardAcct")
	private String cardAcct;
	@JsonProperty("crd")
	private Integer crd;
	@JsonProperty("minPayable")
	private Integer minPayable;
	@JsonProperty("otb")
	private String otb;
	
	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	@JsonProperty("accountNo")
	public String getAccountNo() {
		return accountNo;
	}

	@JsonProperty("accountNo")
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	@JsonProperty("availableAmount")
	public Integer getAvailableAmount() {
		return availableAmount;
	}

	@JsonProperty("availableAmount")
	public void setAvailableAmount(Integer availableAmount) {
		this.availableAmount = availableAmount;
	}

	@JsonProperty("balance")
	public Integer getBalance() {
		return balance;
	}

	@JsonProperty("balance")
	public void setBalance(Integer balance) {
		this.balance = balance;
	}

	@JsonProperty("bankC")
	public String getBankC() {
		return bankC;
	}

	@JsonProperty("bankC")
	public void setBankC(String bankC) {
		this.bankC = bankC;
	}

	@JsonProperty("cardAcct")
	public String getCardAcct() {
		return cardAcct;
	}

	@JsonProperty("cardAcct")
	public void setCardAcct(String cardAcct) {
		this.cardAcct = cardAcct;
	}

	@JsonProperty("crd")
	public Integer getCrd() {
		return crd;
	}

	@JsonProperty("crd")
	public void setCrd(Integer crd) {
		this.crd = crd;
	}

	@JsonProperty("minPayable")
	public Integer getMinPayable() {
		return minPayable;
	}

	@JsonProperty("minPayable")
	public void setMinPayable(Integer minPayable) {
		this.minPayable = minPayable;
	}

	@JsonProperty("otb")
	public String getOtb() {
		return otb;
	}

	@JsonProperty("otb")
	public void setOtb(String otb) {
		this.otb = otb;
	}

}