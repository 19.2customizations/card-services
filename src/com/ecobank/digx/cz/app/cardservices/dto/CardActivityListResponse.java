package com.ecobank.digx.cz.app.cardservices.dto;

import java.util.List;

import com.ofss.digx.service.response.BaseResponseObject;

public class CardActivityListResponse extends BaseResponseObject{
	
	private static final long serialVersionUID = 1L;
	 
	
	
	
	
	private List<CardActivityDTO> activities;


	public List<CardActivityDTO> getActivities() {
		return activities;
	}


	public void setActivities(List<CardActivityDTO> activities) {
		this.activities = activities;
	}
	
	
	
	
}
