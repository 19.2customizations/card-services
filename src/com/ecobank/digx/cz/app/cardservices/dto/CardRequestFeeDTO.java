package com.ecobank.digx.cz.app.cardservices.dto;

import java.math.BigDecimal;

import com.ofss.digx.app.common.dto.DataTransferObject;
import com.ofss.digx.datatype.complex.Account;
import com.ofss.fc.datatype.Date;
import com.ofss.fc.infra.log.impl.MultiEntityLogger;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * @author ojotaiwoakeem
 *
 */
public class CardRequestFeeDTO extends DataTransferObject {

	private static final long serialVersionUID = 1L;
	private static final String THIS_COMPONENT_NAME = CardRequestFeeDTO.class.getName();
	private transient MultiEntityLogger formatter;

	public CardRequestFeeDTO() {
		formatter = MultiEntityLogger.getUniqueInstance();
	}

	
	private String cardCategory;
	private String cardType;
	private String ccyCode;
	private BigDecimal fee;
	private BigDecimal deliveryFee;
	private String note;
	
	private BigDecimal supplementaryCardfee;
	private BigDecimal replacementCardfee;
	
	private BigDecimal monthlyCardfee;
	
	private String incomeAccount;
	private String incomeAccountBranch;
	
	private String cardProduct;
	private String cardProductDesc;
	private String cardFeeDescription;

	public String getCardCategory() {
		return cardCategory;
	}
	public void setCardCategory(String cardCategory) {
		this.cardCategory = cardCategory;
	}
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	public String getCcyCode() {
		return ccyCode;
	}
	public void setCcyCode(String ccyCode) {
		this.ccyCode = ccyCode;
	}
	public BigDecimal getFee() {
		return fee;
	}
	public void setFee(BigDecimal fee) {
		this.fee = fee;
	}
	public BigDecimal getDeliveryFee() {
		return deliveryFee;
	}
	public void setDeliveryFee(BigDecimal deliveryFee) {
		this.deliveryFee = deliveryFee;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public BigDecimal getSupplementaryCardfee() {
		return supplementaryCardfee;
	}
	public void setSupplementaryCardfee(BigDecimal supplementaryCardfee) {
		this.supplementaryCardfee = supplementaryCardfee;
	}
	public BigDecimal getReplacementCardfee() {
		return replacementCardfee;
	}
	public void setReplacementCardfee(BigDecimal replacementCardfee) {
		this.replacementCardfee = replacementCardfee;
	}
	public BigDecimal getMonthlyCardfee() {
		return monthlyCardfee;
	}
	public void setMonthlyCardfee(BigDecimal monthlyCardfee) {
		this.monthlyCardfee = monthlyCardfee;
	}
	public String getIncomeAccount() {
		return incomeAccount;
	}
	public void setIncomeAccount(String incomeAccount) {
		this.incomeAccount = incomeAccount;
	}
	public String getIncomeAccountBranch() {
		return incomeAccountBranch;
	}
	public void setIncomeAccountBranch(String incomeAccountBranch) {
		this.incomeAccountBranch = incomeAccountBranch;
	}
	public String getCardProduct() {
		return cardProduct;
	}
	public void setCardProduct(String cardProduct) {
		this.cardProduct = cardProduct;
	}
	public String getCardProductDesc() {
		return cardProductDesc;
	}
	public void setCardProductDesc(String cardProductDesc) {
		this.cardProductDesc = cardProductDesc;
	}
	public String getCardFeeDescription() {
		return cardFeeDescription;
	}
	public void setCardFeeDescription(String cardFeeDescription) {
		this.cardFeeDescription = cardFeeDescription;
	}
	
	
	
	
	
}