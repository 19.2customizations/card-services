package com.ecobank.digx.cz.app.cardservices.dto;

import java.util.List;

import com.ofss.digx.service.response.BaseResponseObject;

public class FetchCardLimitListResponse extends BaseResponseObject{
	
	private static final long serialVersionUID = 1L;
	
	private String responseCode;
	private String responseMessage;
	 
	
	private List<CardLimitResponseDTO> limits;


	


	public List<CardLimitResponseDTO> getLimits() {
		return limits;
	}


	public void setLimits(List<CardLimitResponseDTO> limits) {
		this.limits = limits;
	}


	public String getResponseCode() {
		return responseCode;
	}


	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}


	public String getResponseMessage() {
		return responseMessage;
	}


	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	

	
	
	
	
}
