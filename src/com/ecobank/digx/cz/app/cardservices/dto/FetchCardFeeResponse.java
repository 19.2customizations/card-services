package com.ecobank.digx.cz.app.cardservices.dto;

import java.util.List;

import com.ofss.digx.service.response.BaseResponseObject;

public class FetchCardFeeResponse extends BaseResponseObject{
	
	private static final long serialVersionUID = 1L;
	
	private String responseCode;
	private String responseMessage;
	private String fee;
	private String cardProduct;
	private String cardProductDesc;
	private String currencyCode;
	private String deliveryFee;
	private String monthlyCardFee;
	 
	

	public String getResponseCode() {
		return responseCode;
	}


	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}


	public String getResponseMessage() {
		return responseMessage;
	}


	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}


	public String getFee() {
		return fee;
	}


	public void setFee(String fee) {
		this.fee = fee;
	}


	public String getCardProduct() {
		return cardProduct;
	}


	public void setCardProduct(String cardProduct) {
		this.cardProduct = cardProduct;
	}


	public String getCardProductDesc() {
		return cardProductDesc;
	}


	public void setCardProductDesc(String cardProductDesc) {
		this.cardProductDesc = cardProductDesc;
	}


	public String getCurrencyCode() {
		return currencyCode;
	}


	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}


	public String getDeliveryFee() {
		return deliveryFee;
	}


	public void setDeliveryFee(String deliveryFee) {
		this.deliveryFee = deliveryFee;
	}


	public String getMonthlyCardFee() {
		return monthlyCardFee;
	}


	public void setMonthlyCardFee(String monthlyCardFee) {
		this.monthlyCardFee = monthlyCardFee;
	}

	

	
	
	
	
}
