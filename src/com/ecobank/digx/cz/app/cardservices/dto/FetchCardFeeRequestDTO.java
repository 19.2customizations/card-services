package com.ecobank.digx.cz.app.cardservices.dto;

import com.ofss.digx.app.common.dto.DataTransferObject;
import com.ofss.digx.datatype.complex.Account;
import com.ofss.fc.infra.log.impl.MultiEntityLogger;

import io.swagger.v3.oas.annotations.media.Schema;

public class FetchCardFeeRequestDTO extends DataTransferObject {
	
	private static final long serialVersionUID = 1L;
	private static final String THIS_COMPONENT_NAME = CardActivityRequestDTO.class.getName();
	private transient MultiEntityLogger formatter;

	public FetchCardFeeRequestDTO() {
		formatter = MultiEntityLogger.getUniqueInstance();
	}
	
	@Schema(description = "Source Account", type = "String", required = true)
	private String sourceAccountNo;
	@Schema(description = "Source Account Id", type = "Account", required = true)
	private Account sourceAccountId;
	
	private String cardCategory;
	private String schemeType;
	private String cardType;
	private String currencyCode;
	private String deliveryMode;
	private String amount;

	public String getSourceAccountNo() {
		return sourceAccountNo;
	}
	public void setSourceAccountNo(String sourceAccountNo) {
		this.sourceAccountNo = sourceAccountNo;
	}
	public Account getSourceAccountId() {
		return sourceAccountId;
	}
	public void setSourceAccountId(Account sourceAccountId) {
		this.sourceAccountId = sourceAccountId;
	}
	public String getCardCategory() {
		return cardCategory;
	}
	public void setCardCategory(String cardCategory) {
		this.cardCategory = cardCategory;
	}
	public String getSchemeType() {
		return schemeType;
	}
	public void setSchemeType(String schemeType) {
		this.schemeType = schemeType;
	}
	public String getDeliveryMode() {
		return deliveryMode;
	}
	public void setDeliveryMode(String deliveryMode) {
		this.deliveryMode = deliveryMode;
	}
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	public String getCurrencyCode() {
		return currencyCode;
	}
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	
	
	
	

}
