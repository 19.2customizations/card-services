package com.ecobank.digx.cz.app.cardservices.dto;

import java.math.BigDecimal;

import com.ecobank.digx.cz.extxface.mule.dto.HostHeaderInfo;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.ofss.digx.app.common.dto.DataTransferObject;
import com.ofss.digx.datatype.complex.Account;
import com.ofss.fc.datatype.Date;
import com.ofss.fc.infra.log.impl.MultiEntityLogger;

import io.swagger.v3.oas.annotations.media.Schema;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"mobileNo",
"travelDestination",
"cardAccounts",
"returnDate",
"travelDate",
"purposeOfTravel",
"accountAlias"
})
public class TravelNotificationRequestDTO extends DataTransferObject {

	private static final long serialVersionUID = 1L;
	private static final String THIS_COMPONENT_NAME = TravelNotificationRequestDTO.class.getName();
	private transient MultiEntityLogger formatter;


	@JsonProperty("hostHeaderInfo")
	private HostHeaderInfo hostHeaderInfo;
	@JsonProperty("affiliateCode")
	private String affiliateCode;
	@JsonProperty("mobileNo")
	private String mobileNo;
	@JsonProperty("travelDestination")
	private String travelDestination;
	@JsonProperty("cardAccounts")
	private String cardAccounts;
	@JsonProperty("returnDate")
	private String returnDate;
	@JsonProperty("travelDate")
	private String travelDate;
	@JsonProperty("purposeOfTravel")
	private String purposeOfTravel;
	@JsonProperty("accountAlias")
	private String accountAlias;
	
	@JsonProperty("hostHeaderInfo")
	public HostHeaderInfo getHostHeaderInfo() {
		return hostHeaderInfo;
	}

	@JsonProperty("hostHeaderInfo")
	public void setHostHeaderInfo(HostHeaderInfo hostHeaderInfo) {
		this.hostHeaderInfo = hostHeaderInfo;
	}

	@JsonProperty("affiliateCode")
	public String getAffiliateCode() {
		return affiliateCode;
	}
	
	@JsonProperty("affiliateCode")
	public void setAffiliateCode(String affiliateCode) {
		this.affiliateCode = affiliateCode;
	}
	@JsonProperty("mobileNo")
	public String getMobileNo() {
	return mobileNo;
	}

	@JsonProperty("mobileNo")
	public void setMobileNo(String mobileNo) {
	this.mobileNo = mobileNo;
	}

	@JsonProperty("travelDestination")
	public String getTravelDestination() {
	return travelDestination;
	}

	@JsonProperty("travelDestination")
	public void setTravelDestination(String travelDestination) {
	this.travelDestination = travelDestination;
	}

	@JsonProperty("cardAccounts")
	public String getCardAccounts() {
	return cardAccounts;
	}

	@JsonProperty("cardAccounts")
	public void setCardAccounts(String cardAccounts) {
	this.cardAccounts = cardAccounts;
	}

	@JsonProperty("returnDate")
	public String getReturnDate() {
	return returnDate;
	}

	@JsonProperty("returnDate")
	public void setReturnDate(String returnDate) {
	this.returnDate = returnDate;
	}

	@JsonProperty("travelDate")
	public String getTravelDate() {
	return travelDate;
	}

	@JsonProperty("travelDate")
	public void setTravelDate(String travelDate) {
	this.travelDate = travelDate;
	}

	@JsonProperty("purposeOfTravel")
	public String getPurposeOfTravel() {
	return purposeOfTravel;
	}

	@JsonProperty("purposeOfTravel")
	public void setPurposeOfTravel(String purposeOfTravel) {
	this.purposeOfTravel = purposeOfTravel;
	}

	@JsonProperty("accountAlias")
	public String getAccountAlias() {
	return accountAlias;
	}

	@JsonProperty("accountAlias")
	public void setAccountAlias(String accountAlias) {
	this.accountAlias = accountAlias;
	}
	
	
}