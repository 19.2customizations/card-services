package com.ecobank.digx.cz.app.cardservices.dto;

import java.math.BigDecimal;
import java.util.List;

import com.ofss.digx.service.response.BaseResponseObject;

public class CardLimitResponseDTO extends BaseResponseObject {
	
	
	private static final long serialVersionUID = 1L;
	
	private String responseCode;
	private String responseMessage;
	
	private List<CardLimitDTO> limits;
	
	private String paymentTransferLimit;
	private String purchaseLimit;
	private String ccy;
	private String onlinePurchaseLimit;
	private String atmCashLimit;
	
	
	
	public String getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	public String getResponseMessage() {
		return responseMessage;
	}
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}
	public List<CardLimitDTO> getLimits() {
		return limits;
	}
	public void setLimits(List<CardLimitDTO> limits) {
		this.limits = limits;
	}
	
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	public String getPaymentTransferLimit() {
		return paymentTransferLimit;
	}
	public void setPaymentTransferLimit(String paymentTransferLimit) {
		this.paymentTransferLimit = paymentTransferLimit;
	}
	public String getPurchaseLimit() {
		return purchaseLimit;
	}
	public void setPurchaseLimit(String purchaseLimit) {
		this.purchaseLimit = purchaseLimit;
	}
	public String getOnlinePurchaseLimit() {
		return onlinePurchaseLimit;
	}
	public void setOnlinePurchaseLimit(String onlinePurchaseLimit) {
		this.onlinePurchaseLimit = onlinePurchaseLimit;
	}
	public String getAtmCashLimit() {
		return atmCashLimit;
	}
	public void setAtmCashLimit(String atmCashLimit) {
		this.atmCashLimit = atmCashLimit;
	}
	
	
	
	
	
}
