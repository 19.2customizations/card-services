package com.ecobank.digx.cz.app.cardservices.dto;

import java.math.BigDecimal;

import com.ofss.digx.service.response.BaseResponseObject;

public class CardLimitDTO extends BaseResponseObject {
	
	
	private static final long serialVersionUID = 1L;
	
	
	private BigDecimal paymentTransferLimit;
	private BigDecimal purchaseLimit;
	private String ccy;
	private BigDecimal onlinePurchaseLimit;
	private BigDecimal atmCashLimit;
	
	private String schemeType;
	private String cardType;
	
	public BigDecimal getPaymentTransferLimit() {
		return paymentTransferLimit;
	}
	public void setPaymentTransferLimit(BigDecimal paymentTransferLimit) {
		this.paymentTransferLimit = paymentTransferLimit;
	}
	public BigDecimal getPurchaseLimit() {
		return purchaseLimit;
	}
	public void setPurchaseLimit(BigDecimal purchaseLimit) {
		this.purchaseLimit = purchaseLimit;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	public BigDecimal getOnlinePurchaseLimit() {
		return onlinePurchaseLimit;
	}
	public void setOnlinePurchaseLimit(BigDecimal onlinePurchaseLimit) {
		this.onlinePurchaseLimit = onlinePurchaseLimit;
	}
	public BigDecimal getAtmCashLimit() {
		return atmCashLimit;
	}
	public void setAtmCashLimit(BigDecimal atmCashLimit) {
		this.atmCashLimit = atmCashLimit;
	}
	public String getSchemeType() {
		return schemeType;
	}
	public void setSchemeType(String schemeType) {
		this.schemeType = schemeType;
	}
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	
	
	

	
	
	
}
