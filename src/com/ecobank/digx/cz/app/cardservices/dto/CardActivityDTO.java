package com.ecobank.digx.cz.app.cardservices.dto;

import java.math.BigDecimal;

import com.ofss.digx.app.common.dto.DataTransferObject;
import com.ofss.digx.datatype.complex.Account;
import com.ofss.fc.datatype.Date;
import com.ofss.fc.infra.log.impl.MultiEntityLogger;

import io.swagger.v3.oas.annotations.media.Schema;

public class CardActivityDTO extends DataTransferObject {

	private static final long serialVersionUID = 1L;
	private static final String THIS_COMPONENT_NAME = CardActivityDTO.class.getName();
	private transient MultiEntityLogger formatter;

	public CardActivityDTO() {
		formatter = MultiEntityLogger.getUniqueInstance();
	}

	private String refId;
	private String cardId;
	private Date activityDate;
	private String activityType;
	private String activityDescription;
	private String status;
	private String responseCode;
	private String responseMessage;
	private String tranRefNo;
	private String fee;
	private String userId;
	private BigDecimal amount;
	private String ccyCode;
	private String activityCode;
	
	private String maskedPan;
	private String cardAccount;
	private String nameOnCard;
	private String cardType;
	private String mobileNo;
	private String cardCustomerId;
	
	private String udf1;
	private String udf2;
	private String udf3;
	
	private String encCardData;
	

	
	private String email;
	

	@Schema(description = "Source Account", type = "String", required = true)
	private String sourceAccountNo;
	@Schema(description = "Source Account Id", type = "Account", required = true)
	private Account sourceAccountId;

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getCcyCode() {
		return ccyCode;
	}

	public void setCcyCode(String ccyCode) {
		this.ccyCode = ccyCode;
	}

	public String getActivityCode() {
		return activityCode;
	}

	public void setActivityCode(String activityCode) {
		this.activityCode = activityCode;
	}

	public String getRefId() {
		return refId;
	}

	public void setRefId(String refId) {
		this.refId = refId;
	}

	public String getCardId() {
		return cardId;
	}

	public void setCardId(String cardId) {
		this.cardId = cardId;
	}

	public Date getActivityDate() {
		return activityDate;
	}

	public void setActivityDate(Date activityDate) {
		this.activityDate = activityDate;
	}

	public String getActivityType() {
		return activityType;
	}

	public void setActivityType(String activityType) {
		this.activityType = activityType;
	}

	public String getActivityDescription() {
		return activityDescription;
	}

	public void setActivityDescription(String activityDescription) {
		this.activityDescription = activityDescription;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public String getTranRefNo() {
		return tranRefNo;
	}

	public void setTranRefNo(String tranRefNo) {
		this.tranRefNo = tranRefNo;
	}

	public String getFee() {
		return fee;
	}

	public void setFee(String fee) {
		this.fee = fee;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getSourceAccountNo() {
		return sourceAccountNo;
	}

	public void setSourceAccountNo(String sourceAccountNo) {
		this.sourceAccountNo = sourceAccountNo;
	}

	public Account getSourceAccountId() {
		return sourceAccountId;
	}

	public void setSourceAccountId(Account sourceAccountId) {
		this.sourceAccountId = sourceAccountId;
	}

	public String getMaskedPan() {
		return maskedPan;
	}

	public void setMaskedPan(String maskedPan) {
		this.maskedPan = maskedPan;
	}

	public String getCardAccount() {
		return cardAccount;
	}

	public void setCardAccount(String cardAccount) {
		this.cardAccount = cardAccount;
	}

	public String getNameOnCard() {
		return nameOnCard;
	}

	public void setNameOnCard(String nameOnCard) {
		this.nameOnCard = nameOnCard;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getCardCustomerId() {
		return cardCustomerId;
	}

	public void setCardCustomerId(String cardCustomerId) {
		this.cardCustomerId = cardCustomerId;
	}

	public String getUdf1() {
		return udf1;
	}

	public void setUdf1(String udf1) {
		this.udf1 = udf1;
	}

	public String getUdf2() {
		return udf2;
	}

	public void setUdf2(String udf2) {
		this.udf2 = udf2;
	}

	public String getUdf3() {
		return udf3;
	}

	public void setUdf3(String udf3) {
		this.udf3 = udf3;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEncCardData() {
		return encCardData;
	}

	public void setEncCardData(String encCardData) {
		this.encCardData = encCardData;
	}
	
	
}