package com.ecobank.digx.cz.app.cardservices.dto;

import java.util.ArrayList;
import java.util.List;

import com.ecobank.digx.cz.extxface.mule.dto.HostHeaderInfo;
import com.ofss.digx.service.response.BaseResponseObject;

public class CardListResponse extends BaseResponseObject{
	
	private static final long serialVersionUID = 1L;
	 
	private String responseCode;
	private String responseMessage;
	public HostHeaderInfo hostHeaderInfo;
	
	private List<CardDetailDTO> cards = new ArrayList<CardDetailDTO>();

	public String getResponseCode() {
		return responseCode;
	}


	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}


	public String getResponseMessage() {
		return responseMessage;
	}


	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}


	public List<CardDetailDTO> getCards() {
		return cards;
	}


	public void setCards(List<CardDetailDTO> cards) {
		this.cards = cards;
	}


	public HostHeaderInfo getHostHeaderInfo() {
		return hostHeaderInfo;
	}


	public void setHostHeaderInfo(HostHeaderInfo hostHeaderInfo) {
		this.hostHeaderInfo = hostHeaderInfo;
	}	
	
}
