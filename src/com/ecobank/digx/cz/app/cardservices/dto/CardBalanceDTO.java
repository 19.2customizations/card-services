package com.ecobank.digx.cz.app.cardservices.dto;

import java.math.BigDecimal;

import com.ofss.digx.app.common.dto.DataTransferObject;
import com.ofss.digx.datatype.complex.Account;
import com.ofss.fc.datatype.Date;
import com.ofss.fc.infra.log.impl.MultiEntityLogger;

import io.swagger.v3.oas.annotations.media.Schema;

public class CardBalanceDTO extends DataTransferObject {

	private static final long serialVersionUID = 1L;
	private static final String THIS_COMPONENT_NAME = CardBalanceDTO.class.getName();
	private transient MultiEntityLogger formatter;

	public CardBalanceDTO() {
		formatter = MultiEntityLogger.getUniqueInstance();
	}

	
	private String cardId;
	private String maskedPan;
	private String cardAccount;
	private String nameOnCard;
	private String cardType;
	private BigDecimal balance;
	private String ccyCode;
	
	private String cardCategory;
	
	private String cvv;
	private String expiryDate;
	private String cardCustomerId;
	private String mobileNo;

	
	private String responseCode;
	private String responseMessage;
	
	

	public String getCardId() {
		return cardId;
	}
	public void setCardId(String cardId) {
		this.cardId = cardId;
	}
	public String getMaskedPan() {
		return maskedPan;
	}
	public void setMaskedPan(String maskedPan) {
		this.maskedPan = maskedPan;
	}
	public String getCardAccount() {
		return cardAccount;
	}
	public void setCardAccount(String cardAccount) {
		this.cardAccount = cardAccount;
	}
	public String getNameOnCard() {
		return nameOnCard;
	}
	public void setNameOnCard(String nameOnCard) {
		this.nameOnCard = nameOnCard;
	}
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	
	public String getCcyCode() {
		return ccyCode;
	}
	public void setCcyCode(String ccyCode) {
		this.ccyCode = ccyCode;
	}
	public String getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	public String getResponseMessage() {
		return responseMessage;
	}
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}
	public BigDecimal getBalance() {
		return balance;
	}
	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}
	public String getCardCategory() {
		return cardCategory;
	}
	public void setCardCategory(String cardCategory) {
		this.cardCategory = cardCategory;
	}
	public String getCvv() {
		return cvv;
	}
	public void setCvv(String cvv) {
		this.cvv = cvv;
	}
	public String getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}
	public String getCardCustomerId() {
		return cardCustomerId;
	}
	public void setCardCustomerId(String cardCustomerId) {
		this.cardCustomerId = cardCustomerId;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}	
	
	
}