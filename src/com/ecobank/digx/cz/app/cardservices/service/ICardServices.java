package com.ecobank.digx.cz.app.cardservices.service;

import com.ecobank.digx.cz.app.cardservices.dto.CardActivityDTO;
import com.ecobank.digx.cz.app.cardservices.dto.CardActivityListResponse;
import com.ecobank.digx.cz.app.cardservices.dto.CardBalanceDTO;
import com.ecobank.digx.cz.app.cardservices.dto.CardBalanceResponse;
import com.ecobank.digx.cz.app.cardservices.dto.CardLimitResponseDTO;
import com.ecobank.digx.cz.app.cardservices.dto.CardListResponse;
import com.ecobank.digx.cz.app.cardservices.dto.CardRequestFeeListResponse;
import com.ecobank.digx.cz.app.cardservices.dto.CardRequestListResponse;
import com.ecobank.digx.cz.app.cardservices.dto.CardServiceCreateRequestDTO;
import com.ecobank.digx.cz.app.cardservices.dto.CardServiceCreateResponseDTO;
import com.ecobank.digx.cz.app.cardservices.dto.CardServiceDTO;
import com.ecobank.digx.cz.app.cardservices.dto.CardServiceResponseDTO;
import com.ecobank.digx.cz.app.cardservices.dto.CardServiceUpdateRequestDTO;
import com.ecobank.digx.cz.app.cardservices.dto.CardStatementListResponse;
import com.ecobank.digx.cz.app.cardservices.dto.CardStatementRequestDTO;
import com.ecobank.digx.cz.app.cardservices.dto.CreditCardBalDetailRespDTO;
import com.ecobank.digx.cz.app.cardservices.dto.FetchCardFeeRequestDTO;
import com.ecobank.digx.cz.app.cardservices.dto.FetchCardFeeResponse;
import com.ecobank.digx.cz.app.cardservices.dto.TravelNotificationRequestDTO;
import com.ecobank.digx.cz.app.domain.cardservices.entity.repository.CardServiceRepository;
import com.ofss.fc.app.context.SessionContext;

public interface ICardServices {
	
	  CardServiceCreateResponseDTO read(SessionContext paramSessionContext, CardServiceCreateRequestDTO paramCardServiceCreateRequestDTO) throws Exception;
	  
	  CardServiceCreateResponseDTO create(SessionContext paramSessionContext, CardServiceCreateRequestDTO paramCardServiceCreateRequestDTO) throws Exception;
	  
	  CardServiceResponseDTO proocessCardRequest(SessionContext paramSessionContext, CardServiceUpdateRequestDTO paramCardServiceUpdateRequestDTO) throws Exception;
	  
	  CardServiceResponseDTO updateCardRequest(SessionContext paramSessionContext, CardServiceUpdateRequestDTO paramCardServiceUpdateRequestDTO) throws Exception;
	  
	  CardServiceCreateResponseDTO fundCard(SessionContext paramSessionContext, CardServiceCreateRequestDTO paramCardServiceCreateRequestDTO) throws Exception;
	  
	  CardLimitResponseDTO fetchCardLimit(SessionContext paramSessionContext, CardActivityDTO paramCardActivityDTO) throws Exception;
	  
	  CardBalanceResponse balanceCheck(SessionContext paramSessionContext, CardBalanceDTO paramCardBalanceDTO) throws Exception;
	  
	  CardServiceCreateResponseDTO miniStatement(SessionContext paramSessionContext, CardServiceCreateRequestDTO paramCardServiceCreateRequestDTO) throws Exception;
	  
	  CardStatementListResponse fetchStatement(SessionContext paramSessionContext, CardStatementRequestDTO paramCardStatementRequestDTO) throws Exception;
	  
	  CardListResponse fetchCardByAccount(SessionContext paramSessionContext, CardStatementRequestDTO paramCardStatementRequestDTO) throws Exception;
	  
	  CardListResponse fetchCardByMsisdn(SessionContext paramSessionContext, String paramString1, String paramString2) throws Exception;
	  
	  CardServiceCreateResponseDTO inquiry(SessionContext paramSessionContext, CardServiceCreateRequestDTO paramCardServiceCreateRequestDTO) throws Exception;
	  
	  CardActivityListResponse listCardActivityById(SessionContext paramSessionContext, String paramString) throws Exception;
	  
	  CardRequestListResponse listCardRequestsByCustomerId(SessionContext paramSessionContext, String paramString1, String paramString2) throws Exception;
	  
	  CardListResponse listCardsByCustomerId(SessionContext paramSessionContext, String paramString1, String paramString2) throws Exception;
	  
	  CardServiceResponseDTO createCardActivity(SessionContext paramSessionContext, CardActivityDTO paramCardActivityDTO) throws Exception;
	  
	  CardServiceResponseDTO resetCardPin(SessionContext paramSessionContext, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7) throws Exception;
	  
	  CardServiceResponseDTO travelNotification(SessionContext paramSessionContext, TravelNotificationRequestDTO paramTravelNotificationRequestDTO) throws Exception;
	  
	  CardServiceResponseDTO changePin(SessionContext paramSessionContext, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, String paramString8, String paramString9) throws Exception;
	  
	  CreditCardBalDetailRespDTO fetchCreditCardBalanceLimit(SessionContext paramSessionContext, String paramString1, String paramString2) throws Exception;
	  
	  CardRequestFeeListResponse listCardRequestFees(SessionContext paramSessionContext, String paramString) throws Exception;
	  
	  CardLimitResponseDTO listCardLimits(SessionContext paramSessionContext, String paramString) throws Exception;
	  
	  FetchCardFeeResponse fetchCardFee(SessionContext paramSessionContext, FetchCardFeeRequestDTO paramFetchCardFeeRequestDTO) throws Exception;
}
