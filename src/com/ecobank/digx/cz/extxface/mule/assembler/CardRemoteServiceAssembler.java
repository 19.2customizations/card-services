package com.ecobank.digx.cz.extxface.mule.assembler;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.logging.Logger;
import java.util.prefs.Preferences;

import com.ecobank.digx.cz.app.cardservices.assembler.CardServiceAssembler;
import com.ecobank.digx.cz.app.cardservices.dto.CardDetailDTO;
import com.ecobank.digx.cz.app.cardservices.dto.CardFundingRequestDTO;
import com.ecobank.digx.cz.app.cardservices.dto.CardLimitResponseDTO;
import com.ecobank.digx.cz.app.cardservices.dto.CardListResponse;
import com.ecobank.digx.cz.app.cardservices.dto.CardStatementDTO;
import com.ecobank.digx.cz.app.cardservices.dto.CardStatementListResponse;
import com.ecobank.digx.cz.app.domain.cardservices.entity.CardActivityInfo;
import com.ecobank.digx.cz.app.domain.cardservices.entity.CardDetail;
import com.ecobank.digx.cz.extxface.mule.adapter.client.AESEncrypterOriginal;
import com.ecobank.digx.cz.extxface.mule.adapter.client.JEncrypt;
import com.ecobank.digx.cz.extxface.mule.dto.CardAccountInfo;
import com.ecobank.digx.cz.extxface.mule.dto.ESBCardRequestDTO;
import com.ecobank.digx.cz.extxface.mule.dto.ESBCardStatementEntry;
import com.ecobank.digx.cz.extxface.mule.dto.ESBCardTransactionHistoryResponseDTO;
import com.ecobank.digx.cz.extxface.mule.dto.ESBGetCardByAccountResponseDTO;
import com.ecobank.digx.cz.extxface.mule.dto.HostHeaderInfo;
import com.ofss.fc.framework.domain.IAbstractDomainObject;
import com.ofss.fc.framework.domain.assembler.AbstractAssembler;
import com.ofss.fc.framework.domain.common.dto.DomainObjectDTO;
import com.ofss.fc.infra.config.ConfigurationFactory;
import com.ofss.fc.infra.exception.FatalException;
import com.ofss.fc.infra.log.impl.MultiEntityLogger;

public class CardRemoteServiceAssembler extends AbstractAssembler {
	private static final String THIS_COMPONENT_NAME = CardServiceAssembler.class.getName();
	private transient Logger logger = MultiEntityLogger.getUniqueInstance().getLogger(THIS_COMPONENT_NAME);
	private Preferences digx_consulting = ConfigurationFactory.getInstance().getConfigurations("DIGXCONSULTING");
	
	@Override
	public DomainObjectDTO fromDomainObject(IAbstractDomainObject arg0) throws FatalException {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public IAbstractDomainObject toDomainObject(DomainObjectDTO arg0) throws FatalException {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	public CardListResponse fromRemoteResponseGetCardByAccounttoDTO(ESBGetCardByAccountResponseDTO response,String accNo,String cardCategory, String ccy)
	{
		
		CardListResponse resp = new CardListResponse();
		List<CardDetailDTO> list = new ArrayList<CardDetailDTO>();
		
		
		String key = digx_consulting.get("ESB_OBDX_AES_KEY", "");
		
		resp.setResponseCode(response.getHostHeaderInfo().getResponseCode());
		resp.setResponseMessage(response.getHostHeaderInfo().getResponseMessage());
		if(response.getCards() != null && response.getCards().size() > 0)
		{
			for(CardAccountInfo c : response.getCards())
			{
				try
				{
					CardDetailDTO cDto = new CardDetailDTO();
					cDto.setCardAccount(accNo);
					cDto.setExpiryDate(c.getExpiryDate());
					
					String eDate = c.getExpiryDate();
					System.out.println("EXPIRY-DATE :: " + eDate);
					if(eDate != null && eDate.length() > 10 && eDate.contains("-"))
					{
						eDate = eDate.substring(0,10);
						System.out.println("EXPIRY-DATE 2 :: " + eDate);
						SimpleDateFormat dff4 = new SimpleDateFormat("yyyy-MM-dd");
				          java.util.Date dtx = dff4.parse(eDate);
				          SimpleDateFormat dff5 = new SimpleDateFormat("MM/YY");
				          String expiryDate = dff5.format(dtx);
				          
				          cDto.setExpiryDate(expiryDate);
					}
					
					String cardNo =c.getCardNumber();
					if(cardNo != null && cardNo.length() > 20)
					{
						/*System.out.println("KEY-AES :: " + key);
						try
						{
						   cardNo = AESEncrypterOriginal.decrypt(cardNo, AESEncrypterOriginal.getEncryptionKey(key));
						   
						}
						catch(Exception ec)
						{
							String err = Arrays.toString(ec.getStackTrace()).replaceAll(",", "\n");
							System.out.println("KEY-AES-ERROR :: " + err);
						}*/
						cardNo = c.getPseudoPan();
						
						
						if(cardNo != null && cardNo.length() > 6)
							cardNo = cardNo.substring(0,6) + "****" + cardNo.substring(cardNo.length() - 4);
						
					}
					cDto.setMaskedPan(cardNo);
					cDto.setCardCategory(cardCategory);
					cDto.setCardId("0");
					cDto.setCardStatus(c.getStatus());
					cDto.setNameOnCard(c.getNameOnCard());
					cDto.setCardType(cardCategory);
					cDto.setCardCcy(ccy);
					cDto.setPseudoPan(c.getPseudoPan());
					list.add(cDto);
				}
				catch(Exception ex)
				{
					String err = Arrays.toString(ex.getStackTrace()).replaceAll(",", "\n");
					System.out.println("KEY-AES-ERROR :: " + err);
					ex.printStackTrace();
				}
				
			}
		}
		
		resp.setCards(list);
		
		
		return resp;
	}
	
	public CardStatementListResponse fromRemoteResponseCardStatementtoDTO(ESBCardTransactionHistoryResponseDTO response)
	{
		
		CardStatementListResponse resp = new CardStatementListResponse();
		List<CardStatementDTO> list = new ArrayList<CardStatementDTO>();
		
		resp.setResponseCode(response.getHostHeaderInfo().getResponseCode());
		resp.setResponseMessage(response.getHostHeaderInfo().getResponseMessage());
		if(response.getTransactions() != null && response.getTransactions().size() > 0)
		{
			for(ESBCardStatementEntry c : response.getTransactions())
			{
				CardStatementDTO cDto = new CardStatementDTO();
				cDto.setCcy(c.getCurrency());
				cDto.setDescription(c.getAdditionalInfo() + " " + c.getDesc() + " PURCHASE");
				cDto.setPaymentAmount(new BigDecimal(c.getAmount()));
				cDto.setTransDate(c.getTranDate());
				cDto.setTranType("PURCHASE");
				cDto.setStatus("SUCCESS");
				list.add(cDto);
				
			}
		}
		
		resp.setCardStatement(list);
		
		return resp;
	}
	
	public ESBCardRequestDTO fromDomainObjectToRemoteActivityFundCreditCard(CardActivityInfo caInfo, String pan, String nameOnCard, String branchCode)
	{
		
		String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String ipx = digx_consulting.get("ESB_OBDX_IP", "");
		
		String affCode = caInfo.getAffiliateCode();
		
		String genRef = caInfo.getRefKey().getRefId();  // GetRefNumber("BC", 12);
		ESBCardRequestDTO esbRequestDTO = new ESBCardRequestDTO();
		HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
		hostHeaderInfo.setAffiliateCode(affCode); //affCode //temporarily hardcoded
		hostHeaderInfo.setIpAddress(ipx);
		hostHeaderInfo.setRequestId(genRef);
		hostHeaderInfo.setRequestToken(genRef);
		hostHeaderInfo.setRequestType("FUNDCARD");
		hostHeaderInfo.setSourceChannelId("WEB");
		hostHeaderInfo.setSourceCode(sourceCode);
		esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);
		//
		System.out.println("caInfo.getCardDat : " + caInfo.getCardDat());
		String cdx = JEncrypt.decrypt(caInfo.getCardDat());
		System.out.println("caInfo.getCardDat : " + cdx);
		esbRequestDTO.setCardActNo(cdx); //caInfo.getCardDat());
		//esbRequestDTO.setRecipientName(caInfo.getUdf2());
		esbRequestDTO.setRecipientName(nameOnCard);
		esbRequestDTO.setAmount(caInfo.getAmount().toString());

		esbRequestDTO.setCcy(caInfo.getCcy()); 
		esbRequestDTO.setAccountNumber(caInfo.getSourceAccount());
		

		esbRequestDTO.setAccountBranch(branchCode);
		esbRequestDTO.setNarration(caInfo.getActivityDescription());
		
		return esbRequestDTO;
	}

	
	public ESBCardRequestDTO fromDomainObjectToRemoteActivityFundVirtual(CardActivityInfo caInfo,String branchCode)
	{
		
		String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String ipx = digx_consulting.get("ESB_OBDX_IP", "");
		String pwd = digx_consulting.get("ESB_OBDX_PWD", "");
		String requestId = GetRefNumber("FC", 12);
		String requestToken = JEncrypt.GenerateTransactionToken(sourceCode, requestId, pwd);
		
		String affCode = caInfo.getAffiliateCode();
		
		String genRef = caInfo.getRefKey().getRefId();  // GetRefNumber("BC", 12);
		//CardFundingRequestDTO esbRequestDTO = new CardFundingRequestDTO();
		
		//String cardNo = caInfo.getCardDat();
		 
		
		ESBCardRequestDTO esbRequestDTO = new ESBCardRequestDTO();
		HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
		hostHeaderInfo.setAffiliateCode(affCode); //temporarily hardcoded
		hostHeaderInfo.setIpAddress(ipx);
		hostHeaderInfo.setRequestId("CF" + genRef);
		hostHeaderInfo.setRequestToken(requestToken);
		hostHeaderInfo.setRequestType("LOAD-VCARD");
		hostHeaderInfo.setSourceChannelId("MOBILE");
		hostHeaderInfo.setSourceCode(sourceCode);
		esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);
		esbRequestDTO.setCardNo(caInfo.getCardAccount());
		//esbRequestDTO.setCardNo(caInfo.getCardId());
		esbRequestDTO.setMsisdn(caInfo.getMobileNo());

		esbRequestDTO.setSourceAccount(caInfo.getSourceAccount());
		

		//esbRequestDTO.setSourceAccount(caInfo.getSourceAccount());
		String amt = new BigDecimal(caInfo.getAmount().doubleValue() + caInfo.getFee().doubleValue()).toString();
		esbRequestDTO.setSourceAccountCcy(caInfo.getCcy());
		esbRequestDTO.setAmount(amt); //caInfo.getAmount().toString());
		esbRequestDTO.setCharge("0"); //caInfo.getFee().toString());
		
		
		/*esbRequestDTO.setAccountBranch(branchCode);
		esbRequestDTO.setAccountNumber(caInfo.getCardAccount());
		esbRequestDTO.setAffiliateCode(caInfo.getAffiliateCode());
		esbRequestDTO.setAmount(caInfo.getAmount());
		esbRequestDTO.setCardAccount(caInfo.getCardAccount());
		esbRequestDTO.setCardCustomerId(caInfo.getCardCustomerId());
		esbRequestDTO.setCardType(caInfo.getCardType());
		//
		esbRequestDTO.setCcyCode(caInfo.getC);
		esbRequestDTO.setRecipientName(nameOnCard);
		esbRequestDTO.setAmount(caInfo.getFee());
		esbRequestDTO.setCcy("GHS"); //temporary
		esbRequestDTO.setAccountNumber(caInfo.getSourceAccount());
		esbRequestDTO.setAccountBranch(branchCode);
		esbRequestDTO.setNarration(caInfo.getActivityDescription());*/
		
		return esbRequestDTO;
	}
	

	
	public CardStatementListResponse fromRemoteResponseCardStatementtoDTOMockingxx(String ccy)
	{
		
		CardStatementListResponse resp = new CardStatementListResponse();
		List<CardStatementDTO> list = new ArrayList<CardStatementDTO>();
		
		resp.setResponseCode("000");
		resp.setResponseMessage("SUCCESS");
		for(int i =0; i<10; i++)
			{
				CardStatementDTO cDto = new CardStatementDTO();
				cDto.setCcy(ccy);
				cDto.setDescription("PURCHASE @ MELCOM");
				cDto.setPaymentAmount(new BigDecimal(100));
				cDto.setTransDate("11-02-2020 11:20AM");
				cDto.setTranType("PURCHASE");
				cDto.setStatus("SUCCESS");
				list.add(cDto);
				
			}
		
		
		resp.setCardStatement(list);
		
		return resp;
	}
	
	public CardStatementListResponse MockingCardLimitxx(String ccy)
	{
		
		CardStatementListResponse resp = new CardStatementListResponse();
		List<CardStatementDTO> list = new ArrayList<CardStatementDTO>();
		
		resp.setResponseCode("000");
		resp.setResponseMessage("SUCCESS");
		for(int i =0; i<10; i++)
			{
				CardStatementDTO cDto = new CardStatementDTO();
				cDto.setCcy(ccy);
				cDto.setDescription("PURCHASE @ MELCOM");
				cDto.setPaymentAmount(new BigDecimal(100));
				cDto.setTransDate("11-02-2020 11:20AM");
				cDto.setTranType("PURCHASE");
				cDto.setStatus("SUCCESS");
				list.add(cDto);
				
			}
		
		
		resp.setCardStatement(list);
		
		return resp;
	}
	
	public CardLimitResponseDTO fetchCardLimitMockingxx()
	{
		CardLimitResponseDTO cardLimitResponseDTO = new CardLimitResponseDTO();
		cardLimitResponseDTO.setPaymentTransferLimit("10000");
		cardLimitResponseDTO.setAtmCashLimit("200000");
		cardLimitResponseDTO.setCcy("NGN");
		cardLimitResponseDTO.setOnlinePurchaseLimit("50000");
		cardLimitResponseDTO.setPurchaseLimit("60000");
		cardLimitResponseDTO.setResponseCode("000");
		
		
		return cardLimitResponseDTO;
	}
	
	
	
	public  String GetRefNumber(String type, int len) {

        String finalString = "";
        int x = 0;
        char[] stringChars = new char[len];
        for (int i = 0; i < len; i++) //4
        {
            Random random = new Random();
            x = random.nextInt(9);

            stringChars[i] = Integer.toString(x).toCharArray()[0];
        }


        finalString = new String(stringChars);
        finalString = type + finalString;
        return finalString.trim();
    }
	

	

}
