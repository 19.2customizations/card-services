package com.ecobank.digx.cz.extxface.mule.adapter.client;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import com.ecobank.digx.cz.app.cardservices.dto.CardDetailDTO;
import com.ecobank.digx.cz.app.cardservices.dto.CardListResponse;
import com.ecobank.digx.cz.app.cardservices.dto.CardServiceResponseDTO;
import com.ecobank.digx.cz.app.domain.cardservices.entity.CardActivityInfo;
import com.ecobank.digx.cz.app.domain.cardservices.entity.CardRequest;
import com.ecobank.digx.cz.extxface.mule.assembler.CardRemoteServiceAssembler;
import com.ecobank.digx.cz.extxface.mule.dto.AccountDetails;
import com.ecobank.digx.cz.extxface.mule.dto.ESBCardRequestDTO;
import com.ecobank.digx.cz.extxface.mule.dto.ESBCardResponseDTO;
import com.ecobank.digx.cz.extxface.mule.dto.FetchVirtualCardRequestDTO;
import com.ecobank.digx.cz.extxface.mule.dto.FetchVirtualCardResponseDTO;
import com.ecobank.digx.cz.extxface.mule.dto.GenerateVirtualCardRequestDTO;
import com.ecobank.digx.cz.extxface.mule.dto.GenerateVirtualCardResponseDTO;
import com.ecobank.digx.cz.extxface.mule.dto.HostHeaderInfo;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.ofss.digx.app.core.AdapterInteraction;
import com.ofss.digx.framework.rest.client.RESTClientFactory;
import com.ofss.digx.framework.rest.client.RESTClientRequestBuilder;
import com.ofss.digx.framework.rest.client.RESTClientRequestConfig;
import com.ofss.digx.framework.rest.client.impl.IRESTClient;
import com.ofss.digx.infra.exceptions.Exception;
import com.thoughtworks.xstream.XStream;

public class Test {
	 
	
	public static void main(String[] args) throws JsonParseException, JsonMappingException, IOException, Exception {

		// new Test().createCardRequest(null, null, null);
		// new Test().generateCard();
		// new Test().fetchVCardByMsisdn();
		//new Test().generateCard(null, null);
		CardListResponse xx = new Test().fetchVCardByMsisdn("233242242107", "393949492200", "EGH");
		System.out.println("XX:: " + xx.getResponseCode());
		System.out.println("YY:: " + xx.getResponseMessage());


	}
	
	
	public CardServiceResponseDTO fundCard(CardActivityInfo caInfo,String branchCode) throws Exception {
		CardServiceResponseDTO response = new CardServiceResponseDTO();
		response.setResponseCode("E04");
		response.setResponseMessage("Failed at interface level");
		AdapterInteraction.begin();

		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESC_VIRTUAL_CARD_FUND");
		RESTClientFactory clientFactory = RESTClientFactory.getInstance();
		IRESTClient restClient = clientFactory.getRESTClientInstance();

		/*String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String ip = digx_consulting.get("ESB_OBDX_IP", "");
		String pwd = digx_consulting.get("ESB_OBDX_PWD", "");
		String requestId = GetRefNumber("FC", 12);
		String requestToken = JEncrypt.GenerateTransactionToken(sourceCode, requestId, pwd);*/
		
		XStream xs = new XStream();
		System.out.println("Fund Virtual Card Request : " + xs.toXML(caInfo));
		
		CardRemoteServiceAssembler assembler = new CardRemoteServiceAssembler();
		
		ESBCardRequestDTO esbRequestDTO = assembler.fromDomainObjectToRemoteActivityFundVirtual(caInfo, branchCode);
		
		
		ESBCardResponseDTO esbCardResponseDTO = null;

		try {
			System.out.println("Fund Virtual Card Request : " + xs.toXML(esbRequestDTO));
			
			
			try {
				ObjectMapper mapper = new ObjectMapper();
				String jsonRequestBody = mapper.writeValueAsString(esbRequestDTO);
				System.out.println(":: jsonRequestBody : " + jsonRequestBody);
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}
			Gson gson = new Gson();
			
			String genResponse = HttpPost(prop.getContextURL(), prop.getServiceURL(), esbRequestDTO);
			esbCardResponseDTO = gson.fromJson(genResponse, ESBCardResponseDTO.class);
			
			try {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponseBody = mapper.writeValueAsString(esbCardResponseDTO);
				System.out.println(":: jsonResponseBody : " + jsonResponseBody);
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}
			System.out.println("getResponseCode         : " + esbCardResponseDTO.getHostHeaderInfo().getResponseCode());
			System.out.println("getResponseMessage      : " + esbCardResponseDTO.getHostHeaderInfo().getResponseMessage());
			
			if (esbCardResponseDTO != null && esbCardResponseDTO.getHostHeaderInfo().getResponseCode().equals("000")) {
				response.setResponseCode(esbCardResponseDTO.getHostHeaderInfo().getResponseCode());
				response.setResponseMessage(esbCardResponseDTO.getHostHeaderInfo().getResponseMessage());
				response.setTransactionRefNo(esbCardResponseDTO.getHostHeaderInfo().getRequestId());
			}
			
		} finally {
		}
		AdapterInteraction.close();

		return response;
	}
	
	
	public CardListResponse fetchVCardByMsisdn(String mobileNo, String referenceNo, String affCode) throws Exception {
		CardListResponse cardListResponse = new CardListResponse();
		cardListResponse.setResponseCode("E04");
		cardListResponse.setResponseMessage("Failed at interface level");
		//AdapterInteraction.begin();

		//RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		//RESTClientRequestConfig prop = clientBuilder.loadConfig("ESC_VIRTUAL_CARD_FETCH_BY_MSISDN");
		
		FetchVirtualCardRequestDTO fetchVirtualCardRequestDTO = new FetchVirtualCardRequestDTO();
		fetchVirtualCardRequestDTO.setAFFILIATE_COUNTRY(affCode);
		fetchVirtualCardRequestDTO.setAPP_VERSION("3.5.0");
		fetchVirtualCardRequestDTO.setDEVICE_TYPE("");
		fetchVirtualCardRequestDTO.setEMCERT_ID("1233185769");
		fetchVirtualCardRequestDTO.setLANG_ID("1");
		fetchVirtualCardRequestDTO.setMSISDN(mobileNo);
		fetchVirtualCardRequestDTO.setSERVICE("FETCH-VCARDS");
		fetchVirtualCardRequestDTO.setUUID(referenceNo);
		
		FetchVirtualCardResponseDTO fetchVirtualCardResponseDTO = null;

		try {
			try {
				ObjectMapper mapper = new ObjectMapper();
				String jsonRequestBody = mapper.writeValueAsString(fetchVirtualCardRequestDTO);
				System.out.println(":: jsonRequestBody : " + jsonRequestBody);
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}
			Gson gson = new Gson();
			
			String genResponse = HttpPost("http://10.4.139.49:7003/unifiedapis/services", "fetchvcard", fetchVirtualCardRequestDTO);
			
			
			try {
				fetchVirtualCardResponseDTO = gson.fromJson(genResponse, FetchVirtualCardResponseDTO.class);
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponseBody = mapper.writeValueAsString(fetchVirtualCardResponseDTO);
				System.out.println(":: jsonResponseBody : " + jsonResponseBody);
			} catch (JsonProcessingException  | com.google.gson.JsonSyntaxException ex) {
				System.out.println(":::::::: ::::::::: :::::::::::");
				ex.printStackTrace();
			}
			System.out.println("getResponseCode         : " + fetchVirtualCardResponseDTO.getSTATUS_CODE());
			System.out.println("getResponseMessage      : " + fetchVirtualCardResponseDTO.getSTATUS_MESSAGE());
			
			
			if (fetchVirtualCardResponseDTO.getSTATUS_CODE().equals("000")) {

				if (fetchVirtualCardResponseDTO.getACCOUNT_DETAILS().size() > 0) {

					for (AccountDetails transactionDetails : fetchVirtualCardResponseDTO.getACCOUNT_DETAILS()) {
						
						CardDetailDTO cardDetailDTO = new CardDetailDTO();
						cardDetailDTO.setBalance(transactionDetails.getBalance() != null ? new BigDecimal(transactionDetails.getBalance()) : new BigDecimal("0"));
						cardDetailDTO.setCardAccount(transactionDetails.getAccountNumber() != null ? transactionDetails.getAccountNumber() : "");
						cardDetailDTO.setCardCategory("VIRTUAL");
						cardDetailDTO.setExpiryDate(transactionDetails.getExpiry() != null ? transactionDetails.getExpiry() : "");
						cardDetailDTO.setMaskedPan(transactionDetails.getCardToken() != null ? transactionDetails.getCardToken() : "");
						cardDetailDTO.setMobileNo(transactionDetails.getOtpNumber() != null ? transactionDetails.getOtpNumber() : "");
						cardDetailDTO.setCustomerId(transactionDetails.getCustId() != null ? transactionDetails.getCustId() : "");
						cardDetailDTO.setCardStatus(""+transactionDetails.getBlocked() != null ? ""+transactionDetails.getBlocked() : "");
						cardListResponse.getCards().add(cardDetailDTO);
						
						System.out.println("******************************************");
						System.out.println("Balance       : " + cardDetailDTO.getBalance());
						System.out.println("CardAccount   : " + cardDetailDTO.getCardAccount());
						System.out.println("Expiry Date   : " + cardDetailDTO.getExpiryDate());
						System.out.println("Card Status   : " + cardDetailDTO.getCardStatus());
						System.out.println("Customer Id   : " + cardDetailDTO.getCustomerId());
						System.out.println("Masked Pan    : " + cardDetailDTO.getMaskedPan());
						System.out.println("Mobile No     : " + cardDetailDTO.getMobileNo());
					}
				}
			}
		}catch(NullPointerException ne) {
		}
		finally {
		}
		System.out.println("respCode ::: " + cardListResponse.getResponseCode());
		System.out.println("respMsg  ::: " + cardListResponse.getResponseMessage());
		//.close();

		return cardListResponse;
	}
	

	public CardServiceResponseDTO generateCard(CardRequest cardRequest, String[] personalData) throws Exception {
		CardServiceResponseDTO response = new CardServiceResponseDTO();
//		response.setResponseCode("E04");
//		response.setResponseMessage("Failed at interface level");
//		AdapterInteraction.begin();
//		
//		String address = personalData[0];
//		String city = personalData[1];
//		String country = personalData[2];
//		String dob = personalData[3];
//		
//		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
//		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESC_VIRTUAL_CARD_CREATE");
//		RESTClientFactory clientFactory = RESTClientFactory.getInstance();
//		IRESTClient restClient = clientFactory.getRESTClientInstance();
//		
//		String affCode = cardRequest.getAffiliateCode();
//		String ccyCode = digx_consulting.get(affCode + "_LCY", "");
//		
//		
//		String fullName = cardRequest.getNameOnCard();
//		String firstName = fullName.split(" ")[0];
//		System.out.println("firstName: " + firstName);
//		int fName = fullName.split(" ")[0].length();
//		String lastName = fullName.substring(fName).isEmpty() ? firstName : fullName.substring(fName);
//		System.out.println("lastName: " + lastName);
//		
//		String addr = address;
//		if(addr != null && addr.length() > 50)
//			addr = address.substring(0, 50);

		try {

			GenerateVirtualCardRequestDTO generateVirtualCardRequestDTO = new GenerateVirtualCardRequestDTO();
			generateVirtualCardRequestDTO.setADDRESS("Accra Agege");
			generateVirtualCardRequestDTO.setAFFILIATE_COUNTRY("EGH");
			generateVirtualCardRequestDTO.setAMOUNT("60");
			generateVirtualCardRequestDTO.setAPP_VERSION("4.0.0.1");
			generateVirtualCardRequestDTO.setCHARGE_AMOUNT("10");
			generateVirtualCardRequestDTO.setCITY("Accra");
			generateVirtualCardRequestDTO.setCURRENCY_CODE("GHS"); // ccyCode
			generateVirtualCardRequestDTO.setCUSTOMER_ID("123300000");
			generateVirtualCardRequestDTO.setDEVICE_TYPE("");
			generateVirtualCardRequestDTO.setDOB("12-Jun-1987");
			generateVirtualCardRequestDTO.setEMAIL("test@ecobank.com");
			generateVirtualCardRequestDTO.setEMCERT_ID("");
			generateVirtualCardRequestDTO.setFIRST_NAME("Danie");
			generateVirtualCardRequestDTO.setLANG_ID("en");
			generateVirtualCardRequestDTO.setLAST_NAME("Ameyaw");
			generateVirtualCardRequestDTO.setMSISDN("233242242107");
			generateVirtualCardRequestDTO.setOTP_NUMBER("233242242107");
			generateVirtualCardRequestDTO.setSCHEME_TYPE("Visa");
			generateVirtualCardRequestDTO.setSERVICE("CREATE-VCARD");
			generateVirtualCardRequestDTO.setSOURCE_ACCOUNT("1441001601306");
			generateVirtualCardRequestDTO.setTOTAL_AMOUNT("50");
			generateVirtualCardRequestDTO.setUUID("11111");
			generateVirtualCardRequestDTO.setVCARD_TYPE("SHOPVCARD");

			Gson gson = new Gson();

			ObjectMapper mapper = new ObjectMapper();
			String jsonRequestBody = mapper.writeValueAsString(generateVirtualCardRequestDTO);

			System.out.println("jsonRequestBody: " + jsonRequestBody);
			GenerateVirtualCardResponseDTO resp = null;

			String genResponse = HttpPost("http://10.4.139.49:7003/unifiedapis/services", "generatevcard",
					generateVirtualCardRequestDTO);

			resp = gson.fromJson(genResponse, GenerateVirtualCardResponseDTO.class);

			System.out.println(":: status Code    : " + resp.getSTATUS_CODE());
			System.out.println(":: status Msg     : " + resp.getSTATUS_MESSAGE());
			System.out.println(":: AccountDetails : " + resp.getACCOUNT_DETAILS().size());

			try {
				String jsonResponseBody = mapper.writeValueAsString(resp);
				System.out.println(":: jsonResponseBody : " + jsonResponseBody);
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}

			if (resp != null && resp.getSTATUS_CODE().equals("000")) {
				System.out.println(":: accountNo      : " + resp.getACCOUNT_DETAILS().get(0).getAccountNumber());
				System.out.println(":: balance        : " + resp.getACCOUNT_DETAILS().get(0).getBalance());
				System.out.println(":: card token     : " + resp.getACCOUNT_DETAILS().get(0).getCardToken());

				response.setResponseCode("000");
				response.setResponseMessage(resp.getSTATUS_MESSAGE());
				response.setTransactionRefNo(resp.getACCOUNT_DETAILS().get(0).getCardAlias());
				response.setCustomerNo(resp.getACCOUNT_DETAILS().get(0).getCustId());

				String panx = resp.getACCOUNT_DETAILS().get(0).getCardAlias();
				String maskedPan = panx.substring(0, 6) + "****" + panx.substring(panx.length() - 4);
				String expDate = resp.getACCOUNT_DETAILS().get(0).getExpiry();

				response.setMaskedPan(maskedPan);
				response.setPseudoPAN(resp.getACCOUNT_DETAILS().get(0).getCardToken());
				response.setLast4Digits(panx.substring(panx.length() - 4));
				response.setCvv(resp.getACCOUNT_DETAILS().get(0).getCvv());
				response.setFullPan(panx);

				response.setNameOnCard(resp.getACCOUNT_DETAILS().get(0).getCardName());

				SimpleDateFormat dff4 = new SimpleDateFormat("dd-MMM-yyyy");
				java.util.Date dtx;

				dtx = dff4.parse(expDate);

				SimpleDateFormat dff5 = new SimpleDateFormat("MM/YY");
				String expiryDate = dff5.format(dtx);
				response.setExpiryDate(expiryDate);

				String cardData = panx + "|" + resp.getACCOUNT_DETAILS().get(0).getCardToken(); // + "|" +
																								// resp.getAccountDetails().get(0).getCustId()
				response.setCardData(cardData);
			}

		} catch (ParseException e) {
			e.printStackTrace();
		} catch (JsonProcessingException e1) {
			e1.printStackTrace();
		}
		return response;
	}
	
	
	
	public static String HttpPost(String contextUrl, String serviceUrl, Object payloadRequest) {

		StringBuffer resp = new StringBuffer();
		URL obj = null;
		Gson gson = new Gson();
		try {
			obj = new URL(contextUrl + "/" + serviceUrl);

			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			con.setRequestMethod("POST");
			con.setRequestProperty("Content-Type", "application/json");

			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(gson.toJson(payloadRequest));

			wr.flush();
			wr.close();
			String responseStatus = con.getResponseMessage();
			System.out.println(responseStatus);
			int responseCode = con.getResponseCode();
			System.out.println("responseCode " + responseCode);

			String inputLine;

			if (responseCode == 200) {
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				while ((inputLine = in.readLine()) != null) {
					resp.append(inputLine);
				}
				in.close();
				System.out.println("response:" + resp.toString());
			} else {
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getErrorStream()));
				while ((inputLine = in.readLine()) != null) {
					resp.append(inputLine);
				}
				in.close();
				System.out.println("response 2" + resp.toString());
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		return resp.toString();

	}
	
	
	public CardServiceResponseDTO createCardRequest(CardRequest cardRequest, String branchCode, String deliveryBranch)
			throws Exception {
		CardServiceResponseDTO response = new CardServiceResponseDTO();
		response.setResponseCode("E04");
		response.setResponseMessage("Failed at interface level");
		AdapterInteraction.begin();

		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESB_DEBIT_CARD_CREATE");
		
		
//		try {
//			enableSSL();
//		} catch (KeyManagementException | NoSuchAlgorithmException e) {
//			e.printStackTrace();
//		}
		
		
		//RESTClientFactory clientFactory = RESTClientFactory.getInstance();
		//IRESTClient restClient = clientFactory.getRESTClientInstance();

		//String url = digx_consulting.get("ESB_DEBIT_CARD_URL", "");
		//String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String sourceCode = "SMARTTELLER";
		//String ipx = digx_consulting.get("ESB_OBDX_IP", "");

		String requestId = cardRequest.getRefKey().getExternalRefNo();

		//String pwd = digx_consulting.get("ESB_OBDX_PWD", "");
		String requestToken = JEncrypt.GenerateTransactionToken(sourceCode, requestId, "");

		SimpleDateFormat dff4 = new SimpleDateFormat("yyyy-MM-dd");
		java.util.Date dt = new java.util.Date();
		String dateCurrent = dff4.format(dt);

		System.out.println("sourceAccountNo: " + cardRequest.getSourceAccountNo());
		
		ESBCardRequestDTO esbRequestDTO = new ESBCardRequestDTO();
		HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
		hostHeaderInfo.setAffiliateCode(cardRequest.getAffiliateCode());
		hostHeaderInfo.setIpAddress("127.0.0.1");
		hostHeaderInfo.setRequestId(requestId);
		hostHeaderInfo.setRequestToken(requestToken);
		hostHeaderInfo.setRequestType("DEBITCARDREQUEST");
		hostHeaderInfo.setSourceChannelId("WEB");
		hostHeaderInfo.setSourceCode(sourceCode);
		esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);
        
		
		esbRequestDTO.setAccountNo(cardRequest.getSourceAccountNo());
		//esbRequestDTO.setAccountNo(cardRequest.getCardAccountNo());
		esbRequestDTO.setCardScheme(cardRequest.getSchemeType());
		esbRequestDTO.setCardProduct("1");
		esbRequestDTO.setRequestBranch(branchCode);
		esbRequestDTO.setCollectionBranch(deliveryBranch.substring(0, 3));
		esbRequestDTO.setPersonalized("Y");
		esbRequestDTO.setCreatedBy("OBDX");
		esbRequestDTO.setCreatedDate(dateCurrent);
		esbRequestDTO.setVerifiedBy("OBDX");
		esbRequestDTO.setVerifiedDate(dateCurrent);
		esbRequestDTO.setAbridgedCardNo("");

		ESBCardResponseDTO esbCardResponseDTO = null;

		try {
			
			XStream xs = new XStream();
			System.out.println("CARDSERVICES-Request : " + xs.toXML(esbRequestDTO));

			System.out.println("endpoint 1 : " + prop.getServiceURL());
			System.out.println("endpoint 2 : " + prop.getContextURL());

			

			//////
			/////
			/////
			
			//esbCardResponseDTO = gson.fromJson(resp.toString(), ESBCardResponseDTO.class);
			esbCardResponseDTO = null;
			System.out.println("::: responseMsg  ::: " + esbCardResponseDTO.getHostHeaderInfo().getResponseMessage());
			System.out.println("::: responsecode ::: " + esbCardResponseDTO.getHostHeaderInfo().getResponseCode());

			response.setResponseCode(esbCardResponseDTO.getHostHeaderInfo().getResponseCode());
			response.setResponseMessage(esbCardResponseDTO.getHostHeaderInfo().getResponseMessage());
			if (esbCardResponseDTO != null && esbCardResponseDTO.getHostHeaderInfo().getResponseCode().equals("000")) {
				response.setTransactionRefNo(esbCardResponseDTO.getHostHeaderInfo().getRequestId());
			}
			
			System.out.println("Request-Response " + response.getResponseCode());

		}
		finally {
		}
		AdapterInteraction.close();
		return null;
	}

	
	
	
	
//	public CardServiceResponseDTO generateCard() throws JsonParseException, JsonMappingException, IOException
//	{
//		CardServiceResponseDTO response = new CardServiceResponseDTO();
//		response.setResponseCode("E04");
//		response.setResponseMessage("Failed at interface level");
//		
//		XStream xs = new XStream();
//		
//		GenerateVirtualCardRequestDTO generateVirtualCardRequestDTO = new GenerateVirtualCardRequestDTO();
//		generateVirtualCardRequestDTO.setAddress("MICHAEL OWOBI EDEH RESIDENCE BEHIND SOLO HOUSE AZUBA BASHA SHABU");
//		generateVirtualCardRequestDTO.setAffiliateCountry("ENG");
//		generateVirtualCardRequestDTO.setAmount("100");
//		generateVirtualCardRequestDTO.setAppVersion("4.0.0.1");
//		generateVirtualCardRequestDTO.setChargeAmount("10");
//		generateVirtualCardRequestDTO.setCity("NIGERIA");
//		generateVirtualCardRequestDTO.setCurrencyCode("GHS"); //ccyCode
//		generateVirtualCardRequestDTO.setCustomerId("22202003030303");
//		generateVirtualCardRequestDTO.setDeviceType("");
//		generateVirtualCardRequestDTO.setDob("1987-06-18");
//		generateVirtualCardRequestDTO.setEmail("test@ecobank.com");
//		generateVirtualCardRequestDTO.setEmcertId("");
//		generateVirtualCardRequestDTO.setFirstName("Felix");
//		generateVirtualCardRequestDTO.setLangId("en");
//		generateVirtualCardRequestDTO.setLastName("Osei");
//		generateVirtualCardRequestDTO.setMsisdn("233242242107");
//		generateVirtualCardRequestDTO.setOptNumber("233242242107");
//		generateVirtualCardRequestDTO.setSchemType("Visa");
//		generateVirtualCardRequestDTO.setService("CREATE-VCARD");
//		generateVirtualCardRequestDTO.setSourceAccount("1000333202211");
//		generateVirtualCardRequestDTO.setTotalAmount(Double.toString(60));
//		generateVirtualCardRequestDTO.setUuid("");
//		generateVirtualCardRequestDTO.setVcardType("SHOPVCARD");
//		
//		
//		GenerateVirtualCardResponseDTO resp = null;
//		
//		try {
//			
//			
//			
//			ObjectMapper mapper = new ObjectMapper();
//
//			 String jsonRequestBody = mapper.writeValueAsString(generateVirtualCardRequestDTO);
//
//			 System.out.println(">>> ::: " + jsonRequestBody);
//			
//			System.out.println("Generate Virtual Card Request: " + xs.toXML(generateVirtualCardRequestDTO));
//			
//			String json = "{\n" + 
//					"    \"STATUS_CODE\": \"V01\",\n" + 
//					"    \"STATUS_MESSAGE\": \"Sorry, we could not create your card. Please chat with us to get this sorted.\",\n" + 
//					"    \"ACCOUNT_DETAILS\": []\n" + 
//					"}";
//			
//			
//			
//			ObjectMapper objectMapper = new ObjectMapper();
//			
//			GenerateVirtualCardResponseDTO product = objectMapper.readValue(json, GenerateVirtualCardResponseDTO.class);
//			
//			resp = (GenerateVirtualCardResponseDTO) product;
//			
//			String jsonResponseBody = mapper.writeValueAsString(resp);
//			System.out.println("jsonResponseBody: " + jsonResponseBody);
//			System.out.println(":: status Code :::::     : " + resp.getSTATUS_CODE());
//			System.out.println(":: status Msg     : " + resp.getStatusMessage());
//						
//			if(resp != null && resp.getSTATUS_CODE().equals("000"))
//			{
//				if(resp.getAccountDetails().size() > 0)
//				{
//					System.out.println(":: accountNo      : " + resp.getAccountDetails().get(0).getAccountNumber());
//					System.out.println(":: balance        : " + resp.getAccountDetails().get(0).getBalance());
//					System.out.println(":: card token     : " + resp.getAccountDetails().get(0).getCardToken());
//
//					response.setResponseCode("000");
//					response.setResponseMessage(resp.getStatusMessage());
//					response.setTransactionRefNo(resp.getAccountDetails().get(0).getCardAlias());
//					response.setCustomerNo(resp.getAccountDetails().get(0).getCustId());
//					
//					String panx = resp.getAccountDetails().get(0).getCardAlias();
//					String maskedPan = panx.substring(0,6) + "****" + panx.substring(panx.length() - 4);
//					String expDate = resp.getAccountDetails().get(0).getExpiry();
//					
//					response.setMaskedPan(maskedPan);
//					response.setPseudoPAN(resp.getAccountDetails().get(0).getCardToken());
//					response.setLast4Digits(panx.substring(panx.length() - 4));
//					response.setCvv(resp.getAccountDetails().get(0).getCvv());
//					response.setFullPan(panx);
//				
//					
//					response.setNameOnCard(resp.getAccountDetails().get(0).getCardName());
//					
//					SimpleDateFormat dff4 = new SimpleDateFormat("dd-MMM-yyyy");
//					//java.util.Date dtx = new java.util.Date();
//					java.util.Date dtx = dff4.parse(expDate);
//					SimpleDateFormat dff5 = new SimpleDateFormat("MM/YY");
//					String expiryDate = dff5.format(dtx);
//					response.setExpiryDate(expiryDate);
//					
//					String cardData = panx + "|" + resp.getAccountDetails().get(0).getCardToken(); // + "|" + resp.getAccountDetails().get(0).getCustId()
//					response.setCardData(cardData);
//					
//				}
//				
//				
//			}
//		} catch (ParseException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} finally {
//		}
//		AdapterInteraction.close();
//	    return response;
//	}


}
