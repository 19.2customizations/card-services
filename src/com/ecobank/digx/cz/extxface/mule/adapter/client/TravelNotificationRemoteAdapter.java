package com.ecobank.digx.cz.extxface.mule.adapter.client;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import java.util.Random;


import java.util.logging.Logger;
import java.util.prefs.Preferences;

import com.ecobank.digx.cz.app.cardservices.dto.CardServiceResponseDTO;

import com.ecobank.digx.cz.app.cardservices.dto.TravelNotificationRequestDTO;
import com.ecobank.digx.cz.app.domain.cardservices.entity.CardActivityInfo;
import com.ecobank.digx.cz.app.domain.cardservices.entity.CardRequest;
import com.ecobank.digx.cz.extxface.mule.dto.GenerateVirtualCardRequestDTO;
import com.ecobank.digx.cz.extxface.mule.dto.GenerateVirtualCardResponseDTO;
import com.ecobank.digx.cz.extxface.mule.dto.HostHeaderInfo;
import com.ecobank.digx.cz.extxface.mule.dto.TravelNotificationResponseDTO;

import com.ecobank.digx.cz.app.domain.cardservices.entity.CardRequest;
import com.ecobank.digx.cz.extxface.mule.dto.GenerateVirtualCardRequestDTO;
import com.ecobank.digx.cz.extxface.mule.dto.GenerateVirtualCardResponseDTO;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.ofss.digx.app.core.AdapterInteraction;
import com.ofss.digx.extxface.response.AbstractResponseHandler;
import com.ofss.digx.framework.rest.client.RESTClientRequestBuilder;
import com.ofss.digx.framework.rest.client.RESTClientRequestConfig;
import com.ofss.digx.infra.exceptions.Exception;
import com.ofss.fc.infra.config.ConfigurationFactory;
import com.ofss.fc.infra.log.impl.MultiEntityLogger;

public class TravelNotificationRemoteAdapter extends AbstractResponseHandler{
	
	private static TravelNotificationRemoteAdapter singletonInstance;

	private Preferences digx_consulting = ConfigurationFactory.getInstance().getConfigurations("DIGXCONSULTING");

	private static final String THIS_COMPONENT_NAME = TravelNotificationRemoteAdapter.class.getName();
	private static transient Logger logger = MultiEntityLogger.getUniqueInstance().getLogger(THIS_COMPONENT_NAME);
	
	public static TravelNotificationRemoteAdapter getInstance() {
		if (singletonInstance == null)
			synchronized (TravelNotificationRemoteAdapter.class) {
				if (singletonInstance == null)
					singletonInstance = new TravelNotificationRemoteAdapter();
			}
		return singletonInstance;
	}
	
	public CardServiceResponseDTO travelNotification(CardActivityInfo cardActivity) throws Exception 
	{
		CardServiceResponseDTO response = null;
		TravelNotificationRequestDTO travelNotificationRequestDTO = new TravelNotificationRequestDTO();
		
		String cardAccount = cardActivity.getCardAccount();
		String mobile = cardActivity.getMobileNo();
		String udf3 = cardActivity.getUdf3();
		String[] udfs = udf3.split("|");
		
		travelNotificationRequestDTO.setCardAccounts(cardAccount);
	      travelNotificationRequestDTO.setMobileNo(mobile);
	      travelNotificationRequestDTO.setPurposeOfTravel(udfs[1]);
	      travelNotificationRequestDTO.setReturnDate(cardActivity.getUdf1());
	      travelNotificationRequestDTO.setTravelDate(cardActivity.getUdf2());
	      travelNotificationRequestDTO.setTravelDestination(udfs[0]);
		response = travelNotification(travelNotificationRequestDTO);
		
		
		return response;
		
		
	}

	public CardServiceResponseDTO travelNotification(TravelNotificationRequestDTO travelNotifRequestDTO) throws Exception {
	    CardServiceResponseDTO response = new CardServiceResponseDTO();
	    response.setResponseCode("E04");
	    response.setResponseMessage("Failed at interface level");
	    AdapterInteraction.begin();
	    RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
	    RESTClientRequestConfig prop = clientBuilder.loadConfig("ESC_TRAVEL_NOTIF_CREATE");
	    try {
	      String url = this.digx_consulting.get("ESC_TRAVEL_NOTIF_CREATE", "");
	      String sourceCode = this.digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
	      String ipx = this.digx_consulting.get("ESB_OBDX_IP", "");
	      String pwd = this.digx_consulting.get("ESB_OBDX_PWD", "");
	      String genRef = GetRefNumber("TN", 12);
	      String requestToken = JEncrypt.GenerateTransactionToken(sourceCode, genRef, pwd);
	      TravelNotificationRequestDTO travelNotificationRequestDTO = new TravelNotificationRequestDTO();
	      HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
	      hostHeaderInfo.setAffiliateCode(travelNotifRequestDTO.getAffiliateCode());
	      hostHeaderInfo.setIpAddress(ipx);
	      hostHeaderInfo.setRequestId(genRef);
	      hostHeaderInfo.setRequestToken(requestToken);
	      hostHeaderInfo.setRequestType("TRAVEL_NOTIFICATION");
	      hostHeaderInfo.setSourceChannelId("MOBILE");
	      hostHeaderInfo.setSourceCode(sourceCode);
	      travelNotificationRequestDTO.setHostHeaderInfo(hostHeaderInfo);
	      travelNotificationRequestDTO.setAccountAlias("N/A");
	      travelNotificationRequestDTO.setCardAccounts(travelNotifRequestDTO.getCardAccounts());
	      travelNotificationRequestDTO.setMobileNo(travelNotifRequestDTO.getMobileNo());
	      travelNotificationRequestDTO.setPurposeOfTravel(travelNotifRequestDTO.getPurposeOfTravel());
	      travelNotificationRequestDTO.setReturnDate(travelNotifRequestDTO.getReturnDate());
	      travelNotificationRequestDTO.setTravelDate(travelNotifRequestDTO.getTravelDate());
	      travelNotificationRequestDTO.setTravelDestination(travelNotifRequestDTO.getTravelDestination());
	      Gson gson = new Gson();
	      ObjectMapper mapper = new ObjectMapper();
	      String jsonRequestBody = mapper.writeValueAsString(travelNotificationRequestDTO);
	      System.out.println("jsonRequestBody: " + jsonRequestBody);
	      System.out.println("URL: " + url);
	      TravelNotificationResponseDTO resp = null;
	      String genResponse = HttpPost(url, travelNotificationRequestDTO);
	      try {
	        resp = (TravelNotificationResponseDTO)gson.fromJson(genResponse, TravelNotificationResponseDTO.class);
	        String jsonResponseBody = mapper.writeValueAsString(resp);
	        System.out.println(":: jsonResponseBody : " + jsonResponseBody);
	      } catch (JsonProcessingException|com.google.gson.JsonSyntaxException ex) {
	        ex.printStackTrace();
	      } 
	      response.setResponseCode(resp.getHostHeaderInfo().getResponseCode());
	      response.setResponseMessage(resp.getHostHeaderInfo().getResponseMessage());
	      if (resp != null && resp.getHostHeaderInfo().getResponseCode().equals("000")) {
	        System.out.println(":: travelRequestId      : " + resp.getTravelRequestId());
	        response.setExternalRefNo(Integer.toString(resp.getTravelRequestId()));
	      } 
	      
	      
	    } catch (JsonProcessingException e1) {
	      e1.printStackTrace();
	    } catch (NullPointerException nullPointerException) {}
	    return response;
	  }
	  

	
	
	public static String HttpPost(String contextUrl,  Object payloadRequest) {
	    StringBuffer resp = new StringBuffer();
	    URL obj = null;
	    Gson gson = new Gson();
	    try {
	    	
	    	System.out.println("URL " + contextUrl);
	    	
	    	if(contextUrl.startsWith("https"))
			{
	    		return HttpsPost( contextUrl,payloadRequest);

			}
			
		
	    	
	      obj = new URL(contextUrl);
	      HttpURLConnection con = (HttpURLConnection)obj.openConnection();
	      con.setRequestMethod("POST");
	      con.setRequestProperty("Content-Type", "application/json");
	      con.setDoOutput(true);
	      DataOutputStream wr = new DataOutputStream(con.getOutputStream());
	      wr.writeBytes(gson.toJson(payloadRequest));
	      wr.flush();
	      wr.close();
	      String responseStatus = con.getResponseMessage();
	      System.out.println(responseStatus);
	      int responseCode = con.getResponseCode();
	      System.out.println("responseCode " + responseCode);
	      if (responseCode == 200) {
	        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
	        String inputLine;
	        while ((inputLine = in.readLine()) != null)
	          resp.append(inputLine); 
	        in.close();
	      } else {
	        BufferedReader in = new BufferedReader(new InputStreamReader(con.getErrorStream()));
	        String inputLine;
	        while ((inputLine = in.readLine()) != null)
	          resp.append(inputLine); 
	        in.close();
	      } 
	    } catch (IOException e) {
	      e.printStackTrace();
	    } 
	    return resp.toString();
	  }
	  
	  public static String HttpsPost(String contextUrl,  Object payloadRequest) {
		    StringBuffer resp = new StringBuffer();
		    URL obj = null;
		    Gson gson = new Gson();
		    try {
		    	
		    	System.out.println("URL " + contextUrl);
		      obj = new URL(contextUrl);
		      HttpURLConnection con = (HttpURLConnection)obj.openConnection();
		      con.setRequestMethod("POST");
		      con.setRequestProperty("Content-Type", "application/json");
		      con.setDoOutput(true);
		      DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		      wr.writeBytes(gson.toJson(payloadRequest));
		      wr.flush();
		      wr.close();
		      String responseStatus = con.getResponseMessage();
		      System.out.println(responseStatus);
		      int responseCode = con.getResponseCode();
		      System.out.println("responseCode " + responseCode);
		      if (responseCode == 200) {
		        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		        String inputLine;
		        while ((inputLine = in.readLine()) != null)
		          resp.append(inputLine); 
		        in.close();
		      } else {
		        BufferedReader in = new BufferedReader(new InputStreamReader(con.getErrorStream()));
		        String inputLine;
		        while ((inputLine = in.readLine()) != null)
		          resp.append(inputLine); 
		        in.close();
		      } 
		    } catch (IOException e) {
		      e.printStackTrace();
		    } 
		    return resp.toString();
		  }
	  

	
	public  String GetRefNumber(String type, int len) {

        String finalString = "";
        int x = 0;
        char[] stringChars = new char[len];
        for (int i = 0; i < len; i++) //4
        {
            Random random = new Random();
            x = random.nextInt(9);

            stringChars[i] = Integer.toString(x).toCharArray()[0];
        }


        finalString = new String(stringChars);
        finalString = type + finalString;
        return finalString.trim();
    }


}
