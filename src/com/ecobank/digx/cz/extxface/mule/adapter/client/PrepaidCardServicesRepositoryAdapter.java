package com.ecobank.digx.cz.extxface.mule.adapter.client;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.InvalidKeyException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.Random;
import java.util.logging.Logger;
import java.util.prefs.Preferences;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import com.ecobank.digx.cz.app.cardservices.dto.CardBalanceDTO;
import com.ecobank.digx.cz.app.cardservices.dto.CardBalanceResponse;
import com.ecobank.digx.cz.app.cardservices.dto.CardServiceResponseDTO;
import com.ecobank.digx.cz.app.cardservices.dto.CardStatementListResponse;
import com.ecobank.digx.cz.app.cardservices.dto.CardStatementRequestDTO;
import com.ecobank.digx.cz.app.domain.cardservices.entity.CardActivityInfo;
import com.ecobank.digx.cz.eco.domain.generic.entity.genericlogger.GenericLoggerConfiguration;
import com.ecobank.digx.cz.extxface.mule.dto.ESBCardRequestDTO;
import com.ecobank.digx.cz.extxface.mule.dto.ESBCardResponseDTO;
import com.ecobank.digx.cz.extxface.mule.dto.ESBPrepaidCardDetailResponseDTO;
import com.ecobank.digx.cz.extxface.mule.dto.HostHeaderInfo;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.ofss.digx.app.core.AdapterInteraction;
import com.ofss.digx.extxface.response.AbstractResponseHandler;
import com.ofss.digx.framework.rest.client.RESTClientRequestBuilder;
import com.ofss.digx.framework.rest.client.RESTClientRequestConfig;
import com.ofss.digx.infra.exceptions.Exception;
import com.ofss.fc.infra.config.ConfigurationFactory;
import com.ofss.fc.infra.log.impl.MultiEntityLogger;
import com.ofss.fc.infra.thread.ThreadAttribute;
import com.thoughtworks.xstream.XStream;

public class PrepaidCardServicesRepositoryAdapter extends AbstractResponseHandler{
	
	private static PrepaidCardServicesRepositoryAdapter singletonInstance;

	private Preferences digx_consulting = ConfigurationFactory.getInstance().getConfigurations("DIGXCONSULTING");

	private static final String THIS_COMPONENT_NAME = PrepaidCardServicesRepositoryAdapter.class.getName();
	private static transient Logger logger = MultiEntityLogger.getUniqueInstance().getLogger(THIS_COMPONENT_NAME);
	
	private static final GenericLoggerConfiguration serviceLogger = new GenericLoggerConfiguration();
    private Preferences eco_generic = ConfigurationFactory.getInstance().getConfigurations("ECOGENERIC");
	
    
	public static PrepaidCardServicesRepositoryAdapter getInstance() {
		if (singletonInstance == null)
			synchronized (PrepaidCardServicesRepositoryAdapter.class) {
				if (singletonInstance == null)
					singletonInstance = new PrepaidCardServicesRepositoryAdapter();
			}
		return singletonInstance;
	}	
	
	
	public CardServiceResponseDTO fundCard(String affCode, CardActivityInfo caInfo,String branchCode) throws Exception {
		CardServiceResponseDTO response = new CardServiceResponseDTO();
		response.setResponseCode("E04");
		response.setResponseMessage("Failed at interface level");
		AdapterInteraction.begin();
		
		com.ofss.fc.datatype.Date startDateTime = new com.ofss.fc.datatype.Date();

		Boolean exceptionFlag = Boolean.valueOf(false);
		String jsonResponseBody ="";
		String jsonRequestBody ="";
		String correlationId = GetRefNumber("BC", 12);

		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESB_PREPAID_CARD_FUND");
		try {
			enableSSL();
		} catch (KeyManagementException | NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		
		String requestId = GetRefNumber("BC", 12);
		String url = digx_consulting.get("ESB_PREPAID_CARD_FUND", "");
		String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String pwd = digx_consulting.get("ESB_OBDX_PWD", "");
		String requestToken = JEncrypt.GenerateTransactionToken(sourceCode, requestId, pwd);
		String ip = digx_consulting.get("ESB_OBDX_IP", "");
		String key = digx_consulting.get("ESB_OBDX_AES_KEY", "");
		
		ESBCardResponseDTO esbCardResponseDTO = null;

		try {
			ESBCardRequestDTO esbRequestDTO = new ESBCardRequestDTO();
			HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
			hostHeaderInfo.setAffiliateCode(affCode); //temporarily hardcoded
			if(affCode.equals("ELR") || affCode.equals("ECV") || affCode.equals("EZW") || affCode.equals("ECD") )
				hostHeaderInfo.setAffiliateCode(affCode + "USD");
			
			System.out.println("PREPAID-CARD-USD-AFF-CODE " + hostHeaderInfo.getAffiliateCode() );
			
			hostHeaderInfo.setIpAddress(ip);
			hostHeaderInfo.setRequestId(caInfo.getRefKey().getRefId());
			hostHeaderInfo.setRequestToken(requestToken);
			hostHeaderInfo.setRequestType("MPAY");
			hostHeaderInfo.setSourceChannelId("MOBILE");
			hostHeaderInfo.setSourceCode(sourceCode);
			esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);
			String expiryDate = "";
			String name = caInfo.getName();
			String cvv ="";
			String last4Digits = "";

			
			try {
				System.out.println("card data : " + caInfo.getCardDat());
				if (caInfo.getCardDat() != null && !caInfo.getCardDat().equals( "")) {
					
					String cdx = caInfo.getCardDat();
					cdx = JEncrypt.decrypt(cdx);
					//System.out.println("card data : " + cdx);
					 
					
					//logger.info("customerID : " + caInfo.getCardDat().split("\\|")[0]);
					//logger.info("expiryDate : " + caInfo.getCardDat().split("\\|")[1]);
					//logger.info("maskedPan  : " + caInfo.getCardDat().split("\\|")[2]);
					//logger.info("cvv        : " + caInfo.getCardDat().split("\\|")[3]);
					String expDate = cdx.split("\\|")[1];
					String month = expDate.split("/")[0];
					String year = expDate.split("/")[1].substring(2);
					expiryDate = year + month;
					System.out.println("expDate: " + expDate);

					last4Digits = cdx.split("\\|")[2];
					cvv = cdx.split("\\|")[3];
					//System.out.println("last 4 digits: " + last4Digits);
				}
			} catch (NullPointerException ne) {
				ne.printStackTrace(System.out);
			}
	
			//logger.info("card data : " + caInfo.getCardDat());
			
			/*if(caInfo.getCardDat() !=null || caInfo.getCardDat() != "") {
				logger.info("customerID : " + caInfo.getCardDat().split("\\|")[0]);
				logger.info("expiryDate : " + caInfo.getCardDat().split("\\|")[1]);
				logger.info("maskedPan  : " + caInfo.getCardDat().split("\\|")[2]);
				logger.info("cvv        : " + caInfo.getCardDat().split("\\|")[3]);
				String expDate = caInfo.getCardDat().split("\\|")[1];
				String month = expDate.split("/")[0];
				String year =  expDate.split("/")[1].substring(2);
				expiryDate = year+month;
				logger.info("expDate: " + expDate);
				
				last4Digits = caInfo.getCardDat().split("\\|")[2];
				cvv = caInfo.getCardDat().split("\\|")[3];
				
			}*/
			

			esbRequestDTO.setFirstName(name != null ? AESEncrypterOriginal.encrypt(name, AESEncrypterOriginal.getEncryptionKey(key)) : "");
			esbRequestDTO.setLastName(name != null ?AESEncrypterOriginal.encrypt(name, AESEncrypterOriginal.getEncryptionKey(key)) : "");
			esbRequestDTO.setCvv(cvv != null ? AESEncrypterOriginal.encrypt(cvv, AESEncrypterOriginal.getEncryptionKey(key)) : "");
			esbRequestDTO.setLast4Digits(last4Digits != null ? AESEncrypterOriginal.encrypt(last4Digits, AESEncrypterOriginal.getEncryptionKey(key)) : "");
			esbRequestDTO.setBalance(""+caInfo.getAmount() != null ? AESEncrypterOriginal.encrypt(""+caInfo.getAmount(), AESEncrypterOriginal.getEncryptionKey(key)) : "");

			esbRequestDTO.setAccountid(caInfo.getSourceAccount() != null ? AESEncrypterOriginal.encrypt(caInfo.getSourceAccount(), AESEncrypterOriginal.getEncryptionKey(key)) : "");
    		esbRequestDTO.setAccountBrn(branchCode != null ? AESEncrypterOriginal.encrypt(branchCode, AESEncrypterOriginal.getEncryptionKey(key)) : "");
			//esbRequestDTO.setAccountid("8pHGtVWTZM459ioJ5G9B+g==");
			//esbRequestDTO.setAccountBrn("vdZCZ8Urb2aO8M5Ts+rDMg==");

		//esbRequestDTO.setAccountid(caInfo.getSourceAccount() != null ? AESEncrypterOriginal.encrypt(caInfo.getSourceAccount(), AESEncrypterOriginal.getEncryptionKey(key)) : "");
		//	esbRequestDTO.setAccountBrn(branchCode != null ? AESEncrypterOriginal.encrypt(branchCode, AESEncrypterOriginal.getEncryptionKey(key)) : "");
			//esbRequestDTO.setAccountid("8pHGtVWTZM459ioJ5G9B+g==");
			//esbRequestDTO.setAccountBrn("vdZCZ8Urb2aO8M5Ts+rDMg==");

			esbRequestDTO.setCurrency(caInfo.getCcy() != null ? AESEncrypterOriginal.encrypt(caInfo.getCcy(), AESEncrypterOriginal.getEncryptionKey(key)) : "");
			esbRequestDTO.setCustomerID(caInfo.getCardCustomerId() != null ? AESEncrypterOriginal.encrypt(caInfo.getCardCustomerId(), AESEncrypterOriginal.getEncryptionKey(key)) : "");
			esbRequestDTO.setMobileNo(caInfo.getMobileNo() != null ? AESEncrypterOriginal.encrypt(caInfo.getMobileNo(), AESEncrypterOriginal.getEncryptionKey(key)) : "");
			esbRequestDTO.setExpiryDate(expiryDate != null ? AESEncrypterOriginal.encrypt(expiryDate, AESEncrypterOriginal.getEncryptionKey(key)) : "");
			esbRequestDTO.setGtpCustID(caInfo.getCardCustomerId() != null ? AESEncrypterOriginal.encrypt(caInfo.getCardCustomerId(), AESEncrypterOriginal.getEncryptionKey(key)) : "");
			esbRequestDTO.setCardType(AESEncrypterOriginal.encrypt("SHOPVCARD", AESEncrypterOriginal.getEncryptionKey(key)));
			XStream xs = new XStream();
			logger.info("Fund Prepaid Card Request : " + xs.toXML(esbRequestDTO));
			logger.info("Fund Prepaid Card Source Code : " + sourceCode);
			
			logger.info("Prepaid Endpoint 1 : " + prop.getServiceURL());
			logger.info("Prepaid Endpoint 2 : " + prop.getContextURL());

			Gson gson = new Gson();


				ObjectMapper mapper = new ObjectMapper();
				jsonRequestBody = mapper.writeValueAsString(esbRequestDTO);
				System.out.println(":: jsonRequestBody : " + jsonRequestBody);
			
			
				String genResponse = HttpPost(url, esbRequestDTO);
				esbCardResponseDTO = gson.fromJson(genResponse, ESBCardResponseDTO.class);
				
				//esbCardResponseDTO = gson.fromJson(MockServcie.fundPrepaidCard(), ESBCardResponseDTO.class);
				
				
				jsonResponseBody = mapper.writeValueAsString(esbCardResponseDTO);
				logger.info(":: jsonResponseBody : " + jsonResponseBody);
				logger.info("Fund Prepaid Card getResponseCode : " + esbCardResponseDTO.getHostHeaderInfo().getResponseCode());
				logger.info("Fund Prepaid Card getResponseMsg  : " + esbCardResponseDTO.getHostHeaderInfo().getResponseMessage());
			
			

			/*try {
				ObjectMapper mapper = new ObjectMapper();
				String jsonRequestBody = mapper.writeValueAsString(esbRequestDTO);
				System.out.println(":: jsonRequestBody : " + jsonRequestBody);
			} catch (JsonProcessingException | com.google.gson.JsonSyntaxException ex) {
				ex.printStackTrace();
			}
			
			String genResponse = HttpPost(prop.getContextURL(), prop.getServiceURL(), esbRequestDTO);
			*/
			/*try {
				esbCardResponseDTO = gson.fromJson(genResponse, ESBCardResponseDTO.class);
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponseBody = mapper.writeValueAsString(esbCardResponseDTO);
				logger.info(":: jsonResponseBody : " + jsonResponseBody);
				logger.info("Fund Prepaid Card getResponseCode : " + esbCardResponseDTO.getHostHeaderInfo().getResponseCode());
				logger.info("Fund Prepaid Card getResponseMsg  : " + esbCardResponseDTO.getHostHeaderInfo().getResponseMessage());
			} catch (JsonProcessingException | com.google.gson.JsonSyntaxException ex) {
				ex.printStackTrace();
			}*/
			

			response.setResponseCode(esbCardResponseDTO.getHostHeaderInfo().getResponseCode());
			response.setResponseMessage(esbCardResponseDTO.getHostHeaderInfo().getResponseMessage());
			
			if (esbCardResponseDTO != null && esbCardResponseDTO.getHostHeaderInfo().getResponseCode().equals("000")) {
				response.setTransactionRefNo(esbCardResponseDTO.getHostHeaderInfo().getRequestId());
			}
		} catch (InvalidKeyException | NoSuchPaddingException | NoSuchAlgorithmException | BadPaddingException
				| IllegalBlockSizeException e1) {
			e1.printStackTrace();
		}
	catch(NullPointerException ne) {

	} catch (JsonProcessingException e) {
		exceptionFlag = Boolean.valueOf(true);

	      //if ("Y".equalsIgnoreCase(this.eco_generic.get("PREPAIDCARD_TOPUP.INTERFACE_LOGGER_REQUIRED", "")))
        {
          serviceLogger.dbLogger((Object)jsonRequestBody, (Object)e.getMessage(), "PREPAIDCARD_TOPUP",correlationId,"EXCEPTION",ThreadAttribute.get("SUBJECT_NAME").toString(),ThreadAttribute.get("TARGET_UNIT_CODE").toString(), startDateTime);

	       }
	      
		e.printStackTrace();
	} finally {
		//if ("Y".equalsIgnoreCase(this.eco_generic.get("PREPAIDCARD_TOPUP.INTERFACE_LOGGER_REQUIRED", "")) && !exceptionFlag.booleanValue())
        {
           serviceLogger.dbLogger((Object)jsonRequestBody , (Object)jsonResponseBody , "PREPAIDCARD_TOPUP",correlationId,"",ThreadAttribute.get("SUBJECT_NAME").toString(),ThreadAttribute.get("TARGET_UNIT_CODE").toString(), startDateTime);

        }
		

	} 
		AdapterInteraction.close();

		return response;
	}
	
	

	public CardBalanceResponse balanceCheck(String affCode,CardBalanceDTO requestDTO) throws Exception {

		logger.info("Inside balanceCheck : ");
		CardBalanceResponse response = new CardBalanceResponse();
		response.setResponseCode("E04");
		response.setResponseMessage("Failed at interface level");
		AdapterInteraction.begin();

		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESC_PREPAID_CARD_BALANCE");
		try {
			enableSSL();
		} catch (KeyManagementException | NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		
		String requestId = GetRefNumber("BC", 12);
		String url = digx_consulting.get("ESC_PREPAID_CARD_BALANCE", "");
		String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String pwd = digx_consulting.get("ESB_OBDX_PWD", "");
		String requestToken = JEncrypt.GenerateTransactionToken(sourceCode, requestId, pwd);
		String ip = digx_consulting.get("ESB_OBDX_IP", "");
		String key = digx_consulting.get("ESB_OBDX_AES_KEY", "");

		ESBCardResponseDTO esbCardResponseDTO = null;

		try {
			String expiryDate = requestDTO.getExpiryDate();
			String pan = requestDTO.getMaskedPan();
			String last4 = pan;
			if(last4.length() > 4)
				last4 = pan.substring(pan.length() - 4);
			
			ESBCardRequestDTO esbRequestDTO = new ESBCardRequestDTO();
			HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
			hostHeaderInfo.setAffiliateCode(affCode); //temporarily hardcoded
			hostHeaderInfo.setIpAddress(ip);
			hostHeaderInfo.setRequestId(requestId);
			hostHeaderInfo.setRequestToken(requestToken);
			hostHeaderInfo.setRequestType("MPAY");
			hostHeaderInfo.setSourceChannelId("MOBILE");
			hostHeaderInfo.setSourceCode(sourceCode);
			esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);
			esbRequestDTO.setLast4Digits(AESEncrypterOriginal.encrypt(last4, AESEncrypterOriginal.getEncryptionKey(key)));
			esbRequestDTO.setCvv(AESEncrypterOriginal.encrypt(requestDTO.getCvv(), AESEncrypterOriginal.getEncryptionKey(key)));
			esbRequestDTO.setExpiryDate(AESEncrypterOriginal.encrypt(expiryDate, AESEncrypterOriginal.getEncryptionKey(key)));
			esbRequestDTO.setGtpCustID(AESEncrypterOriginal.encrypt(requestDTO.getCardCustomerId(), AESEncrypterOriginal.getEncryptionKey(key)));
			esbRequestDTO.setCardType(AESEncrypterOriginal.encrypt("SHOPVCARD", AESEncrypterOriginal.getEncryptionKey(key)));
			
			XStream xs = new XStream();
			logger.info("Prepaid Balance Check Request : " + xs.toXML(esbRequestDTO));
			logger.info("Prepaid Balance Check Source Code : " + sourceCode);

			Gson gson = new Gson();
			ObjectMapper mapper = new ObjectMapper();
			String jsonRequestBody = mapper.writeValueAsString(esbRequestDTO);
			System.out.println(":: jsonRequestBody 2 : " + jsonRequestBody);


			String genResponse = HttpPost(url, esbRequestDTO);
			esbCardResponseDTO = gson.fromJson(genResponse, ESBCardResponseDTO.class);
			
			//esbCardResponseDTO = gson.fromJson(MockServcie.getPrepaidBalance(), ESBCardResponseDTO.class);
			
			
			if (esbCardResponseDTO != null && esbCardResponseDTO.getHostHeaderInfo().getResponseCode().equals("000")) {

				response.setBalance(esbCardResponseDTO.getBalance() != null ? new BigDecimal(AESEncrypterOriginal.decrypt((esbCardResponseDTO.getBalance()), AESEncrypterOriginal.getEncryptionKey(key))) : new BigDecimal("0"));  
				response.setNameOnCard(esbCardResponseDTO.getName() != null ? AESEncrypterOriginal.decrypt((esbCardResponseDTO.getName()), AESEncrypterOriginal.getEncryptionKey(key)) : "");  
				response.setCcyCode(esbCardResponseDTO.getCcy() != null ? AESEncrypterOriginal.decrypt((esbCardResponseDTO.getCcy()), AESEncrypterOriginal.getEncryptionKey(key)) : "");  
				response.setPseudoPAN(esbCardResponseDTO.getPsuedoPan() != null ? AESEncrypterOriginal.decrypt((esbCardResponseDTO.getPsuedoPan()), AESEncrypterOriginal.getEncryptionKey(key)) : "");  
				response.setExpiry(esbCardResponseDTO.getExpiryDate() != null ? AESEncrypterOriginal.decrypt((esbCardResponseDTO.getExpiryDate()), AESEncrypterOriginal.getEncryptionKey(key)) : "");
				//response.setExpiry(expiryDate);
				//response.setCardAccount(AESEncrypterOriginal.decrypt((esbCardResponseDTO.), AESEncrypterOriginal.getEncryptionKey(key)));  
				//response.setCcyCode(esbCardResponseDTO.getCcy());
				response.setCardType("PREPAID");
				response.setResponseCode(esbCardResponseDTO.getHostHeaderInfo().getResponseCode());
				response.setResponseMessage(esbCardResponseDTO.getHostHeaderInfo().getResponseMessage());
				response.setResponseCode("000");
				CardBalanceResponse resp3 = this.fetchCardDetail(affCode,requestDTO);

				System.out.println("::Response Code Prepaid : " + response.getResponseCode());
				//CardBalanceResponse resp3 = this.fetchCardDetail(requestDTO);

				if(resp3 != null && resp3.getResponseCode().equals("000")) {
					response.setMobileNo(resp3.getMobileNo());
					response.setEmail(resp3.getEmail());

				}
				
				
			} else {
				response.setResponseCode(esbCardResponseDTO.getHostHeaderInfo().getResponseCode());
				response.setResponseMessage(esbCardResponseDTO.getHostHeaderInfo().getResponseMessage());

			}
		} catch (InvalidKeyException | NoSuchPaddingException | NoSuchAlgorithmException | BadPaddingException
				| IllegalBlockSizeException | IOException e1) {
			e1.printStackTrace();
		}catch(NullPointerException ne) {
		} finally {
		}
		AdapterInteraction.close();

		return response;
	}
	

	public CardBalanceResponse fetchCardDetail(String affCode,CardBalanceDTO requestDTO) throws Exception {

		logger.info("Inside fetchCardDetail : ");
		CardBalanceResponse response = new CardBalanceResponse();
		response.setResponseCode("E04");
		response.setResponseMessage("Failed at interface level");
		AdapterInteraction.begin();

		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESC_PREPAID_CARD_DETAIL");
		try {
			enableSSL();
		} catch (KeyManagementException | NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		
		String requestId = GetRefNumber("BC", 12);
		String url = digx_consulting.get("ESC_PREPAID_CARD_DETAIL", "");
		String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String pwd = digx_consulting.get("ESB_OBDX_PWD", "");
		String requestToken = JEncrypt.GenerateTransactionToken(sourceCode, requestId, pwd);
		String ip = digx_consulting.get("ESB_OBDX_IP", "");
		String key = digx_consulting.get("ESB_OBDX_AES_KEY", "");

		ESBPrepaidCardDetailResponseDTO esbCardResponseDTO = null;

		try {
			
			ESBCardRequestDTO esbRequestDTO = new ESBCardRequestDTO();
			HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
			hostHeaderInfo.setAffiliateCode(affCode); //temporarily hardcoded
			hostHeaderInfo.setIpAddress(ip);
			hostHeaderInfo.setRequestId(requestId);
			hostHeaderInfo.setRequestToken(requestToken);
			hostHeaderInfo.setRequestType("MPAY");
			hostHeaderInfo.setSourceChannelId("MOBILE");
			hostHeaderInfo.setSourceCode(sourceCode);
			esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);
			esbRequestDTO.setGtpCustID(AESEncrypterOriginal.encrypt(requestDTO.getCardCustomerId(), AESEncrypterOriginal.getEncryptionKey(key)));
			esbRequestDTO.setCardType(AESEncrypterOriginal.encrypt("SHOPVCARD", AESEncrypterOriginal.getEncryptionKey(key)));
			
			Gson gson = new Gson();

			ObjectMapper mapper = new ObjectMapper();
			String jsonRequestBody = mapper.writeValueAsString(esbRequestDTO);
			System.out.println(":: jsonRequestBody : " + jsonRequestBody);

			String genResponse = HttpPost(url, esbRequestDTO);
			esbCardResponseDTO = gson.fromJson(genResponse, ESBPrepaidCardDetailResponseDTO.class);
			
			//esbCardResponseDTO = gson.fromJson(MockServcie.prepaidCustDetails(), ESBPrepaidCardDetailResponseDTO.class);
			
			String jsonResponseBody = mapper.writeValueAsString(esbCardResponseDTO);
			System.out.println(":: jsonResponseBody : " + jsonResponseBody);

			
			logger.info("fetchCardDetail getResponseCode : " + esbCardResponseDTO.getHostHeaderInfo().getResponseCode());
			logger.info("fetchCardDetail getResponseMsg  : "+  esbCardResponseDTO.getHostHeaderInfo().getResponseMessage());
			
			if (esbCardResponseDTO != null) {
				if (esbCardResponseDTO.getHostHeaderInfo().getResponseCode().equals("000")) {
					logger.info("Card Details Successful: ");
					response.setResponseCode(esbCardResponseDTO.getHostHeaderInfo().getResponseCode());
					response.setResponseMessage(esbCardResponseDTO.getHostHeaderInfo().getResponseMessage());
					// response.setBalance(new
					// BigDecimal(AESEncrypterOriginal.decrypt((esbCardResponseDTO.getBalance()),
					// AESEncrypterOriginal.getEncryptionKey(key))));
					response.setNameOnCard(AESEncrypterOriginal.decrypt((esbCardResponseDTO.getPreferredName()),AESEncrypterOriginal.getEncryptionKey(key)));
					response.setEmail(AESEncrypterOriginal.decrypt((esbCardResponseDTO.getEmail()), AESEncrypterOriginal.getEncryptionKey(key)));
					response.setMobileNo(AESEncrypterOriginal.decrypt((esbCardResponseDTO.getMobileNo()), AESEncrypterOriginal.getEncryptionKey(key)));

					// response.setCardAccount(AESEncrypterOriginal.decrypt((esbCardResponseDTO.),
					// AESEncrypterOriginal.getEncryptionKey(key)));
					// response.setCcyCode(esbCardResponseDTO.getCcy());
					response.setCardType("PREPAID");
				} else {
					response.setResponseCode(esbCardResponseDTO.getHostHeaderInfo().getResponseCode());
					response.setResponseMessage(esbCardResponseDTO.getHostHeaderInfo().getResponseMessage());
				}
			}
			
			System.out.println(":: Final-Response-code : " + response.getResponseCode());

			/*try {
				ObjectMapper mapper = new ObjectMapper();
				String jsonRequestBody = mapper.writeValueAsString(esbRequestDTO);
				System.out.println(":: jsonRequestBody : " + jsonRequestBody);
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}
			
			String genResponse = HttpPost(prop.getContextURL(), prop.getServiceURL(), esbRequestDTO);
			
			try {
				esbCardResponseDTO = gson.fromJson(genResponse, ESBPrepaidCardDetailResponseDTO.class);
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponseBody = mapper.writeValueAsString(esbCardResponseDTO);
				System.out.println(":: jsonResponseBody : " + jsonResponseBody);
			} catch (JsonProcessingException | com.google.gson.JsonSyntaxException ex) {
				ex.printStackTrace();
			}

			esbCardResponseDTO = gson.fromJson(genResponse.toString(), ESBPrepaidCardDetailResponseDTO.class);
			logger.info("Fund Prepaid Card Response        : " + xs.toXML(genResponse));
			logger.info("Fund Prepaid Card getResponseCode : " + esbCardResponseDTO.getHostHeaderInfo().getResponseCode());
			logger.info("Fund Prepaid Card getResponseMsg  : " + esbCardResponseDTO.getHostHeaderInfo().getResponseMessage());
			logger.info("Fund Prepaid Card Mobile No       : " + esbCardResponseDTO.getMobileNo());
			response.setResponseCode(esbCardResponseDTO.getHostHeaderInfo().getResponseCode());
			response.setResponseMessage(esbCardResponseDTO.getHostHeaderInfo().getResponseMessage());
			*/
				
			
		} catch (InvalidKeyException | NoSuchPaddingException | NoSuchAlgorithmException | BadPaddingException
				| IllegalBlockSizeException | IOException e1) {
			e1.printStackTrace();
		}catch(NullPointerException ne) {
		} finally {
		}
		AdapterInteraction.close();

		return response;
	}

	
	
	
	public CardServiceResponseDTO blockCard(String affCode,String cardNo, String mobileNo) throws Exception {
		CardServiceResponseDTO response = new CardServiceResponseDTO();
		response.setResponseCode("E04");
		response.setResponseMessage("Failed at interface level");
		AdapterInteraction.begin();

		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESB_PREPAID_CARD_BLOCK");
		try {
			enableSSL();
		} catch (KeyManagementException | NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		
		String requestId = GetRefNumber("BC", 12);
		String url = digx_consulting.get("ESB_PREPAID_CARD_BLOCK", "");
		String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String pwd = digx_consulting.get("ESB_OBDX_PWD", "");
		String requestToken = JEncrypt.GenerateTransactionToken(sourceCode, requestId, pwd);
		String ip = digx_consulting.get("ESB_OBDX_IP", "");
		String key = digx_consulting.get("ESB_OBDX_AES_KEY", "");
		
		ESBCardResponseDTO esbCardResponseDTO = null;

		try {
			ESBCardRequestDTO esbRequestDTO = new ESBCardRequestDTO();
			HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
			hostHeaderInfo.setAffiliateCode(affCode); //temporarily hardcoded
			hostHeaderInfo.setIpAddress(ip);
			hostHeaderInfo.setRequestId(requestId);
			hostHeaderInfo.setRequestToken(requestToken);
			hostHeaderInfo.setRequestType("BLOCKVCARD");
			hostHeaderInfo.setSourceChannelId("MOBILE");
			hostHeaderInfo.setSourceCode(sourceCode);
			esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);
			
			esbRequestDTO.setCardNo(AESEncrypterOriginal.encrypt(cardNo, AESEncrypterOriginal.getEncryptionKey(key)));
			esbRequestDTO.setMsisdn(AESEncrypterOriginal.encrypt(mobileNo, AESEncrypterOriginal.getEncryptionKey(key)));
			
			Gson gson = new Gson();

			ObjectMapper mapper = new ObjectMapper();
			String jsonRequestBody = mapper.writeValueAsString(esbRequestDTO);
			System.out.println(":: jsonRequestBody : " + jsonRequestBody);

			String genResponse = HttpPost(url, esbRequestDTO);
			esbCardResponseDTO = gson.fromJson(genResponse, ESBCardResponseDTO.class);
			
			
			String jsonResponseBody = mapper.writeValueAsString(esbCardResponseDTO);
			System.out.println(":: jsonResponseBody : " + jsonResponseBody);

			logger.info("Block Prepaid Card getResponseCode : " + esbCardResponseDTO.getHostHeaderInfo().getResponseCode());
			logger.info("Block Prepaid Card getResponseMsg  : " + esbCardResponseDTO.getHostHeaderInfo().getResponseMessage());

			if (esbCardResponseDTO != null) {
				if (esbCardResponseDTO.getHostHeaderInfo().getResponseCode().equals("000")) {
					response.setResponseCode(esbCardResponseDTO.getHostHeaderInfo().getResponseCode());
					response.setResponseMessage(esbCardResponseDTO.getHostHeaderInfo().getResponseMessage());
					response.setTransactionRefNo(esbCardResponseDTO.getHostHeaderInfo().getRequestId());
				}else {
					response.setResponseCode(esbCardResponseDTO.getHostHeaderInfo().getResponseCode());
					response.setResponseMessage(esbCardResponseDTO.getHostHeaderInfo().getResponseMessage());
				}
			}

			
			/*try {
				ObjectMapper mapper = new ObjectMapper();
				String jsonRequestBody = mapper.writeValueAsString(esbRequestDTO);
				System.out.println(":: jsonRequestBody : " + jsonRequestBody);
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}
			
			String genResponse = HttpPost(prop.getContextURL(), prop.getServiceURL(), esbRequestDTO);
			
			try {
				esbCardResponseDTO = gson.fromJson(genResponse, ESBCardResponseDTO.class);
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponseBody = mapper.writeValueAsString(esbCardResponseDTO);
				System.out.println(":: jsonResponseBody : " + jsonResponseBody);
			} catch (JsonProcessingException | com.google.gson.JsonSyntaxException ex) {
				ex.printStackTrace();
			}
			
			esbCardResponseDTO = gson.fromJson(resp.toString(), ESBCardResponseDTO.class);
			logger.info("Block Prepaid Card Response        : " + xs.toXML(resp));
			logger.info("Block Prepaid Card getResponseCode : " + esbCardResponseDTO.getHostHeaderInfo().getResponseCode());
			logger.info("Block Prepaid Card getResponseMsg  : " + esbCardResponseDTO.getHostHeaderInfo().getResponseMessage());
			response.setResponseCode(esbCardResponseDTO.getHostHeaderInfo().getResponseCode());
			response.setResponseMessage(esbCardResponseDTO.getHostHeaderInfo().getResponseMessage());
             */
			
			
		} catch (InvalidKeyException | NoSuchPaddingException | NoSuchAlgorithmException | BadPaddingException
				| IllegalBlockSizeException e1) {
			e1.printStackTrace();
		}catch(NullPointerException ne) {

		} catch (JsonProcessingException e) {
			e.printStackTrace();

		} finally {
		}
		AdapterInteraction.close();

		return response;
	}

	
	public CardServiceResponseDTO unblockCard(String affCode,String cardNo, String mobileNo) throws Exception {
		CardServiceResponseDTO response = new CardServiceResponseDTO();
		response.setResponseCode("E04");
		response.setResponseMessage("Failed at interface level");
		AdapterInteraction.begin();

		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESB_PREPAID_CARD_UNBLOCK");
		try {
			enableSSL();
		} catch (KeyManagementException | NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		
		String requestId = GetRefNumber("BC", 12);
		String url = digx_consulting.get("ESB_PREPAID_CARD_UNBLOCK", "");
		String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String pwd = digx_consulting.get("ESB_OBDX_PWD", "");
		String requestToken = JEncrypt.GenerateTransactionToken(sourceCode, requestId, pwd);
		String ip = digx_consulting.get("ESB_OBDX_IP", "");
		String key = digx_consulting.get("ESB_OBDX_AES_KEY", "");
		
		ESBCardResponseDTO esbCardResponseDTO = null;

		try{
			ESBCardRequestDTO esbRequestDTO = new ESBCardRequestDTO();
			HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
			hostHeaderInfo.setAffiliateCode(affCode); // temporarily hardcoded
			hostHeaderInfo.setIpAddress(ip);
			hostHeaderInfo.setRequestId(requestId);
			hostHeaderInfo.setRequestToken(requestToken);
			hostHeaderInfo.setRequestType("MPAY");
			hostHeaderInfo.setSourceChannelId("MOBILE");
			hostHeaderInfo.setSourceCode(sourceCode);
			esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);

			esbRequestDTO.setCardNo(AESEncrypterOriginal.encrypt(cardNo, AESEncrypterOriginal.getEncryptionKey(key)));
			esbRequestDTO.setMsisdn(AESEncrypterOriginal.encrypt(mobileNo, AESEncrypterOriginal.getEncryptionKey(key)));

			Gson gson = new Gson();


			ObjectMapper mapper = new ObjectMapper();
			String jsonRequestBody = mapper.writeValueAsString(esbRequestDTO);
			System.out.println(":: jsonRequestBody : " + jsonRequestBody);

			//String genResponse = HttpPost(prop.getContextURL(), prop.getServiceURL(), esbRequestDTO);
			//esbCardResponseDTO = gson.fromJson(genResponse, ESBCardResponseDTO.class);
			
			esbCardResponseDTO = gson.fromJson(MockServcie.unblockPrepaidCard(), ESBCardResponseDTO.class);
			
			String jsonResponseBody = mapper.writeValueAsString(esbCardResponseDTO);
			System.out.println(":: jsonResponseBody : " + jsonResponseBody);

			/*try {
				ObjectMapper mapper = new ObjectMapper();
				String jsonRequestBody = mapper.writeValueAsString(esbRequestDTO);
				System.out.println(":: jsonRequestBody : " + jsonRequestBody);
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}*/
			
			String genResponse = HttpPost(url, esbRequestDTO);
			
			/*try {
				esbCardResponseDTO = gson.fromJson(genResponse, ESBCardResponseDTO.class);
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponseBody = mapper.writeValueAsString(esbCardResponseDTO);
				System.out.println(":: jsonResponseBody : " + jsonResponseBody);
			} catch (JsonProcessingException | com.google.gson.JsonSyntaxException ex) {
				ex.printStackTrace();
			}*/


			logger.info("unBlock Prepaid Card getResponseCode : "
					+ esbCardResponseDTO.getHostHeaderInfo().getResponseCode());
			logger.info("unBlock Prepaid Card getResponseMsg  : "
					+ esbCardResponseDTO.getHostHeaderInfo().getResponseMessage());
			response.setResponseCode(esbCardResponseDTO.getHostHeaderInfo().getResponseCode());
			response.setResponseMessage(esbCardResponseDTO.getHostHeaderInfo().getResponseMessage());

			if (esbCardResponseDTO != null && esbCardResponseDTO.getHostHeaderInfo().getResponseCode().equals("000")) {
				response.setTransactionRefNo(esbCardResponseDTO.getHostHeaderInfo().getRequestId());
			}

		} catch (NullPointerException ne) {
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (BadPaddingException e) {
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		} finally {

		}
		AdapterInteraction.close();

		return response;
	}
	
	
	
	public CardStatementListResponse getStatement(String affCode,CardStatementRequestDTO cardStatementRequestDTO) throws Exception {
		CardStatementListResponse cardStatementListResponse = new CardStatementListResponse();
		cardStatementListResponse.setResponseCode("E04");
		cardStatementListResponse.setResponseMessage("Failed at interface level");
		AdapterInteraction.begin();

		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESB_PREPAID_CARD_STATEMENT");
		
		try {
			enableSSL();
		} catch (KeyManagementException | NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		
		String requestId = GetRefNumber("ST", 12);
		String url = digx_consulting.get("ESB_PREAPAID_CARD_STATEMENT", "");
		String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String pwd = digx_consulting.get("ESB_OBDX_PWD", "");
		String requestToken = JEncrypt.GenerateTransactionToken(sourceCode, requestId, pwd);
		String ip = digx_consulting.get("ESB_OBDX_IP", "");
		String key = digx_consulting.get("ESB_OBDX_AES_KEY", "");
		
		ESBCardResponseDTO esbCardResponseDTO = null;

		try {
			ESBCardRequestDTO esbRequestDTO = new ESBCardRequestDTO();
			HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
			hostHeaderInfo.setAffiliateCode(affCode); // temporarily hardcoded
			hostHeaderInfo.setIpAddress(ip);
			hostHeaderInfo.setRequestId(requestId);
			hostHeaderInfo.setRequestToken(requestToken);
			hostHeaderInfo.setRequestType("MPAY");
			hostHeaderInfo.setSourceChannelId("MOBILE");
			hostHeaderInfo.setSourceCode(sourceCode);
			esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);

			esbRequestDTO.setLast4Digits(AESEncrypterOriginal.encrypt("", AESEncrypterOriginal.getEncryptionKey(key)));
			esbRequestDTO.setGtpCustID(AESEncrypterOriginal.encrypt("", AESEncrypterOriginal.getEncryptionKey(key)));
			esbRequestDTO.setCardType(AESEncrypterOriginal.encrypt("SHOPVCARD", AESEncrypterOriginal.getEncryptionKey(key)));

			// date format : 01-JUL-2019 : DD-MON-YYYY
			esbRequestDTO.setStrRange(AESEncrypterOriginal.encrypt(cardStatementRequestDTO.getStartDate(),AESEncrypterOriginal.getEncryptionKey(key)));
			esbRequestDTO.setEndRange(AESEncrypterOriginal.encrypt(cardStatementRequestDTO.getEndDate(), AESEncrypterOriginal.getEncryptionKey(key)));

			esbRequestDTO.setNumOfTrans(AESEncrypterOriginal.encrypt("" + cardStatementRequestDTO.getTransCount(), AESEncrypterOriginal.getEncryptionKey(key)));

			StringBuffer resp = new StringBuffer();
			XStream xs = new XStream();
			logger.info("Prepaid Card Statement Request : " + xs.toXML(esbRequestDTO));
			logger.info("Prepaid Card Statement Source Code : " + sourceCode);

			logger.info("Prepaid Card  Endpoint 1 : " + prop.getServiceURL());
			logger.info("Prepaid Card  Endpoint 2 : " + prop.getContextURL());

			Gson gson = new Gson();
			ObjectMapper mapper = new ObjectMapper();
			String jsonRequestBody = mapper.writeValueAsString(esbRequestDTO);
			System.out.println(":: jsonRequestBody : " + jsonRequestBody);


			String genResponse = HttpPost(url, esbRequestDTO);
			esbCardResponseDTO = gson.fromJson(genResponse, ESBCardResponseDTO.class);
			
			
			
			String jsonResponseBody = mapper.writeValueAsString(esbCardResponseDTO);
			System.out.println(":: jsonResponseBody : " + jsonResponseBody);

			esbCardResponseDTO = gson.fromJson(resp.toString(), ESBCardResponseDTO.class);
			logger.info("Prepaid Card Statement Response        : " + xs.toXML(resp));
			logger.info("Prepaid Card Statement ResponseCode : "
					+ esbCardResponseDTO.getHostHeaderInfo().getResponseCode());
			logger.info("Prepaid Card Statement ResponseMsg  : "
					+ esbCardResponseDTO.getHostHeaderInfo().getResponseMessage());
			// response.setResponseCode(esbCardResponseDTO.getHostHeaderInfo().getResponseCode());
			// response.setResponseMessage(esbCardResponseDTO.getHostHeaderInfo().getResponseMessage());

			// if (esbCardResponseDTO != null &&
			// esbCardResponseDTO.getHostHeaderInfo().getResponseCode().equals("000")) {
			// response.setTransactionRefNo(esbCardResponseDTO.getHostHeaderInfo().getRequestId());
			// }
		} catch (InvalidKeyException | NoSuchPaddingException | NoSuchAlgorithmException
				| BadPaddingException				| IllegalBlockSizeException e1) {
			e1.printStackTrace();
		}catch(NullPointerException ne) {
			
		
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

			
		} 
		finally {
		}
		AdapterInteraction.close();

		return null;
	}

	
	
	
	
	public  String GetRefNumber(String type, int len) {

        String finalString = "";
        int x = 0;
        char[] stringChars = new char[len];
        for (int i = 0; i < len; i++) //4
        {
            Random random = new Random();
            x = random.nextInt(9);

            stringChars[i] = Integer.toString(x).toCharArray()[0];
        }


        finalString = new String(stringChars);
        finalString = type + finalString;
        return finalString.trim();
    }
	
	public static String HttpPost(String contextUrl,  Object payloadRequest) {
	    StringBuffer resp = new StringBuffer();
	    URL obj = null;
	    Gson gson = new Gson();
	    try {
	    	
	    	System.out.println("URL " + contextUrl);
	    	
	    	if(contextUrl.startsWith("https"))
			{
	    		return HttpsPost( contextUrl,payloadRequest);

			}
			
		
	    	
	      obj = new URL(contextUrl);
	      HttpURLConnection con = (HttpURLConnection)obj.openConnection();
	      con.setRequestMethod("POST");
	      con.setRequestProperty("Content-Type", "application/json");
	      con.setDoOutput(true);
	      DataOutputStream wr = new DataOutputStream(con.getOutputStream());
	      wr.writeBytes(gson.toJson(payloadRequest));
	      wr.flush();
	      wr.close();
	      String responseStatus = con.getResponseMessage();
	      System.out.println(responseStatus);
	      int responseCode = con.getResponseCode();
	      System.out.println("responseCode " + responseCode);
	      if (responseCode == 200) {
	        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
	        String inputLine;
	        while ((inputLine = in.readLine()) != null)
	          resp.append(inputLine); 
	        in.close();
	      } else {
	        BufferedReader in = new BufferedReader(new InputStreamReader(con.getErrorStream()));
	        String inputLine;
	        while ((inputLine = in.readLine()) != null)
	          resp.append(inputLine); 
	        in.close();
	      } 
	      
	      System.out.println("response: " + resp);
	      
	    } catch (IOException e) {
	      e.printStackTrace();
	    } 
	    return resp.toString();
	  }
	  
	  public static String HttpsPost(String contextUrl,  Object payloadRequest) {
		    StringBuffer resp = new StringBuffer();
		    URL obj = null;
		    Gson gson = new Gson();
		    try {
		    	
		    	System.out.println("URL " + contextUrl);
		      obj = new URL(contextUrl);
		      HttpURLConnection con = (HttpURLConnection)obj.openConnection();
		      con.setRequestMethod("POST");
		      con.setRequestProperty("Content-Type", "application/json");
		      con.setDoOutput(true);
		      DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		      wr.writeBytes(gson.toJson(payloadRequest));
		      wr.flush();
		      wr.close();
		      String responseStatus = con.getResponseMessage();
		      System.out.println(responseStatus);
		      int responseCode = con.getResponseCode();
		      System.out.println("responseCode " + responseCode);
		      if (responseCode == 200) {
		        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		        String inputLine;
		        while ((inputLine = in.readLine()) != null)
		          resp.append(inputLine); 
		        in.close();
		      } else {
		        BufferedReader in = new BufferedReader(new InputStreamReader(con.getErrorStream()));
		        String inputLine;
		        while ((inputLine = in.readLine()) != null)
		          resp.append(inputLine); 
		        in.close();
		      } 
		      
		      
		      System.out.println("response: " + resp);
		      
		    } catch (IOException e) {
		      e.printStackTrace();
		    } 
		    return resp.toString();
	}
	  
	
	
	public static void enableSSL() throws NoSuchAlgorithmException, KeyManagementException {

		// Create a trust manager that does not validate certificate chains
		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			public void checkClientTrusted(X509Certificate[] certs, String authType) {
			}

			public void checkServerTrusted(X509Certificate[] certs, String authType) {
			}
		} };

		// Install the all-trusting trust manager
		SSLContext sc = SSLContext.getInstance("SSL");
		sc.init(null, trustAllCerts, new java.security.SecureRandom());
		HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

		// Create all-trusting host name verifier
		HostnameVerifier allHostsValid = new HostnameVerifier() {
			public boolean verify(String hostname, SSLSession session) {
				return true;
			}
		};

		// Install the all-trusting host verifier
		HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
	}

}
