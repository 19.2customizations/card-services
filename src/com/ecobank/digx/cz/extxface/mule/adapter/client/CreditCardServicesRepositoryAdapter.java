 package com.ecobank.digx.cz.extxface.mule.adapter.client;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Random;
import java.util.logging.Logger;
import java.util.prefs.Preferences;

import com.ecobank.digx.cz.app.cardservices.dto.CardBalanceResponse;
import com.ecobank.digx.cz.app.cardservices.dto.CardDetailDTO;
import com.ecobank.digx.cz.app.cardservices.dto.CardListResponse;
import com.ecobank.digx.cz.app.cardservices.dto.CardServiceResponseDTO;
import com.ecobank.digx.cz.app.cardservices.dto.CardStatementListResponse;
import com.ecobank.digx.cz.app.cardservices.dto.CardStatementRequestDTO;
import com.ecobank.digx.cz.app.cardservices.dto.CreditCardBalDetailRespDTO;
import com.ecobank.digx.cz.app.domain.cardservices.entity.CardActivityInfo;
import com.ecobank.digx.cz.eco.domain.generic.entity.genericlogger.GenericLoggerConfiguration;
import com.ecobank.digx.cz.extxface.mule.assembler.CardRemoteServiceAssembler;
import com.ecobank.digx.cz.extxface.mule.dto.CreditCardBalanceDetailResponseDTO;
import com.ecobank.digx.cz.extxface.mule.dto.ESBCardBlockRequestDTO;
import com.ecobank.digx.cz.extxface.mule.dto.ESBCardChangePinRequestDTO;
import com.ecobank.digx.cz.extxface.mule.dto.ESBCardRequestDTO;
import com.ecobank.digx.cz.extxface.mule.dto.ESBCardResetPinRequestDTO;
import com.ecobank.digx.cz.extxface.mule.dto.ESBCardResponseDTO;
import com.ecobank.digx.cz.extxface.mule.dto.ESBCardTransactionHistoryResponseDTO;
import com.ecobank.digx.cz.extxface.mule.dto.HostHeaderInfo;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.ofss.digx.app.core.AdapterInteraction;
import com.ofss.digx.framework.rest.client.RESTClientFactory;
import com.ofss.digx.framework.rest.client.RESTClientRequestBuilder;
import com.ofss.digx.framework.rest.client.RESTClientRequestConfig;
import com.ofss.digx.framework.rest.client.impl.IRESTClient;
import com.ofss.digx.infra.exceptions.Exception;
import com.ofss.fc.infra.config.ConfigurationFactory;
import com.ofss.fc.infra.log.impl.MultiEntityLogger;
//import com.ofss.digx.infra.thread.ThreadAttribute;
import com.ofss.fc.infra.thread.ThreadAttribute;
import com.thoughtworks.xstream.XStream;

public class CreditCardServicesRepositoryAdapter {
	private static CreditCardServicesRepositoryAdapter singletonInstance;

	private Preferences digx_consulting = ConfigurationFactory.getInstance().getConfigurations("DIGXCONSULTING");
	

	private static final String THIS_COMPONENT_NAME = CreditCardServicesRepositoryAdapter.class.getName();
	private static transient Logger logger = MultiEntityLogger.getUniqueInstance().getLogger(THIS_COMPONENT_NAME);
	
	private static final GenericLoggerConfiguration serviceLogger = new GenericLoggerConfiguration();
    private Preferences eco_generic = ConfigurationFactory.getInstance().getConfigurations("ECOGENERIC");
	
    
	public static CreditCardServicesRepositoryAdapter getInstance() {
		if (singletonInstance == null)
			synchronized (CreditCardServicesRepositoryAdapter.class) {
				if (singletonInstance == null)
					singletonInstance = new CreditCardServicesRepositoryAdapter();
			}
		return singletonInstance;
	}
	
	public CardBalanceResponse balanceCheck(String cardId, String affCode) throws Exception {
		CardBalanceResponse response = new CardBalanceResponse();
		response.setResponseCode("E04");
		response.setResponseMessage("Failed at interface level");
		AdapterInteraction.begin();
		
		String requestId = GetRefNumber("BC", 12);
		com.ofss.fc.datatype.Date startDateTime = new com.ofss.fc.datatype.Date();
		Boolean exceptionFlag = Boolean.valueOf(false);
		String jsonResponseBody ="";
		String jsonRequestBody ="";
		String correlationId = requestId;

		try {
			RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
			RESTClientRequestConfig prop = clientBuilder.loadConfig("ESB_CREDIT_CARD_DETAIL_FETCH");

			String url = digx_consulting.get("ESB_CREDIT_CARD_DETAIL_FETCH", "");
			String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
			String pwd = digx_consulting.get("ESB_OBDX_PWD", "");
			String requestToken = JEncrypt.GenerateTransactionToken(sourceCode, requestId, pwd);
			String ipx = digx_consulting.get("ESB_OBDX_IP", "");

			ESBCardRequestDTO esbRequestDTO = new ESBCardRequestDTO();
			HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
			hostHeaderInfo.setAffiliateCode(affCode); // temporarily hardcoded
			hostHeaderInfo.setIpAddress(ipx);
			hostHeaderInfo.setRequestId(requestId);
			hostHeaderInfo.setRequestToken(requestToken);
			hostHeaderInfo.setRequestType("GETCARDBALANCE");
			hostHeaderInfo.setSourceChannelId("WEB");
			hostHeaderInfo.setSourceCode(sourceCode);
			esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);
			// esbRequestDTO.setCardNo(cardId);
			esbRequestDTO.setCardAcct(cardId);

			Gson gson = new Gson();
			ObjectMapper mapper = new ObjectMapper();
			 jsonRequestBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(esbRequestDTO);

			System.out.println("balanceCheck RequestBody: " + jsonRequestBody);
			ESBCardResponseDTO resp = null;

			String genResponse = HttpPost(url, esbRequestDTO);
			resp = gson.fromJson(genResponse, ESBCardResponseDTO.class);
			
			//resp = gson.fromJson(MockServcie.getCCardPseudoPAN(), ESBCardResponseDTO.class);
			
			 jsonResponseBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(resp);
			System.out.println("balanceCheck ResponseBody: " + jsonResponseBody);

			logger.info("Generate Name           : " + resp.getName());
			logger.info("getResponseCode         : " + resp.getHostHeaderInfo().getResponseCode());
			logger.info("getResponseMessage      : " + resp.getHostHeaderInfo().getResponseMessage());

			if (resp != null) {
				if (resp.getHostHeaderInfo().getResponseCode().equals("000")) {
					response.setCcyCode(resp.getCcy());
					String pan = resp.getPsuedoPan();
					response.setPseudoPAN(pan);

					CardBalanceResponse cardBalanceResponse = getCardBalance(affCode,cardId);
					logger.info("credit card balance : " + cardBalanceResponse.getBalance());
					logger.info("credit Pseudo card account : " + cardBalanceResponse.getCardAccount());

					response.setResponseCode("000");
					response.setResponseMessage(resp.getHostHeaderInfo().getResponseMessage());
					response.setBalance(cardBalanceResponse.getBalance());

					System.out.println("Fetch Pseudo PAN-Bal::: " + cardBalanceResponse.getExpiry());
					if (cardBalanceResponse.getExpiry() != null) {
						String exp = cardBalanceResponse.getExpiry();
						if (exp.length() >= 10) {
							String expx = exp.substring(5, 7) + "/" + exp.substring(2, 4);
							response.setExpiry(expx);
						} else
							response.setExpiry(cardBalanceResponse.getExpiry());
					} else
						response.setExpiry("");

					response.setNameOnCard(resp.getName());
					response.setCardAccount(cardBalanceResponse.getCardAccount());

					// To be replaced with Pseudo PAN or Masked PAN in next release
					String maskedPan = pan.substring(0, 6) + "****" + pan.substring(pan.length() - 4);

					response.setMaskedPan(maskedPan);
					response.setSourceAccount(cardBalanceResponse.getSourceAccount());
					response.setCardType("CREDIT");

					System.out.println("Fetch Pseudo PAN::: " + cardId);
					/*
					 * CardBalanceResponse respPsuedo = getCardPseudoPAN(cardId, affCode);
					 * if(respPsuedo != null && respPsuedo.getResponseCode().equals("000")) {
					 * cardBalanceResponse.setPseudoPAN(respPsuedo.getPseudoPAN());
					 * cardBalanceResponse.setCcyCode(respPsuedo.getCcyCode()); pan =
					 * respPsuedo.getPseudoPAN(); maskedPan = pan.substring(0,6) + "****" +
					 * pan.substring(pan.length() - 4); response.setMaskedPan(maskedPan); }
					 */
				} else {
					response.setResponseCode(resp.getHostHeaderInfo().getResponseCode());
					response.setResponseMessage(resp.getHostHeaderInfo().getResponseMessage());
				}
			}
		} catch (JsonProcessingException e1) {
			 exceptionFlag = Boolean.valueOf(true);

		      if ("Y".equalsIgnoreCase(this.eco_generic.get("CREDIT_CARD_BALANCE.INTERFACE_LOGGER_REQUIRED", "")))
              {
                serviceLogger.dbLogger((Object)jsonRequestBody, (Object)e1.getMessage(), "CREDIT_CARD_BALANCE",correlationId,"EXCEPTION",ThreadAttribute.get("SUBJECT_NAME").toString(),ThreadAttribute.get("TARGET_UNIT_CODE").toString(), startDateTime);

		       }
		      
			e1.printStackTrace();
		} catch (NullPointerException ne) {
		} finally {
			
			if ("Y".equalsIgnoreCase(this.eco_generic.get("CREDIT_CARD_BALANCE.INTERFACE_LOGGER_REQUIRED", "")) && !exceptionFlag.booleanValue())
            {
               serviceLogger.dbLogger((Object)jsonRequestBody , (Object)jsonResponseBody , "CREDIT_CARD_BALANCE",correlationId,"",ThreadAttribute.get("SUBJECT_NAME").toString(),ThreadAttribute.get("TARGET_UNIT_CODE").toString(), startDateTime);

            }

		}
		AdapterInteraction.close();
		return response;
	}
	
	public CardBalanceResponse getCardBalance(String affCode,String cardId) throws Exception {
		CardBalanceResponse cardBalanceResponse = new CardBalanceResponse();
		AdapterInteraction.begin();

		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESB_CREDIT_CARD_BALANCE");

		String url = digx_consulting.get("ESB_CREDIT_CARD_BALANCE", "");
		String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String ipx = digx_consulting.get("ESB_OBDX_IP", "");
		
		String requestId = GetRefNumber("BC", 12);
		String pwd = digx_consulting.get("ESB_OBDX_PWD", "");
		String requestToken = JEncrypt.GenerateTransactionToken(sourceCode, requestId, pwd);
		
		
		ESBCardRequestDTO esbRequestDTO = new ESBCardRequestDTO();
		HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
		hostHeaderInfo.setAffiliateCode(affCode); //temporarily hardcoded
		hostHeaderInfo.setIpAddress(ipx);
		hostHeaderInfo.setRequestId(requestId);
		hostHeaderInfo.setRequestToken(requestToken);
		hostHeaderInfo.setRequestType("POSTBILL");
		hostHeaderInfo.setSourceChannelId("WEB");
		hostHeaderInfo.setSourceCode(sourceCode);
		esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);
		esbRequestDTO.setCardAccount(cardId);

		ESBCardResponseDTO resp = null;

		try {
			ObjectMapper mapper = new ObjectMapper();
			String jsonRequestBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(esbRequestDTO);
			logger.info("::: CC-FundCard RequestBody: " + jsonRequestBody);

			Gson gson = new Gson();
			String genResponse = HttpPost(url, esbRequestDTO);
			resp = gson.fromJson(genResponse, ESBCardResponseDTO.class);
			
			//resp = gson.fromJson(MockServcie.getCCardBalance(), ESBCardResponseDTO.class);
			
			logger.info("getResponseCode         : " + resp.getHostHeaderInfo().getResponseCode());
			logger.info("getResponseMessage      : " + resp.getHostHeaderInfo().getResponseMessage());
			if (resp != null && resp.getHostHeaderInfo().getResponseCode().equals("000")) {
				cardBalanceResponse.setBalance(new BigDecimal(resp.getAvailableAmount()));
				cardBalanceResponse.setCardAccount(resp.getCardAccount());
				cardBalanceResponse.setExpiry(resp.getExpiry());
				cardBalanceResponse.setSourceAccount(resp.getAccountNo());
			}
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		} finally {
		}
		AdapterInteraction.close();

		return cardBalanceResponse;
	}
	
	
	public CardBalanceResponse getCardPseudoPAN(String cardId,String affCode) throws Exception {
		CardBalanceResponse cardBalanceResponse = new CardBalanceResponse();
		AdapterInteraction.begin();
		
		System.out.println("Entering Pseudo balance get: " );

		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESB_CREDIT_CARD_DETAIL_FETCH");

		String url = digx_consulting.get("ESB_CREDIT_CARD_DETAIL_FETCH", "");
		String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String ipx = digx_consulting.get("ESB_OBDX_IP", "");
		
		String requestId = GetRefNumber("BC", 12);
		String pwd = digx_consulting.get("ESB_OBDX_PWD", "");
		String requestToken = JEncrypt.GenerateTransactionToken(sourceCode, requestId, pwd);
		
		
		ESBCardRequestDTO esbRequestDTO = new ESBCardRequestDTO();
		HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
		hostHeaderInfo.setAffiliateCode(affCode); //temporarily hardcoded
		hostHeaderInfo.setIpAddress(ipx);
		hostHeaderInfo.setRequestId(requestId);
		hostHeaderInfo.setRequestToken(requestToken);
		hostHeaderInfo.setRequestType("POSTBILL");
		hostHeaderInfo.setSourceChannelId("WEB");
		hostHeaderInfo.setSourceCode(sourceCode);
		esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);
		esbRequestDTO.setCardAcct(cardId);
		
		System.out.println("Pseudo balance get: " + prop.getServiceURL() + " " + prop.getContextURL() + " " + cardId);

		ESBCardResponseDTO resp = null;

		try {
			ObjectMapper mapper = new ObjectMapper();
			String jsonRequestBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(esbRequestDTO);
			logger.info("::: getCardPseudoPAN RequestBody: " + jsonRequestBody);
			
			Gson gson = new Gson();
			
			String genResponse = HttpPost(url, esbRequestDTO);
			resp = gson.fromJson(genResponse, ESBCardResponseDTO.class);
			
			//resp = gson.fromJson(MockServcie.getCCardPseudoPAN(), ESBCardResponseDTO.class);
			
			String jsonResponseBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(resp);
			logger.info("::: getCardPseudoPAN ResponseBody: " + jsonResponseBody);
			
			logger.info("Available Balance       : " + resp.getAvailableAmount());
			logger.info("getResponseCode         : " + resp.getHostHeaderInfo().getResponseCode());
			logger.info("getResponseMessage      : " + resp.getHostHeaderInfo().getResponseMessage());
			if (resp != null) {
				if (resp.getHostHeaderInfo().getResponseCode().equals("000")) {
					// balance = resp.getAvailableAmount();
					cardBalanceResponse.setPseudoPAN(resp.getPsuedoPan());
					cardBalanceResponse.setCcyCode(resp.getCcy());
					cardBalanceResponse.setResponseCode("000");
					cardBalanceResponse.setResponseMessage("SUCCESS");
				} else {
					cardBalanceResponse.setResponseCode(resp.getHostHeaderInfo().getResponseCode());
					cardBalanceResponse.setResponseMessage(resp.getHostHeaderInfo().getResponseMessage());
				}
			}
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		} finally {
		}
		AdapterInteraction.close();

		return cardBalanceResponse;
	}
	
	
	public CardStatementListResponse getStatement(CardStatementRequestDTO cardStatementRequestDTO) throws Exception {
		
		CardStatementListResponse cardStatementListResponse = new CardStatementListResponse();
		cardStatementListResponse.setResponseCode("E04");
		cardStatementListResponse.setResponseMessage("Failed at interface level");
		
		AdapterInteraction.begin();

		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESB_CREDIT_CARD_STATEMENT");

		String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String ipx = digx_consulting.get("ESB_OBDX_IP", "");
		
		String requestId = GetRefNumber("BC", 12);
		String pwd = digx_consulting.get("ESB_OBDX_PWD", "");
		String requestToken = JEncrypt.GenerateTransactionToken(sourceCode, requestId, pwd);
		
		
		ESBCardRequestDTO esbRequestDTO = new ESBCardRequestDTO();
		HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
		hostHeaderInfo.setAffiliateCode(cardStatementRequestDTO.getAffCode()); //temporarily hardcoded
		hostHeaderInfo.setIpAddress(ipx);
		hostHeaderInfo.setRequestId(requestId);
		hostHeaderInfo.setRequestToken(requestToken);
		hostHeaderInfo.setRequestType("GETCREDITCARDHIST");
		hostHeaderInfo.setSourceChannelId("WEB");
		hostHeaderInfo.setSourceCode(sourceCode);
		esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);
		esbRequestDTO.setCardAccount(cardStatementRequestDTO.getCustomerNo());
		esbRequestDTO.setStartDate(cardStatementRequestDTO.getStartDate());
		esbRequestDTO.setEndDate(cardStatementRequestDTO.getEndDate());

		ESBCardTransactionHistoryResponseDTO resp = null;
		CardRemoteServiceAssembler assembler = new CardRemoteServiceAssembler();

		try {
			ObjectMapper mapper = new ObjectMapper();
			String jsonRequestBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(esbRequestDTO);
			logger.info("::: getStatement RequestBody: " + jsonRequestBody);
			
			Gson gson = new Gson();
			//String genResponse = HttpPost(prop.getContextURL(), prop.getServiceURL(), esbRequestDTO);
			//resp = gson.fromJson(genResponse, ESBCardTransactionHistoryResponseDTO.class);
			
			resp = gson.fromJson(MockServcie.getCCStatement(), ESBCardTransactionHistoryResponseDTO.class);
			
			String jsonResponseBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(resp);
			logger.info("::: getStatement ResponseBody: " + jsonResponseBody);
			
			if (resp != null && resp.getHostHeaderInfo().getResponseCode().equals("000")) {
				cardStatementListResponse  = assembler.fromRemoteResponseCardStatementtoDTO(resp);
			}
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		} finally {
		}
		AdapterInteraction.close();

		return cardStatementListResponse;
	}
	
	
	public CardListResponse getCardByAccount(CardStatementRequestDTO cardStatementRequestDTO) throws Exception {
		CardListResponse cardListResponse = new CardListResponse();
		cardListResponse.setResponseCode("E04");
		cardListResponse.setResponseMessage("Failed at interface level");
		AdapterInteraction.begin();

		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESB_CARD_BY_ACCOUNT");
		RESTClientFactory clientFactory = RESTClientFactory.getInstance();
		IRESTClient restClient = clientFactory.getRESTClientInstance();

		String url = digx_consulting.get("ESB_CARD_BY_ACCOUNT", "");
		String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String ipx = digx_consulting.get("ESB_OBDX_IP", "");
		String pwd = digx_consulting.get("ESB_OBDX_PWD", "");
		
		String genRef = GetRefNumber("CS", 12);
		String requestToken = JEncrypt.GenerateTransactionToken(sourceCode, genRef, pwd);
		ESBCardRequestDTO esbRequestDTO = new ESBCardRequestDTO();
		HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
		hostHeaderInfo.setAffiliateCode(cardStatementRequestDTO.getAffCode());
		hostHeaderInfo.setIpAddress(ipx);
		hostHeaderInfo.setRequestId(genRef);
		hostHeaderInfo.setRequestToken(requestToken);
		hostHeaderInfo.setRequestType("MPAY");
		hostHeaderInfo.setSourceChannelId("WEB");
		hostHeaderInfo.setSourceCode(sourceCode);
		esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);
		esbRequestDTO.setAccountNo(cardStatementRequestDTO.getCustomerNo());
		esbRequestDTO.setCardScheme("");

		try {
			ObjectMapper mapper = new ObjectMapper();
			String jsonRequestBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(esbRequestDTO);
			logger.info("::: getCardByAccount RequestBody: " + jsonRequestBody);
			
			Gson gson = new Gson();
			String genResponse = HttpPost(url, esbRequestDTO);
			cardListResponse = gson.fromJson(genResponse, CardListResponse.class);
			
			//cardListResponse = gson.fromJson(MockServcie.getdebitcardsbyaccount(), CardListResponse.class);
			
			
			String jsonResponseBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(cardListResponse);
			logger.info("::: getCardByAccount ResponseBody: " + jsonResponseBody);
			
			cardListResponse = (CardListResponse) restClient.post(prop, CardListResponse.class, esbRequestDTO);
			logger.info("getResponseCode         : " + cardListResponse.getHostHeaderInfo().getResponseCode());
			logger.info("getResponseMessage      : " + cardListResponse.getHostHeaderInfo().getResponseMessage());
			
			if (cardListResponse.getHostHeaderInfo().getResponseCode().equals("000")) {

				if (cardListResponse.getCards().size() > 0) {

					for (CardDetailDTO cardDetailDTO : cardListResponse.getCards()) {
						cardListResponse.getCards().add(cardDetailDTO);
						logger.info("******************************************");
						logger.info("Expiry Date   : " + cardDetailDTO.getExpiryDate());
						logger.info("Card Status   : " + cardDetailDTO.getCardStatus());
						logger.info("Name-On-Card  : " + cardDetailDTO.getNameOnCard());
						logger.info("Masked Pan    : " + cardDetailDTO.getMaskedPan());
					}
				}
			}
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		} finally {
		}
		return cardListResponse;
	}

	

	public CardServiceResponseDTO fundCard(CardActivityInfo cardActivityInfo, String pan, String nameOnCard, String branchCode) throws Exception {
		CardServiceResponseDTO response = new CardServiceResponseDTO();
		response.setResponseCode("E04");
		response.setResponseMessage("Failed at interface level");
		AdapterInteraction.begin();
		
		String requestId = GetRefNumber("BC", 12);
		com.ofss.fc.datatype.Date startDateTime = new com.ofss.fc.datatype.Date();
		Boolean exceptionFlag = Boolean.valueOf(false);
		String jsonResponseBody ="";
		String jsonRequestBody ="";
		String correlationId = requestId;
		
		CardRemoteServiceAssembler assembler = new CardRemoteServiceAssembler();

		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESB_CREDIT_CARD_FUND");



		String url = digx_consulting.get("ESB_CREDIT_CARD_FUND", "");
		String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		

		ESBCardRequestDTO esbRequestDTO = assembler.fromDomainObjectToRemoteActivityFundCreditCard(cardActivityInfo, pan, nameOnCard, branchCode);
		
		ESBCardResponseDTO resp = null;

		try {
			ObjectMapper mapper = new ObjectMapper();
			 jsonRequestBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(esbRequestDTO);
			System.out.println("::: CC-FundCard RequestBody: " + jsonRequestBody);

			Gson gson = new Gson();
			String genResponse = HttpPost(url, esbRequestDTO);
			resp = gson.fromJson(genResponse, ESBCardResponseDTO.class);
			
			//resp = gson.fromJson(MockServcie.fundCCCard(), ESBCardResponseDTO.class);
			
			 jsonResponseBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(resp);
			logger.info("::: CC-fundCard ResponseBody   : " + jsonResponseBody);
			
			logger.info("::: Generate CustomerNo     : " + resp.getCustomerNo());
			logger.info("::: getResponseCode         : " + resp.getHostHeaderInfo().getResponseCode());
			logger.info("::: getResponseMessage      : " + resp.getHostHeaderInfo().getResponseMessage());
			logger.info("::: internalRefNo           : " + resp.getInternalRefNo());

			if (resp != null) {
				if (resp.getHostHeaderInfo().getResponseCode().equals("000")) {
					response.setResponseCode("000");
					response.setResponseMessage(resp.getHostHeaderInfo().getResponseMessage());
					response.setTransactionRefNo(resp.getTransRefNo());
				} else {
					response.setResponseCode(resp.getHostHeaderInfo().getResponseCode());
					response.setResponseMessage(resp.getHostHeaderInfo().getResponseMessage());
				}
			}
		} catch (JsonProcessingException e) {
			exceptionFlag = Boolean.valueOf(true);

		      if ("Y".equalsIgnoreCase(this.eco_generic.get("CREDIT_CARD_TOPUP.INTERFACE_LOGGER_REQUIRED", "")))
            {
              //serviceLogger.dbLogger((Object)jsonRequestBody, (Object)e.getMessage(), "CREDIT_CARD_TOPUP",correlationId,"EXCEPTION",ThreadAttribute.get("SUBJECT_NAME").toString(),ThreadAttribute.get("TARGET_UNIT_CODE").toString(), startDateTime);

		       }
		      
			e.printStackTrace();
		} finally {
			if ("Y".equalsIgnoreCase(this.eco_generic.get("CREDIT_CARD_TOPUP.INTERFACE_LOGGER_REQUIRED", "")) && !exceptionFlag.booleanValue())
            {
               //serviceLogger.dbLogger((Object)jsonRequestBody , (Object)jsonResponseBody , "CREDIT_CARD_TOPUP",correlationId,"",ThreadAttribute.get("SUBJECT_NAME").toString(),ThreadAttribute.get("TARGET_UNIT_CODE").toString(), startDateTime);

            }
		}
		AdapterInteraction.close();

		return response;
	}
	
	
	 
		public CardServiceResponseDTO blockCard(String affCode, String cardNo, String expiryDate, String reason ) throws Exception {
		CardServiceResponseDTO response = new CardServiceResponseDTO();
		response.setResponseCode("E04");
		response.setResponseMessage("Failed at interface level");
		AdapterInteraction.begin();
		
		System.out.println("Card Block-Credit :" + cardNo + " " + expiryDate + " " + reason);

		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESB_CREDIT_CARD_BLOCK");

		String requestId = GetRefNumber("BC", 12);
		String url = digx_consulting.get("ESB_CREDIT_CARD_BLOCK", "");
		String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String pwd = digx_consulting.get("ESB_OBDX_PWD", "");
		String requestToken = JEncrypt.GenerateTransactionToken(sourceCode, requestId, pwd);
		String ipx = digx_consulting.get("ESB_OBDX_IP", "");
		
		ESBCardBlockRequestDTO esbRequestDTO = new ESBCardBlockRequestDTO();
		HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
		hostHeaderInfo.setAffiliateCode(affCode); //temporarily hardcoded
		hostHeaderInfo.setIpAddress(ipx);
		hostHeaderInfo.setRequestId(requestId);
		hostHeaderInfo.setRequestToken(requestToken);
		hostHeaderInfo.setRequestType("BLOCKCARD");
		hostHeaderInfo.setSourceChannelId("MOBILE");
		hostHeaderInfo.setSourceCode(sourceCode);
		esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);
		esbRequestDTO.setReasonCode(reason);
		esbRequestDTO.setCardNo(cardNo); 
		//MM/YY
		String expDate = expiryDate.substring(3,5) + expiryDate.substring(0,2);
		esbRequestDTO.setExpiryDate(expDate);

		ESBCardResponseDTO resp = null;

		try {
			ObjectMapper mapper = new ObjectMapper();
			String jsonRequestBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(esbRequestDTO);
			logger.info("::: blockCard RequestBody: " + jsonRequestBody);

			Gson gson = new Gson();
			String genResponse = HttpPost(url, esbRequestDTO);
			resp = gson.fromJson(genResponse, ESBCardResponseDTO.class);
			
			
			String jsonResponseBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(resp);
			logger.info("::: blockCard ResponseBody   : " + jsonResponseBody);
			
			
			logger.info("::: Generate CustomerNo      : " + resp.getCustomerNo());
			logger.info("::: getResponseCode          : " + resp.getHostHeaderInfo().getResponseCode());
			logger.info("::: getResponseMessage       : " + resp.getHostHeaderInfo().getResponseMessage());
			if (resp != null) {
				if (resp.getHostHeaderInfo().getResponseCode().equals("000")) {

					response.setResponseCode("000");
					response.setResponseMessage(resp.getHostHeaderInfo().getResponseMessage());
					// response.setActivityDescription("BLOCK CARD");
					// response.setCardId("");
					// response.setRefId(genRef);
				}else {
					response.setResponseCode(resp.getHostHeaderInfo().getResponseCode());
					response.setResponseMessage(resp.getHostHeaderInfo().getResponseMessage());
				}
			}
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		} finally {
		}
		AdapterInteraction.close();

		return response;
	}
	
	
	public CardServiceResponseDTO unblockCard(String affCode,String cardNo, String expiryDate, String reason ) throws Exception {
		CardServiceResponseDTO response = new CardServiceResponseDTO();
		response.setResponseCode("E04");
		response.setResponseMessage("Failed at interface level");
		AdapterInteraction.begin();

		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESB_CREDIT_CARD_UNBLOCK");

		String requestId = GetRefNumber("BC", 12);
		String url = digx_consulting.get("ESB_CREDIT_CARD_UNBLOCK", "");
		String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String pwd = digx_consulting.get("ESB_OBDX_PWD", "");
		String requestToken = JEncrypt.GenerateTransactionToken(sourceCode, requestId, pwd);
		String ipx = digx_consulting.get("ESB_OBDX_IP", "");
		
		ESBCardBlockRequestDTO esbRequestDTO = new ESBCardBlockRequestDTO();
		HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
		hostHeaderInfo.setAffiliateCode(affCode); //temporarily hardcoded
		hostHeaderInfo.setIpAddress(ipx);
		hostHeaderInfo.setRequestId(requestId);
		hostHeaderInfo.setRequestToken(requestToken);
		hostHeaderInfo.setRequestType("UNBLOCKCARD");
		hostHeaderInfo.setSourceChannelId("MOBILE");
		hostHeaderInfo.setSourceCode(sourceCode);
		esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);
		esbRequestDTO.setReasonCode(reason);
		esbRequestDTO.setCardNo(cardNo); 
		//MM/YY
		String expDate = expiryDate.substring(3,5) + expiryDate.substring(0,2);
		esbRequestDTO.setExpiryDate(expDate);

		ESBCardResponseDTO resp = null;

		try {
			ObjectMapper mapper = new ObjectMapper();
			String jsonRequestBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(esbRequestDTO);
			logger.info("::: unblockCard RequestBody: " + jsonRequestBody);

			Gson gson = new Gson();
			String genResponse = HttpPost(url, esbRequestDTO);
			resp = gson.fromJson(genResponse, ESBCardResponseDTO.class);
			
			
			String jsonResponseBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(resp);
			logger.info("::: blockCard ResponseBody   : " + jsonResponseBody);
			
			logger.info("getResponseCode         : " + resp.getHostHeaderInfo().getResponseCode());
			logger.info("getResponseMessage      : " + resp.getHostHeaderInfo().getResponseMessage());
			if (resp != null) {
				if (resp.getHostHeaderInfo().getResponseCode().equals("000")) {
					response.setResponseCode("000");
					response.setResponseMessage(resp.getHostHeaderInfo().getResponseMessage());
					// response.setActivityDescription("UNBLOCK CARD");
					// response.setCardId("");
					// response.setRefId(genRef);
				} else {
					response.setResponseCode(resp.getHostHeaderInfo().getResponseCode());
					response.setResponseMessage(resp.getHostHeaderInfo().getResponseMessage());
				}
			}
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		} finally {
		}
		AdapterInteraction.close();
		return response;
	}
	
	
	public CardServiceResponseDTO changecardpin(String affCode, String account, String cvv, String expiryDate, String mobileNo, String newPin, String oldPin, String pan) throws Exception {
		CardServiceResponseDTO response = new CardServiceResponseDTO();
		response.setResponseCode("E04");
		response.setResponseMessage("Failed at interface level");
		AdapterInteraction.begin();

		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESC_CREDIT_CARD_CHANGEPIN");
		String requestId = GetRefNumber("CP", 12);
		String url = digx_consulting.get("ESC_CREDIT_CARD_CHANGEPIN", "");
		String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String pwd = digx_consulting.get("ESB_OBDX_PWD", "");
		String requestToken = JEncrypt.GenerateTransactionToken(sourceCode, requestId, pwd);
		String ipx = digx_consulting.get("ESB_OBDX_IP", "");
		
		ESBCardChangePinRequestDTO esbRequestDTO = new ESBCardChangePinRequestDTO();
		HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
		hostHeaderInfo.setAffiliateCode(affCode);
		hostHeaderInfo.setIpAddress(ipx);
		hostHeaderInfo.setRequestId(requestId);
		hostHeaderInfo.setRequestToken(requestToken);
		hostHeaderInfo.setRequestType("resetcardpin");
		hostHeaderInfo.setSourceChannelId("ECOBANKMOBILEAPP");
		hostHeaderInfo.setSourceCode(sourceCode);
		esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);
		esbRequestDTO.setPan(JEncrypt.encrypt(pan));
		esbRequestDTO.setExpiryDate(expiryDate);
		esbRequestDTO.setCurrentPin(JEncrypt.encrypt(oldPin));
		esbRequestDTO.setNewPin(JEncrypt.encrypt(newPin));
		esbRequestDTO.setEmcertid("");

		ESBCardResponseDTO resp = null;

		try {
			ObjectMapper mapper = new ObjectMapper();
			String jsonRequestBody = mapper.writeValueAsString(esbRequestDTO);
			System.out.println(":: changecardpin jsonRequestBody : " + jsonRequestBody);

			Gson gson = new Gson();

			String genResponse = HttpPost(url, esbRequestDTO);
			resp = gson.fromJson(genResponse, ESBCardResponseDTO.class);
			
			//resp = gson.fromJson(MockServcie.resetCardPin(), ESBCardResponseDTO.class);
			
			String jsonResponseBody = mapper.writeValueAsString(resp);
			System.out.println(":: jsonResponseBody : " + jsonResponseBody);

			System.out.println("getResponseCode         : " + resp.getHostHeaderInfo().getResponseCode());
			System.out.println("getResponseMessage      : " + resp.getHostHeaderInfo().getResponseMessage());

			if (resp != null) {
				response.setResponseCode(resp.getHostHeaderInfo().getResponseCode());
				response.setResponseMessage(resp.getHostHeaderInfo().getResponseMessage());
			}

		} catch (JsonProcessingException e) {
			e.printStackTrace();
		} finally {
		}
		AdapterInteraction.close();

		return response;
	}
	
	
	public CardServiceResponseDTO resetPin(String account, String affCode, String expiryDate, String mobileNo, String pin, String pan) throws Exception {
		CardServiceResponseDTO response = new CardServiceResponseDTO();
		response.setResponseCode("E04");
		response.setResponseMessage("Failed at interface level");
		AdapterInteraction.begin();

		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESC_CREDIT_CARD_RESETPIN");

		String requestId = GetRefNumber("RP", 12);
		String url = digx_consulting.get("ESC_CREDIT_CARD_RESETPIN", "");
		String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String pwd = digx_consulting.get("ESB_OBDX_PWD", "");
		String requestToken = JEncrypt.GenerateTransactionToken(sourceCode, requestId, pwd);
		String ipx = digx_consulting.get("ESB_OBDX_IP", "");
		
		ESBCardResetPinRequestDTO esbRequestDTO = new ESBCardResetPinRequestDTO();
		HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
		hostHeaderInfo.setAffiliateCode(affCode);
		hostHeaderInfo.setIpAddress(ipx);
		hostHeaderInfo.setRequestId(requestId);
		hostHeaderInfo.setRequestToken(requestToken);
		hostHeaderInfo.setRequestType("resetcardpin");
		hostHeaderInfo.setSourceChannelId("ECOBANKMOBILEAPP");
		hostHeaderInfo.setSourceCode(sourceCode);
		esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);
		esbRequestDTO.setPan(JEncrypt.encrypt(pan));
		esbRequestDTO.setPin(JEncrypt.encrypt(pin));
		esbRequestDTO.setExpiryDate(expiryDate);
		esbRequestDTO.setAccount(account);
		esbRequestDTO.setMobileNo(mobileNo);
		esbRequestDTO.setEmcertid("");

		ESBCardResponseDTO resp = null;

		try {
			ObjectMapper mapper = new ObjectMapper();
			String jsonRequestBody = mapper.writeValueAsString(esbRequestDTO);
			System.out.println(":: resetPin jsonRequestBody : " + jsonRequestBody);

			Gson gson = new Gson();

			String genResponse = HttpPost(url, esbRequestDTO);
			resp = gson.fromJson(genResponse, ESBCardResponseDTO.class);
			
			//resp = gson.fromJson(MockServcie.resetCardPin(), ESBCardResponseDTO.class);
			
			String jsonResponseBody = mapper.writeValueAsString(resp);
			System.out.println(":: resetPin jsonResponseBody : " + jsonResponseBody);

			System.out.println("getResponseCode         : " + resp.getHostHeaderInfo().getResponseCode());
			System.out.println("getResponseMessage      : " + resp.getHostHeaderInfo().getResponseMessage());

			if (resp != null) {
				response.setResponseCode(resp.getHostHeaderInfo().getResponseCode());
				response.setResponseMessage(resp.getHostHeaderInfo().getResponseMessage());
			}
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		} finally {
		}
		AdapterInteraction.close();

		return response;
	}
	
	
	public CreditCardBalDetailRespDTO fetchCreditCardBalanceDetails(String cardAcct, String affCode) throws Exception {
		CreditCardBalDetailRespDTO response = new CreditCardBalDetailRespDTO();
		response.setResponseCode("E04");
		response.setResponseMessage("Failed at interface level");
		AdapterInteraction.begin();

		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESB_CREDIT_CARD_BALANCE");

		
		String url = digx_consulting.get("ESB_CREDIT_CARD_BALANCE", "");
		
		ESBCardRequestDTO esbRequestDTO = new ESBCardRequestDTO();
		esbRequestDTO.setCardAcct(cardAcct);
		esbRequestDTO.setAffCode(affCode);

		CreditCardBalanceDetailResponseDTO resp = null;

		try {
			ObjectMapper mapper = new ObjectMapper();
			String jsonRequestBody = mapper.writeValueAsString(esbRequestDTO);
			System.out.println(":: fetchCreditCardBalanceDetails jsonRequestBody : " + jsonRequestBody);

			Gson gson = new Gson();

			String genResponse = HttpPost(url, esbRequestDTO);
			resp = gson.fromJson(genResponse, CreditCardBalanceDetailResponseDTO.class);
			
			//resp = gson.fromJson(MockServcie.fetchCreditCardBalanceDetails(), CreditCardBalanceDetailResponseDTO.class);
			
			String jsonResponseBody = mapper.writeValueAsString(resp);
			System.out.println(":: fetchCreditCardBalanceDetails jsonResponseBody : " + jsonResponseBody);

			System.out.println("getResponseCode         : " + resp.getHostHeaderInfo().getResponseCode());
			System.out.println("getResponseMessage      : " + resp.getHostHeaderInfo().getResponseMessage());

			response.setResponseCode(resp.getHostHeaderInfo().getResponseCode());
			response.setResponseMessage(resp.getHostHeaderInfo().getResponseMessage());
			if (resp != null && resp.getHostHeaderInfo().getResponseCode().equals("000")) {
				response.setAccountNo(resp.getAccountNo());
				response.setAvailableAmount(resp.getAvailableAmount());
				response.setBalance(resp.getBalance());
				response.setBankC(resp.getBankC());
				response.setCardAcct(resp.getCardAcct());
				response.setCrd(resp.getCrd());
				response.setMinPayable(resp.getMinPayable());
				response.setOtb(resp.getOtb());
			}
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		} finally {
		}
		AdapterInteraction.close();

		return response;
	}
	
	
	public CardServiceResponseDTO changecardpin(String account, String cvv, String expiryDate, String mobileNo, String newPin, String oldPin, String pan) throws Exception {
		CardServiceResponseDTO response = new CardServiceResponseDTO();
		response.setResponseCode("E04");
		response.setResponseMessage("Failed at interface level");
		AdapterInteraction.begin();

		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESC_CREDIT_CARD_CHANGEPIN");
		RESTClientFactory clientFactory = RESTClientFactory.getInstance();
		IRESTClient restClient = clientFactory.getRESTClientInstance();

		String requestId = GetRefNumber("BC", 12);
		String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String pwd = digx_consulting.get("ESB_OBDX_PWD", "");
		String requestToken = JEncrypt.GenerateTransactionToken(sourceCode, requestId, pwd);
		String ipx = digx_consulting.get("ESB_OBDX_IP", "");
		
		ESBCardChangePinRequestDTO esbRequestDTO = new ESBCardChangePinRequestDTO();
		HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
		hostHeaderInfo.setAffiliateCode("EGH"); //temporarily hardcoded
		hostHeaderInfo.setIpAddress(ipx);
		hostHeaderInfo.setRequestId(requestId);
		hostHeaderInfo.setRequestToken(requestToken);
		hostHeaderInfo.setRequestType("resetcardpin");
		hostHeaderInfo.setSourceChannelId("ECOBANKMOBILEAPP");
		hostHeaderInfo.setSourceCode(sourceCode);
		esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);
		esbRequestDTO.setPan(JEncrypt.encrypt(pan));
		esbRequestDTO.setExpiryDate(expiryDate);
		//esbRequestDTO.setCurrentPin(oldPin);
		//esbRequestDTO.setNewPin(newPin);
		esbRequestDTO.setCurrentPin(JEncrypt.encrypt(oldPin));
		esbRequestDTO.setNewPin(JEncrypt.encrypt(newPin));
		esbRequestDTO.setEmcertid("");

		ESBCardResponseDTO resp = null;

		try {
			XStream xs = new XStream();
			logger.info("Card Change Pin Request : " + xs.toXML(esbRequestDTO));
			logger.info("Card Change Pin Source Code : " + sourceCode);

			resp = (ESBCardResponseDTO) restClient.post(prop, ESBCardResponseDTO.class, esbRequestDTO);
			logger.info("Generate CustomerNo     : " + resp.getCustomerNo());
			logger.info("Generate Card-Response  : " + xs.toXML(resp));
			logger.info("getResponseCode         : " + resp.getHostHeaderInfo().getResponseCode());
			logger.info("getResponseMessage      : " + resp.getHostHeaderInfo().getResponseMessage());
			if (resp != null && resp.getHostHeaderInfo().getResponseCode().equals("000")) {
				response.setResponseCode("000");
				response.setResponseMessage(resp.getHostHeaderInfo().getResponseMessage());
				//response.setActivityDescription("UNBLOCK CARD");
				//response.setCardId("");
				//response.setRefId(genRef);
			}
		} finally {
		}
		AdapterInteraction.close();

		return response;
	}
	
	
	public CardServiceResponseDTO resetPin2(String account, String cvv, String expiryDate, String mobileNo, String pin, String pan) throws Exception {
		CardServiceResponseDTO response = new CardServiceResponseDTO();
		response.setResponseCode("E04");
		response.setResponseMessage("Failed at interface level");
		AdapterInteraction.begin();

		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESC_CREDIT_CARD_RESETPIN");
		RESTClientFactory clientFactory = RESTClientFactory.getInstance();
		IRESTClient restClient = clientFactory.getRESTClientInstance();

		String requestId = GetRefNumber("BC", 12);
		String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String pwd = digx_consulting.get("ESB_OBDX_PWD", "");
		String requestToken = JEncrypt.GenerateTransactionToken(sourceCode, requestId, pwd);
		String ipx = digx_consulting.get("ESB_OBDX_IP", "");
		
		ESBCardResetPinRequestDTO esbRequestDTO = new ESBCardResetPinRequestDTO();
		HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
		hostHeaderInfo.setAffiliateCode("EGH"); //temporarily hardcoded
		hostHeaderInfo.setIpAddress(ipx);
		hostHeaderInfo.setRequestId(requestId);
		hostHeaderInfo.setRequestToken(requestToken);
		hostHeaderInfo.setRequestType("resetcardpin");
		hostHeaderInfo.setSourceChannelId("ECOBANKMOBILEAPP");
		hostHeaderInfo.setSourceCode(sourceCode);
		esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);
		//esbRequestDTO.setPan(pan);
		esbRequestDTO.setPan(JEncrypt.encrypt(pan));
		esbRequestDTO.setExpiryDate(expiryDate);
		esbRequestDTO.setAccount(account);
		esbRequestDTO.setMobileNo(mobileNo);
		//esbRequestDTO.setPin(pin);
		esbRequestDTO.setPin(JEncrypt.encrypt(pin));
		esbRequestDTO.setEmcertid("");

		ESBCardResponseDTO resp = null;

		try {
			XStream xs = new XStream();
			logger.info("Card PinReset Request : " + xs.toXML(esbRequestDTO));
			logger.info("Card PinReset Source Code : " + sourceCode);

			resp = (ESBCardResponseDTO) restClient.post(prop, ESBCardResponseDTO.class, esbRequestDTO);
			logger.info("Generate CustomerNo     : " + resp.getCustomerNo());
			logger.info("Generate Card-Response  : " + xs.toXML(resp));
			logger.info("getResponseCode         : " + resp.getHostHeaderInfo().getResponseCode());
			logger.info("getResponseMessage      : " + resp.getHostHeaderInfo().getResponseMessage());
			if (resp != null && resp.getHostHeaderInfo().getResponseCode().equals("000")) {
				response.setResponseCode("000");
				response.setResponseMessage(resp.getHostHeaderInfo().getResponseMessage());
				//response.setActivityDescription("UNBLOCK CARD");
				//response.setCardId("");
				//response.setRefId(genRef);
			}
		} finally {
		}
		AdapterInteraction.close();

		return response;
	}
	
	
	
	public static String HttpPost(String contextUrl,  Object payloadRequest) {

		StringBuffer resp = new StringBuffer();
		URL obj = null;
		Gson gson = new Gson();
		try {
			System.out.println("URL " + contextUrl);
			obj = new URL(contextUrl );

			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			con.setRequestMethod("POST");
			con.setRequestProperty("Content-Type", "application/json");

			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(gson.toJson(payloadRequest));

			wr.flush();
			wr.close();
			String responseStatus = con.getResponseMessage();
			System.out.println(responseStatus);
			int responseCode = con.getResponseCode();
			System.out.println("responseCode " + responseCode);

			String inputLine;

			if (responseCode == 200) {
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				while ((inputLine = in.readLine()) != null) {
					resp.append(inputLine);
				}
				in.close();
				//System.out.println("response:" + resp.toString());
			} else {
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getErrorStream()));
				while ((inputLine = in.readLine()) != null) {
					resp.append(inputLine);
				}
				in.close();
				//System.out.println("response 2" + resp.toString());
			}
			
			System.out.println("response: " + resp);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		return resp.toString();

	}
	
	
	public  String GetRefNumber(String type, int len) {

        String finalString = "";
        int x = 0;
        char[] stringChars = new char[len];
        for (int i = 0; i < len; i++) //4
        {
            Random random = new Random();
            x = random.nextInt(9);

            stringChars[i] = Integer.toString(x).toCharArray()[0];
        }


        finalString = new String(stringChars);
        finalString = type + finalString;
        return finalString.trim();
    }
}
