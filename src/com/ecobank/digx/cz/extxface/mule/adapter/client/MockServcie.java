package com.ecobank.digx.cz.extxface.mule.adapter.client;

public class MockServcie {
	
	public static String generateVCard() {
		return "{\n" + 
				" \"STATUS_CODE\": \"000\",\n" + 
				" \"STATUS_MESSAGE\": \"Your Transaction Request is Successful and Approved\",\n" + 
				" \"ACCOUNT_DETAILS\": [\n" + 
				" {\n" + 
				" \"accountNumber\": \"1441001601306\",\n" + 
				" \"cardAlias\": \"9900990881417756\",\n" + 
				" \"expiry\": \"31-MAR-2022\",\n" + 
				" \"cvv\": \"666\",\n" + 
				" \"scheme\": \"Visa\",\n" + 
				" \"cardType\": \"SHOPVCARD\",\n" + 
				" \"share\": false,\n" + 
				" \"activated\": true,\n" + 
				" \"blocked\": false,\n" + 
				" \"balance\": \"0\",\n" + 
				" \"currencyCode\": \"\",\n" + 
				" \"otpNumber\": \"233204009110\",\n" + 
				" \"cardToken\": \"fb38570358fd413c826ffe4020b55f73\",\n" + 
				" \"CardName\": \"Shopping Card\",\n" + 
				" \"custId\": \"12121951\"\n" + 
				" }\n" + 
				" ]\n" + 
				"}";
	}
	
	public static String fetchVCardByMsisdn() {
		String resp = "{\n" + 
				"    \"STATUS_CODE\": \"000\",\n" + 
				"    \"STATUS_MESSAGE\": \"Your Transaction Request is Successful and Approved\",\n" + 
				"    \"ACCOUNT_DETAILS\": [\n" + 
				"        {\n" + 
				"            \"accountNumber\": \"9900990465114662\",\n" + 
				"            \"cardAlias\": \"990099XXXXXX4662\",\n" + 
				"            \"expiry\": \"30-APR-2022\",\n" + 
				"            \"cvv\": \"319\",\n" + 
				"            \"scheme\": \"Visa\",\n" + 
				"            \"cardType\": \"SHOPVCARD\",\n" + 
				"            \"share\": false,\n" + 
				"            \"activated\": true,\n" + 
				"            \"blocked\": false,\n" + 
				"            \"balance\": null,\n" + 
				"            \"currencyCode\": \"GHS\",\n" + 
				"            \"otpNumber\": \"233242242107\",\n" + 
				"            \"cardToken\": \"ca6ec2ed387a4f6b9294bb959fcd7104\",\n" + 
				"            \"CardName\": \"Shopping Card\",\n" + 
				"            \"custId\": \"12307644\"\n" + 
				"        },\n" + 
				"        {\n" + 
				"            \"accountNumber\": \"9900990996151043\",\n" + 
				"            \"cardAlias\": \"990099XXXXXX1043\",\n" + 
				"            \"expiry\": \"30-APR-2022\",\n" + 
				"            \"cvv\": \"067\",\n" + 
				"            \"scheme\": \"Visa\",\n" + 
				"            \"cardType\": \"SHOPVCARD\",\n" + 
				"            \"share\": false,\n" + 
				"            \"activated\": true,\n" + 
				"            \"blocked\": false,\n" + 
				"            \"balance\": null,\n" + 
				"            \"currencyCode\": \"GHS\",\n" + 
				"            \"otpNumber\": \"233242242107\",\n" + 
				"            \"cardToken\": \"700c1f2c3b264c4f9211fe717b6f52d4\",\n" + 
				"            \"CardName\": \"Shopping Card\",\n" + 
				"            \"custId\": \"12306933\"\n" + 
				"        },\n" + 
				"        {\n" + 
				"            \"accountNumber\": \"9900990370592549\",\n" + 
				"            \"cardAlias\": \"990099XXXXXX2549\",\n" + 
				"            \"expiry\": \"30-APR-2022\",\n" + 
				"            \"cvv\": \"842\",\n" + 
				"            \"scheme\": \"Visa\",\n" + 
				"            \"cardType\": \"SHOPVCARD\",\n" + 
				"            \"share\": false,\n" + 
				"            \"activated\": true,\n" + 
				"            \"blocked\": false,\n" + 
				"            \"balance\": null,\n" + 
				"            \"currencyCode\": \"GHS\",\n" + 
				"            \"otpNumber\": \"233242242107\",\n" + 
				"            \"cardToken\": \"2617d6f36bc74d9db152b15f6ae2187d\",\n" + 
				"            \"CardName\": \"Shopping Card\",\n" + 
				"            \"custId\": \"12299731\"\n" + 
				"        },\n" + 
				"        {\n" + 
				"            \"accountNumber\": \"9900990216965495\",\n" + 
				"            \"cardAlias\": \"990099XXXXXX5495\",\n" + 
				"            \"expiry\": \"30-APR-2022\",\n" + 
				"            \"cvv\": \"653\",\n" + 
				"            \"scheme\": \"Visa\",\n" + 
				"            \"cardType\": \"SHOPVCARD\",\n" + 
				"            \"share\": false,\n" + 
				"            \"activated\": true,\n" + 
				"            \"blocked\": false,\n" + 
				"            \"balance\": null,\n" + 
				"            \"currencyCode\": \"GHS\",\n" + 
				"            \"otpNumber\": \"233242242107\",\n" + 
				"            \"cardToken\": \"94056c92562d4d1b899e5dce00141024\",\n" + 
				"            \"CardName\": \"Shopping Card\",\n" + 
				"            \"custId\": \"12299726\"\n" + 
				"        },\n" + 
				"        {\n" + 
				"            \"accountNumber\": \"9900990979817438\",\n" + 
				"            \"cardAlias\": \"990099XXXXXX7438\",\n" + 
				"            \"expiry\": \"30-APR-2022\",\n" + 
				"            \"cvv\": \"749\",\n" + 
				"            \"scheme\": \"Visa\",\n" + 
				"            \"cardType\": \"SHOPVCARD\",\n" + 
				"            \"share\": false,\n" + 
				"            \"activated\": true,\n" + 
				"            \"blocked\": false,\n" + 
				"            \"balance\": null,\n" + 
				"            \"currencyCode\": \"GHS\",\n" + 
				"            \"otpNumber\": \"233242242107\",\n" + 
				"            \"cardToken\": \"d8c2fc0c493147fabf4e02bc928d59c7\",\n" + 
				"            \"CardName\": \"Shopping Card\",\n" + 
				"            \"custId\": \"12229870\"\n" + 
				"        },\n" + 
				"        {\n" + 
				"            \"accountNumber\": \"9900990319136911\",\n" + 
				"            \"cardAlias\": \"990099XXXXXX6911\",\n" + 
				"            \"expiry\": \"30-APR-2022\",\n" + 
				"            \"cvv\": \"629\",\n" + 
				"            \"scheme\": \"Visa\",\n" + 
				"            \"cardType\": \"SHOPVCARD\",\n" + 
				"            \"share\": false,\n" + 
				"            \"activated\": true,\n" + 
				"            \"blocked\": false,\n" + 
				"            \"balance\": null,\n" + 
				"            \"currencyCode\": \"GHS\",\n" + 
				"            \"otpNumber\": \"233242242107\",\n" + 
				"            \"cardToken\": \"1c75c92dc8af47e49114e13df405637c\",\n" + 
				"            \"CardName\": \"Shopping Card\",\n" + 
				"            \"custId\": \"12229835\"\n" + 
				"        },\n" + 
				"        {\n" + 
				"            \"accountNumber\": \"9900990270662830\",\n" + 
				"            \"cardAlias\": \"990099XXXXXX2830\",\n" + 
				"            \"expiry\": \"31-MAR-2022\",\n" + 
				"            \"cvv\": \"191\",\n" + 
				"            \"scheme\": \"Visa\",\n" + 
				"            \"cardType\": \"SHOPVCARD\",\n" + 
				"            \"share\": false,\n" + 
				"            \"activated\": true,\n" + 
				"            \"blocked\": false,\n" + 
				"            \"balance\": null,\n" + 
				"            \"currencyCode\": \"GHS\",\n" + 
				"            \"otpNumber\": \"233242242107\",\n" + 
				"            \"cardToken\": \"b01351f2ef3445a19a07563a007b4fd5\",\n" + 
				"            \"CardName\": \"Shopping Card\",\n" + 
				"            \"custId\": \"12208922\"\n" + 
				"        },\n" + 
				"        {\n" + 
				"            \"accountNumber\": \"9900990395191384\",\n" + 
				"            \"cardAlias\": \"990099XXXXXX1384\",\n" + 
				"            \"expiry\": \"31-MAR-2022\",\n" + 
				"            \"cvv\": \"019\",\n" + 
				"            \"scheme\": \"Visa\",\n" + 
				"            \"cardType\": \"SHOPVCARD\",\n" + 
				"            \"share\": false,\n" + 
				"            \"activated\": true,\n" + 
				"            \"blocked\": false,\n" + 
				"            \"balance\": null,\n" + 
				"            \"currencyCode\": \"GHS\",\n" + 
				"            \"otpNumber\": \"233242242107\",\n" + 
				"            \"cardToken\": \"0f4b96275ee94114bd2ddad777bea80a\",\n" + 
				"            \"CardName\": \"Shopping Card\",\n" + 
				"            \"custId\": \"12179255\"\n" + 
				"        },\n" + 
				"        {\n" + 
				"            \"accountNumber\": \"9900990029161985\",\n" + 
				"            \"cardAlias\": \"990099XXXXXX1985\",\n" + 
				"            \"expiry\": \"31-MAR-2022\",\n" + 
				"            \"cvv\": \"328\",\n" + 
				"            \"scheme\": \"Visa\",\n" + 
				"            \"cardType\": \"SHOPVCARD\",\n" + 
				"            \"share\": false,\n" + 
				"            \"activated\": true,\n" + 
				"            \"blocked\": false,\n" + 
				"            \"balance\": null,\n" + 
				"            \"currencyCode\": \"GHS\",\n" + 
				"            \"otpNumber\": \"233242242107\",\n" + 
				"            \"cardToken\": \"e615c759b6194f86adc56f8ee8431fee\",\n" + 
				"            \"CardName\": \"Shopping Card\",\n" + 
				"            \"custId\": \"12165871\"\n" + 
				"        },\n" + 
				"        {\n" + 
				"            \"accountNumber\": \"9900990952855975\",\n" + 
				"            \"cardAlias\": \"990099XXXXXX5975\",\n" + 
				"            \"expiry\": \"31-MAR-2022\",\n" + 
				"            \"cvv\": \"722\",\n" + 
				"            \"scheme\": \"Visa\",\n" + 
				"            \"cardType\": \"SHOPVCARD\",\n" + 
				"            \"share\": false,\n" + 
				"            \"activated\": true,\n" + 
				"            \"blocked\": false,\n" + 
				"            \"balance\": null,\n" + 
				"            \"currencyCode\": \"GHS\",\n" + 
				"            \"otpNumber\": \"233242242107\",\n" + 
				"            \"cardToken\": \"83eb633744c742649086104e3cabadc0\",\n" + 
				"            \"CardName\": \"Shopping Card\",\n" + 
				"            \"custId\": \"12165870\"\n" + 
				"        },\n" + 
				"        {\n" + 
				"            \"accountNumber\": \"9900990090219431\",\n" + 
				"            \"cardAlias\": \"990099XXXXXX9431\",\n" + 
				"            \"expiry\": \"31-MAR-2022\",\n" + 
				"            \"cvv\": \"675\",\n" + 
				"            \"scheme\": \"Visa\",\n" + 
				"            \"cardType\": \"SHOPVCARD\",\n" + 
				"            \"share\": false,\n" + 
				"            \"activated\": true,\n" + 
				"            \"blocked\": false,\n" + 
				"            \"balance\": null,\n" + 
				"            \"currencyCode\": \"GHS\",\n" + 
				"            \"otpNumber\": \"233242242107\",\n" + 
				"            \"cardToken\": \"d61a7430c4a4406aa707260c7046c460\",\n" + 
				"            \"CardName\": \"Shopping Card\",\n" + 
				"            \"custId\": \"12165835\"\n" + 
				"        },\n" + 
				"        {\n" + 
				"            \"accountNumber\": \"9900990250379660\",\n" + 
				"            \"cardAlias\": \"990099XXXXXX9660\",\n" + 
				"            \"expiry\": \"31-MAR-2022\",\n" + 
				"            \"cvv\": \"621\",\n" + 
				"            \"scheme\": \"Visa\",\n" + 
				"            \"cardType\": \"SHOPVCARD\",\n" + 
				"            \"share\": false,\n" + 
				"            \"activated\": true,\n" + 
				"            \"blocked\": false,\n" + 
				"            \"balance\": null,\n" + 
				"            \"currencyCode\": \"GHS\",\n" + 
				"            \"otpNumber\": \"233242242107\",\n" + 
				"            \"cardToken\": \"a72c584075574e4195cdda3e6aa9d8b9\",\n" + 
				"            \"CardName\": \"Shopping Card\",\n" + 
				"            \"custId\": \"12165834\"\n" + 
				"        },\n" + 
				"        {\n" + 
				"            \"accountNumber\": \"9900990886020233\",\n" + 
				"            \"cardAlias\": \"990099XXXXXX0233\",\n" + 
				"            \"expiry\": \"31-MAR-2022\",\n" + 
				"            \"cvv\": \"046\",\n" + 
				"            \"scheme\": \"Visa\",\n" + 
				"            \"cardType\": \"SHOPVCARD\",\n" + 
				"            \"share\": false,\n" + 
				"            \"activated\": true,\n" + 
				"            \"blocked\": false,\n" + 
				"            \"balance\": null,\n" + 
				"            \"currencyCode\": \"GHS\",\n" + 
				"            \"otpNumber\": \"233242242107\",\n" + 
				"            \"cardToken\": \"70c01fcb718c4d2aa394a6dbb777b293\",\n" + 
				"            \"CardName\": \"Shopping Card\",\n" + 
				"            \"custId\": \"12165832\"\n" + 
				"        },\n" + 
				"        {\n" + 
				"            \"accountNumber\": \"9900990664612730\",\n" + 
				"            \"cardAlias\": \"990099XXXXXX2730\",\n" + 
				"            \"expiry\": \"31-MAR-2022\",\n" + 
				"            \"cvv\": \"303\",\n" + 
				"            \"scheme\": \"Visa\",\n" + 
				"            \"cardType\": \"SHOPVCARD\",\n" + 
				"            \"share\": false,\n" + 
				"            \"activated\": true,\n" + 
				"            \"blocked\": false,\n" + 
				"            \"balance\": null,\n" + 
				"            \"currencyCode\": \"GHS\",\n" + 
				"            \"otpNumber\": \"233242242107\",\n" + 
				"            \"cardToken\": \"784265d5efff47bb8b789e56f73c0cf6\",\n" + 
				"            \"CardName\": \"Shopping Card\",\n" + 
				"            \"custId\": \"12165831\"\n" + 
				"        },\n" + 
				"        {\n" + 
				"            \"accountNumber\": \"9900990338788841\",\n" + 
				"            \"cardAlias\": \"990099XXXXXX8841\",\n" + 
				"            \"expiry\": \"31-MAR-2022\",\n" + 
				"            \"cvv\": \"655\",\n" + 
				"            \"scheme\": \"Visa\",\n" + 
				"            \"cardType\": \"SHOPVCARD\",\n" + 
				"            \"share\": false,\n" + 
				"            \"activated\": true,\n" + 
				"            \"blocked\": false,\n" + 
				"            \"balance\": null,\n" + 
				"            \"currencyCode\": \"GHS\",\n" + 
				"            \"otpNumber\": \"233242242107\",\n" + 
				"            \"cardToken\": \"0e0f00507d2342dc852a17af796695cb\",\n" + 
				"            \"CardName\": \"Shopping Card\",\n" + 
				"            \"custId\": \"12165830\"\n" + 
				"        },\n" + 
				"        {\n" + 
				"            \"accountNumber\": \"9900990925577664\",\n" + 
				"            \"cardAlias\": \"990099XXXXXX7664\",\n" + 
				"            \"expiry\": \"31-MAR-2022\",\n" + 
				"            \"cvv\": \"660\",\n" + 
				"            \"scheme\": \"Visa\",\n" + 
				"            \"cardType\": \"SHOPVCARD\",\n" + 
				"            \"share\": false,\n" + 
				"            \"activated\": true,\n" + 
				"            \"blocked\": false,\n" + 
				"            \"balance\": null,\n" + 
				"            \"currencyCode\": \"GHS\",\n" + 
				"            \"otpNumber\": \"233242242107\",\n" + 
				"            \"cardToken\": \"f8192b30677e4fd398204b0a49f83bc7\",\n" + 
				"            \"CardName\": \"Shopping Card\",\n" + 
				"            \"custId\": \"12165827\"\n" + 
				"        },\n" + 
				"        {\n" + 
				"            \"accountNumber\": \"9900990825358108\",\n" + 
				"            \"cardAlias\": \"990099XXXXXX8108\",\n" + 
				"            \"expiry\": \"31-MAR-2022\",\n" + 
				"            \"cvv\": \"648\",\n" + 
				"            \"scheme\": \"Visa\",\n" + 
				"            \"cardType\": \"SHOPVCARD\",\n" + 
				"            \"share\": false,\n" + 
				"            \"activated\": true,\n" + 
				"            \"blocked\": false,\n" + 
				"            \"balance\": null,\n" + 
				"            \"currencyCode\": \"GHS\",\n" + 
				"            \"otpNumber\": \"233242242107\",\n" + 
				"            \"cardToken\": \"d6edb376afea4d3db8aae6e11fac0243\",\n" + 
				"            \"CardName\": \"Shopping Card\",\n" + 
				"            \"custId\": \"12165825\"\n" + 
				"        },\n" + 
				"        {\n" + 
				"            \"accountNumber\": \"9900990374800336\",\n" + 
				"            \"cardAlias\": \"990099XXXXXX0336\",\n" + 
				"            \"expiry\": \"31-MAR-2022\",\n" + 
				"            \"cvv\": \"927\",\n" + 
				"            \"scheme\": \"Visa\",\n" + 
				"            \"cardType\": \"SHOPVCARD\",\n" + 
				"            \"share\": false,\n" + 
				"            \"activated\": true,\n" + 
				"            \"blocked\": false,\n" + 
				"            \"balance\": null,\n" + 
				"            \"currencyCode\": \"GHS\",\n" + 
				"            \"otpNumber\": \"233242242107\",\n" + 
				"            \"cardToken\": \"d308bbe17b0a4dce8e4af3284fbd96d6\",\n" + 
				"            \"CardName\": \"Shopping Card\",\n" + 
				"            \"custId\": \"12165824\"\n" + 
				"        },\n" + 
				"        {\n" + 
				"            \"accountNumber\": \"9900990859309720\",\n" + 
				"            \"cardAlias\": \"990099XXXXXX9720\",\n" + 
				"            \"expiry\": \"31-MAR-2022\",\n" + 
				"            \"cvv\": \"762\",\n" + 
				"            \"scheme\": \"Visa\",\n" + 
				"            \"cardType\": \"SHOPVCARD\",\n" + 
				"            \"share\": false,\n" + 
				"            \"activated\": true,\n" + 
				"            \"blocked\": false,\n" + 
				"            \"balance\": null,\n" + 
				"            \"currencyCode\": \"GHS\",\n" + 
				"            \"otpNumber\": \"233242242107\",\n" + 
				"            \"cardToken\": \"441e7dafe9a84630a81f0bb2670446a6\",\n" + 
				"            \"CardName\": \"Shopping Card\",\n" + 
				"            \"custId\": \"12165823\"\n" + 
				"        },\n" + 
				"        {\n" + 
				"            \"accountNumber\": \"9900990979905449\",\n" + 
				"            \"cardAlias\": \"990099XXXXXX5449\",\n" + 
				"            \"expiry\": \"31-MAR-2022\",\n" + 
				"            \"cvv\": \"933\",\n" + 
				"            \"scheme\": \"Visa\",\n" + 
				"            \"cardType\": \"SHOPVCARD\",\n" + 
				"            \"share\": false,\n" + 
				"            \"activated\": true,\n" + 
				"            \"blocked\": false,\n" + 
				"            \"balance\": null,\n" + 
				"            \"currencyCode\": \"GHS\",\n" + 
				"            \"otpNumber\": \"233242242107\",\n" + 
				"            \"cardToken\": \"3b25d2d9e6a341e8bd35706d91a060bb\",\n" + 
				"            \"CardName\": \"Shopping Card\",\n" + 
				"            \"custId\": \"12165817\"\n" + 
				"        },\n" + 
				"        {\n" + 
				"            \"accountNumber\": \"9900990815106384\",\n" + 
				"            \"cardAlias\": \"990099XXXXXX6384\",\n" + 
				"            \"expiry\": \"31-MAR-2022\",\n" + 
				"            \"cvv\": \"898\",\n" + 
				"            \"scheme\": \"Visa\",\n" + 
				"            \"cardType\": \"SHOPVCARD\",\n" + 
				"            \"share\": false,\n" + 
				"            \"activated\": true,\n" + 
				"            \"blocked\": false,\n" + 
				"            \"balance\": null,\n" + 
				"            \"currencyCode\": \"GHS\",\n" + 
				"            \"otpNumber\": \"233242242107\",\n" + 
				"            \"cardToken\": \"9102286c37f444e28a4556898835f557\",\n" + 
				"            \"CardName\": \"Shopping Card\",\n" + 
				"            \"custId\": \"12165814\"\n" + 
				"        },\n" + 
				"        {\n" + 
				"            \"accountNumber\": \"9900990258324601\",\n" + 
				"            \"cardAlias\": \"990099XXXXXX4601\",\n" + 
				"            \"expiry\": \"31-MAR-2022\",\n" + 
				"            \"cvv\": \"891\",\n" + 
				"            \"scheme\": \"Visa\",\n" + 
				"            \"cardType\": \"SHOPVCARD\",\n" + 
				"            \"share\": false,\n" + 
				"            \"activated\": true,\n" + 
				"            \"blocked\": false,\n" + 
				"            \"balance\": null,\n" + 
				"            \"currencyCode\": \"GHS\",\n" + 
				"            \"otpNumber\": \"233242242107\",\n" + 
				"            \"cardToken\": \"72a291e01d6c40328abb67a209b712a9\",\n" + 
				"            \"CardName\": \"Shopping Card\",\n" + 
				"            \"custId\": \"12165806\"\n" + 
				"        },\n" + 
				"        {\n" + 
				"            \"accountNumber\": \"9900990981559689\",\n" + 
				"            \"cardAlias\": \"990099XXXXXX9689\",\n" + 
				"            \"expiry\": \"31-MAR-2022\",\n" + 
				"            \"cvv\": \"083\",\n" + 
				"            \"scheme\": \"Visa\",\n" + 
				"            \"cardType\": \"SHOPVCARD\",\n" + 
				"            \"share\": false,\n" + 
				"            \"activated\": true,\n" + 
				"            \"blocked\": false,\n" + 
				"            \"balance\": null,\n" + 
				"            \"currencyCode\": \"GHS\",\n" + 
				"            \"otpNumber\": \"233242242107\",\n" + 
				"            \"cardToken\": \"513769a4d4af4bf486862151150eed13\",\n" + 
				"            \"CardName\": \"Shopping Card\",\n" + 
				"            \"custId\": \"12165796\"\n" + 
				"        },\n" + 
				"        {\n" + 
				"            \"accountNumber\": \"9900990988492587\",\n" + 
				"            \"cardAlias\": \"990099XXXXXX2587\",\n" + 
				"            \"expiry\": \"31-MAR-2022\",\n" + 
				"            \"cvv\": \"719\",\n" + 
				"            \"scheme\": \"Visa\",\n" + 
				"            \"cardType\": \"SHOPVCARD\",\n" + 
				"            \"share\": false,\n" + 
				"            \"activated\": true,\n" + 
				"            \"blocked\": false,\n" + 
				"            \"balance\": null,\n" + 
				"            \"currencyCode\": \"GHS\",\n" + 
				"            \"otpNumber\": \"233242242107\",\n" + 
				"            \"cardToken\": \"efc961850e1a4e5489a51576062f1fac\",\n" + 
				"            \"CardName\": \"Shopping Card\",\n" + 
				"            \"custId\": \"12165789\"\n" + 
				"        },\n" + 
				"        {\n" + 
				"            \"accountNumber\": \"9900990105365245\",\n" + 
				"            \"cardAlias\": \"990099XXXXXX5245\",\n" + 
				"            \"expiry\": \"31-MAR-2022\",\n" + 
				"            \"cvv\": \"990\",\n" + 
				"            \"scheme\": \"Visa\",\n" + 
				"            \"cardType\": \"SHOPVCARD\",\n" + 
				"            \"share\": false,\n" + 
				"            \"activated\": true,\n" + 
				"            \"blocked\": false,\n" + 
				"            \"balance\": null,\n" + 
				"            \"currencyCode\": \"GHS\",\n" + 
				"            \"otpNumber\": \"233242242107\",\n" + 
				"            \"cardToken\": \"980c8ea0e3a54aa08c23b65b6df73559\",\n" + 
				"            \"CardName\": \"Shopping Card\",\n" + 
				"            \"custId\": \"12165787\"\n" + 
				"        },\n" + 
				"        {\n" + 
				"            \"accountNumber\": \"9900990944017973\",\n" + 
				"            \"cardAlias\": \"990099XXXXXX7973\",\n" + 
				"            \"expiry\": \"31-MAR-2022\",\n" + 
				"            \"cvv\": \"501\",\n" + 
				"            \"scheme\": \"Visa\",\n" + 
				"            \"cardType\": \"SHOPVCARD\",\n" + 
				"            \"share\": false,\n" + 
				"            \"activated\": true,\n" + 
				"            \"blocked\": false,\n" + 
				"            \"balance\": null,\n" + 
				"            \"currencyCode\": \"GHS\",\n" + 
				"            \"otpNumber\": \"233242242107\",\n" + 
				"            \"cardToken\": \"308357937c8242328bebe3af94540914\",\n" + 
				"            \"CardName\": \"Shopping Card\",\n" + 
				"            \"custId\": \"12165772\"\n" + 
				"        },\n" + 
				"        {\n" + 
				"            \"accountNumber\": \"9900990732246917\",\n" + 
				"            \"cardAlias\": \"990099XXXXXX6917\",\n" + 
				"            \"expiry\": \"31-MAR-2022\",\n" + 
				"            \"cvv\": \"243\",\n" + 
				"            \"scheme\": \"Visa\",\n" + 
				"            \"cardType\": \"SHOPVCARD\",\n" + 
				"            \"share\": false,\n" + 
				"            \"activated\": true,\n" + 
				"            \"blocked\": false,\n" + 
				"            \"balance\": null,\n" + 
				"            \"currencyCode\": \"GHS\",\n" + 
				"            \"otpNumber\": \"233242242107\",\n" + 
				"            \"cardToken\": \"c0dff02c6103434a9c23c7d6e1028b82\",\n" + 
				"            \"CardName\": \"Shopping Card\",\n" + 
				"            \"custId\": \"12165770\"\n" + 
				"        },\n" + 
				"        {\n" + 
				"            \"accountNumber\": \"9900990536927324\",\n" + 
				"            \"cardAlias\": \"990099XXXXXX7324\",\n" + 
				"            \"expiry\": \"31-MAR-2022\",\n" + 
				"            \"cvv\": \"766\",\n" + 
				"            \"scheme\": \"Visa\",\n" + 
				"            \"cardType\": \"SHOPVCARD\",\n" + 
				"            \"share\": false,\n" + 
				"            \"activated\": true,\n" + 
				"            \"blocked\": false,\n" + 
				"            \"balance\": null,\n" + 
				"            \"currencyCode\": \"GHS\",\n" + 
				"            \"otpNumber\": \"233242242107\",\n" + 
				"            \"cardToken\": \"7ebd28faaa8e498481b0066a7ccc7af3\",\n" + 
				"            \"CardName\": \"Shopping Card\",\n" + 
				"            \"custId\": \"12165769\"\n" + 
				"        },\n" + 
				"        {\n" + 
				"            \"accountNumber\": \"9900990240626345\",\n" + 
				"            \"cardAlias\": \"990099XXXXXX6345\",\n" + 
				"            \"expiry\": \"31-MAR-2022\",\n" + 
				"            \"cvv\": \"717\",\n" + 
				"            \"scheme\": \"Visa\",\n" + 
				"            \"cardType\": \"SHOPVCARD\",\n" + 
				"            \"share\": false,\n" + 
				"            \"activated\": true,\n" + 
				"            \"blocked\": false,\n" + 
				"            \"balance\": null,\n" + 
				"            \"currencyCode\": \"GHS\",\n" + 
				"            \"otpNumber\": \"233242242107\",\n" + 
				"            \"cardToken\": \"6b335e6601b24593b1fdfc72c44d8807\",\n" + 
				"            \"CardName\": \"Shopping Card\",\n" + 
				"            \"custId\": \"12165672\"\n" + 
				"        },\n" + 
				"        {\n" + 
				"            \"accountNumber\": \"9900990248529582\",\n" + 
				"            \"cardAlias\": \"990099XXXXXX9582\",\n" + 
				"            \"expiry\": \"31-MAR-2022\",\n" + 
				"            \"cvv\": \"590\",\n" + 
				"            \"scheme\": \"Visa\",\n" + 
				"            \"cardType\": \"SHOPVCARD\",\n" + 
				"            \"share\": false,\n" + 
				"            \"activated\": true,\n" + 
				"            \"blocked\": false,\n" + 
				"            \"balance\": null,\n" + 
				"            \"currencyCode\": \"GHS\",\n" + 
				"            \"otpNumber\": \"233242242107\",\n" + 
				"            \"cardToken\": \"4e23335213094a86894601f2071bd824\",\n" + 
				"            \"CardName\": \"Shopping Card\",\n" + 
				"            \"custId\": \"12110783\"\n" + 
				"        },\n" + 
				"        {\n" + 
				"            \"accountNumber\": null,\n" + 
				"            \"cardAlias\": \"430545XXXXXX3939\",\n" + 
				"            \"expiry\": null,\n" + 
				"            \"cvv\": null,\n" + 
				"            \"scheme\": \"Visa\",\n" + 
				"            \"cardType\": \"SHOPVCARD\",\n" + 
				"            \"share\": false,\n" + 
				"            \"activated\": true,\n" + 
				"            \"blocked\": false,\n" + 
				"            \"balance\": null,\n" + 
				"            \"currencyCode\": \"GHS\",\n" + 
				"            \"otpNumber\": \"233242242107\",\n" + 
				"            \"cardToken\": \"a506c73850ad4777be38f85e5ada6c03\",\n" + 
				"            \"CardName\": \"Shopping Card\",\n" + 
				"            \"custId\": \"11180354\"\n" + 
				"        }\n" + 
				"    ]\n" + 
				"}";
		return resp;
	}
	
	public static String balanceVCheck() {
		return "{\n" + 
				"    \"hostHeaderInfo\": {\n" + 
				"        \"sourceCode\": \"ECOBANKMOBILE\",\n" + 
				"        \"requestId\": \"891237128000\",\n" + 
				"        \"affiliateCode\": \"ECI\",\n" + 
				"        \"responseCode\": \"000\",\n" + 
				"        \"responseMessage\": \"Your Transaction Request is Successful and Approved\",\n" + 
				"        \"ipAddress\": \"10.10.0.10\"\n" + 
				"    },\n" + 
				"    \"balance\": 200.98,\n" + 
				"    \"blocked\": \"N\"\n" + 
				"}";
	}
	
	
	public static String fundVCard() {
		return "{\n" + 
				"    \"hostHeaderInfo\": {\n" + 
				"        \"sourceCode\": \"ECOBANKMOBILE\",\n" + 
				"        \"requestId\": \"9879879082\",\n" + 
				"        \"affiliateCode\": \"EGH\",\n" + 
				"        \"responseCode\": \"000\",\n" + 
				"        \"responseMessage\": \"Your Transaction Request is Successful and Approved\",\n" + 
				"        \"ipAddress\": \"10.2.5.6\"\n" + 
				"    },\n" + 
				"    \"transactionReference\": null,\n" + 
				"    \"fulfilmentReference\": null,\n" + 
				"    \"details\": {\n" + 
				"        \"accountNumber\": \"1441001601306\",\n" + 
				"        \"cardAlias\": \"XXXXXXXXXXXX2830\",\n" + 
				"        \"expiry\": \"/A/N/\",\n" + 
				"        \"cvv\": \"N/A\",\n" + 
				"        \"scheme\": \"PREPAID\",\n" + 
				"        \"cardType\": \"SHOPVCARD\",\n" + 
				"        \"share\": false,\n" + 
				"        \"activated\": true,\n" + 
				"        \"blocked\": false,\n" + 
				"        \"balance\": \"39.2\",\n" + 
				"        \"currencyCode\": \"GHS\",\n" + 
				"        \"otpNumber\": \"233242242107\",\n" + 
				"        \"cardToken\": \"b01351f2ef3445a19a07563a007b4fd5\",\n" + 
				"        \"CardName\": null,\n" + 
				"        \"custId\": null\n" + 
				"    }\n" + 
				"}";
	}
	
	
	public static String miniVStatement() {
		return "{\n" + 
				"    \"hostHeaderInfo\": {\n" + 
				"        \"sourceCode\": \"ECOBANKMOBILE\",\n" + 
				"        \"requestId\": \"2384923752\",\n" + 
				"        \"affiliateCode\": \"EGH\",\n" + 
				"        \"operatorCode\": null,\n" + 
				"        \"responseCode\": \"000\",\n" + 
				"        \"responseMessage\": \"Success\",\n" + 
				"        \"ipAddress\": \"10.10.0.10\"\n" + 
				"    },\n" + 
				"    \"transactionList\": [\n" + 
				"        {\n" + 
				"            \"transactionId\": \"373073172\",\n" + 
				"            \"transactionDate\": \"03-AUG-2019\",\n" + 
				"            \"transactionTime\": \"12:25:03\",\n" + 
				"            \"amount\": \"0\",\n" + 
				"            \"fee\": \"-5\",\n" + 
				"            \"totalAmount\": \"-5\",\n" + 
				"            \"transactionDesc\": \"Mini Statement\",\n" + 
				"            \"referenceInfo\": \"SMS Mini Statement\"\n" + 
				"        },\n" + 
				"        {\n" + 
				"            \"transactionId\": \"372171399\",\n" + 
				"            \"transactionDate\": \"01-AUG-2019\",\n" + 
				"            \"transactionTime\": \"06:19:25\",\n" + 
				"            \"amount\": \"100\",\n" + 
				"            \"fee\": \"0\",\n" + 
				"            \"totalAmount\": \"100\",\n" + 
				"            \"transactionDesc\": \"Funds Transfer External Account to Card\",\n" + 
				"            \"referenceInfo\": \"Mobile TopUp\"\n" + 
				"        }\n" + 
				"    ]\n" + 
				"}";
	}
	
	
	public static String blockVCard() {
		return "{ \"hostHeaderInfo\": {\n" + 
				"      \"sourceCode\": \"ECOBANKMOBILE\",\n" + 
				"      \"requestId\": \"891237128938\",\n" + 
				"      \"affiliateCode\": \"EGH\",\n" + 
				"      \"responseCode\": \"000\",\n" + 
				"      \"responseMessage\": \"Success\"\n" + 
				"   }\n" + 
				"}";
	}
	
	
	public static String unblockVCard() {
		return "{\n" + 
				"    \"hostHeaderInfo\": {\n" + 
				"        \"sourceCode\": \"ECOBANKMOBILE\",\n" + 
				"        \"requestId\": \"891237128938\",\n" + 
				"        \"affiliateCode\": \"EGH\",\n" + 
				"        \"responseCode\": \"000\",\n" + 
				"        \"responseMessage\": \"Success\"\n" + 
				"    }\n" + 
				"}";
	}
	
	public static String updateVOTPNumber() {
		return "{\n" + 
			"    \"hostHeaderInfo\": {\n" + 
			"        \"sourceCode\": \"ECOBANKMOBILE\",\n" + 
			"        \"requestId\": \"123123123\",\n" + 
			"        \"affiliateCode\": \"EGH\",\n" + 
			"        \"operatorCode\": \"\",\n" + 
			"        \"responseCode\": \"000\",\n" + 
			"        \"responseMessage\": \"Success\",\n" + 
			"        \"ipAddress\": \"10.1.1.1\"\n" + 
			"    },\n" + 
			"    \"amount\": 100,\n" + 
			"    \"charge\": 3,\n" + 
			"    \"tax\": 15,\n" + 
			"    \"totalCharge\": 18,\n" + 
			"    \"exchRate\": 0,\n" + 
			"    \"sourceAccountCcy\": \"GHS\",\n" + 
			"    \"udf1\": null,\n" + 
			"    \"sourceAccount\": \"1441001655169\",\n" + 
			"    \"sourceAccountName\": \"\",\n" + 
			"    \"sourceAccountStatus\": \"\"\n" + 
			"}";
	}
	
	
	public static String closeOrDeleteVCard() {
		return "{\n" + 
				"    \"hostHeaderInfo\": {\n" + 
				"        \"sourceCode\": \"ECOBANKMOBILE\",\n" + 
				"        \"requestId\": \"230923489024\",\n" + 
				"        \"affiliateCode\": \"EGH\",\n" + 
				"        \"operatorCode\": null,\n" + 
				"        \"responseCode\": \"000\",\n" + 
				"        \"responseMessage\": \"Success\",\n" + 
				"        \"ipAddress\": \"10.10.0.10\"\n" + 
				"    }\n" + 
				"}";
	}
	
	
	
	
	///////////////////////////////////////////////////////////////
    ////////////////////PREPAID MOCK SERVICES//////////////////////
    ///////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////
	
	public static String getPrepaidBalance() {
		return "{\n" + 
				"    \"hostHeaderInfo\": {\n" + 
				"        \"sourceCode\": \"ECOBANKMOBILEAPP\",\n" + 
				"        \"requestId\": \"123\",\n" + 
				"        \"affiliateCode\": \"EGH\",\n" + 
				"        \"responseCode\": \"000\",\n" + 
				"        \"responseMessage\": \"Balance Enquiry Successful\"\n" + 
				"    },\n" + 
				"    \"last4Digits\": \"uzvpQZroPOHcTjE0WYJ/cA==\",\n" + 
				"    \"expiryDate\": \"KGrgDWH6yIriAvmAXs8F8A==\",\n" + 
				"    \"cvv\": \"ISR8hCKHdrI2C8Xd9BWSXQ==\",\n" + 
				"    \"balance\": \"OGE9gdVae+hYCfFLY3gDAg==\",\n" + 
				"    \"blocked\": \"duQvX2Pw7LsYxktZrSp/7Q==\"\n" + 
				"}";
	}
	
	public static String fundPrepaidCard() {
		return "{\n" + 
				"    \"hostHeaderInfo\": {\n" + 
				"        \"sourceCode\": \"ECOBANKMOBILEAPP\",\n" + 
				"        \"requestId\": \"11236001\",\n" + 
				"        \"affiliateCode\": \"EGH\",\n" + 
				"        \"responseCode\": \"000\",\n" + 
				"        \"responseMessage\": \"Success with transactionID 668108881\"\n" + 
				"    },\n" + 
				"    \"last4Digits\": \"uzvpQZroPOHcTjE0WYJ/cA==\",\n" + 
				"    \"expiryDate\": \"EMOuWQyNcVnavSz51trFIw==\",\n" + 
				"    \"cvv\": \"ISR8hCKHdrI2C8Xd9BWSXQ==\",\n" + 
				"    \"balance\": \"OGE9gdVae+hYCfFLY3gDAg==\",\n" + 
				"    \"blocked\": \"duQvX2Pw7LsYxktZrSp/7Q==\"\n" + 
				"}";
	}
	
	public static String prepaidCustDetails() {
		return "{\n" + 
				"    \"hostHeaderInfo\": {\n" + 
				"        \"sourceCode\": \"ECOBANKMOBILEAPP\",\n" + 
				"        \"requestId\": \"123\",\n" + 
				"        \"affiliateCode\": \"EGH\",\n" + 
				"        \"responseCode\": \"000\",\n" + 
				"        \"responseMessage\": \"Success\"\n" + 
				"    },\n" + 
				"    \"firstName\": \"UEZfck4iD0tOZ5Vcp+e5tA==\",\n" + 
				"    \"middleName\": \"EMOuWQyNcVnavSz51trFIw==\",\n" + 
				"    \"lastName\": \"ZFgzYZK0jmKHuJfb+s74Eg==\",\n" + 
				"    \"preferredName\": \"EMOuWQyNcVnavSz51trFIw==\",\n" + 
				"    \"otherCustomerID\": \"EMOuWQyNcVnavSz51trFIw==\",\n" + 
				"    \"address1\": \"W9oGq01LVdVjAzImbrsWLA==\",\n" + 
				"    \"address2\": \"EMOuWQyNcVnavSz51trFIw==\",\n" + 
				"    \"address3\": \"EMOuWQyNcVnavSz51trFIw==\",\n" + 
				"    \"city\": \"W9oGq01LVdVjAzImbrsWLA==\",\n" + 
				"    \"country\": \"HnwyYt5BsGtkvWOPxUeTAg==\",\n" + 
				"    \"stateRegion\": \"3GAcmW96DsPOBacs/5LlmQ==\",\n" + 
				"    \"postalCode\": \"uFA3RWaWenR61QY87Y77/w==\",\n" + 
				"    \"dob\": \"kA7nKERECizLkPKU+DLKHg==\",\n" + 
				"    \"idType\": \"RYr7+mOEizWFDVfX+v8/xQ==\",\n" + 
				"    \"idValue\": \"EmiYxgmwJPc/zbJhK7teew==\",\n" + 
				"    \"mobileNo\": \"EmiYxgmwJPc/zbJhK7teew==\",\n" + 
				"    \"alternatePhoneNo\": \"EMOuWQyNcVnavSz51trFIw==\",\n" + 
				"    \"email\": \"xzQXSlVgHVbSAZZ/nXMamHM0YZGm5o1KoEHRnTMbFNk=\"\n" + 
				"}";
	}
	
	public static String prepaidCardHistory() {
		return "{\n" + 
				"    \"hostHeaderInfo\": {\n" + 
				"        \"sourceCode\": \"ECOBANKMOBILEAPP\",\n" + 
				"        \"requestId\": \"123\",\n" + 
				"        \"affiliateCode\": \"EGH\",\n" + 
				"        \"responseCode\": \"000\",\n" + 
				"        \"responseMessage\": \"Success\"\n" + 
				"    },\n" + 
				"    \"transactionDetailsList\": []\n" + 
				"}";
	}
	
	
	public static String blockPrepaidCard() {
		return "{\n" + 
				"    \"hostHeaderInfo\": {\n" + 
				"        \"sourceCode\": \"ECOBANKMOBILEAPP\",\n" + 
				"        \"requestId\": \"123\",\n" + 
				"        \"affiliateCode\": \"EGH\",\n" + 
				"        \"responseCode\": \"000\",\n" + 
				"        \"responseMessage\": \"Card Blocked Successfully\"\n" + 
				"    },\n" + 
				"    \"last4Digits\": \"uzvpQZroPOHcTjE0WYJ/cA==\",\n" + 
				"    \"expiryDate\": \"KGrgDWH6yIriAvmAXs8F8A==\",\n" + 
				"    \"cvv\": \"ISR8hCKHdrI2C8Xd9BWSXQ==\",\n" + 
				"    \"balance\": \"tuIK4ktTYO7bQ2T5Tt80aw==\",\n" + 
				"    \"blocked\": \"rG6KmpyM1ersuXQz9uAjFw==\"\n" + 
				"}";
	}
	
	
	public static String unblockPrepaidCard() {
		return "{\n" + 
				"    \"hostHeaderInfo\": {\n" + 
				"        \"sourceCode\": \"ECOBANKMOBILEAPP\",\n" + 
				"        \"requestId\": \"123\",\n" + 
				"        \"affiliateCode\": \"EGH\",\n" + 
				"        \"responseCode\": \"000\",\n" + 
				"        \"responseMessage\": \"Card UnBlocked Successfully\"\n" + 
				"    },\n" + 
				"    \"last4Digits\": \"uzvpQZroPOHcTjE0WYJ/cA==\",\n" + 
				"    \"expiryDate\": \"KGrgDWH6yIriAvmAXs8F8A==\",\n" + 
				"    \"cvv\": \"ISR8hCKHdrI2C8Xd9BWSXQ==\",\n" + 
				"    \"balance\": \"tuIK4ktTYO7bQ2T5Tt80aw==\",\n" + 
				"    \"blocked\": \"duQvX2Pw7LsYxktZrSp/7Q==\"\n" + 
				"}";
	}
	
	
	public static String updatepcardOTP() {
		return "{\n" + 
				"    \"hostHeaderInfo\": {\n" + 
				"        \"sourceCode\": \"ECOBANKMOBILEAPP\",\n" + 
				"        \"requestId\": \"123\",\n" + 
				"        \"affiliateCode\": \"EGH\",\n" + 
				"        \"responseCode\": \"000\",\n" + 
				"        \"responseMessage\": \"Success\"\n" + 
				"    },\n" + 
				"    \"updateCount\": \"ZngSLonqM1pke+LIOyxiIg==\"\n" + 
				"}";
	}
	
	
	
	///////////////////////////////////////////////////////////////
	////////////////////CREDITCARD MOCK SERVICES//////////////////////
	///////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////
	
	
	public static String getCCardBalance() {
		return "{\n" + 
				"    \"hostHeaderInfo\": {\n" + 
				"        \"sourceCode\": \"ECOBANKMOBILE\",\n" + 
				"        \"requestId\": \"E200403201133XD\",\n" + 
				"        \"affiliateCode\": \"EGH\",\n" + 
				"        \"responseCode\": \"000\",\n" + 
				"        \"responseMessage\": \"Your Transaction Request is Successful and Approved\"\n" + 
				"    },\n" + 
				"    \"accountNo\": \"11182932\",\n" + 
				"    \"cardAccount\": \"4691685100\",\n" + 
				"    \"status\": \"1\",\n" + 
				"    \"currency\": \"GHS\",\n" + 
				"    \"endBalance\": 108100,\n" + 
				"    \"lockedAmount\": 0,\n" + 
				"    \"availableAmount\": 117100,\n" + 
				"    \"stopCause\": \"6\",\n" + 
				"    \"expiry\": \"2023-06-30T00:00:00\"\n" + 
				"}";
	}
	
	
	public static String getCCardPseudoPAN() {
		
		return "{\n" + 
				"    \"hostHeaderInfo\": {\n" + 
				"        \"sourceCode\": \"OBDX\",\n" + 
				"        \"requestId\": \"E2004032101133XD\",\n" + 
				"        \"affiliateCode\": \"EGH\",\n" + 
				"        \"responseCode\": \"000\",\n" + 
				"        \"responseMessage\": \"Your Transaction Request is Successful and Approved\",\n" + 
				"        \"ipAddress\": \"1.1.1.1\"\n" + 
				"    },\n" + 
				"    \"name\": \"PLAT \",\n" + 
				"    \"cardNo\": \"Ur15LnZk/TqIP1F9AkbsfqJP1Zdt2FBj\",\n" + 
				"    \"cardAcct\": \"4691695600\",\n" + 
				"    \"psuedoPan\": \"513798LOIZDD3330\",\n" + 
				"    \"expDate\": \"2023-12-31T00:00:00\",\n" + 
				"    \"limitType\": null,\n" + 
				"    \"limitValue\": null,\n" + 
				"    \"limitName\": null,\n" + 
				"    \"ccy\": \"GHS\"\n" + 
				"}";
		
	}
	
	
	public static String getCCStatement() {
		return "{\n" + 
				"    \"hostHeaderInfo\": {\n" + 
				"        \"sourceCode\": \"ECOBANKMOBILE\",\n" + 
				"        \"requestId\": \"46775\",\n" + 
				"        \"affiliateCode\": \"EGH\",\n" + 
				"        \"responseCode\": \"000\",\n" + 
				"        \"responseMessage\": \"Your Transaction Request is Successful and Approved\"\n" + 
				"    },\n" + 
				"    \"transactions\": [\n" + 
				"        {\n" + 
				"            \"cardAccount\": \"4691685100\",\n" + 
				"            \"accountNo\": \"11182932\",\n" + 
				"            \"card\": \"422170NRXQFL2770\",\n" + 
				"            \"currency\": \"GHS\",\n" + 
				"            \"amount\": 4000,\n" + 
				"            \"tranDate\": \"2020-06-02T00:00:00\",\n" + 
				"            \"recDate\": \"2020-06-02T00:00:00\",\n" + 
				"            \"terminal\": \"\",\n" + 
				"            \"merchant\": \"\",\n" + 
				"            \"country\": \"\",\n" + 
				"            \"city\": \"\",\n" + 
				"            \"internalNo\": \"108735560\",\n" + 
				"            \"desc\": \"\",\n" + 
				"            \"additionalInfo\": \"\",\n" + 
				"            \"tranType\": \"32D\",\n" + 
				"            \"ccyExponent\": \"2\"\n" + 
				"        },\n" + 
				"        {\n" + 
				"            \"cardAccount\": \"4691685100\",\n" + 
				"            \"accountNo\": \"11182932\",\n" + 
				"            \"card\": \"422170YRTXEK7471\",\n" + 
				"            \"currency\": \"GHS\",\n" + 
				"            \"amount\": 4000,\n" + 
				"            \"tranDate\": \"2020-06-02T00:00:00\",\n" + 
				"            \"recDate\": \"2020-06-02T00:00:00\",\n" + 
				"            \"terminal\": \"\",\n" + 
				"            \"merchant\": \"\",\n" + 
				"            \"country\": \"\",\n" + 
				"            \"city\": \"\",\n" + 
				"            \"internalNo\": \"108735561\",\n" + 
				"            \"desc\": \"\",\n" + 
				"            \"additionalInfo\": \"\",\n" + 
				"            \"tranType\": \"32D\",\n" + 
				"            \"ccyExponent\": \"2\"\n" + 
				"        },\n" + 
				"        {\n" + 
				"            \"cardAccount\": \"4691685100\",\n" + 
				"            \"accountNo\": \"11182932\",\n" + 
				"            \"card\": \"\",\n" + 
				"            \"currency\": \"GHS\",\n" + 
				"            \"amount\": 1000,\n" + 
				"            \"tranDate\": \"2020-06-02T00:00:00\",\n" + 
				"            \"recDate\": \"2020-06-02T00:00:00\",\n" + 
				"            \"terminal\": \"\",\n" + 
				"            \"merchant\": \"\",\n" + 
				"            \"country\": \"\",\n" + 
				"            \"city\": \"\",\n" + 
				"            \"internalNo\": \"108735590\",\n" + 
				"            \"desc\": \"Testing 4\",\n" + 
				"            \"additionalInfo\": \"API_ONLINE_PAYMENT\",\n" + 
				"            \"tranType\": \"110\",\n" + 
				"            \"ccyExponent\": \"2\"\n" + 
				"        },\n" + 
				"        {\n" + 
				"            \"cardAccount\": \"4691685100\",\n" + 
				"            \"accountNo\": \"11182932\",\n" + 
				"            \"card\": \"\",\n" + 
				"            \"currency\": \"GHS\",\n" + 
				"            \"amount\": 1000,\n" + 
				"            \"tranDate\": \"2020-06-02T00:00:00\",\n" + 
				"            \"recDate\": \"2020-06-02T00:00:00\",\n" + 
				"            \"terminal\": \"\",\n" + 
				"            \"merchant\": \"\",\n" + 
				"            \"country\": \"\",\n" + 
				"            \"city\": \"\",\n" + 
				"            \"internalNo\": \"108735591\",\n" + 
				"            \"desc\": \"Test\",\n" + 
				"            \"additionalInfo\": \"API_ONLINE_PAYMENT\",\n" + 
				"            \"tranType\": \"110\",\n" + 
				"            \"ccyExponent\": \"2\"\n" + 
				"        },\n" + 
				"        {\n" + 
				"            \"cardAccount\": \"4691685100\",\n" + 
				"            \"accountNo\": \"11182932\",\n" + 
				"            \"card\": \"\",\n" + 
				"            \"currency\": \"GHS\",\n" + 
				"            \"amount\": 10000,\n" + 
				"            \"tranDate\": \"2020-06-02T00:00:00\",\n" + 
				"            \"recDate\": \"2020-06-02T00:00:00\",\n" + 
				"            \"terminal\": \"\",\n" + 
				"            \"merchant\": \"\",\n" + 
				"            \"country\": \"\",\n" + 
				"            \"city\": \"\",\n" + 
				"            \"internalNo\": \"108735592\",\n" + 
				"            \"desc\": \"\",\n" + 
				"            \"additionalInfo\": \"API_ONLINE_PAYMENT\",\n" + 
				"            \"tranType\": \"110\",\n" + 
				"            \"ccyExponent\": \"2\"\n" + 
				"        },\n" + 
				"        {\n" + 
				"            \"cardAccount\": \"4691685100\",\n" + 
				"            \"accountNo\": \"11182932\",\n" + 
				"            \"card\": \"\",\n" + 
				"            \"currency\": \"GHS\",\n" + 
				"            \"amount\": 3000,\n" + 
				"            \"tranDate\": \"2020-07-06T00:00:00\",\n" + 
				"            \"recDate\": \"2020-07-06T00:00:00\",\n" + 
				"            \"terminal\": \"\",\n" + 
				"            \"merchant\": \"\",\n" + 
				"            \"country\": \"\",\n" + 
				"            \"city\": \"\",\n" + 
				"            \"internalNo\": \"108735623\",\n" + 
				"            \"desc\": \"qwerty testing 01\",\n" + 
				"            \"additionalInfo\": \"API_ONLINE_PAYMENT\",\n" + 
				"            \"tranType\": \"110\",\n" + 
				"            \"ccyExponent\": \"2\"\n" + 
				"        },\n" + 
				"        {\n" + 
				"            \"cardAccount\": \"4691685100\",\n" + 
				"            \"accountNo\": \"11182932\",\n" + 
				"            \"card\": \"\",\n" + 
				"            \"currency\": \"GHS\",\n" + 
				"            \"amount\": 3000,\n" + 
				"            \"tranDate\": \"2020-07-06T00:00:00\",\n" + 
				"            \"recDate\": \"2020-07-06T00:00:00\",\n" + 
				"            \"terminal\": \"\",\n" + 
				"            \"merchant\": \"\",\n" + 
				"            \"country\": \"\",\n" + 
				"            \"city\": \"\",\n" + 
				"            \"internalNo\": \"108735624\",\n" + 
				"            \"desc\": \"qwerty testing 01\",\n" + 
				"            \"additionalInfo\": \"API_ONLINE_PAYMENT\",\n" + 
				"            \"tranType\": \"110\",\n" + 
				"            \"ccyExponent\": \"2\"\n" + 
				"        },\n" + 
				"        {\n" + 
				"            \"cardAccount\": \"4691685100\",\n" + 
				"            \"accountNo\": \"11182932\",\n" + 
				"            \"card\": \"\",\n" + 
				"            \"currency\": \"GHS\",\n" + 
				"            \"amount\": 1000,\n" + 
				"            \"tranDate\": \"2020-07-06T00:00:00\",\n" + 
				"            \"recDate\": \"2020-07-06T00:00:00\",\n" + 
				"            \"terminal\": \"\",\n" + 
				"            \"merchant\": \"\",\n" + 
				"            \"country\": \"\",\n" + 
				"            \"city\": \"\",\n" + 
				"            \"internalNo\": \"108735625\",\n" + 
				"            \"desc\": \"qwerty testing 01\",\n" + 
				"            \"additionalInfo\": \"API_ONLINE_PAYMENT\",\n" + 
				"            \"tranType\": \"110\",\n" + 
				"            \"ccyExponent\": \"2\"\n" + 
				"        },\n" + 
				"        {\n" + 
				"            \"cardAccount\": \"4691685100\",\n" + 
				"            \"accountNo\": \"11182932\",\n" + 
				"            \"card\": \"\",\n" + 
				"            \"currency\": \"GHS\",\n" + 
				"            \"amount\": 1000,\n" + 
				"            \"tranDate\": \"2020-07-06T00:00:00\",\n" + 
				"            \"recDate\": \"2020-07-06T00:00:00\",\n" + 
				"            \"terminal\": \"\",\n" + 
				"            \"merchant\": \"\",\n" + 
				"            \"country\": \"\",\n" + 
				"            \"city\": \"\",\n" + 
				"            \"internalNo\": \"108735626\",\n" + 
				"            \"desc\": \"qwerty testing 01\",\n" + 
				"            \"additionalInfo\": \"API_ONLINE_PAYMENT\",\n" + 
				"            \"tranType\": \"110\",\n" + 
				"            \"ccyExponent\": \"2\"\n" + 
				"        },\n" + 
				"        {\n" + 
				"            \"cardAccount\": \"4691685100\",\n" + 
				"            \"accountNo\": \"11182932\",\n" + 
				"            \"card\": \"\",\n" + 
				"            \"currency\": \"GHS\",\n" + 
				"            \"amount\": 3000,\n" + 
				"            \"tranDate\": \"2020-07-06T00:00:00\",\n" + 
				"            \"recDate\": \"2020-07-06T00:00:00\",\n" + 
				"            \"terminal\": \"\",\n" + 
				"            \"merchant\": \"\",\n" + 
				"            \"country\": \"\",\n" + 
				"            \"city\": \"\",\n" + 
				"            \"internalNo\": \"108735627\",\n" + 
				"            \"desc\": \"qwerty testing 01\",\n" + 
				"            \"additionalInfo\": \"API_ONLINE_PAYMENT\",\n" + 
				"            \"tranType\": \"110\",\n" + 
				"            \"ccyExponent\": \"2\"\n" + 
				"        },\n" + 
				"        {\n" + 
				"            \"cardAccount\": \"4691685100\",\n" + 
				"            \"accountNo\": \"11182932\",\n" + 
				"            \"card\": \"\",\n" + 
				"            \"currency\": \"GHS\",\n" + 
				"            \"amount\": 1000,\n" + 
				"            \"tranDate\": \"2020-07-06T00:00:00\",\n" + 
				"            \"recDate\": \"2020-07-06T00:00:00\",\n" + 
				"            \"terminal\": \"\",\n" + 
				"            \"merchant\": \"\",\n" + 
				"            \"country\": \"\",\n" + 
				"            \"city\": \"\",\n" + 
				"            \"internalNo\": \"108735628\",\n" + 
				"            \"desc\": \"qwerty testing 01\",\n" + 
				"            \"additionalInfo\": \"API_ONLINE_PAYMENT\",\n" + 
				"            \"tranType\": \"110\",\n" + 
				"            \"ccyExponent\": \"2\"\n" + 
				"        },\n" + 
				"        {\n" + 
				"            \"cardAccount\": \"4691685100\",\n" + 
				"            \"accountNo\": \"11182932\",\n" + 
				"            \"card\": \"\",\n" + 
				"            \"currency\": \"GHS\",\n" + 
				"            \"amount\": 1000,\n" + 
				"            \"tranDate\": \"2020-07-06T00:00:00\",\n" + 
				"            \"recDate\": \"2020-07-06T00:00:00\",\n" + 
				"            \"terminal\": \"\",\n" + 
				"            \"merchant\": \"\",\n" + 
				"            \"country\": \"\",\n" + 
				"            \"city\": \"\",\n" + 
				"            \"internalNo\": \"108735629\",\n" + 
				"            \"desc\": \"qwerty testing 01\",\n" + 
				"            \"additionalInfo\": \"API_ONLINE_PAYMENT\",\n" + 
				"            \"tranType\": \"110\",\n" + 
				"            \"ccyExponent\": \"2\"\n" + 
				"        },\n" + 
				"        {\n" + 
				"            \"cardAccount\": \"4691685100\",\n" + 
				"            \"accountNo\": \"11182932\",\n" + 
				"            \"card\": \"\",\n" + 
				"            \"currency\": \"GHS\",\n" + 
				"            \"amount\": 1000,\n" + 
				"            \"tranDate\": \"2020-07-06T00:00:00\",\n" + 
				"            \"recDate\": \"2020-07-06T00:00:00\",\n" + 
				"            \"terminal\": \"\",\n" + 
				"            \"merchant\": \"\",\n" + 
				"            \"country\": \"\",\n" + 
				"            \"city\": \"\",\n" + 
				"            \"internalNo\": \"108735630\",\n" + 
				"            \"desc\": \"qwerty testing 01\",\n" + 
				"            \"additionalInfo\": \"API_ONLINE_PAYMENT\",\n" + 
				"            \"tranType\": \"110\",\n" + 
				"            \"ccyExponent\": \"2\"\n" + 
				"        },\n" + 
				"        {\n" + 
				"            \"cardAccount\": \"4691685100\",\n" + 
				"            \"accountNo\": \"11182932\",\n" + 
				"            \"card\": \"\",\n" + 
				"            \"currency\": \"GHS\",\n" + 
				"            \"amount\": 1000,\n" + 
				"            \"tranDate\": \"2020-07-06T00:00:00\",\n" + 
				"            \"recDate\": \"2020-07-06T00:00:00\",\n" + 
				"            \"terminal\": \"\",\n" + 
				"            \"merchant\": \"\",\n" + 
				"            \"country\": \"\",\n" + 
				"            \"city\": \"\",\n" + 
				"            \"internalNo\": \"108735631\",\n" + 
				"            \"desc\": \"qwerty testing 02\",\n" + 
				"            \"additionalInfo\": \"API_ONLINE_PAYMENT\",\n" + 
				"            \"tranType\": \"110\",\n" + 
				"            \"ccyExponent\": \"2\"\n" + 
				"        },\n" + 
				"        {\n" + 
				"            \"cardAccount\": \"4691685100\",\n" + 
				"            \"accountNo\": \"11182932\",\n" + 
				"            \"card\": \"\",\n" + 
				"            \"currency\": \"GHS\",\n" + 
				"            \"amount\": 1000,\n" + 
				"            \"tranDate\": \"2020-07-06T00:00:00\",\n" + 
				"            \"recDate\": \"2020-07-06T00:00:00\",\n" + 
				"            \"terminal\": \"\",\n" + 
				"            \"merchant\": \"\",\n" + 
				"            \"country\": \"\",\n" + 
				"            \"city\": \"\",\n" + 
				"            \"internalNo\": \"108735632\",\n" + 
				"            \"desc\": \"qwerty testing 01\",\n" + 
				"            \"additionalInfo\": \"API_ONLINE_PAYMENT\",\n" + 
				"            \"tranType\": \"110\",\n" + 
				"            \"ccyExponent\": \"2\"\n" + 
				"        },\n" + 
				"        {\n" + 
				"            \"cardAccount\": \"4691685100\",\n" + 
				"            \"accountNo\": \"11182932\",\n" + 
				"            \"card\": \"422170NRXQFL2770\",\n" + 
				"            \"currency\": \"GHS\",\n" + 
				"            \"amount\": 6000,\n" + 
				"            \"tranDate\": \"2020-07-06T00:00:00\",\n" + 
				"            \"recDate\": \"2020-07-06T00:00:00\",\n" + 
				"            \"terminal\": \"\",\n" + 
				"            \"merchant\": \"\",\n" + 
				"            \"country\": \"\",\n" + 
				"            \"city\": \"\",\n" + 
				"            \"internalNo\": \"108735633\",\n" + 
				"            \"desc\": \"\",\n" + 
				"            \"additionalInfo\": \"\",\n" + 
				"            \"tranType\": \"325\",\n" + 
				"            \"ccyExponent\": \"2\"\n" + 
				"        },\n" + 
				"        {\n" + 
				"            \"cardAccount\": \"4691685100\",\n" + 
				"            \"accountNo\": \"11182932\",\n" + 
				"            \"card\": \"422170YRTXEK7471\",\n" + 
				"            \"currency\": \"GHS\",\n" + 
				"            \"amount\": 6000,\n" + 
				"            \"tranDate\": \"2020-07-06T00:00:00\",\n" + 
				"            \"recDate\": \"2020-07-06T00:00:00\",\n" + 
				"            \"terminal\": \"\",\n" + 
				"            \"merchant\": \"\",\n" + 
				"            \"country\": \"\",\n" + 
				"            \"city\": \"\",\n" + 
				"            \"internalNo\": \"108735634\",\n" + 
				"            \"desc\": \"\",\n" + 
				"            \"additionalInfo\": \"\",\n" + 
				"            \"tranType\": \"325\",\n" + 
				"            \"ccyExponent\": \"2\"\n" + 
				"        },\n" + 
				"        {\n" + 
				"            \"cardAccount\": \"4691685100\",\n" + 
				"            \"accountNo\": \"11182932\",\n" + 
				"            \"card\": \"\",\n" + 
				"            \"currency\": \"GHS\",\n" + 
				"            \"amount\": 1000,\n" + 
				"            \"tranDate\": \"2020-07-06T00:00:00\",\n" + 
				"            \"recDate\": \"2020-07-06T00:00:00\",\n" + 
				"            \"terminal\": \"\",\n" + 
				"            \"merchant\": \"\",\n" + 
				"            \"country\": \"\",\n" + 
				"            \"city\": \"\",\n" + 
				"            \"internalNo\": \"108735635\",\n" + 
				"            \"desc\": \"the test\",\n" + 
				"            \"additionalInfo\": \"API_ONLINE_PAYMENT\",\n" + 
				"            \"tranType\": \"110\",\n" + 
				"            \"ccyExponent\": \"2\"\n" + 
				"        },\n" + 
				"        {\n" + 
				"            \"cardAccount\": \"4691685100\",\n" + 
				"            \"accountNo\": \"11182932\",\n" + 
				"            \"card\": \"\",\n" + 
				"            \"currency\": \"GHS\",\n" + 
				"            \"amount\": 1000,\n" + 
				"            \"tranDate\": \"2020-07-06T00:00:00\",\n" + 
				"            \"recDate\": \"2020-07-06T00:00:00\",\n" + 
				"            \"terminal\": \"\",\n" + 
				"            \"merchant\": \"\",\n" + 
				"            \"country\": \"\",\n" + 
				"            \"city\": \"\",\n" + 
				"            \"internalNo\": \"108735636\",\n" + 
				"            \"desc\": \"qwerty testing 01\",\n" + 
				"            \"additionalInfo\": \"API_ONLINE_PAYMENT\",\n" + 
				"            \"tranType\": \"110\",\n" + 
				"            \"ccyExponent\": \"2\"\n" + 
				"        },\n" + 
				"        {\n" + 
				"            \"cardAccount\": \"4691685100\",\n" + 
				"            \"accountNo\": \"11182932\",\n" + 
				"            \"card\": \"\",\n" + 
				"            \"currency\": \"GHS\",\n" + 
				"            \"amount\": 1000,\n" + 
				"            \"tranDate\": \"2020-07-06T00:00:00\",\n" + 
				"            \"recDate\": \"2020-07-06T00:00:00\",\n" + 
				"            \"terminal\": \"\",\n" + 
				"            \"merchant\": \"\",\n" + 
				"            \"country\": \"\",\n" + 
				"            \"city\": \"\",\n" + 
				"            \"internalNo\": \"108735641\",\n" + 
				"            \"desc\": \"qwerty testing 01\",\n" + 
				"            \"additionalInfo\": \"API_ONLINE_PAYMENT\",\n" + 
				"            \"tranType\": \"110\",\n" + 
				"            \"ccyExponent\": \"2\"\n" + 
				"        },\n" + 
				"        {\n" + 
				"            \"cardAccount\": \"4691685100\",\n" + 
				"            \"accountNo\": \"11182932\",\n" + 
				"            \"card\": \"\",\n" + 
				"            \"currency\": \"GHS\",\n" + 
				"            \"amount\": 1000,\n" + 
				"            \"tranDate\": \"2020-07-06T00:00:00\",\n" + 
				"            \"recDate\": \"2020-07-06T00:00:00\",\n" + 
				"            \"terminal\": \"\",\n" + 
				"            \"merchant\": \"\",\n" + 
				"            \"country\": \"\",\n" + 
				"            \"city\": \"\",\n" + 
				"            \"internalNo\": \"108735644\",\n" + 
				"            \"desc\": \"qwerty testing 01\",\n" + 
				"            \"additionalInfo\": \"API_ONLINE_PAYMENT\",\n" + 
				"            \"tranType\": \"110\",\n" + 
				"            \"ccyExponent\": \"2\"\n" + 
				"        },\n" + 
				"        {\n" + 
				"            \"cardAccount\": \"4691685100\",\n" + 
				"            \"accountNo\": \"11182932\",\n" + 
				"            \"card\": \"\",\n" + 
				"            \"currency\": \"GHS\",\n" + 
				"            \"amount\": 3000,\n" + 
				"            \"tranDate\": \"2020-07-06T00:00:00\",\n" + 
				"            \"recDate\": \"2020-07-06T00:00:00\",\n" + 
				"            \"terminal\": \"\",\n" + 
				"            \"merchant\": \"\",\n" + 
				"            \"country\": \"\",\n" + 
				"            \"city\": \"\",\n" + 
				"            \"internalNo\": \"108735645\",\n" + 
				"            \"desc\": \"qwerty testing 01\",\n" + 
				"            \"additionalInfo\": \"API_ONLINE_PAYMENT\",\n" + 
				"            \"tranType\": \"110\",\n" + 
				"            \"ccyExponent\": \"2\"\n" + 
				"        },\n" + 
				"        {\n" + 
				"            \"cardAccount\": \"4691685100\",\n" + 
				"            \"accountNo\": \"11182932\",\n" + 
				"            \"card\": \"\",\n" + 
				"            \"currency\": \"GHS\",\n" + 
				"            \"amount\": 3000,\n" + 
				"            \"tranDate\": \"2020-07-06T00:00:00\",\n" + 
				"            \"recDate\": \"2020-07-06T00:00:00\",\n" + 
				"            \"terminal\": \"\",\n" + 
				"            \"merchant\": \"\",\n" + 
				"            \"country\": \"\",\n" + 
				"            \"city\": \"\",\n" + 
				"            \"internalNo\": \"108735646\",\n" + 
				"            \"desc\": \"qwerty testing 01\",\n" + 
				"            \"additionalInfo\": \"API_ONLINE_PAYMENT\",\n" + 
				"            \"tranType\": \"110\",\n" + 
				"            \"ccyExponent\": \"2\"\n" + 
				"        },\n" + 
				"        {\n" + 
				"            \"cardAccount\": \"4691685100\",\n" + 
				"            \"accountNo\": \"11182932\",\n" + 
				"            \"card\": \"\",\n" + 
				"            \"currency\": \"GHS\",\n" + 
				"            \"amount\": 5000,\n" + 
				"            \"tranDate\": \"2020-07-06T00:00:00\",\n" + 
				"            \"recDate\": \"2020-07-06T00:00:00\",\n" + 
				"            \"terminal\": \"\",\n" + 
				"            \"merchant\": \"\",\n" + 
				"            \"country\": \"\",\n" + 
				"            \"city\": \"\",\n" + 
				"            \"internalNo\": \"108735713\",\n" + 
				"            \"desc\": \"CREDIT CARD FUNDING\",\n" + 
				"            \"additionalInfo\": \"API_ONLINE_PAYMENT\",\n" + 
				"            \"tranType\": \"110\",\n" + 
				"            \"ccyExponent\": \"2\"\n" + 
				"        },\n" + 
				"        {\n" + 
				"            \"cardAccount\": \"4691685100\",\n" + 
				"            \"accountNo\": \"11182932\",\n" + 
				"            \"card\": \"\",\n" + 
				"            \"currency\": \"GHS\",\n" + 
				"            \"amount\": 10000,\n" + 
				"            \"tranDate\": \"2020-07-06T00:00:00\",\n" + 
				"            \"recDate\": \"2020-07-06T00:00:00\",\n" + 
				"            \"terminal\": \"\",\n" + 
				"            \"merchant\": \"\",\n" + 
				"            \"country\": \"\",\n" + 
				"            \"city\": \"\",\n" + 
				"            \"internalNo\": \"108735719\",\n" + 
				"            \"desc\": \"CARD FUNDING\",\n" + 
				"            \"additionalInfo\": \"API_ONLINE_PAYMENT\",\n" + 
				"            \"tranType\": \"110\",\n" + 
				"            \"ccyExponent\": \"2\"\n" + 
				"        },\n" + 
				"        {\n" + 
				"            \"cardAccount\": \"4691685100\",\n" + 
				"            \"accountNo\": \"11182932\",\n" + 
				"            \"card\": \"\",\n" + 
				"            \"currency\": \"GHS\",\n" + 
				"            \"amount\": 10000,\n" + 
				"            \"tranDate\": \"2020-07-06T00:00:00\",\n" + 
				"            \"recDate\": \"2020-07-06T00:00:00\",\n" + 
				"            \"terminal\": \"\",\n" + 
				"            \"merchant\": \"\",\n" + 
				"            \"country\": \"\",\n" + 
				"            \"city\": \"\",\n" + 
				"            \"internalNo\": \"108735770\",\n" + 
				"            \"desc\": \"\",\n" + 
				"            \"additionalInfo\": \"API_ONLINE_PAYMENT\",\n" + 
				"            \"tranType\": \"110\",\n" + 
				"            \"ccyExponent\": \"2\"\n" + 
				"        },\n" + 
				"        {\n" + 
				"            \"cardAccount\": \"4691685100\",\n" + 
				"            \"accountNo\": \"11182932\",\n" + 
				"            \"card\": \"\",\n" + 
				"            \"currency\": \"GHS\",\n" + 
				"            \"amount\": 10000,\n" + 
				"            \"tranDate\": \"2020-07-06T00:00:00\",\n" + 
				"            \"recDate\": \"2020-07-06T00:00:00\",\n" + 
				"            \"terminal\": \"\",\n" + 
				"            \"merchant\": \"\",\n" + 
				"            \"country\": \"\",\n" + 
				"            \"city\": \"\",\n" + 
				"            \"internalNo\": \"108735771\",\n" + 
				"            \"desc\": \"\",\n" + 
				"            \"additionalInfo\": \"API_ONLINE_PAYMENT\",\n" + 
				"            \"tranType\": \"110\",\n" + 
				"            \"ccyExponent\": \"2\"\n" + 
				"        },\n" + 
				"        {\n" + 
				"            \"cardAccount\": \"4691685100\",\n" + 
				"            \"accountNo\": \"11182932\",\n" + 
				"            \"card\": \"\",\n" + 
				"            \"currency\": \"GHS\",\n" + 
				"            \"amount\": 10000,\n" + 
				"            \"tranDate\": \"2020-07-06T00:00:00\",\n" + 
				"            \"recDate\": \"2020-07-06T00:00:00\",\n" + 
				"            \"terminal\": \"\",\n" + 
				"            \"merchant\": \"\",\n" + 
				"            \"country\": \"\",\n" + 
				"            \"city\": \"\",\n" + 
				"            \"internalNo\": \"108735772\",\n" + 
				"            \"desc\": \"\",\n" + 
				"            \"additionalInfo\": \"API_ONLINE_PAYMENT\",\n" + 
				"            \"tranType\": \"110\",\n" + 
				"            \"ccyExponent\": \"2\"\n" + 
				"        },\n" + 
				"        {\n" + 
				"            \"cardAccount\": \"4691685100\",\n" + 
				"            \"accountNo\": \"11182932\",\n" + 
				"            \"card\": \"\",\n" + 
				"            \"currency\": \"GHS\",\n" + 
				"            \"amount\": 10000,\n" + 
				"            \"tranDate\": \"2020-07-06T00:00:00\",\n" + 
				"            \"recDate\": \"2020-07-06T00:00:00\",\n" + 
				"            \"terminal\": \"\",\n" + 
				"            \"merchant\": \"\",\n" + 
				"            \"country\": \"\",\n" + 
				"            \"city\": \"\",\n" + 
				"            \"internalNo\": \"108735775\",\n" + 
				"            \"desc\": \"\",\n" + 
				"            \"additionalInfo\": \"API_ONLINE_PAYMENT\",\n" + 
				"            \"tranType\": \"110\",\n" + 
				"            \"ccyExponent\": \"2\"\n" + 
				"        }\n" + 
				"    ]\n" + 
				"}";
	}
	
	public static String fundCCCard() {
		return "{\n" + 
				" \"hostHeaderInfo\": {\n" + 
				" \"sourceCode\": \"ECOMOBILE\",\n" + 
				" \"requestId\": \"12389693387\",\n" + 
				" \"affiliateCode\": \"EGH\",\n" + 
				" \"responseCode\": \"000\",\n" + 
				" \"responseMessage\": \"Your Transaction Request is Successful and Approved\",\n" + 
				" \"ipAddress\": \"10.10.0.10\"\n" + 
				" },\n" + 
				" \"internalRefNo\": \"108735950\",\n" + 
				" \"ccy\": null\n" + 
				"}";
	}
	
	
	public static String blockCCCard() {
		return "{\n" + 
				" \"hostHeaderInfo\": {\n" + 
				" \"sourceCode\": \"ECOBANKMOBILE\",\n" + 
				" \"requestId\": \"1334989023\",\n" + 
				" \"affiliateCode\": \"EGH\",\n" + 
				" \"responseCode\": \"000\",\n" + 
				" \"responseMessage\": \"Your Transaction Request is Successful and Approved\",\n" + 
				" \"ipAddress\": \"10.1.1.1\"\n" + 
				" }\n" + 
				"}";
	}
	
	
	public static String resetCardPin() {
		return "{\n" + 
				"\"hostHeaderInfo\": {\n" + 
				"\"sourceCode\": \"ECOBANKMOBILEAPP\",\n" + 
				"\"requestId\": \"1230001\",\n" + 
				"\"affiliateCode\": \"EGH\",\n" + 
				"\"responseCode\": \"000\",\n" + 
				"\"responseMessage\": \"Success\"\n" + 
				" }\n" + 
				"}";
	}
	
	
	public static String fetchCreditCardBalanceDetails() {
		return "{\n" + 
				"    \"accountNo\": \"4691695600\",\n" + 
				"    \"availableAmount\": 0.0,\n" + 
				"    \"balance\": -60.0,\n" + 
				"    \"bankC\": \"10\",\n" + 
				"    \"cardAcct\": \"4691695600\",\n" + 
				"    \"crd\": 10000.0,\n" + 
				"    \"hostHeaderInfo\": {\n" + 
				"        \"responseCode\": \"000\",\n" + 
				"        \"responseMessage\": \"Record Retrieved Successfully\"\n" + 
				"    },\n" + 
				"    \"minPayable\": 0.0,\n" + 
				"    \"otb\": \"9940\"\n" + 
				"}";
	}
	
	
	public static String getdebitcardsbyaccount() {
		return "{\n" + 
				"    \"hostHeaderInfo\": {\n" + 
				"        \"sourceCode\": \"SMARTTELLER\",\n" + 
				"        \"requestId\": \"43117\",\n" + 
				"        \"affiliateCode\": \"ENG\",\n" + 
				"        \"responseCode\": \"000\",\n" + 
				"        \"responseMessage\": \"Your Transaction Request is Successful and Approved\"\n" + 
				"    },\n" + 
				"    \"cardScheme\": \"\",\n" + 
				"    \"cards\": [\n" + 
				"        {\n" + 
				"            \"expiryDate\": \"2105\",\n" + 
				"            \"hotlistStatus\": \"Hotlisted\",\n" + 
				"            \"nameOnCard\": \"JIBUAKU CHINELO\",\n" + 
				"            \"cardNumber\": \"529778******0095\",\n" + 
				"            \"status\": \"InActive\"\n" + 
				"        },\n" + 
				"        {\n" + 
				"            \"expiryDate\": \"2105\",\n" + 
				"            \"hotlistStatus\": \"Active\",\n" + 
				"            \"nameOnCard\": \"Anonymous\",\n" + 
				"            \"cardNumber\": \"537011******0607\",\n" + 
				"            \"status\": \"Active\"\n" + 
				"        }\n" + 
				"    ]\n" + 
				"}";
	}
	
	
	
	//////DEBIT CARD SERVICES
	
	public static String createDebitCardRequest() {
		return "{\n" + 
				"\"hostHeaderInfo\": {\n" + 
				"\"sourceCode\": \"ECOBANKMOBILEAPP\",\n" + 
				"\"requestId\": \"1230001\",\n" + 
				"\"affiliateCode\": \"EGH\",\n" + 
				"\"responseCode\": \"000\",\n" + 
				"\"responseMessage\": \"Success\"\n" + 
				" },\n" + 
				"\"transactionRefNo\": \"1002210230001\"\n" + 
				"}";
	}
	
	

}
