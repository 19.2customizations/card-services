package com.ecobank.digx.cz.extxface.mule.adapter.client;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Random;
import java.util.logging.Logger;
import java.util.prefs.Preferences;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import com.ecobank.digx.cz.app.cardservices.dto.CardBalanceResponse;
import com.ecobank.digx.cz.app.cardservices.dto.CardDetailDTO;
import com.ecobank.digx.cz.app.cardservices.dto.CardListResponse;
import com.ecobank.digx.cz.app.cardservices.dto.CardServiceResponseDTO;
import com.ecobank.digx.cz.app.cardservices.dto.CardStatementDTO;
import com.ecobank.digx.cz.app.cardservices.dto.CardStatementListResponse;
import com.ecobank.digx.cz.app.cardservices.dto.CardStatementRequestDTO;
import com.ecobank.digx.cz.app.cardservices.dto.FetchCardFeeResponse;
import com.ecobank.digx.cz.app.domain.cardservices.entity.CardActivityInfo;
import com.ecobank.digx.cz.app.domain.cardservices.entity.CardRequest;
import com.ecobank.digx.cz.eco.domain.generic.entity.genericlogger.GenericLoggerConfiguration;
import com.ecobank.digx.cz.extxface.mule.assembler.CardRemoteServiceAssembler;
import com.ecobank.digx.cz.extxface.mule.dto.AccountDetails;
import com.ecobank.digx.cz.extxface.mule.dto.ESBCardRequestDTO;
import com.ecobank.digx.cz.extxface.mule.dto.ESBCardResponseDTO;
import com.ecobank.digx.cz.extxface.mule.dto.FetchVirtualCardFeeRequestDTO;
import com.ecobank.digx.cz.extxface.mule.dto.FetchVirtualCardFeeResponseDTO;
import com.ecobank.digx.cz.extxface.mule.dto.FetchVirtualCardRequestDTO;
import com.ecobank.digx.cz.extxface.mule.dto.FetchVirtualCardResponseDTO;
import com.ecobank.digx.cz.extxface.mule.dto.GenerateVirtualCardRequestDTO;
import com.ecobank.digx.cz.extxface.mule.dto.GenerateVirtualCardResponseDTO;
import com.ecobank.digx.cz.extxface.mule.dto.HostHeaderInfo;
import com.ecobank.digx.cz.extxface.mule.dto.TransactionList;
import com.ecobank.digx.cz.extxface.mule.dto.VirtualMiniStatementResponseDTO;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.ofss.digx.app.core.AdapterInteraction;
import com.ofss.digx.extxface.response.AbstractResponseHandler;
import com.ofss.digx.framework.rest.client.RESTClientFactory;
import com.ofss.digx.framework.rest.client.RESTClientRequestBuilder;
import com.ofss.digx.framework.rest.client.RESTClientRequestConfig;
import com.ofss.digx.framework.rest.client.impl.IRESTClient;
import com.ofss.digx.infra.exceptions.Exception;
import com.ofss.fc.infra.config.ConfigurationFactory;
import com.ofss.fc.infra.log.impl.MultiEntityLogger;
import com.ofss.fc.infra.thread.ThreadAttribute;
import com.thoughtworks.xstream.XStream;

public class VirtualCardServicesRepositoryAdapter extends AbstractResponseHandler {
  private static VirtualCardServicesRepositoryAdapter singletonInstance;
  
  private final CardServiceResponseHandler responseHandler = new CardServiceResponseHandler();
  
  private Preferences digx_consulting = ConfigurationFactory.getInstance().getConfigurations("DIGXCONSULTING");
  
  private static final String THIS_COMPONENT_NAME = VirtualCardServicesRepositoryAdapter.class.getName();
  
  private static transient Logger logger = MultiEntityLogger.getUniqueInstance().getLogger(THIS_COMPONENT_NAME);
  
  private static final GenericLoggerConfiguration serviceLogger = new GenericLoggerConfiguration();
  
  private Preferences eco_generic = ConfigurationFactory.getInstance().getConfigurations("ECOGENERIC");
  
  public static VirtualCardServicesRepositoryAdapter getInstance() {
    if (singletonInstance == null)
      synchronized (VirtualCardServicesRepositoryAdapter.class) {
        if (singletonInstance == null)
          singletonInstance = new VirtualCardServicesRepositoryAdapter(); 
      }  
    return singletonInstance;
  }
  
  public CardServiceResponseDTO generateCard(CardRequest cardRequest, String[] personalData) throws Exception {
    logger.info("inside generateCard: ");
    CardServiceResponseDTO response = new CardServiceResponseDTO();
    response.setResponseCode("E04");
    response.setResponseMessage("Failed at interface level");
    AdapterInteraction.begin();
    com.ofss.fc.datatype.Date startDateTime = new com.ofss.fc.datatype.Date();
    
    Boolean exceptionFlag = Boolean.valueOf(false);
    String jsonResponseBody = "";
    String jsonRequestBody = "";
    String correlationId = cardRequest.getRefKey().getExternalRefNo();
    String address = personalData[0];
    String city = personalData[1];
    String dob = personalData[3];
    logger.info("dob: " + dob);
    
    String cardType2 ="SHOPVCARD";
    String cardType = cardRequest.getCardType();
    System.out.println("CARD TYPE:  " + cardType );
    if(cardType.contains("Gift"))
    	cardType2 ="GVCARD";
    
    System.out.println("CARD TYPE22:  " + cardType2 );
    
    
    if (city == null || city.equals(""))
      city = address; 
    RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
    RESTClientRequestConfig prop = clientBuilder.loadConfig("ESC_VIRTUAL_CARD_CREATE");
    
    String url = this.digx_consulting.get("ESC_VIRTUAL_CARD_CREATE", "");
    String affCode = cardRequest.getAffiliateCode();
    String ccyCode = cardRequest.getCcy(); //  this.digx_consulting.get(String.valueOf(affCode) + "_LCY", "");
    String charge = "0";
    String fullName = cardRequest.getNameOnCard();
    String firstName = fullName.split(" ")[0];
    System.out.println("firstName: " + firstName);
    int fName = fullName.split(" ")[0].length();
    String lastName = fullName.substring(fName).isEmpty() ? firstName : fullName.substring(fName);
    System.out.println("lastName: " + lastName);
    String addr = address;
    if (addr != null && addr.length() > 50)
      addr = address.substring(0, 50); 
    try {
      SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
      java.util.Date date = format.parse(dob);
      format = new SimpleDateFormat("dd-MMM-yyyy");
      String strDate = format.format(date);
      GenerateVirtualCardRequestDTO generateVirtualCardRequestDTO = new GenerateVirtualCardRequestDTO();
      generateVirtualCardRequestDTO.setADDRESS((address.length() > 50) ? address.substring(0, 50) : address);
      generateVirtualCardRequestDTO.setAFFILIATE_COUNTRY(affCode);
      generateVirtualCardRequestDTO.setAMOUNT(Double.toString(cardRequest.getAmount()));
      generateVirtualCardRequestDTO.setAPP_VERSION("4.0.0.1");
      generateVirtualCardRequestDTO.setCHARGE_AMOUNT(charge);
      generateVirtualCardRequestDTO.setCITY(city);
      generateVirtualCardRequestDTO.setCURRENCY_CODE(ccyCode);
      generateVirtualCardRequestDTO.setCUSTOMER_ID(cardRequest.getCustomerNo());
      generateVirtualCardRequestDTO.setDEVICE_TYPE("");
      generateVirtualCardRequestDTO.setDOB(strDate);
      generateVirtualCardRequestDTO.setEMAIL(cardRequest.getEmail());
      generateVirtualCardRequestDTO.setEMCERT_ID("");
      generateVirtualCardRequestDTO.setFIRST_NAME(firstName);
      generateVirtualCardRequestDTO.setLANG_ID("en");
      generateVirtualCardRequestDTO.setLAST_NAME(lastName);
      generateVirtualCardRequestDTO.setMSISDN(cardRequest.getMobileNo());
      generateVirtualCardRequestDTO.setOTP_NUMBER(cardRequest.getMobileNo());
      generateVirtualCardRequestDTO.setSCHEME_TYPE(cardRequest.getSchemeType());
      generateVirtualCardRequestDTO.setSERVICE("CREATE-VCARD");
      generateVirtualCardRequestDTO.setSOURCE_ACCOUNT(cardRequest.getSourceAccountNo());
      generateVirtualCardRequestDTO.setTOTAL_AMOUNT(Double.toString(cardRequest.getAmount()));
      generateVirtualCardRequestDTO.setUUID(cardRequest.getSourceAccountNo());
      generateVirtualCardRequestDTO.setVCARD_TYPE(cardType2);
      Gson gson = new Gson();
      ObjectMapper mapper = new ObjectMapper();
      jsonRequestBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(generateVirtualCardRequestDTO);
      System.out.println("generateCard RequestBody: " + jsonRequestBody);
      GenerateVirtualCardResponseDTO resp = null;
      String genResponse = HttpPost(url, generateVirtualCardRequestDTO);
      resp = (GenerateVirtualCardResponseDTO)gson.fromJson(genResponse, GenerateVirtualCardResponseDTO.class);
      jsonResponseBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(resp);
      System.out.println("generateCard ResponseBody: " + jsonResponseBody);
      if (resp != null)
        if (resp.getSTATUS_CODE().equals("000")) {
          System.out.println(":: accountNo      : " + ((AccountDetails)resp.getACCOUNT_DETAILS().get(0)).getAccountNumber());
          System.out.println(":: balance        : " + ((AccountDetails)resp.getACCOUNT_DETAILS().get(0)).getBalance());
          System.out.println(":: card token     : " + ((AccountDetails)resp.getACCOUNT_DETAILS().get(0)).getCardToken());
          response.setResponseCode("000");
          response.setResponseMessage(resp.getSTATUS_MESSAGE());
          response.setTransactionRefNo(((AccountDetails)resp.getACCOUNT_DETAILS().get(0)).getCardAlias());
          response.setCustomerNo(((AccountDetails)resp.getACCOUNT_DETAILS().get(0)).getCustId());
          String panx = ((AccountDetails)resp.getACCOUNT_DETAILS().get(0)).getCardAlias();
          String maskedPan = String.valueOf(panx.substring(0, 6)) + "****" + panx.substring(panx.length() - 4);
          String expDate = ((AccountDetails)resp.getACCOUNT_DETAILS().get(0)).getExpiry();
          response.setMaskedPan(maskedPan);
          response.setPseudoPAN(((AccountDetails)resp.getACCOUNT_DETAILS().get(0)).getCardToken());
          response.setLast4Digits(panx.substring(panx.length() - 4));
          response.setCvv(((AccountDetails)resp.getACCOUNT_DETAILS().get(0)).getCvv());
          response.setFullPan(panx);
          response.setCcyCode(ccyCode);
          response.setCardSchemeType(cardType);
          String cname = ((AccountDetails)resp.getACCOUNT_DETAILS().get(0)).getCardName();
          if(cname == null)
        	  cname = firstName + " " + lastName;
          response.setNameOnCard(cname);
          SimpleDateFormat dff4 = new SimpleDateFormat("dd-MMM-yyyy");
          java.util.Date dtx = dff4.parse(expDate);
          SimpleDateFormat dff5 = new SimpleDateFormat("MM/YY");
          String expiryDate = dff5.format(dtx);
          response.setExpiryDate(expiryDate);
          String cardData = String.valueOf(panx) + "|" + ((AccountDetails)resp.getACCOUNT_DETAILS().get(0)).getCardToken();
          response.setCardData(cardData);
        } else {
          response.setResponseCode(resp.getSTATUS_CODE());
          response.setResponseMessage(resp.getSTATUS_MESSAGE());
          this.responseHandler.showOBDXErrorMapped(resp.getSTATUS_CODE(), resp.getSTATUS_MESSAGE());
        }  
    } catch (ParseException e) {
      exceptionFlag = Boolean.valueOf(true);
      if ("Y".equalsIgnoreCase(this.eco_generic.get("VIRTUALCARDREQUEST.INTERFACE_LOGGER_REQUIRED", "")))
        serviceLogger.dbLogger(jsonRequestBody, e.getMessage(), "VIRTUALCARDREQUEST", correlationId, "EXCEPTION", ThreadAttribute.get("SUBJECT_NAME").toString(), ThreadAttribute.get("TARGET_UNIT_CODE").toString(), startDateTime); 
      e.printStackTrace();
    } catch (JsonProcessingException e1) {
      e1.printStackTrace();
    } catch (NullPointerException nullPointerException) {
    
    } finally {
      if ("Y".equalsIgnoreCase(this.eco_generic.get("VIRTUALCARDREQUEST.INTERFACE_LOGGER_REQUIRED", "")) && !exceptionFlag.booleanValue())
        serviceLogger.dbLogger(jsonRequestBody, jsonResponseBody, "VIRTUALCARDREQUEST", correlationId, "", ThreadAttribute.get("SUBJECT_NAME").toString(), ThreadAttribute.get("TARGET_UNIT_CODE").toString(), startDateTime); 
    } 
    return response;
  }
  
  
  public FetchCardFeeResponse getCardRequestCharge(String accountNo, String affCode,
		  String schemeType,String amount,String sourceAccountCcy, String cardType) throws Exception {
	  FetchCardFeeResponse cardChargeResponse = new FetchCardFeeResponse();
	  cardChargeResponse.setResponseCode("E04");
	  cardChargeResponse.setResponseMessage("Failed at interface level");
		AdapterInteraction.begin();

		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESC_VIRTUAL_CARD_FETCH_FEE");

		String urlx = digx_consulting.get("ESC_VIRTUAL_CARD_FETCH_FEE", "");
		String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String ipx = digx_consulting.get("ESB_OBDX_IP", "");
		String pwd = digx_consulting.get("ESB_OBDX_PWD", "");
		
		String lcyCcy = digx_consulting.get(affCode + "_LCY", "");
		
		String cardType2 ="SHOPVCARD";
	   
	    System.out.println("CARD TYPE:  " + cardType );
	    if(cardType.contains("Gift"))
	    	cardType2 ="GVCARD";
	    
	    System.out.println("CARD TYPE22:  " + cardType2 );
		
		String requestId = GetRefNumber("FVF", 12);
		String requestToken = JEncrypt.GenerateTransactionToken(sourceCode, requestId, pwd);
		FetchVirtualCardFeeRequestDTO esbRequestDTO = new FetchVirtualCardFeeRequestDTO();
		HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
		hostHeaderInfo.setAffiliateCode(affCode);
		hostHeaderInfo.setIpAddress(ipx);
		hostHeaderInfo.setRequestId(requestId);
		hostHeaderInfo.setRequestToken(requestToken);
		hostHeaderInfo.setRequestType("TRANSFERCHARGE");
		hostHeaderInfo.setSourceChannelId("WEB");
		hostHeaderInfo.setSourceCode(sourceCode);
		esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);
		esbRequestDTO.setSourceAccount(accountNo);
		esbRequestDTO.setAmount(amount);
		esbRequestDTO.setSourceAccountCcy(sourceAccountCcy);
		esbRequestDTO.setSourceAccountType("A");
		esbRequestDTO.setTransactionType("VIRTUALCARD");
		esbRequestDTO.setUdf1(cardType2);
		esbRequestDTO.setUdf2("");
		esbRequestDTO.setUdf3("");
		
		CardRemoteServiceAssembler assembler = new CardRemoteServiceAssembler();

		try {
			XStream xs = new XStream();
			logger.info("GET Cardcharge : " + xs.toXML(esbRequestDTO));
			logger.info("Source Code : " + sourceCode);

			//ESBGetCardByAccountResponseDTO cardListResponse2 = (ESBGetCardByAccountResponseDTO) restClient.post(prop, ESBGetCardByAccountResponseDTO.class, esbRequestDTO);
			try {
				enableSSL();
			} catch (KeyManagementException | NoSuchAlgorithmException e) {
				e.printStackTrace();
			}
			
			Gson gson = new Gson();
			//String urlx = prop.getContextURL() + "/" +prop.getServiceURL();
			System.out.println("fetch card charge url " + urlx);

			URL obj = null;
			try {
				obj = new URL(urlx);
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
			
			try {

				String clientId = digx_consulting.get("ESB_ACCOUNT_NAME_INQUIRY_CLIENT_ID", "");
				String secret = digx_consulting.get("ESB_ACCOUNT_NAME_INQUIRY_CLIENT_SECRET", "");

				System.out.println("fetch card by account url 1 : " + clientId);
				System.out.println("fetch card by account url 2 : " + secret);

				// String requestToken = JEncrypt.GenerateTransactionToken(sourceCode,
				// requestId, pwd);

				String genResponse = HttpPost(urlx, esbRequestDTO);
				
				System.out.println("fetch fee response : " + genResponse);
				
				FetchVirtualCardFeeResponseDTO cardChargeResponse2 = gson.fromJson(genResponse, FetchVirtualCardFeeResponseDTO.class);

				if(cardChargeResponse2.getHostHeaderInfo().getResponseCode().equals("000"))
				{
					cardChargeResponse.setResponseCode("000");
					cardChargeResponse.setResponseMessage("SUCCESS");
					cardChargeResponse.setFee(new BigDecimal(cardChargeResponse2.getCharge()).toString());
				}
				else
				{
					cardChargeResponse.setResponseCode(cardChargeResponse2.getHostHeaderInfo().getResponseCode());
					cardChargeResponse.setResponseMessage(cardChargeResponse2.getHostHeaderInfo().getResponseMessage());
				}
				
				
			} catch (java.lang.Exception ex) {
				ex.printStackTrace();
			}

          System.out.println("Get Card Charge: " + xs.toXML(cardChargeResponse));
			
		} finally {
			
		}
		return cardChargeResponse;
	}
  
  public CardListResponse fetchVCardByMsisdn(String mobileNo, String referenceNo, String affCode) throws Exception {
    CardListResponse cardListResponse = new CardListResponse();
    cardListResponse.setResponseCode("E04");
    cardListResponse.setResponseMessage("Failed at interface level");
    AdapterInteraction.begin();
    RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
    RESTClientRequestConfig prop = clientBuilder.loadConfig("ESC_VIRTUAL_CARD_FETCH_BY_MSISDN");
    String url = digx_consulting.get("ESC_VIRTUAL_CARD_FETCH_BY_MSISDN", "");
    
    FetchVirtualCardRequestDTO fetchVirtualCardRequestDTO = new FetchVirtualCardRequestDTO();
    fetchVirtualCardRequestDTO.setAFFILIATE_COUNTRY(affCode);
    fetchVirtualCardRequestDTO.setAPP_VERSION("3.5.0");
    fetchVirtualCardRequestDTO.setDEVICE_TYPE("");
    fetchVirtualCardRequestDTO.setEMCERT_ID("");
    fetchVirtualCardRequestDTO.setLANG_ID("1");
    fetchVirtualCardRequestDTO.setMSISDN(mobileNo);
    fetchVirtualCardRequestDTO.setSERVICE("FETCH-VCARDS");
    fetchVirtualCardRequestDTO.setUUID(referenceNo);
    FetchVirtualCardResponseDTO fetchVirtualCardResponseDTO = null;
    try {
      ObjectMapper mapper = new ObjectMapper();
      String jsonRequestBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(fetchVirtualCardRequestDTO);
      logger.info("fetchVCardByMsisdn RequestBody: " + jsonRequestBody);
      Gson gson = new Gson();
      String genResponse = HttpPost(url, fetchVirtualCardRequestDTO);
      fetchVirtualCardResponseDTO = (FetchVirtualCardResponseDTO)gson.fromJson(genResponse, FetchVirtualCardResponseDTO.class);
      String jsonResponseBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(fetchVirtualCardResponseDTO);
      logger.info("fetchVCardByMsisdn ResponseBody : " + jsonResponseBody);
      logger.info("getResponseCode         : " + fetchVirtualCardResponseDTO.getSTATUS_CODE());
      if (fetchVirtualCardResponseDTO != null)
        if (fetchVirtualCardResponseDTO.getSTATUS_CODE().equals("000")) {
          if (fetchVirtualCardResponseDTO.getACCOUNT_DETAILS().size() > 0)
            for (AccountDetails transactionDetails : fetchVirtualCardResponseDTO.getACCOUNT_DETAILS()) {
              CardDetailDTO cardDetailDTO = new CardDetailDTO();
              cardDetailDTO.setBalance((transactionDetails.getBalance() != null) ? 
                  new BigDecimal(transactionDetails.getBalance()) : 
                  new BigDecimal("0"));
              cardDetailDTO.setCardAccount(
                  (transactionDetails.getCardToken() != null) ? transactionDetails.getCardToken() : "");
              cardDetailDTO.setPseudoPan(
                      (transactionDetails.getCardToken() != null) ? transactionDetails.getCardToken() : "");
              cardDetailDTO.setCardCategory("VIRTUAL");
              String eDate = transactionDetails.getExpiry();
              
              if(eDate != null && eDate.length() >= 10 )
				{
					
					System.out.println("EXPIRY-DATE 2 :: " + eDate);
					SimpleDateFormat dff4 = new SimpleDateFormat("dd-MMM-yyyy");
			          java.util.Date dtx = new java.util.Date();
			          String expiryDate = "";
					try {
						dtx = dff4.parse(eDate);
						SimpleDateFormat dff5 = new SimpleDateFormat("MM/YY");
				           expiryDate = dff5.format(dtx);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			          
			          
			          cardDetailDTO.setExpiryDate(expiryDate);
				}
				
				String cardNo = transactionDetails.getCardAlias() ;
				if(cardNo != null && cardNo.length() > 6)
				{
					cardNo = cardNo.substring(0,6) + "****" + cardNo.substring(cardNo.length() - 4);
				}
              
             // cardDetailDTO.setExpiryDate(
                  //(transactionDetails.getExpiry() != null) ? transactionDetails.getExpiry() : "");
             // cardDetailDTO.setMaskedPan(
                //  (transactionDetails.getCardAlias() != null) ? transactionDetails.getCardAlias() : "");
             
				cardDetailDTO.setMaskedPan(cardNo);
				cardDetailDTO.setMobileNo(
                  (transactionDetails.getOtpNumber() != null) ? transactionDetails.getOtpNumber() : "");
              cardDetailDTO.setCustomerId(
                  (transactionDetails.getCustId() != null) ? transactionDetails.getCustId() : "");
             // cardDetailDTO.setCardStatus(
							// (transactionDetails.getBlocked() != null) ?transactionDetails.getBlocked() :
			                 // "");
              cardDetailDTO.setCardCcy(
                  (transactionDetails.getCurrencyCode() != null) ? transactionDetails.getCurrencyCode() : 
                  "");
              cardDetailDTO.setNameOnCard(
                  (transactionDetails.getCardName() != null) ? transactionDetails.getCardName() : "");
              cardListResponse.getCards().add(cardDetailDTO);
              /*logger.info("******************************************");
              logger.info("Balance       : " + cardDetailDTO.getBalance());
              logger.info("CardAccount   : " + cardDetailDTO.getCardAccount());
              logger.info("Expiry Date   : " + cardDetailDTO.getExpiryDate());
              logger.info("Card Status   : " + cardDetailDTO.getCardStatus());
              logger.info("Customer Id   : " + cardDetailDTO.getCustomerId());
              logger.info("Masked Pan    : " + cardDetailDTO.getMaskedPan());
              logger.info("Mobile No     : " + cardDetailDTO.getMobileNo());*/
            }  
          cardListResponse.setResponseCode("000");
          cardListResponse.setResponseMessage(fetchVirtualCardResponseDTO.getSTATUS_MESSAGE());
        } else {
          cardListResponse.setResponseCode(fetchVirtualCardResponseDTO.getSTATUS_CODE());
          cardListResponse.setResponseMessage(fetchVirtualCardResponseDTO.getSTATUS_MESSAGE());
        }  
    } catch (NullPointerException nullPointerException) {
    
    } catch (JsonProcessingException e) {
      e.printStackTrace();
    } 
    AdapterInteraction.close();
    return cardListResponse;
  }
  
  public CardBalanceResponse balanceCheck(String affCode, String cardCustomerId, String mobileNo) throws Exception {
    CardBalanceResponse response = new CardBalanceResponse();
    response.setResponseCode("E04");
    response.setResponseMessage("Failed at interface level");
    AdapterInteraction.begin();
    RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
    RESTClientRequestConfig prop = clientBuilder.loadConfig("ESC_VIRTUAL_CARD_BALANCE");
    String sourceCode = this.digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
    String url = digx_consulting.get("ESC_VIRTUAL_CARD_BALANCE", "");
    String ip = this.digx_consulting.get("ESB_OBDX_IP", "");
    String pwd = this.digx_consulting.get("ESB_OBDX_PWD", "");
    String requestId = GetRefNumber("BC", 12);
    String requestToken = JEncrypt.GenerateTransactionToken(sourceCode, requestId, pwd);
    String genRef = GetRefNumber("BC", 12);
    ESBCardRequestDTO esbRequestDTO = new ESBCardRequestDTO();
    HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
    hostHeaderInfo.setAffiliateCode("EGH");
    hostHeaderInfo.setIpAddress(ip);
    hostHeaderInfo.setRequestId(genRef);
    hostHeaderInfo.setRequestToken(requestToken);
    hostHeaderInfo.setRequestType("GETBAL");
    hostHeaderInfo.setSourceChannelId("MOBILE");
    hostHeaderInfo.setSourceCode(sourceCode);
    esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);
    esbRequestDTO.setCardNo(cardCustomerId);
    esbRequestDTO.setMsisdn(mobileNo);
    ESBCardResponseDTO esbCardResponseDTO = null;
    try {
      Gson gson = new Gson();
      logger.info("Card Balance Source Code : " + sourceCode);
      ObjectMapper mapper = new ObjectMapper();
      String jsonRequestBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(esbRequestDTO);
      logger.info("balanceCheck RequestBody: " + jsonRequestBody);
      String genResponse = HttpPost(url, esbRequestDTO);
      esbCardResponseDTO = (ESBCardResponseDTO)gson.fromJson(genResponse, ESBCardResponseDTO.class);
      String jsonResponseBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(esbCardResponseDTO);
      logger.info("balanceCheck ResponseBody : " + jsonResponseBody);
      if (esbCardResponseDTO != null) {
        logger.info("Generate CustomerNo     : " + esbCardResponseDTO.getCustomerNo());
        logger.info("getResponseCode         : " + esbCardResponseDTO.getHostHeaderInfo().getResponseCode());
        logger.info("getResponseMessage      : " + esbCardResponseDTO.getHostHeaderInfo().getResponseMessage());
        logger.info("getBalance Check        : " + esbCardResponseDTO.getBalance());
        logger.info("isBlocked               : " + esbCardResponseDTO.getBlocked());
        if (esbCardResponseDTO.getHostHeaderInfo().getResponseCode().equals("000")) {
          response.setResponseCode("000");
          response.setResponseMessage(esbCardResponseDTO.getHostHeaderInfo().getResponseMessage());
          response.setBalance(new BigDecimal(esbCardResponseDTO.getBalance()));
          response.setCardType("VIRTUAL");
        } else {
          response.setResponseCode(esbCardResponseDTO.getHostHeaderInfo().getResponseCode());
          response.setResponseMessage(esbCardResponseDTO.getHostHeaderInfo().getResponseMessage());
        } 
      } 
    } catch (NullPointerException nullPointerException) {
    
    } catch (JsonProcessingException e) {
      e.printStackTrace();
    } 
    AdapterInteraction.close();
    return response;
  }
  
  public CardServiceResponseDTO fundCard(CardActivityInfo caInfo, String branchCode) throws Exception {
    CardServiceResponseDTO response = new CardServiceResponseDTO();
    response.setResponseCode("E04");
    response.setResponseMessage("Failed at interface level");
    AdapterInteraction.begin();
    RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
    RESTClientRequestConfig prop = clientBuilder.loadConfig("ESC_VIRTUAL_CARD_FUND");
    
    String url = digx_consulting.get("ESC_VIRTUAL_CARD_FUND", "");
    
    XStream xs = new XStream();
    logger.info("::: Fund Virtual Card Request : " + xs.toXML(caInfo));
    CardRemoteServiceAssembler assembler = new CardRemoteServiceAssembler();
    ESBCardRequestDTO esbRequestDTO = assembler.fromDomainObjectToRemoteActivityFundVirtual(caInfo, branchCode);
    ESBCardResponseDTO esbCardResponseDTO = null;
    try {
      logger.info("::: Fund Virtual Card Request : " + xs.toXML(esbRequestDTO));
      ObjectMapper mapper = new ObjectMapper();
      String jsonRequestBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(esbRequestDTO);
      logger.info("::: V-FundCard RequestBody: " + jsonRequestBody);
      Gson gson = new Gson();
      String genResponse = HttpPost(url, esbRequestDTO);
      esbCardResponseDTO = (ESBCardResponseDTO)gson.fromJson(genResponse, ESBCardResponseDTO.class);
      String jsonResponseBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(esbCardResponseDTO);
      logger.info("::: fundCard ResponseBody: " + jsonResponseBody);
      logger.info("::: getResponseCode         : " + esbCardResponseDTO.getHostHeaderInfo().getResponseCode());
      logger.info("::: getResponseMessage      : " + esbCardResponseDTO.getHostHeaderInfo().getResponseMessage());
      if (esbCardResponseDTO != null) {
        response.setResponseCode(esbCardResponseDTO.getHostHeaderInfo().getResponseCode());
        response.setResponseMessage(esbCardResponseDTO.getHostHeaderInfo().getResponseMessage());
        response.setTransactionRefNo(esbCardResponseDTO.getHostHeaderInfo().getRequestId());
      } 
    } catch (NullPointerException nullPointerException) {
    
    } catch (JsonProcessingException e) {
      e.printStackTrace();
    } 
    AdapterInteraction.close();
    return response;
  }
  
  public CardStatementListResponse getStatement(CardStatementRequestDTO cardStatementRequestDTO) throws Exception {
    CardStatementListResponse cardStatementListResponse = new CardStatementListResponse();
    cardStatementListResponse.setResponseCode("E04");
    cardStatementListResponse.setResponseMessage("Failed at interface level");
    AdapterInteraction.begin();
    RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
    RESTClientRequestConfig prop = clientBuilder.loadConfig("ESC_VIRTUAL_CARD_STATEMENT");
    
    String url = digx_consulting.get("ESC_VIRTUAL_CARD_STATEMENT", "");
    String sourceCode = this.digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
    String ipx = this.digx_consulting.get("ESB_OBDX_IP", "");
    String pwd = this.digx_consulting.get("ESB_OBDX_PWD", "");
    logger.info("getStatement AffCode : " + cardStatementRequestDTO.getAffCode());
    String genRef = GetRefNumber("CS", 12);
    String requestToken = JEncrypt.GenerateTransactionToken(sourceCode, genRef, pwd);
    ESBCardRequestDTO esbRequestDTO = new ESBCardRequestDTO();
    HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
    hostHeaderInfo.setAffiliateCode(cardStatementRequestDTO.getAffCode());
    hostHeaderInfo.setIpAddress(ipx);
    hostHeaderInfo.setRequestId(genRef);
    hostHeaderInfo.setRequestToken(requestToken);
    hostHeaderInfo.setRequestType("MINISTATEMENT");
    hostHeaderInfo.setSourceChannelId("OBDX");
    hostHeaderInfo.setSourceCode(sourceCode);
    esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);
    esbRequestDTO.setCardNo(cardStatementRequestDTO.getCardId());
    esbRequestDTO.setMsisdn(cardStatementRequestDTO.getMsisdn());
    esbRequestDTO.setStartDate(cardStatementRequestDTO.getStartDate());
    esbRequestDTO.setEndDate(cardStatementRequestDTO.getEndDate());
    VirtualMiniStatementResponseDTO virtualMiniStatementResponseDTO = null;
    try {
      ObjectMapper mapper = new ObjectMapper();
      String jsonRequestBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(esbRequestDTO);
      logger.info("::: getStatement RequestBody: " + jsonRequestBody);
      Gson gson = new Gson();
      String genResponse = HttpPost(url, esbRequestDTO);
      virtualMiniStatementResponseDTO = (VirtualMiniStatementResponseDTO)gson.fromJson(genResponse, VirtualMiniStatementResponseDTO.class);
      String jsonResponseBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(virtualMiniStatementResponseDTO);
      logger.info("::: getStatement ResponseBody: " + jsonResponseBody);
      logger.info("::: getResponseCode         : " + virtualMiniStatementResponseDTO.getHostHeaderInfo().getResponseCode());
      logger.info("::: getResponseMessage      : " + virtualMiniStatementResponseDTO.getHostHeaderInfo().getResponseMessage());
      if (virtualMiniStatementResponseDTO.getHostHeaderInfo().getResponseCode().equals("000"))
        if (virtualMiniStatementResponseDTO.getTransactionList().size() > 0)
          for (TransactionList transactionDetails : virtualMiniStatementResponseDTO.getTransactionList()) {
            CardStatementDTO cardStatementDTO = new CardStatementDTO();
            cardStatementDTO.setBalance("");
            cardStatementDTO.setBalance("");
            cardStatementDTO.setDescription(transactionDetails.getTransactionDesc());
            cardStatementDTO.setFee(new BigDecimal(transactionDetails.getFee()));
            cardStatementDTO.setPaymentAmount(new BigDecimal(transactionDetails.getTotalAmount()));
            cardStatementDTO.setReferenceNo(transactionDetails.getTransactionId());
            cardStatementDTO.setTransDate(transactionDetails.getTransactionDate());
            cardStatementListResponse.getCardStatement().add(cardStatementDTO);
            System.out.println("******************************************");
            System.out.println("fee        : " + transactionDetails.getFee());
            System.out.println("narration  : " + transactionDetails.getTransactionDesc());
            System.out.println("refInfo    : " + transactionDetails.getReferenceInfo());
            System.out.println("totalAmount: " + transactionDetails.getTotalAmount());
            System.out.println("transDate  : " + transactionDetails.getTransactionDate());
          }   
    } catch (NullPointerException nullPointerException) {
    
    } catch (JsonProcessingException e) {
      e.printStackTrace();
    } 
    return cardStatementListResponse;
  }
  
  public CardServiceResponseDTO blockCard(String affCode, String pan, String mobileNo) throws Exception {
    CardServiceResponseDTO response = new CardServiceResponseDTO();
    response.setResponseCode("E04");
    response.setResponseMessage("Failed at interface level");
    AdapterInteraction.begin();
    RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
    RESTClientRequestConfig prop = clientBuilder.loadConfig("ESC_VIRTUAL_CARD_BLOCK");
    String requestId = GetRefNumber("BC", 12);
    String sourceCode = this.digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
    String url = digx_consulting.get("ESC_VIRTUAL_CARD_BLOCK", "");
    String pwd = this.digx_consulting.get("ESB_OBDX_PWD", "");
    String requestToken = JEncrypt.GenerateTransactionToken(sourceCode, requestId, pwd);
    String ipx = this.digx_consulting.get("ESB_OBDX_IP", "");
    ESBCardRequestDTO esbRequestDTO = new ESBCardRequestDTO();
    HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
    hostHeaderInfo.setAffiliateCode(affCode);
    hostHeaderInfo.setIpAddress(ipx);
    hostHeaderInfo.setRequestId(requestId);
    hostHeaderInfo.setRequestToken(requestToken);
    hostHeaderInfo.setRequestType("BLOCKVCARD");
    hostHeaderInfo.setSourceChannelId("MOBILE");
    hostHeaderInfo.setSourceCode(sourceCode);
    esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);
    esbRequestDTO.setCardNo(pan);
    esbRequestDTO.setMsisdn(mobileNo);
    ESBCardResponseDTO resp = null;
    try {
      ObjectMapper mapper = new ObjectMapper();
      String jsonRequestBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(esbRequestDTO);
      logger.info("blockCard RequestBody: " + jsonRequestBody);
      Gson gson = new Gson();
      logger.info("Card Block Source Code : " + sourceCode);
      String genResponse = HttpPost(url, esbRequestDTO);
      resp = (ESBCardResponseDTO)gson.fromJson(genResponse, ESBCardResponseDTO.class);
      String jsonResponseBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(resp);
      System.out.println("blockCard ResponseBody: " + jsonResponseBody);
      logger.info("getResponseCode         : " + resp.getHostHeaderInfo().getResponseCode());
      logger.info("getResponseMessage      : " + resp.getHostHeaderInfo().getResponseMessage());
      if (resp != null)
        if (resp.getHostHeaderInfo().getResponseCode().equals("000")) {
          response.setResponseCode("000");
          response.setResponseMessage(resp.getHostHeaderInfo().getResponseMessage());
        } else {
          response.setResponseCode(resp.getHostHeaderInfo().getResponseCode());
          response.setResponseMessage(resp.getHostHeaderInfo().getResponseMessage());
        }  
    } catch (NullPointerException nullPointerException) {
    
    } catch (JsonProcessingException e) {
      e.printStackTrace();
    } 
    AdapterInteraction.close();
    return response;
  }
  
  public CardServiceResponseDTO unblockCard(String affCode, String pan, String mobileNo) throws Exception {
    CardServiceResponseDTO response = new CardServiceResponseDTO();
    response.setResponseCode("E04");
    response.setResponseMessage("Failed at interface level");
    AdapterInteraction.begin();
    RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
    RESTClientRequestConfig prop = clientBuilder.loadConfig("ESC_VIRTUAL_CARD_UNBLOCK");
    String url = digx_consulting.get("ESC_VIRTUAL_CARD_UNBLOCK", "");
    String requestId = GetRefNumber("BC", 12);
    String sourceCode = this.digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
    String pwd = this.digx_consulting.get("ESB_OBDX_PWD", "");
    String requestToken = JEncrypt.GenerateTransactionToken(sourceCode, requestId, pwd);
    String ipx = this.digx_consulting.get("ESB_OBDX_IP", "");
    ESBCardRequestDTO esbRequestDTO = new ESBCardRequestDTO();
    HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
    hostHeaderInfo.setAffiliateCode(affCode);
    hostHeaderInfo.setIpAddress(ipx);
    hostHeaderInfo.setRequestId(requestId);
    hostHeaderInfo.setRequestToken(requestToken);
    hostHeaderInfo.setRequestType("UNBLOCKVCARD");
    hostHeaderInfo.setSourceChannelId("MOBILE");
    hostHeaderInfo.setSourceCode(sourceCode);
    esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);
    esbRequestDTO.setCardNo(pan);
    esbRequestDTO.setMsisdn(mobileNo);
    ESBCardResponseDTO resp = null;
    try {
      ObjectMapper mapper = new ObjectMapper();
      String jsonRequestBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(esbRequestDTO);
      System.out.println("unblockCard RequestBody: " + jsonRequestBody);
      Gson gson = new Gson();
      logger.info("Card Unlock Source Code : " + sourceCode);
      String genResponse = HttpPost(url, esbRequestDTO);
      resp = (ESBCardResponseDTO)gson.fromJson(genResponse, ESBCardResponseDTO.class);
      String jsonResponseBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(resp);
      System.out.println("unblockCard ResponseBody: " + jsonResponseBody);
      logger.info("getResponseCode         : " + resp.getHostHeaderInfo().getResponseCode());
      logger.info("getResponseMessage      : " + resp.getHostHeaderInfo().getResponseMessage());
      if (resp != null)
        if (resp.getHostHeaderInfo().getResponseCode().equals("000")) {
          response.setResponseCode("000");
          response.setResponseMessage(resp.getHostHeaderInfo().getResponseMessage());
        } else {
          response.setResponseCode(resp.getHostHeaderInfo().getResponseCode());
          response.setResponseMessage(resp.getHostHeaderInfo().getResponseMessage());
        }  
    } catch (NullPointerException nullPointerException) {
    
    } catch (JsonProcessingException e) {
      e.printStackTrace();
    } 
    AdapterInteraction.close();
    return response;
  }
  
  public CardServiceResponseDTO updateOTPNumber(String affCode, String pan, String customerNo, String mobileNo, String otp, String email) throws Exception {
    CardServiceResponseDTO response = new CardServiceResponseDTO();
    response.setResponseCode("E04");
    response.setResponseMessage("Failed at interface level");
    AdapterInteraction.begin();
    RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
    RESTClientRequestConfig prop = clientBuilder.loadConfig("ESC_VIRTUAL_CARD_OTP");
    RESTClientFactory clientFactory = RESTClientFactory.getInstance();
    IRESTClient restClient = clientFactory.getRESTClientInstance();
    String requestId = GetRefNumber("BC", 12);
    String sourceCode = this.digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
    String url = digx_consulting.get("ESC_VIRTUAL_CARD_OTP", "");
    String pwd = this.digx_consulting.get("ESB_OBDX_PWD", "");
    String requestToken = JEncrypt.GenerateTransactionToken(sourceCode, requestId, pwd);
    String ipx = this.digx_consulting.get("ESB_OBDX_IP", "");
    ESBCardRequestDTO esbRequestDTO = new ESBCardRequestDTO();
    HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
    hostHeaderInfo.setAffiliateCode(affCode);
    hostHeaderInfo.setIpAddress(ipx);
    hostHeaderInfo.setRequestId(requestId);
    hostHeaderInfo.setRequestToken(requestToken);
    hostHeaderInfo.setRequestType("BLOCKVCARD");
    hostHeaderInfo.setSourceChannelId("MOBILE");
    hostHeaderInfo.setSourceCode(sourceCode);
    esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);
    esbRequestDTO.setCardNo(pan);
    esbRequestDTO.setMsisdn(mobileNo);
    esbRequestDTO.setOtp(otp);
    esbRequestDTO.setEmail(email);
    ESBCardResponseDTO resp = null;
    try {
      ObjectMapper mapper = new ObjectMapper();
      String jsonRequestBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(esbRequestDTO);
      logger.info("updateOTPNumber: " + jsonRequestBody);
      Gson gson = new Gson();
      logger.info("updateOTPNumber : " + sourceCode);
      String genResponse = HttpPost(url, esbRequestDTO);
      resp = (ESBCardResponseDTO)gson.fromJson(genResponse, ESBCardResponseDTO.class);
      String jsonResponseBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(resp);
      System.out.println("updateOTPNumber ResponseBody: " + jsonResponseBody);
      logger.info("getResponseCode         : " + resp.getHostHeaderInfo().getResponseCode());
      logger.info("getResponseMessage      : " + resp.getHostHeaderInfo().getResponseMessage());
      if (resp != null && resp.getHostHeaderInfo().getResponseCode().equals("000")) {
        response.setResponseCode("000");
        response.setResponseMessage(resp.getHostHeaderInfo().getResponseMessage());
      } 
    } catch (NullPointerException nullPointerException) {
    
    } catch (JsonProcessingException e) {
      e.printStackTrace();
    } 
    AdapterInteraction.close();
    return response;
  }
  
  public CardServiceResponseDTO closeOrDeleteCard(String affCode, String pan, String mobileNo, String sourceAcct, String ccy) throws Exception {
    CardServiceResponseDTO response = new CardServiceResponseDTO();
    response.setResponseCode("E04");
    response.setResponseMessage("Failed at interface level");
    AdapterInteraction.begin();
    RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
    RESTClientRequestConfig prop = clientBuilder.loadConfig("ESC_VIRTUAL_DELETE_CARD");
    RESTClientFactory clientFactory = RESTClientFactory.getInstance();
    IRESTClient restClient = clientFactory.getRESTClientInstance();
    String requestId = GetRefNumber("BC", 12);
    String url = digx_consulting.get("ESC_VIRTUAL_DELETE_CARD", "");
    String sourceCode = this.digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
    String pwd = this.digx_consulting.get("ESB_OBDX_PWD", "");
    String requestToken = JEncrypt.GenerateTransactionToken(sourceCode, requestId, pwd);
    String ipx = this.digx_consulting.get("ESB_OBDX_IP", "");
    ESBCardRequestDTO esbRequestDTO = new ESBCardRequestDTO();
    HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
    hostHeaderInfo.setAffiliateCode(affCode);
    hostHeaderInfo.setIpAddress(ipx);
    hostHeaderInfo.setRequestId(requestId);
    hostHeaderInfo.setRequestToken(requestToken);
    hostHeaderInfo.setRequestType("CLOSEVCARD");
    hostHeaderInfo.setSourceChannelId("MOBILE");
    hostHeaderInfo.setSourceCode(sourceCode);
    esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);
    esbRequestDTO.setCardNo(pan);
    esbRequestDTO.setMsisdn(mobileNo);
    esbRequestDTO.setSourceAccount(sourceAcct);
    esbRequestDTO.setSourceAccountCcy(ccy);
    ESBCardResponseDTO resp = null;
    try {
      ObjectMapper mapper = new ObjectMapper();
      String jsonRequestBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(esbRequestDTO);
      logger.info("closeOrDeleteCard: " + jsonRequestBody);
      Gson gson = new Gson();
      logger.info("closeOrDeleteCard : " + sourceCode);
      String genResponse = HttpPost(url, esbRequestDTO);
      resp = (ESBCardResponseDTO)gson.fromJson(genResponse, ESBCardResponseDTO.class);
      String jsonResponseBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(resp);
      System.out.println("updateOTPNumber ResponseBody: " + jsonResponseBody);
      logger.info("getResponseCode         : " + resp.getHostHeaderInfo().getResponseCode());
      logger.info("getResponseMessage      : " + resp.getHostHeaderInfo().getResponseMessage());
      if (resp != null && resp.getHostHeaderInfo().getResponseCode().equals("000")) {
        response.setResponseCode("000");
        response.setResponseMessage(resp.getHostHeaderInfo().getResponseMessage());
      } 
    } catch (NullPointerException nullPointerException) {
    
    } catch (JsonProcessingException e) {
      e.printStackTrace();
    } 
    AdapterInteraction.close();
    return response;
  }
  
  public static String HttpPost(String contextUrl,  Object payloadRequest) {
    StringBuffer resp = new StringBuffer();
    URL obj = null;
    Gson gson = new Gson();
    try {
    	
    	System.out.println("URL " + contextUrl);
    	
    	if(contextUrl.startsWith("https"))
		{
    		return HttpsPost( contextUrl,payloadRequest);

		}
		
	
    	
      obj = new URL(contextUrl);
      HttpURLConnection con = (HttpURLConnection)obj.openConnection();
      con.setRequestMethod("POST");
      con.setRequestProperty("Content-Type", "application/json");
      con.setDoOutput(true);
      DataOutputStream wr = new DataOutputStream(con.getOutputStream());
      wr.writeBytes(gson.toJson(payloadRequest));
      wr.flush();
      wr.close();
      String responseStatus = con.getResponseMessage();
      System.out.println(responseStatus);
      int responseCode = con.getResponseCode();
      System.out.println("responseCode " + responseCode);
      if (responseCode == 200) {
        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        while ((inputLine = in.readLine()) != null)
          resp.append(inputLine); 
        in.close();
      } else {
        BufferedReader in = new BufferedReader(new InputStreamReader(con.getErrorStream()));
        String inputLine;
        while ((inputLine = in.readLine()) != null)
          resp.append(inputLine); 
        in.close();
      } 
      
      System.out.println("response: " + resp);
      
    } catch (IOException e) {
      e.printStackTrace();
    } 
    return resp.toString();
  }
  
  public static String HttpsPost(String contextUrl,  Object payloadRequest) {
	    StringBuffer resp = new StringBuffer();
	    URL obj = null;
	    Gson gson = new Gson();
	    try {
	    	
	    	System.out.println("URL " + contextUrl);
	      obj = new URL(contextUrl);
	      HttpURLConnection con = (HttpURLConnection)obj.openConnection();
	      con.setRequestMethod("POST");
	      con.setRequestProperty("Content-Type", "application/json");
	      con.setDoOutput(true);
	      DataOutputStream wr = new DataOutputStream(con.getOutputStream());
	      wr.writeBytes(gson.toJson(payloadRequest));
	      wr.flush();
	      wr.close();
	      String responseStatus = con.getResponseMessage();
	      System.out.println(responseStatus);
	      int responseCode = con.getResponseCode();
	      System.out.println("responseCode " + responseCode);
	      if (responseCode == 200) {
	        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
	        String inputLine;
	        while ((inputLine = in.readLine()) != null)
	          resp.append(inputLine); 
	        in.close();
	      } else {
	        BufferedReader in = new BufferedReader(new InputStreamReader(con.getErrorStream()));
	        String inputLine;
	        while ((inputLine = in.readLine()) != null)
	          resp.append(inputLine); 
	        in.close();
	      } 
	      
	      System.out.println("response: " + resp);
	      
	      
	    } catch (IOException e) {
	      e.printStackTrace();
	    } 
	    return resp.toString();
	  }
  
  
  public static void enableSSL() throws NoSuchAlgorithmException, KeyManagementException {

		// Create a trust manager that does not validate certificate chains
		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			public void checkClientTrusted(X509Certificate[] certs, String authType) {
			}

			public void checkServerTrusted(X509Certificate[] certs, String authType) {
			}
		} };

		// Install the all-trusting trust manager
		SSLContext sc = SSLContext.getInstance("SSL");
		sc.init(null, trustAllCerts, new java.security.SecureRandom());
		HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

		// Create all-trusting host name verifier
		HostnameVerifier allHostsValid = new HostnameVerifier() {
			public boolean verify(String hostname, SSLSession session) {
				return true;
			}
		};

		// Install the all-trusting host verifier
		HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
	}
  
  public String GetRefNumber(String type, int len) {
    String finalString = "";
    int x = 0;
    char[] stringChars = new char[len];
    for (int i = 0; i < len; i++) {
      Random random = new Random();
      x = random.nextInt(9);
      stringChars[i] = Integer.toString(x).toCharArray()[0];
    } 
    finalString = new String(stringChars);
    finalString = String.valueOf(type) + finalString;
    return finalString.trim();
  }
}
