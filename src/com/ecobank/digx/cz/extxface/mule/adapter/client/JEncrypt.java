package com.ecobank.digx.cz.extxface.mule.adapter.client;

import java.security.MessageDigest;
import java.util.logging.Logger;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

import com.ofss.fc.infra.log.impl.MultiEntityLogger;
import com.ofss.fc.infra.util.Base64;

public class JEncrypt {
	
	private static final String THIS_COMPONENT_NAME = JEncrypt.class.getName();

	private static transient Logger logger = MultiEntityLogger.getUniqueInstance().getLogger(THIS_COMPONENT_NAME);
  // static Logger logger = Logger.getLogger(JEncrypt.class);
   public static String charsetName = "UTF8";
   public static String algorithm = "DES";
   //public static String key = "ECO654RDS033FGV1";
   public static String key = "ECO654RDS033FGV1";

   public static void main(String[] args) {
      //String pwd = "fee36bb7bac1c0ecd5bfa15ef1611867260ff90260997ca9cd790ba42869b2c8f880712b9576660213a00b242f497a87754cb7815f877f263f22ea924cfde58b";
      //String pwd1 = "fNofZhPAhMCFPkqgX61r+oeK+Qbb+n3+dK3cJCKsxXJja2nqppQdfQRB01OZYEXGllgws4X/WWpspy/LSuMjyoq/E6LlRjJpt7wlMhf22ArjvIcAy4UIIK4T31ceuxvHDpukAGY8OIyzE0yKmlBWAXBW/aNiLVHI7nGOX3PKHtiiT9WXbdhQYw==";
      
	   String pwd = "1234";
	   String pwd1 = "N+TVJauN9S0=";
	   String ee = encrypt(pwd);

      //String de = decrypt("pwd1");

      String de = decrypt(pwd1);

      logger.info("Decyrypted " + de);
      logger.info("Encrypted " + ee);
   }

   public static String encrypt(String data) {
      if (key != null && data != null) {
         try {
            DESKeySpec desKeySpec = new DESKeySpec(key.getBytes(charsetName));
            SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance(algorithm);
            SecretKey secretKey = secretKeyFactory.generateSecret(desKeySpec);
            byte[] dataBytes = data.getBytes(charsetName);
            Cipher cipher = Cipher.getInstance(algorithm);
            cipher.init(1, secretKey);
            String s = new String(Base64.encodeBytes(cipher.doFinal(dataBytes)));
            logger.info("ENC:::" + s);
            logger.info("Data-Enc:::" + s);
            return s;
         } catch (Exception var7) {
           // LoggingUtil.logError(var7);
            return null;
         }
      } else {
         return null;
      }
   }

   public static String decrypt(String data) {
      if (key != null && data != null) {
         try {
            byte[] dataBytes =Base64.decode(data); // Base64.decode(arg0).encodeBytes(data.getBytes());
            DESKeySpec desKeySpec = new DESKeySpec(key.getBytes(charsetName));
            SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance(algorithm);
            SecretKey secretKey = secretKeyFactory.generateSecret(desKeySpec);
            Cipher cipher = Cipher.getInstance(algorithm);
            cipher.init(2, secretKey);
            byte[] dataBytesDecrypted = cipher.doFinal(dataBytes);
            String s = new String(dataBytesDecrypted);
            return s;
         } catch (Exception var8) {
           // LoggingUtil.logError(var8);
            var8.printStackTrace();
            return null;
         }
      } else {
         return null;
      }
   }

   public static boolean ValidateTransactionToken(String scode, String amount, String acc, String requestID, String token) {
      boolean isValid = false;

      try {
         logger.info("SCODE::" + scode + " " + token + " " + amount + "  " + requestID + "  " + acc);
         String pwd = ""; // Settings.getInstance("").getProperty(scode + "_TOKEN");
         String spwd = decrypt(pwd);
         String data = "";
         (new StringBuilder()).append(acc).append(requestID).append(scode).append(spwd).toString();
         if (scode.equals("INTERAFRICA")) {
            data = requestID + scode + spwd;
         } else {
            data = acc + requestID + scode + spwd;
         }

         logger.info("STOKEN::" + data);
         logger.info("TOKEN::" + token);
         MessageDigest digest = MessageDigest.getInstance("SHA-512");
         byte[] hashBytes = data.getBytes("UTF-8");
         byte[] messageDigest = digest.digest(hashBytes);
         StringBuffer sb = new StringBuffer();

         for(int i = 0; i < messageDigest.length; ++i) {
            String h;
            for(h = Integer.toHexString(255 & messageDigest[i]); h.length() < 2; h = "0" + h) {
            }

            sb.append(h);
         }

         logger.info("SBB-TOKEN:" + sb.toString());
         if (sb.toString().equals(token)) {
            logger.info("SBB-TOKEN VALID:");
            sb = null;
            isValid = true;
            return true;
         }
      } catch (Exception var15) {
         //LoggingUtil.logError(var15);
      }

      return isValid;
   }
   
    public static String GenerateTransactionToken(String scode, String requestID, String pwd) {
	      String result = "";

	      try {
	         //logger.info("SCODE::" + scode + " " + token + " " + amount + "  " + requestID + "  " + acc);
	        
	         String data = scode + requestID + pwd;
	         

	         System.out.println("STOKEN:: " + data);
	         //logger.info("TOKEN::" + token);
	         MessageDigest digest = MessageDigest.getInstance("SHA-512");
	         byte[] hashBytes = data.getBytes("UTF-8");
	         byte[] messageDigest = digest.digest(hashBytes);
	         StringBuffer sb = new StringBuffer();

	         for(int i = 0; i < messageDigest.length; ++i) {
	            String h;
	            for(h = Integer.toHexString(255 & messageDigest[i]); h.length() < 2; h = "0" + h) {
	            }

	            sb.append(h);
	         }
	         
	         result = sb.toString();

	         System.out.println("SBB-TOKEN:" + sb.toString());
	        
	         
	      } catch (Exception var15) {
	         //LoggingUtil.logError(var15);
	      }

	      return result;
	   }

   public static boolean ValidateTokenSourcePassReq(String scode, String requestID, String token) {
      boolean isValid = false;

      try {
         String pwd = ""; //Settings.getInstance("").getProperty(scode + "_TOKEN");
         String spwd = decrypt(pwd);
         String data = requestID + scode + spwd;
         logger.info("STOKEN::" + data);
         String validateToken = ""; //Settings.getInstance("").getProperty("VALIDATE_TOKEN");
         if (validateToken.equals("N")) {
            return true;
         }

         MessageDigest digest = MessageDigest.getInstance("SHA-512");
         byte[] hashBytes = data.getBytes("UTF-8");
         byte[] messageDigest = digest.digest(hashBytes);
         StringBuffer sb = new StringBuffer();

         for(int i = 0; i < messageDigest.length; ++i) {
            String h;
            for(h = Integer.toHexString(255 & messageDigest[i]); h.length() < 2; h = "0" + h) {
            }

            sb.append(h);
         }

         logger.info("SBB-TOKEN:" + sb.toString());
         if (sb.toString().equals(token)) {
            logger.info("SBB-TOKEN VALID:");
            sb = null;
            isValid = true;
            return true;
         }
      } catch (Exception var14) {
         //LoggingUtil.logError(var14);
      }

      return isValid;
   }

   public static boolean ValidateTokenSourcePassReqAlways(String scode, String requestID, String token) {
      boolean isValid = false;

      try {
         String pwd = "";// Settings.getInstance("").getProperty(scode + "_TOKEN");
         String spwd = decrypt(pwd);
         String data = requestID + scode + spwd;
         logger.info("STOKEN::" + data);
         String validateToken ="Y"; // Settings.getInstance("").getProperty("VALIDATE_TOKEN");
         MessageDigest digest = MessageDigest.getInstance("SHA-512");
         byte[] hashBytes = data.getBytes("UTF-8");
         byte[] messageDigest = digest.digest(hashBytes);
         StringBuffer sb = new StringBuffer();

         for(int i = 0; i < messageDigest.length; ++i) {
            String h;
            for(h = Integer.toHexString(255 & messageDigest[i]); h.length() < 2; h = "0" + h) {
            }

            sb.append(h);
         }

         logger.info("SBB-TOKEN::::" + sb.toString());
         if (sb.toString().equals(token)) {
            logger.info("SBB-TOKEN VALID:");
            sb = null;
            isValid = true;
            return true;
         }
      } catch (Exception var14) {
         //LoggingUtil.logError(var14);
      }

      return isValid;
   }

   public static String Hash512Msg(String data) {
      String isValid = "";

      try {
         logger.info("STOKEN::" + data);
         MessageDigest digest = MessageDigest.getInstance("SHA-512");
         byte[] hashBytes = data.getBytes("UTF-8");
         byte[] messageDigest = digest.digest(hashBytes);
         StringBuffer sb = new StringBuffer();

         for(int i = 0; i < messageDigest.length; ++i) {
            String h;
            for(h = Integer.toHexString(255 & messageDigest[i]); h.length() < 2; h = "0" + h) {
            }

            sb.append(h);
         }

         logger.info("SBB-TOKEN:" + sb.toString());
         isValid = sb.toString();
      } catch (Exception var8) {
         //LoggingUtil.logError(var8);
      }

      return isValid;
   }
}
