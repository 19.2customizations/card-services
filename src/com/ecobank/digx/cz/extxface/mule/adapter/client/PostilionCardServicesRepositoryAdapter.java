package com.ecobank.digx.cz.extxface.mule.adapter.client;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.Random;
import java.util.logging.Logger;
import java.util.prefs.Preferences;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import com.ecobank.digx.cz.app.cardservices.dto.CardLimitResponseDTO;
import com.ecobank.digx.cz.app.cardservices.dto.CardListResponse;
import com.ecobank.digx.cz.app.cardservices.dto.CardRequestFeeDTO;
import com.ecobank.digx.cz.app.cardservices.dto.CardServiceResponseDTO;
import com.ecobank.digx.cz.app.domain.cardservices.entity.CardRequest;
import com.ecobank.digx.cz.eco.domain.generic.entity.genericlogger.GenericLoggerConfiguration;
import com.ecobank.digx.cz.extxface.mule.assembler.CardRemoteServiceAssembler;
import com.ecobank.digx.cz.extxface.mule.dto.CardChargeResponseDTO;
import com.ecobank.digx.cz.extxface.mule.dto.ESBCardBlockRequestDTO;
import com.ecobank.digx.cz.extxface.mule.dto.ESBCardRequestDTO;
import com.ecobank.digx.cz.extxface.mule.dto.ESBCardResponseDTO;
import com.ecobank.digx.cz.extxface.mule.dto.FetchCardDebitLimitRequestDTO;
import com.ecobank.digx.cz.extxface.mule.dto.FetchCardDebitLimitResponseDTO;
import com.ecobank.digx.cz.extxface.mule.dto.HostHeaderInfo;
import com.ecobank.digx.cz.extxface.mule.dto.PstFetchCardByAccountRequest;
import com.ecobank.digx.cz.extxface.mule.dto.PstFetchCardByAccountResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.ofss.digx.app.core.AdapterInteraction;
import com.ofss.digx.extxface.response.AbstractResponseHandler;
import com.ofss.digx.framework.rest.client.RESTClientRequestBuilder;
import com.ofss.digx.framework.rest.client.RESTClientRequestConfig;
import com.ofss.digx.infra.exceptions.Exception;
import com.ofss.fc.datatype.Date;
import com.ofss.fc.infra.config.ConfigurationFactory;
import com.ofss.fc.infra.log.impl.MultiEntityLogger;
import com.ofss.fc.infra.thread.ThreadAttribute;
import com.thoughtworks.xstream.XStream;

public class PostilionCardServicesRepositoryAdapter extends AbstractResponseHandler {
	private static PostilionCardServicesRepositoryAdapter singletonInstance;
	
	private final CardServiceResponseHandler responseHandler = new CardServiceResponseHandler();

	private Preferences digx_consulting = ConfigurationFactory.getInstance().getConfigurations("DIGXCONSULTING");

	private static final String THIS_COMPONENT_NAME = PostilionCardServicesRepositoryAdapter.class.getName();
	private static transient Logger logger = MultiEntityLogger.getUniqueInstance().getLogger(THIS_COMPONENT_NAME);
	
	private static final GenericLoggerConfiguration serviceLogger = new GenericLoggerConfiguration();
    private Preferences eco_generic = ConfigurationFactory.getInstance().getConfigurations("ECOGENERIC");
	
	
	public static PostilionCardServicesRepositoryAdapter getInstance() {
		if (singletonInstance == null)
			synchronized (PostilionCardServicesRepositoryAdapter.class) {
				if (singletonInstance == null)
					singletonInstance = new PostilionCardServicesRepositoryAdapter();
			}
		return singletonInstance;
	}
	
	
	
	public CardServiceResponseDTO createCardRequest(CardRequest cardRequest, String branchCode, String deliveryBranch, CardRequestFeeDTO cardFee)
			throws Exception {
		CardServiceResponseDTO response = new CardServiceResponseDTO();
		response.setResponseCode("E04");
		response.setResponseMessage("Failed at interface level");
		AdapterInteraction.begin();

		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESB_DEBIT_CARD_CREATE");
		try {
			enableSSL();
		} catch (KeyManagementException | NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String ipx = digx_consulting.get("ESB_OBDX_IP", "");
		String requestId = cardRequest.getRefKey().getExternalRefNo();
		String pwd = digx_consulting.get("ESB_OBDX_PWD", "");
		String requestToken = JEncrypt.GenerateTransactionToken(sourceCode, requestId, pwd);

		SimpleDateFormat dff4 = new SimpleDateFormat("yyyy-MM-dd");
		java.util.Date dt = new java.util.Date();
		String dateCurrent = dff4.format(dt);

		logger.info("sourceAccountNo: " + cardRequest.getSourceAccountNo());
		
		ESBCardRequestDTO esbRequestDTO = new ESBCardRequestDTO();
		HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
		hostHeaderInfo.setAffiliateCode(cardRequest.getAffiliateCode());
		hostHeaderInfo.setIpAddress(ipx);
		hostHeaderInfo.setRequestId(requestId);
		hostHeaderInfo.setRequestToken(requestToken);
		hostHeaderInfo.setRequestType("DEBITCARDREQUEST");
		hostHeaderInfo.setSourceChannelId("WEB");
		hostHeaderInfo.setSourceCode(sourceCode);
		esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);
		
		if(deliveryBranch == null || deliveryBranch.equals(""))
			deliveryBranch = branchCode;
		
		String deliveryMode = cardRequest.getDeliveryLocationType();
		if(deliveryMode.equalsIgnoreCase("BRANCH"))
			deliveryMode = "BRANCHPICKUP";
		else
			deliveryMode ="BRANCHPICKUP";   // not activated//DELIVERY
		
		System.out.println("Delivery Branch : " + deliveryBranch);
		
		esbRequestDTO.setAccountNo(cardRequest.getSourceAccountNo());
		//esbRequestDTO.setAccountNo(cardRequest.getCardAccountNo());
		esbRequestDTO.setCardScheme(cardRequest.getSchemeType().toUpperCase());
		esbRequestDTO.setCardProduct(cardFee.getCardProduct());
		esbRequestDTO.setRequestBranch(branchCode);
		esbRequestDTO.setCollectionBranch(deliveryBranch.substring(0, 3));
		esbRequestDTO.setAmountToCharge(cardFee.getFee().toString());
		esbRequestDTO.setCardProductDesc(cardFee.getCardProduct()); //Desc());
		esbRequestDTO.setChargeCurrency(cardFee.getCcyCode());
		esbRequestDTO.setChargeDescription(cardFee.getCardFeeDescription());
		esbRequestDTO.setIncomeAccount(cardFee.getIncomeAccount());
		esbRequestDTO.setIncomeAccountBranch(cardFee.getIncomeAccountBranch());
		esbRequestDTO.setDeliveryMode(deliveryMode);
		
		String schemeType = cardRequest.getSchemeType().toUpperCase();
		if(schemeType.contains("VISA") || schemeType.contains("MASTERCARD"))
		{
			System.out.println("Fee Card Request Product fee : " + schemeType);
			CardChargeResponseDTO chargeResp = getCardRequestCharge(cardRequest.getSourceAccountNo(),  cardRequest.getAffiliateCode(), schemeType, deliveryMode);
			
			if(chargeResp != null && chargeResp.getHostHeaderInfo().getResponseCode().equals("000"))
			{
				System.out.println("Fee Card Request Product fee FOUND 2: " + schemeType);
				esbRequestDTO.setCardProduct(chargeResp.getCharges().get(0).getCardProduct());
				esbRequestDTO.setAmountToCharge(chargeResp.getCharges().get(0).getIssuanceFee().toString());
				esbRequestDTO.setCardProductDesc(chargeResp.getCharges().get(0).getCardProductDesc());
				esbRequestDTO.setChargeCurrency(chargeResp.getCharges().get(0).getChargeCurrency());
				esbRequestDTO.setCardProductDesc(esbRequestDTO.getCardProduct());
				//esbRequestDTO.setChargeDescription(chargeResp.getCharges().get(0).);
				//esbRequestDTO.setIncomeAccount(chargeResp.getCharges().get(0).);
				//esbRequestDTO.setIncomeAccountBranch(cardFee.getIncomeAccountBranch());
			}
		}
		
		Date startDateTime = new Date();

		Boolean exceptionFlag = Boolean.valueOf(false);
		String jsonResponseBody ="";
		String jsonRequestBody ="";
		String correlationId = requestId;
		/*esbRequestDTO.setPersonalized("Y");
		esbRequestDTO.setCreatedBy("OBDX");
		esbRequestDTO.setCreatedDate(dateCurrent);
		esbRequestDTO.setVerifiedBy("OBDX");
		esbRequestDTO.setVerifiedDate(dateCurrent);
		esbRequestDTO.setAbridgedCardNo("");*/
		
		
		

		ESBCardResponseDTO esbCardResponseDTO = null;

		try {
			ObjectMapper mapper = new ObjectMapper();
			 jsonRequestBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(esbRequestDTO);
			logger.info("::: createCardRequest RequestBody: " + jsonRequestBody);

			Gson gson = new Gson();
			
			System.out.println("URL: " + prop.getContextURL() + " " + prop.getServiceURL());
			String genResponse = HttpPost(prop.getContextURL(), prop.getServiceURL(), esbRequestDTO, "","");
			esbCardResponseDTO = gson.fromJson(genResponse, ESBCardResponseDTO.class);
			
			//esbCardResponseDTO = gson.fromJson(MockServcie.createDebitCardRequest(), ESBCardResponseDTO.class);
			
			 jsonResponseBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(esbCardResponseDTO);
			logger.info("::: createCardRequest ResponseBody: " + jsonResponseBody);
			
			
			logger.info("::: responseMsg  ::: " + esbCardResponseDTO.getHostHeaderInfo().getResponseMessage());
			logger.info("::: responsecode ::: " + esbCardResponseDTO.getHostHeaderInfo().getResponseCode());

			
			if (esbCardResponseDTO != null) {
				if (esbCardResponseDTO.getHostHeaderInfo().getResponseCode().equals("000")) {
					response.setResponseCode(esbCardResponseDTO.getHostHeaderInfo().getResponseCode());
					response.setResponseMessage(esbCardResponseDTO.getHostHeaderInfo().getResponseMessage());
					response.setTransactionRefNo(esbCardResponseDTO.getHostHeaderInfo().getRequestId());
				}else {
					response.setResponseCode(esbCardResponseDTO.getHostHeaderInfo().getResponseCode());
					response.setResponseMessage(esbCardResponseDTO.getHostHeaderInfo().getResponseMessage());
					//this.responseHandler.showOBDXErrorMapped(esbCardResponseDTO.getHostHeaderInfo().getResponseCode());
				}
				logger.info("Request-Response " + response.getResponseCode());
			}
		} catch (IOException e) {
			
			 exceptionFlag = Boolean.valueOf(true);

		      if ("Y".equalsIgnoreCase(this.eco_generic.get("DEBITCARDREQUEST.INTERFACE_LOGGER_REQUIRED", "")))
              {
                  serviceLogger.dbLogger((Object)jsonRequestBody, (Object)e.getMessage(), "DEBITCARDREQUEST",correlationId,"EXCEPTION",ThreadAttribute.get("SUBJECT_NAME").toString(),ThreadAttribute.get("TARGET_UNIT_CODE").toString(), startDateTime);

		       }
		      
			e.printStackTrace();
			this.responseHandler.showOBDXErrorMapped(null);
		}
		finally {
			
			if ("Y".equalsIgnoreCase(this.eco_generic.get("DEBITCARDREQUEST.INTERFACE_LOGGER_REQUIRED", "")) && !exceptionFlag.booleanValue())
            {
               serviceLogger.dbLogger((Object)jsonRequestBody , (Object)jsonResponseBody , "DEBITCARDREQUEST",correlationId,"",ThreadAttribute.get("SUBJECT_NAME").toString(),ThreadAttribute.get("TARGET_UNIT_CODE").toString(), startDateTime);

            }
		}
		AdapterInteraction.close();
		return response;
	}
	
	public CardListResponse getCardByAccount(String accountNo, String affCode) throws Exception {
		CardListResponse cardListResponse = new CardListResponse();
		cardListResponse.setResponseCode("E04");
		cardListResponse.setResponseMessage("Failed at interface level");
		AdapterInteraction.begin();

		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("PST_DEBIT_CARD_BY_ACCOUNT");

		String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String ipx = digx_consulting.get("ESB_OBDX_IP", "");
		String pwd = digx_consulting.get("ESB_OBDX_PWD", "");
		
		String lcyCcy = digx_consulting.get(affCode + "_LCY", "");
		
		String requestId = GetRefNumber("", 12);
		String requestToken = JEncrypt.GenerateTransactionToken(sourceCode, requestId, pwd);
		PstFetchCardByAccountRequest esbRequestDTO = new PstFetchCardByAccountRequest();
		
		
		
		esbRequestDTO.setAffiliateCode(affCode);
		esbRequestDTO.setCardAcctNumber(accountNo);
		esbRequestDTO.setTranRef(requestId);
		esbRequestDTO.setDateTime(GetDateFormat6(new java.util.Date()) + "T10:34:00");
		
		CardRemoteServiceAssembler assembler = new CardRemoteServiceAssembler();

		try {
			XStream xs = new XStream();
			logger.info("GET Card By Account Request : " + xs.toXML(esbRequestDTO));
			logger.info("Source Code : " + sourceCode);

			//ESBGetCardByAccountResponseDTO cardListResponse2 = (ESBGetCardByAccountResponseDTO) restClient.post(prop, ESBGetCardByAccountResponseDTO.class, esbRequestDTO);
			try {
				enableSSL();
			} catch (KeyManagementException | NoSuchAlgorithmException e) {
				e.printStackTrace();
			}
			
			Gson gson = new Gson();
			String urlx = prop.getContextURL() + "/" +prop.getServiceURL();
			System.out.println("fetch card by account url " + urlx);

			
			
			try {

				String clientId = digx_consulting.get("ESB_ACCOUNT_NAME_INQUIRY_CLIENT_ID", "");
				String secret = digx_consulting.get("ESB_ACCOUNT_NAME_INQUIRY_CLIENT_SECRET", "");

				System.out.println("fetch card by account url 1 : " + clientId);
				System.out.println("fetch card by account url 2 : " + secret);

				String genResponse = HttpPost(prop.getContextURL(), prop.getServiceURL(), esbRequestDTO, "","");
				System.out.println("fetch card by account response 2 : " + genResponse);
				PstFetchCardByAccountResponse cardListResponse2 = gson.fromJson(genResponse, PstFetchCardByAccountResponse.class);
				
				//ESBGetCardByAccountResponseDTO cardListResponse2 = null;

				if (cardListResponse2 != null) {
					

						

				} 

			} catch (java.lang.Exception ex) {
				ex.printStackTrace();
			}

            System.out.println("Get Card By Account Response: " + xs.toXML(cardListResponse));
			
		} finally {
			
		}
		return cardListResponse;
	}
	
	public CardChargeResponseDTO getCardRequestCharge(String accountNo, String affCode,String schemeType,String deliveryMode) throws Exception {
		CardChargeResponseDTO cardChargeResponse = new CardChargeResponseDTO();
		//cardListResponse.setResponseCode("E04");
		//cardListResponse.setResponseMessage("Failed at interface level");
		AdapterInteraction.begin();

		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESC_FETCH_DEBIT_CARD_CHARGE");

		String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String ipx = digx_consulting.get("ESB_OBDX_IP", "");
		String pwd = digx_consulting.get("ESB_OBDX_PWD", "");
		
		String lcyCcy = digx_consulting.get(affCode + "_LCY", "");
		
		String requestId = GetRefNumber("CH", 12);
		String requestToken = JEncrypt.GenerateTransactionToken(sourceCode, requestId, pwd);
		ESBCardRequestDTO esbRequestDTO = new ESBCardRequestDTO();
		HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
		hostHeaderInfo.setAffiliateCode(affCode);
		hostHeaderInfo.setIpAddress(ipx);
		hostHeaderInfo.setRequestId(requestId);
		hostHeaderInfo.setRequestToken(requestToken);
		hostHeaderInfo.setRequestType("NEWCARD");
		hostHeaderInfo.setSourceChannelId("WEB");
		hostHeaderInfo.setSourceCode(sourceCode);
		esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);
		esbRequestDTO.setAccountNo(accountNo);  //accountNo
		esbRequestDTO.setCardScheme(schemeType);
		esbRequestDTO.setDeliveryMode(deliveryMode);
		esbRequestDTO.setRequestType("NEWCARD");
		
		CardRemoteServiceAssembler assembler = new CardRemoteServiceAssembler();

		try {
			XStream xs = new XStream();
			logger.info("GET Cardcharge : " + xs.toXML(esbRequestDTO));
			logger.info("Source Code : " + sourceCode);

			//ESBGetCardByAccountResponseDTO cardListResponse2 = (ESBGetCardByAccountResponseDTO) restClient.post(prop, ESBGetCardByAccountResponseDTO.class, esbRequestDTO);
			try {
				enableSSL();
			} catch (KeyManagementException | NoSuchAlgorithmException e) {
				e.printStackTrace();
			}
			
			Gson gson = new Gson();
			String urlx = prop.getContextURL() + "/" +prop.getServiceURL();
			System.out.println("fetch card charge url " + urlx);

			URL obj = null;
			try {
				obj = new URL(urlx);
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
			
			try {

				String clientId = digx_consulting.get("ESB_ACCOUNT_NAME_INQUIRY_CLIENT_ID", "");
				String secret = digx_consulting.get("ESB_ACCOUNT_NAME_INQUIRY_CLIENT_SECRET", "");

				System.out.println("fetch card by account url 1 : " + clientId);
				System.out.println("fetch card by account url 2 : " + secret);

				// String requestToken = JEncrypt.GenerateTransactionToken(sourceCode,
				// requestId, pwd);

				String genResponse = HttpPost(prop.getContextURL(), prop.getServiceURL(), esbRequestDTO,clientId,secret);
				cardChargeResponse = gson.fromJson(genResponse, CardChargeResponseDTO.class);

			} catch (java.lang.Exception ex) {
				ex.printStackTrace();
			}

            System.out.println("Get Card Charge: " + xs.toXML(cardChargeResponse));
			
		} finally {
			
		}
		return cardChargeResponse;
	}
	
	
	
	public CardLimitResponseDTO fetchDebitCardLimit(String cardType, String cardAccountCustNo,String affCode)
			throws NoSuchAlgorithmException, KeyManagementException, IOException, Exception {
		
		CardLimitResponseDTO cardLimitResponseDTO = new CardLimitResponseDTO();
		CardServiceResponseDTO responsex = new CardServiceResponseDTO();
		responsex.setResponseCode("E04");
		responsex.setResponseMessage("Failed at interface level");
		
		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESB_DEBIT_CARD_LIMIT");
		
		enableSSL();
		String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String ipx = digx_consulting.get("ESB_OBDX_IP", "");
		//String requestId = cardRequest.getRefKey().getExternalRefNo();
		String requestId = GetRefNumber("FC", 12);
		String pwd = digx_consulting.get("ESB_OBDX_PWD", "");
		String requestToken = JEncrypt.GenerateTransactionToken(sourceCode, requestId, pwd);

		FetchCardDebitLimitRequestDTO requestDTO = new FetchCardDebitLimitRequestDTO();
		HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
		hostHeaderInfo.setAffiliateCode(affCode);
		hostHeaderInfo.setIpAddress(ipx);
		hostHeaderInfo.setRequestId(requestId);
		hostHeaderInfo.setRequestToken(requestToken);
		hostHeaderInfo.setRequestType("MPAY");
		hostHeaderInfo.setSourceChannelId("WEB");
		hostHeaderInfo.setSourceCode(sourceCode);
		requestDTO.setHostHeaderInfo(hostHeaderInfo);
		requestDTO.setPan(cardAccountCustNo);

		FetchCardDebitLimitResponseDTO fetchCardDebitLimitResponseDTO = null;

		try {
			StringBuffer resp = new StringBuffer();
			logger.info("cardLimit endpoint 1 : " + prop.getServiceURL());
			logger.info("cardLimit endpoint 2 : " + prop.getContextURL());
			
			
			XStream xs = new XStream();
			System.out.println("cardLimit-Request : " + xs.toXML(requestDTO));

			URL obj = null;
			try {
				obj = new URL(prop.getContextURL() + "/" +prop.getServiceURL());
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
			HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
			con.setRequestMethod("POST");
			con.setRequestProperty("Content-Type", "application/xml");

			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(xs.toXML(requestDTO));
			wr.flush();
			wr.close();
			String responseStatus = con.getResponseMessage();
			System.out.println(responseStatus);
			int responseCode = con.getResponseCode();
			logger.info("responseCode " + responseCode);

			String inputLine;

			if (responseCode == 200) {
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				while ((inputLine = in.readLine()) != null) {
					resp.append(inputLine);
				}
				in.close();
				logger.info("response 200: " + resp.toString());
			} else {
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getErrorStream()));
				while ((inputLine = in.readLine()) != null) {
					resp.append(inputLine);
				}
				in.close();
				logger.info("response XXX: " + resp.toString());
			}

			//xs.processAnnotations(FetchCardDebitLimitResponseDTO.class);
			
			//XStream.setupDefaultSecurity(xs);
			//xs.allowTypeHierarchy(FetchCardDebitLimitResponseDTO.class);
			
			fetchCardDebitLimitResponseDTO = (FetchCardDebitLimitResponseDTO) xs.fromXML(resp.toString());
			
			if(fetchCardDebitLimitResponseDTO.getHostHeaderInfo().size() > 0) {
				logger.info("::: purchaseLimit  ::: " + fetchCardDebitLimitResponseDTO.getPurchaseLimit());
				logger.info("::: transferLimit  ::: " + fetchCardDebitLimitResponseDTO.getPaymentTransferLimit());
				logger.info("::: responseMsg    ::: " + fetchCardDebitLimitResponseDTO.getHostHeaderInfo().get(0).getResponseMessage());
				logger.info("::: responsecode   ::: " + fetchCardDebitLimitResponseDTO.getHostHeaderInfo().get(0).getResponseCode());
				
				cardLimitResponseDTO.setResponseCode(fetchCardDebitLimitResponseDTO.getHostHeaderInfo().get(0).getResponseCode());
				cardLimitResponseDTO.setResponseMessage(fetchCardDebitLimitResponseDTO.getHostHeaderInfo().get(0).getResponseMessage());
				cardLimitResponseDTO.setPurchaseLimit(""+fetchCardDebitLimitResponseDTO.getPurchaseLimit());
				cardLimitResponseDTO.setPaymentTransferLimit(""+fetchCardDebitLimitResponseDTO.getPaymentTransferLimit());
				cardLimitResponseDTO.setAtmCashLimit(""+fetchCardDebitLimitResponseDTO.getPaymentTransferLimit());
				cardLimitResponseDTO.setOnlinePurchaseLimit(""+fetchCardDebitLimitResponseDTO.getPaymentTransferLimit());
			}

		} finally {
		}
		return cardLimitResponseDTO;
	}

	
	public CardLimitResponseDTO setDebitCardLimit(CardRequest cardRequest)
			throws NoSuchAlgorithmException, KeyManagementException, IOException, Exception {
		
		CardLimitResponseDTO cardLimitResponseDTO = new CardLimitResponseDTO();
		CardServiceResponseDTO responsex = new CardServiceResponseDTO();
		responsex.setResponseCode("E04");
		responsex.setResponseMessage("Failed at interface level");
		
		String affCode = cardRequest.getAffiliateCode();
		
		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESB_DEBIT_CARD_LIMIT");
		
		enableSSL();
		String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String ipx = digx_consulting.get("ESB_OBDX_IP", "");
		//String requestId = cardRequest.getRefKey().getExternalRefNo();
		String requestId = GetRefNumber("FC", 12);
		String pwd = digx_consulting.get("ESB_OBDX_PWD", "");
		String requestToken = JEncrypt.GenerateTransactionToken(sourceCode, requestId, pwd);

		FetchCardDebitLimitRequestDTO requestDTO = new FetchCardDebitLimitRequestDTO();
		HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
		hostHeaderInfo.setAffiliateCode(affCode);
		hostHeaderInfo.setIpAddress(ipx);
		hostHeaderInfo.setRequestId(requestId);
		hostHeaderInfo.setRequestToken(requestToken);
		hostHeaderInfo.setRequestType("BATCHPOST");
		hostHeaderInfo.setSourceChannelId("WEB");
		hostHeaderInfo.setSourceCode(sourceCode);
		requestDTO.setHostHeaderInfo(hostHeaderInfo);
		requestDTO.setPan(cardRequest.getMaskedCardNo());
		//requestDTO.setCardNotPresentLimit("10000");
		//requestDTO.setCashWithdrawalLimit("10000");
		//requestDTO.setPaymentTransferLimit("10000");
		//requestDTO.setPurchaseLimit("10000");

		FetchCardDebitLimitResponseDTO fetchCardDebitLimitResponseDTO = null;

		try {
			StringBuffer resp = new StringBuffer();
			logger.info("cardLimit endpoint 1 : " + prop.getServiceURL());
			logger.info("cardLimit endpoint 2 : " + prop.getContextURL());
			
			
			XStream xs = new XStream();
			System.out.println("cardLimit-Request : " + xs.toXML(requestDTO));

			URL obj = null;
			try {
				obj = new URL(prop.getContextURL() + "/" +prop.getServiceURL());
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
			HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
			con.setRequestMethod("POST");
			con.setRequestProperty("Content-Type", "application/xml");

			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(xs.toXML(requestDTO));
			wr.flush();
			wr.close();
			String responseStatus = con.getResponseMessage();
			System.out.println(responseStatus);
			int responseCode = con.getResponseCode();
			logger.info("responseCode " + responseCode);

			String inputLine;

			if (responseCode == 200) {
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				while ((inputLine = in.readLine()) != null) {
					resp.append(inputLine);
				}
				in.close();
				logger.info("response 200: " + resp.toString());
			} else {
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getErrorStream()));
				while ((inputLine = in.readLine()) != null) {
					resp.append(inputLine);
				}
				in.close();
				logger.info("response XXX: " + resp.toString());
			}

			xs.processAnnotations(FetchCardDebitLimitResponseDTO.class);
			
			XStream.setupDefaultSecurity(xs);
			xs.allowTypeHierarchy(FetchCardDebitLimitResponseDTO.class);
			
			fetchCardDebitLimitResponseDTO = (FetchCardDebitLimitResponseDTO) xs.fromXML(resp.toString());
			
			if(fetchCardDebitLimitResponseDTO.getHostHeaderInfo().size() > 0) {
				logger.info("::: purchaseLimit  ::: " + fetchCardDebitLimitResponseDTO.getPurchaseLimit());
				logger.info("::: transferLimit  ::: " + fetchCardDebitLimitResponseDTO.getPaymentTransferLimit());
				logger.info("::: responseMsg    ::: " + fetchCardDebitLimitResponseDTO.getHostHeaderInfo().get(0).getResponseMessage());
				logger.info("::: responsecode   ::: " + fetchCardDebitLimitResponseDTO.getHostHeaderInfo().get(0).getResponseCode());
				
				cardLimitResponseDTO.setResponseCode(fetchCardDebitLimitResponseDTO.getHostHeaderInfo().get(0).getResponseCode());
				cardLimitResponseDTO.setResponseMessage(fetchCardDebitLimitResponseDTO.getHostHeaderInfo().get(0).getResponseMessage());
				cardLimitResponseDTO.setPurchaseLimit(""+fetchCardDebitLimitResponseDTO.getPurchaseLimit());
				cardLimitResponseDTO.setPaymentTransferLimit(""+fetchCardDebitLimitResponseDTO.getPaymentTransferLimit());
			}

		} finally {
		}
		return cardLimitResponseDTO;
	}
	
	
	public CardServiceResponseDTO blockCard(String affCode,String cardNo, String expiryDate, String reason ) throws Exception {
		CardServiceResponseDTO response = new CardServiceResponseDTO();
		response.setResponseCode("E04");
		response.setResponseMessage("Failed at interface level");
		AdapterInteraction.begin();
		String requestId = GetRefNumber("BC", 12);
		com.ofss.fc.datatype.Date startDateTime = new com.ofss.fc.datatype.Date();
		Boolean exceptionFlag = Boolean.valueOf(false);
		String jsonResponseBody ="";
		String jsonRequestBody ="";
		String correlationId = requestId;
		
		System.out.println("Card Block-Credit :" + cardNo + " " + expiryDate + " " + reason);

		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESB_CREDIT_CARD_BLOCK");

		
		String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String pwd = digx_consulting.get("ESB_OBDX_PWD", "");
		String requestToken = JEncrypt.GenerateTransactionToken(sourceCode, requestId, pwd);
		String ipx = digx_consulting.get("ESB_OBDX_IP", "");
		
		ESBCardBlockRequestDTO esbRequestDTO = new ESBCardBlockRequestDTO();
		HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
		hostHeaderInfo.setAffiliateCode(affCode); //temporarily hardcoded
		hostHeaderInfo.setIpAddress(ipx);
		hostHeaderInfo.setRequestId(requestId);
		hostHeaderInfo.setRequestToken(requestToken);
		hostHeaderInfo.setRequestType("BLOCKCARD");
		hostHeaderInfo.setSourceChannelId("MOBILE");
		hostHeaderInfo.setSourceCode(sourceCode);
		esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);
		esbRequestDTO.setReasonCode(reason);
		esbRequestDTO.setCardNo(cardNo); 
		//MM/YY
		//String expDate = expiryDate.substring(3,5) + expiryDate.substring(0,2);
		String expDate = "";
		if(expDate != null && expDate.length() >= 5)
		   expDate = expiryDate.substring(3,5) + expiryDate.substring(0,2);
		
		esbRequestDTO.setExpiryDate(expDate);

		ESBCardResponseDTO resp = null;

		try {
			ObjectMapper mapper = new ObjectMapper();
			 jsonRequestBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(esbRequestDTO);
			logger.info("::: blockCard RequestBody: " + jsonRequestBody);

			Gson gson = new Gson();
			//String genResponse = HttpPost(prop.getContextURL(), prop.getServiceURL(), esbRequestDTO);
			//resp = gson.fromJson(genResponse, ESBCardResponseDTO.class);
			
			//resp = gson.fromJson(MockServcie.blockCCCard(), ESBCardResponseDTO.class);
			
			 jsonResponseBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(resp);
			logger.info("::: blockCard ResponseBody   : " + jsonResponseBody);
			
			
			logger.info("::: Generate CustomerNo      : " + resp.getCustomerNo());
			logger.info("::: getResponseCode          : " + resp.getHostHeaderInfo().getResponseCode());
			logger.info("::: getResponseMessage       : " + resp.getHostHeaderInfo().getResponseMessage());
			if (resp != null) {
				if (resp.getHostHeaderInfo().getResponseCode().equals("000")) {

					response.setResponseCode("000");
					response.setResponseMessage(resp.getHostHeaderInfo().getResponseMessage());
					// response.setActivityDescription("BLOCK CARD");
					// response.setCardId("");
					// response.setRefId(genRef);
				}else {
					response.setResponseCode(resp.getHostHeaderInfo().getResponseCode());
					response.setResponseMessage(resp.getHostHeaderInfo().getResponseMessage());
				}
			}
		} catch (JsonProcessingException e) {
			 exceptionFlag = Boolean.valueOf(true);

		      if ("Y".equalsIgnoreCase(this.eco_generic.get("BLOCKCARD_DEBIT.INTERFACE_LOGGER_REQUIRED", "")))
             {
                 serviceLogger.dbLogger((Object)jsonRequestBody, (Object)e.getMessage(), "BLOCKCARD_DEBIT",correlationId,"EXCEPTION",ThreadAttribute.get("SUBJECT_NAME").toString(),ThreadAttribute.get("TARGET_UNIT_CODE").toString(), startDateTime);

		       }
			e.printStackTrace();
		} finally {
			if ("Y".equalsIgnoreCase(this.eco_generic.get("BLOCKCARD_DEBIT.INTERFACE_LOGGER_REQUIRED", "")) && !exceptionFlag.booleanValue())
            {
               serviceLogger.dbLogger((Object)jsonRequestBody , (Object)jsonResponseBody , "BLOCKCARD_DEBIT",correlationId,"",ThreadAttribute.get("SUBJECT_NAME").toString(),ThreadAttribute.get("TARGET_UNIT_CODE").toString(), startDateTime);

            }
		}
		AdapterInteraction.close();

		return response;
	}
	
	
	public CardServiceResponseDTO unblockCard(String affCode, String cardNo, String expiryDate, String reason ) throws Exception {
		CardServiceResponseDTO response = new CardServiceResponseDTO();
		response.setResponseCode("E04");
		response.setResponseMessage("Failed at interface level");
		AdapterInteraction.begin();
		
		String respCode = "";
		
		String requestId = GetRefNumber("BC", 12);
		com.ofss.fc.datatype.Date startDateTime = new com.ofss.fc.datatype.Date();
		Boolean exceptionFlag = Boolean.valueOf(false);
		String jsonResponseBody ="";
		String jsonRequestBody ="";
		String correlationId = requestId;

		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESB_CREDIT_CARD_UNBLOCK");

		
		String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String pwd = digx_consulting.get("ESB_OBDX_PWD", "");
		String requestToken = JEncrypt.GenerateTransactionToken(sourceCode, requestId, pwd);
		String ipx = digx_consulting.get("ESB_OBDX_IP", "");
		
		ESBCardBlockRequestDTO esbRequestDTO = new ESBCardBlockRequestDTO();
		HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
		hostHeaderInfo.setAffiliateCode(affCode); //temporarily hardcoded
		hostHeaderInfo.setIpAddress(ipx);
		hostHeaderInfo.setRequestId(requestId);
		hostHeaderInfo.setRequestToken(requestToken);
		hostHeaderInfo.setRequestType("UNBLOCKCARD");
		hostHeaderInfo.setSourceChannelId("MOBILE");
		hostHeaderInfo.setSourceCode(sourceCode);
		esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);
		esbRequestDTO.setReasonCode(reason);
		esbRequestDTO.setCardNo(cardNo); 
		//MM/YY
		String expDate = "";
		if(expDate != null && expDate.length() >= 5)
		    expDate = expiryDate.substring(3,5) + expiryDate.substring(0,2);
		esbRequestDTO.setExpiryDate(expDate);

		ESBCardResponseDTO resp = null;

		try {
			ObjectMapper mapper = new ObjectMapper();
			 jsonRequestBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(esbRequestDTO);
			logger.info("::: unblockCard RequestBody: " + jsonRequestBody);

			String clientId = digx_consulting.get("ESB_ACCOUNT_NAME_INQUIRY_CLIENT_ID", "");
			String secret = digx_consulting.get("ESB_ACCOUNT_NAME_INQUIRY_CLIENT_SECRET", "");

			
			Gson gson = new Gson();
			String genResponse = HttpPost(prop.getContextURL(), prop.getServiceURL(), esbRequestDTO,clientId,secret);
			resp = gson.fromJson(genResponse, ESBCardResponseDTO.class);
			
			//resp = gson.fromJson(MockServcie.blockCCCard(), ESBCardResponseDTO.class);
			
			 jsonResponseBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(resp);
			logger.info("::: blockCard ResponseBody   : " + jsonResponseBody);
			
			logger.info("getResponseCode         : " + resp.getHostHeaderInfo().getResponseCode());
			logger.info("getResponseMessage      : " + resp.getHostHeaderInfo().getResponseMessage());
			if (resp != null) {
				if (resp.getHostHeaderInfo().getResponseCode().equals("000")) {
					response.setResponseCode("000");
					response.setResponseMessage(resp.getHostHeaderInfo().getResponseMessage());
					// response.setActivityDescription("UNBLOCK CARD");
					// response.setCardId("");
					// response.setRefId(genRef);
				} else {
					//response.setResponseCode(resp.getHostHeaderInfo().getResponseCode());
					//response.setResponseMessage(resp.getHostHeaderInfo().getResponseMessage());
					responseHandler.showOBDXErrorMapped(respCode);
				}
			}
		} catch (JsonProcessingException e) {
			exceptionFlag = Boolean.valueOf(true);

		      if ("Y".equalsIgnoreCase(this.eco_generic.get("BLOCKCARD_DEBIT.INTERFACE_LOGGER_REQUIRED", "")))
           {
               serviceLogger.dbLogger((Object)jsonRequestBody, (Object)e.getMessage(), "BLOCKCARD_DEBIT",correlationId,"EXCEPTION",ThreadAttribute.get("SUBJECT_NAME").toString(),ThreadAttribute.get("TARGET_UNIT_CODE").toString(), startDateTime);

		       }
			e.printStackTrace();
			responseHandler.showOBDXErrorMapped(respCode);
		} finally {
			if ("Y".equalsIgnoreCase(this.eco_generic.get("BLOCKCARD_DEBIT.INTERFACE_LOGGER_REQUIRED", "")) && !exceptionFlag.booleanValue())
            {
               serviceLogger.dbLogger((Object)jsonRequestBody , (Object)jsonResponseBody , "BLOCKCARD_DEBIT",correlationId,"",ThreadAttribute.get("SUBJECT_NAME").toString(),ThreadAttribute.get("TARGET_UNIT_CODE").toString(), startDateTime);

            }
		}
		AdapterInteraction.close();
		return response;
	}

	
	
	public static String HttpPost(String contextUrl, String serviceUrl, Object payloadRequest,String clientId, String secret) {

		StringBuffer resp = new StringBuffer();
		URL obj = null;
		Gson gson = new Gson();
		try {
			obj = new URL(contextUrl + "/" + serviceUrl);

			if(contextUrl.startsWith("https"))
			{
				

			}
			else
				return HttpPostOnly( contextUrl,  serviceUrl,  payloadRequest, clientId,  secret);
			
			HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
			
		
			
			String requestId = "";
			String requestToken = ""; // JEncrypt.GenerateTransactionToken(sourceCode, requestId, pwd);

			con.setRequestMethod("POST");
			con.setRequestProperty("Content-Type", "application/json");
			
			con.setRequestProperty("x-client-id", clientId);
			con.setRequestProperty("x-client-secret", secret);
			//con.setRequestProperty("x-request-id", requestId);
			//con.setRequestProperty("x-request-token", requestToken);

			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(gson.toJson(payloadRequest));

			wr.flush();
			wr.close();
			String responseStatus = con.getResponseMessage();
			System.out.println(responseStatus);
			int responseCode = con.getResponseCode();
			System.out.println("responseCode " + responseCode);

			String inputLine;

			if (responseCode == 200) {
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				while ((inputLine = in.readLine()) != null) {
					resp.append(inputLine);
				}
				in.close();
			} else {
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getErrorStream()));
				while ((inputLine = in.readLine()) != null) {
					resp.append(inputLine);
				}
				in.close();
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		return resp.toString();

	}
	
	public static String HttpPostOnly(String contextUrl, String serviceUrl, Object payloadRequest,String clientId, String secret) {

		StringBuffer resp = new StringBuffer();
		URL obj = null;
		Gson gson = new Gson();
		try {
			obj = new URL(contextUrl + "/" + serviceUrl);

			//if(contextUrl.startsWith("https"))
			
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			con.setRequestMethod("POST");
			con.setRequestProperty("Content-Type", "application/json");

			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(gson.toJson(payloadRequest));

			wr.flush();
			wr.close();
			String responseStatus = con.getResponseMessage();
			System.out.println(responseStatus);
			int responseCode = con.getResponseCode();
			System.out.println("responseCode " + responseCode);

			String inputLine;

			if (responseCode == 200) {
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				while ((inputLine = in.readLine()) != null) {
					resp.append(inputLine);
				}
				in.close();
			} else {
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getErrorStream()));
				while ((inputLine = in.readLine()) != null) {
					resp.append(inputLine);
				}
				in.close();
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		return resp.toString();

	}


	
	public static void enableSSL() throws NoSuchAlgorithmException, KeyManagementException {

		// Create a trust manager that does not validate certificate chains
		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			public void checkClientTrusted(X509Certificate[] certs, String authType) {
			}

			public void checkServerTrusted(X509Certificate[] certs, String authType) {
			}
		} };

		// Install the all-trusting trust manager
		SSLContext sc = SSLContext.getInstance("SSL");
		sc.init(null, trustAllCerts, new java.security.SecureRandom());
		HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

		// Create all-trusting host name verifier
		HostnameVerifier allHostsValid = new HostnameVerifier() {
			public boolean verify(String hostname, SSLSession session) {
				return true;
			}
		};

		// Install the all-trusting host verifier
		HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
	}
	
	
	public  String GetRefNumber(String type, int len) {

        String finalString = "";
        int x = 0;
        char[] stringChars = new char[len];
        for (int i = 0; i < len; i++) //4
        {
            Random random = new Random();
            x = random.nextInt(9);

            stringChars[i] = Integer.toString(x).toCharArray()[0];
        }


        finalString = new String(stringChars);
        finalString = type + finalString;
        return finalString.trim();
    }
	
	public void checkResponse(String hostResponseCode, String hostResponseMessage) throws Exception {

		
		//String successCodes = null;
		//String localErrorCode = null;

		//Object hostResponseMessage = null;

		if (hostResponseCode == null)
         {
			analyseResponse("DIGX_CZ_ERROR_ESB_CODE_NOR", getClass());
         }
         else if (hostResponseCode != null)
     	 {
			//successCodes = helper.getESBSuccessCode("BILL");
        	 analyseResponse("DIGX_CZ_ERROR_ESB_CODE_" + hostResponseCode, getClass());
			  //analyseResponse("DIGX_CZ_ERROR_ESB_CODE_" + hostResponseCode , getClass(), hostResponseMessage);
     	 }


	

	}
	
	public  String GetDateFormat6(java.util.Date dt)
    {
		 SimpleDateFormat dff4 = new SimpleDateFormat("yyyy-MM-dd");
		return dff4.format(dt) ;
    }
	
}
