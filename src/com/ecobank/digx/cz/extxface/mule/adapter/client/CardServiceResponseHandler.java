package com.ecobank.digx.cz.extxface.mule.adapter.client;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


import com.ofss.digx.extxface.message.Message;
import com.ofss.digx.extxface.response.AbstractResponseHandler;
import com.ofss.digx.infra.exceptions.Exception;
import com.ofss.digx.infra.resource.ErrorMessageResourceBundle;
import com.ofss.fc.infra.log.impl.MultiEntityLogger;

public class CardServiceResponseHandler extends AbstractResponseHandler {

	private static final String THIS_COMPONENT_NAME = CardServiceResponseHandler.class.getName();
	private static final MultiEntityLogger FORMATTER = MultiEntityLogger.getUniqueInstance();
	private static transient Logger logger = MultiEntityLogger.getUniqueInstance().getLogger(THIS_COMPONENT_NAME);

	private static final String DEFAULT_ERROR_CARD_REQUEST = "DIGX_CZ_ESB_ERC_0001";
	private static final String DEFAULT_ERROR_CARD_TOPUP = "DIGX_CZ_ESB_ERC_0002";

	
	public void showOBDXErrorMapped(String errorCode) throws Exception {
		
		 showOBDXErrorMapped( errorCode, "");
		
	}
	public void showOBDXErrorMapped(String errorCode, String errorMessage) throws Exception {
		
		    System.out.println("Show Error Code: " + errorCode);
		    if(errorCode == null || errorCode.equals(""))
		    	errorCode = DEFAULT_ERROR_CARD_REQUEST;
		    
		    if(errorCode.startsWith("V"))
		    {
		    	System.out.println("Show Error Code -VFOUND : " + errorCode + " " + errorMessage);
		    	//analyseResponse(DEFAULT_ERROR_CARD_TOPUP, getClass(), errorMessage);
               return;
		    }
		    	
		    if(!errorCode.equals("000"))
			  analyseResponse("DIGX_CZ_ESB_ERC_" + errorCode, getClass());
		    
		    
		    
		    
		
	}

	public void exceptionForRTFeeInquiry(Exception e) {
	}

	public void exceptionForRTSendMoney(Exception e) {
	}

}
