package com.ecobank.digx.cz.extxface.mule.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({  "cardAcctNumber", "affiliateCode", "dateTime" ,"tranRef" })
public class PstFetchCardByAccountRequest {
	

	@JsonProperty("cardAcctNumber")
	private String cardAcctNumber;
	
	@JsonProperty("affiliateCode")
	private String affiliateCode;
	
	@JsonProperty("dateTime")
	private String dateTime;
	
	@JsonProperty("tranRef")
	private String tranRef;

	public String getCardAcctNumber() {
		return cardAcctNumber;
	}

	public void setCardAcctNumber(String cardAcctNumber) {
		this.cardAcctNumber = cardAcctNumber;
	}

	public String getAffiliateCode() {
		return affiliateCode;
	}

	public void setAffiliateCode(String affiliateCode) {
		this.affiliateCode = affiliateCode;
	}

	public String getDateTime() {
		return dateTime;
	}

	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

	public String getTranRef() {
		return tranRef;
	}

	public void setTranRef(String tranRef) {
		this.tranRef = tranRef;
	}
	
	

	
}