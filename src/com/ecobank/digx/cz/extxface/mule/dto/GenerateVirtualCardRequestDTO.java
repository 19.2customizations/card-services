package com.ecobank.digx.cz.extxface.mule.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"ADDRESS",
"AFFILIATE_COUNTRY",
"AMOUNT",
"APP_VERSION",
"CHARGE_AMOUNT",
"CITY",
"CURRENCY_CODE",
"CUSTOMER_ID",
"DEVICE_TYPE",
"DOB",
"EMAIL",
"EMCERT_ID",
"FIRST_NAME",
"LANG_ID",
"LAST_NAME",
"MSISDN",
"OTP_NUMBER",
"SCHEME_TYPE",
"SERVICE",
"SOURCE_ACCOUNT",
"TOTAL_AMOUNT",
"UUID",
"VCARD_TYPE"
})
public class GenerateVirtualCardRequestDTO {

@JsonProperty("ADDRESS")
private String ADDRESS;
@JsonProperty("AFFILIATE_COUNTRY")
private String AFFILIATE_COUNTRY;
@JsonProperty("AMOUNT")
private String AMOUNT;
@JsonProperty("APP_VERSION")
private String APP_VERSION;
@JsonProperty("CHARGE_AMOUNT")
private String CHARGE_AMOUNT;
@JsonProperty("CITY")
private String CITY;
@JsonProperty("CURRENCY_CODE")
private String CURRENCY_CODE;
@JsonProperty("CUSTOMER_ID")
private String CUSTOMER_ID;
@JsonProperty("DEVICE_TYPE")
private String DEVICE_TYPE;
@JsonProperty("DOB")
private String DOB;
@JsonProperty("EMAIL")
private String EMAIL;
@JsonProperty("EMCERT_ID")
private String EMCERT_ID;
@JsonProperty("FIRST_NAME")
private String FIRST_NAME;
@JsonProperty("LANG_ID")
private String LANG_ID;
@JsonProperty("LAST_NAME")
private String LAST_NAME;
@JsonProperty("MSISDN")
private String MSISDN;
@JsonProperty("OTP_NUMBER")
private String OTP_NUMBER;
@JsonProperty("SCHEME_TYPE")
private String SCHEME_TYPE;
@JsonProperty("SERVICE")
private String SERVICE;
@JsonProperty("SOURCE_ACCOUNT")
private String SOURCE_ACCOUNT;
@JsonProperty("TOTAL_AMOUNT")
private String TOTAL_AMOUNT;
@JsonProperty("UUID")
private String UUID;
@JsonProperty("VCARD_TYPE")
private String VCARD_TYPE;
public String getADDRESS() {
	return ADDRESS;
}
public void setADDRESS(String aDDRESS) {
	ADDRESS = aDDRESS;
}
public String getAFFILIATE_COUNTRY() {
	return AFFILIATE_COUNTRY;
}
public void setAFFILIATE_COUNTRY(String aFFILIATE_COUNTRY) {
	AFFILIATE_COUNTRY = aFFILIATE_COUNTRY;
}
public String getAMOUNT() {
	return AMOUNT;
}
public void setAMOUNT(String aMOUNT) {
	AMOUNT = aMOUNT;
}
public String getAPP_VERSION() {
	return APP_VERSION;
}
public void setAPP_VERSION(String aPP_VERSION) {
	APP_VERSION = aPP_VERSION;
}
public String getCHARGE_AMOUNT() {
	return CHARGE_AMOUNT;
}
public void setCHARGE_AMOUNT(String cHARGE_AMOUNT) {
	CHARGE_AMOUNT = cHARGE_AMOUNT;
}
public String getCITY() {
	return CITY;
}
public void setCITY(String cITY) {
	CITY = cITY;
}
public String getCURRENCY_CODE() {
	return CURRENCY_CODE;
}
public void setCURRENCY_CODE(String cURRENCY_CODE) {
	CURRENCY_CODE = cURRENCY_CODE;
}
public String getCUSTOMER_ID() {
	return CUSTOMER_ID;
}
public void setCUSTOMER_ID(String cUSTOMER_ID) {
	CUSTOMER_ID = cUSTOMER_ID;
}
public String getDEVICE_TYPE() {
	return DEVICE_TYPE;
}
public void setDEVICE_TYPE(String dEVICE_TYPE) {
	DEVICE_TYPE = dEVICE_TYPE;
}
public String getDOB() {
	return DOB;
}
public void setDOB(String dOB) {
	DOB = dOB;
}
public String getEMAIL() {
	return EMAIL;
}
public void setEMAIL(String eMAIL) {
	EMAIL = eMAIL;
}
public String getEMCERT_ID() {
	return EMCERT_ID;
}
public void setEMCERT_ID(String eMCERT_ID) {
	EMCERT_ID = eMCERT_ID;
}
public String getFIRST_NAME() {
	return FIRST_NAME;
}
public void setFIRST_NAME(String fIRST_NAME) {
	FIRST_NAME = fIRST_NAME;
}
public String getLANG_ID() {
	return LANG_ID;
}
public void setLANG_ID(String lANG_ID) {
	LANG_ID = lANG_ID;
}
public String getLAST_NAME() {
	return LAST_NAME;
}
public void setLAST_NAME(String lAST_NAME) {
	LAST_NAME = lAST_NAME;
}
public String getMSISDN() {
	return MSISDN;
}
public void setMSISDN(String mSISDN) {
	MSISDN = mSISDN;
}
public String getOTP_NUMBER() {
	return OTP_NUMBER;
}
public void setOTP_NUMBER(String oTP_NUMBER) {
	OTP_NUMBER = oTP_NUMBER;
}
public String getSCHEME_TYPE() {
	return SCHEME_TYPE;
}
public void setSCHEME_TYPE(String sCHEME_TYPE) {
	SCHEME_TYPE = sCHEME_TYPE;
}
public String getSERVICE() {
	return SERVICE;
}
public void setSERVICE(String sERVICE) {
	SERVICE = sERVICE;
}
public String getSOURCE_ACCOUNT() {
	return SOURCE_ACCOUNT;
}
public void setSOURCE_ACCOUNT(String sOURCE_ACCOUNT) {
	SOURCE_ACCOUNT = sOURCE_ACCOUNT;
}
public String getTOTAL_AMOUNT() {
	return TOTAL_AMOUNT;
}
public void setTOTAL_AMOUNT(String tOTAL_AMOUNT) {
	TOTAL_AMOUNT = tOTAL_AMOUNT;
}
public String getUUID() {
	return UUID;
}
public void setUUID(String uUID) {
	UUID = uUID;
}
public String getVCARD_TYPE() {
	return VCARD_TYPE;
}
public void setVCARD_TYPE(String vCARD_TYPE) {
	VCARD_TYPE = vCARD_TYPE;
}



}