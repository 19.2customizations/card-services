package com.ecobank.digx.cz.extxface.mule.dto;

import java.math.BigDecimal;

public class VirtualTransactionDetails {
	
	public String balance;
    public BigDecimal baseAmount;
    public BigDecimal fee;
    public String narration;
    public BigDecimal totalAmount;
    public String transDate;
    public String transRefNo;
    public String transTime;
    public String referenceInfo;
    
    
	public String getBalance() {
		return balance;
	}
	public void setBalance(String balance) {
		this.balance = balance;
	}
	public BigDecimal getBaseAmount() {
		return baseAmount;
	}
	public void setBaseAmount(BigDecimal baseAmount) {
		this.baseAmount = baseAmount;
	}
	public BigDecimal getFee() {
		return fee;
	}
	public void setFee(BigDecimal fee) {
		this.fee = fee;
	}
	public String getNarration() {
		return narration;
	}
	public void setNarration(String narration) {
		this.narration = narration;
	}
	public BigDecimal getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}
	public String getTransDate() {
		return transDate;
	}
	public void setTransDate(String transDate) {
		this.transDate = transDate;
	}
	public String getTransRefNo() {
		return transRefNo;
	}
	public void setTransRefNo(String transRefNo) {
		this.transRefNo = transRefNo;
	}
	public String getTransTime() {
		return transTime;
	}
	public void setTransTime(String transTime) {
		this.transTime = transTime;
	}
	public String getReferenceInfo() {
		return referenceInfo;
	}
	public void setReferenceInfo(String referenceInfo) {
		this.referenceInfo = referenceInfo;
	}
    
    
	

}
