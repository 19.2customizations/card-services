package com.ecobank.digx.cz.extxface.mule.dto;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"hostHeaderInfo",
"travelRequestId"
})
public class TravelNotificationResponseDTO {

@JsonProperty("hostHeaderInfo")
private HostHeaderInfo hostHeaderInfo;
@JsonProperty("travelRequestId")
private Integer travelRequestId;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

@JsonProperty("hostHeaderInfo")
public HostHeaderInfo getHostHeaderInfo() {
return hostHeaderInfo;
}

@JsonProperty("hostHeaderInfo")
public void setHostHeaderInfo(HostHeaderInfo hostHeaderInfo) {
this.hostHeaderInfo = hostHeaderInfo;
}

@JsonProperty("travelRequestId")
public Integer getTravelRequestId() {
return travelRequestId;
}

@JsonProperty("travelRequestId")
public void setTravelRequestId(Integer travelRequestId) {
this.travelRequestId = travelRequestId;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}