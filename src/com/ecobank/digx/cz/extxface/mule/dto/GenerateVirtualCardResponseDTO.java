package com.ecobank.digx.cz.extxface.mule.dto;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "STATUS_CODE", "STATUS_MESSAGE", "ACCOUNT_DETAILS" })
public class GenerateVirtualCardResponseDTO {

	@JsonProperty("STATUS_CODE")
	private String STATUS_CODE;
	@JsonProperty("STATUS_MESSAGE")
	private String STATUS_MESSAGE;
	@JsonProperty("ACCOUNT_DETAILS")
	
	private List<AccountDetails> ACCOUNT_DETAILS;
	
	public String getSTATUS_CODE() {
		return STATUS_CODE;
	}
	public void setSTATUS_CODE(String sTATUS_CODE) {
		STATUS_CODE = sTATUS_CODE;
	}
	public String getSTATUS_MESSAGE() {
		return STATUS_MESSAGE;
	}
	public void setSTATUS_MESSAGE(String sTATUS_MESSAGE) {
		STATUS_MESSAGE = sTATUS_MESSAGE;
	}
	public List<AccountDetails> getACCOUNT_DETAILS() {
		return ACCOUNT_DETAILS;
	}
	public void setACCOUNT_DETAILS(List<AccountDetails> aCCOUNT_DETAILS) {
		ACCOUNT_DETAILS = aCCOUNT_DETAILS;
	}
	

	
}