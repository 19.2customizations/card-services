package com.ecobank.digx.cz.extxface.mule.dto;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "hostHeaderInfo", "transactions" })
public class ESBCardTransactionHistoryResponseDTO {

	@JsonProperty("hostHeaderInfo")
	private HostHeaderInfo hostHeaderInfo;
	
	
	@JsonProperty("transactions")
	private List<ESBCardStatementEntry> transactions;
	
	
	

	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();
	
	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

	public HostHeaderInfo getHostHeaderInfo() {
		return hostHeaderInfo;
	}

	public void setHostHeaderInfo(HostHeaderInfo hostHeaderInfo) {
		this.hostHeaderInfo = hostHeaderInfo;
	}

	
	public void setAdditionalProperties(Map<String, Object> additionalProperties) {
		this.additionalProperties = additionalProperties;
	}

	public List<ESBCardStatementEntry> getTransactions() {
		return transactions;
	}

	public void setTransactions(List<ESBCardStatementEntry> transactions) {
		this.transactions = transactions;
	}
	
	

}
