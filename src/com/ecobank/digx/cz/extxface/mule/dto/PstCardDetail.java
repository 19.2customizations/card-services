package com.ecobank.digx.cz.extxface.mule.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({  "cardStatus", "expiryDate", "accountType" ,"sequenceNumber","cardProgram", "cardName",
	"customerId","cardAcctNumber","issuedDate","card"})
public class PstCardDetail {
	
	

	@JsonProperty("cardStatus")
	private String cardStatus;
	
	@JsonProperty("expiryDate")
	private String expiryDate;
	
	@JsonProperty("accountType")
	private String accountType;
	
	@JsonProperty("sequenceNumber")
	private String sequenceNumber;

	@JsonProperty("cardProgram")
	private String cardProgram;
	
	@JsonProperty("cardName")
	private String cardName;
	
	@JsonProperty("customerId")
	private String customerId;
	
	@JsonProperty("cardAcctNumber")
	private String cardAcctNumber;
	
	@JsonProperty("issuedDate")
	private String issuedDate;
	
	@JsonProperty("card")
	private String card;

	public String getCardStatus() {
		return cardStatus;
	}

	public void setCardStatus(String cardStatus) {
		this.cardStatus = cardStatus;
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public String getSequenceNumber() {
		return sequenceNumber;
	}

	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}

	public String getCardProgram() {
		return cardProgram;
	}

	public void setCardProgram(String cardProgram) {
		this.cardProgram = cardProgram;
	}

	public String getCardName() {
		return cardName;
	}

	public void setCardName(String cardName) {
		this.cardName = cardName;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getCardAcctNumber() {
		return cardAcctNumber;
	}

	public void setCardAcctNumber(String cardAcctNumber) {
		this.cardAcctNumber = cardAcctNumber;
	}

	public String getIssuedDate() {
		return issuedDate;
	}

	public void setIssuedDate(String issuedDate) {
		this.issuedDate = issuedDate;
	}

	public String getCard() {
		return card;
	}

	public void setCard(String card) {
		this.card = card;
	}
	
	

	

	
}