package com.ecobank.digx.cz.extxface.mule.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({  "responseCode", "tranRef", "dateTime" ,"remarks","Cdata" })
public class PstFetchCardByAccountResponse {
	
	@JsonProperty("responseCode")
	private String responseCode;
	
	@JsonProperty("remarks")
	private String remarks;
	
	@JsonProperty("dateTime")
	private String dateTime;
	
	@JsonProperty("tranRef")
	private String tranRef;
	
	@JsonProperty("Cdata")
	private List<PstCardDetail> cdata;

	

	public String getDateTime() {
		return dateTime;
	}

	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

	public String getTranRef() {
		return tranRef;
	}

	public void setTranRef(String tranRef) {
		this.tranRef = tranRef;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public List<PstCardDetail> getCdata() {
		return cdata;
	}

	public void setCdata(List<PstCardDetail> cdata) {
		this.cdata = cdata;
	}
	
	

	
}