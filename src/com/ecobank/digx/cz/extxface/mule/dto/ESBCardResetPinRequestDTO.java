package com.ecobank.digx.cz.extxface.mule.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "hostHeaderInfo", "pan", "expiryDate", "account",  "mobileNo",  "oldPin",  "newPin",  "cvv" })
public class ESBCardResetPinRequestDTO {

	@JsonProperty("hostHeaderInfo")
	private HostHeaderInfo hostHeaderInfo;
	@JsonProperty("pan")
	private String pan;
	@JsonProperty("expiryDate")
	private String expiryDate;
	
	@JsonProperty("account")
	private String account;
	
	@JsonProperty("mobileNo")
	private String mobileNo;
	
	@JsonProperty("emcertid")
	private String emcertid;
	
	@JsonProperty("pin")
	private String pin;

	@JsonProperty("hostHeaderInfo")
	public HostHeaderInfo getHostHeaderInfo() {
		return hostHeaderInfo;
	}

	@JsonProperty("hostHeaderInfo")
	public void setHostHeaderInfo(HostHeaderInfo hostHeaderInfo) {
		this.hostHeaderInfo = hostHeaderInfo;
	}

	@JsonProperty("expiryDate")
	public String getExpiryDate() {
		return expiryDate;
	}

	@JsonProperty("expiryDate")
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	@JsonProperty("pan")
	public String getPan() {
		return pan;
	}

	@JsonProperty("pan")
	public void setPan(String pan) {
		this.pan = pan;
	}

	@JsonProperty("account")
	public String getAccount() {
		return account;
	}

	@JsonProperty("account")
	public void setAccount(String account) {
		this.account = account;
	}

	@JsonProperty("mobileNo")
	public String getMobileNo() {
		return mobileNo;
	}

	@JsonProperty("mobileNo")
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	@JsonProperty("emcertid")
	public String getEmcertid() {
		return emcertid;
	}

	@JsonProperty("emcertid")
	public void setEmcertid(String emcertid) {
		this.emcertid = emcertid;
	}

	@JsonProperty("pin")
	public String getPin() {
		return pin;
	}

	@JsonProperty("pin")
	public void setPin(String pin) {
		this.pin = pin;
	}
	
	

	
	
}