package com.ecobank.digx.cz.extxface.mule.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "hostHeaderInfo", "pan", "expiryDate", "account",  "mobileNo",  "oldPin",  "newPin",  "cvv" })
public class ESBCardChangePinRequestDTO {

	@JsonProperty("hostHeaderInfo")
	private HostHeaderInfo hostHeaderInfo;
	@JsonProperty("pan")
	private String pan;
	@JsonProperty("expiryDate")
	private String expiryDate;
	
	@JsonProperty("currentPin")
	private String currentPin;
	
	@JsonProperty("newPin")
	private String newPin;
	
	@JsonProperty("emcertid")
	private String emcertid;

	@JsonProperty("hostHeaderInfo")
	public HostHeaderInfo getHostHeaderInfo() {
		return hostHeaderInfo;
	}

	@JsonProperty("hostHeaderInfo")
	public void setHostHeaderInfo(HostHeaderInfo hostHeaderInfo) {
		this.hostHeaderInfo = hostHeaderInfo;
	}

	@JsonProperty("expiryDate")
	public String getExpiryDate() {
		return expiryDate;
	}

	@JsonProperty("expiryDate")
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	@JsonProperty("pan")
	public String getPan() {
		return pan;
	}

	@JsonProperty("pan")
	public void setPan(String pan) {
		this.pan = pan;
	}

	@JsonProperty("currentPin")
	public String getCurrentPin() {
		return currentPin;
	}

	@JsonProperty("currentPin")
	public void setCurrentPin(String currentPin) {
		this.currentPin = currentPin;
	}

	@JsonProperty("newPin")
	public String getNewPin() {
		return newPin;
	}

	@JsonProperty("newPin")
	public void setNewPin(String newPin) {
		this.newPin = newPin;
	}

	@JsonProperty("emcertid")
	public String getEmcertid() {
		return emcertid;
	}

	@JsonProperty("emcertid")
	public void setEmcertid(String emcertid) {
		this.emcertid = emcertid;
	}

	

	
}