package com.ecobank.digx.cz.extxface.mule.dto;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"hostHeaderInfo",
"transactionList"
})
public class VirtualMiniStatementResponseDTO {

@JsonProperty("hostHeaderInfo")
private HostHeaderInfo hostHeaderInfo;
@JsonProperty("transactionList")
private List<TransactionList> transactionList = null;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

@JsonProperty("hostHeaderInfo")
public HostHeaderInfo getHostHeaderInfo() {
return hostHeaderInfo;
}

@JsonProperty("hostHeaderInfo")
public void setHostHeaderInfo(HostHeaderInfo hostHeaderInfo) {
this.hostHeaderInfo = hostHeaderInfo;
}

@JsonProperty("transactionList")
public List<TransactionList> getTransactionList() {
return transactionList;
}

@JsonProperty("transactionList")
public void setTransactionList(List<TransactionList> transactionList) {
this.transactionList = transactionList;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}