package com.ecobank.digx.cz.extxface.mule.dto;

import java.util.List;

public class VirtualCardStatementResponseDTO {
	
	private String responseCode;
	private String responseMessage;
	public HostHeaderInfo hostHeaderInfo;
	public List<VirtualTransactionDetails> transactionDetails;
	
	
	public String getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	public String getResponseMessage() {
		return responseMessage;
	}
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}
	public HostHeaderInfo getHostHeaderInfo() {
		return hostHeaderInfo;
	}
	public void setHostHeaderInfo(HostHeaderInfo hostHeaderInfo) {
		this.hostHeaderInfo = hostHeaderInfo;
	}
	public List<VirtualTransactionDetails> getTransactionDetails() {
		return transactionDetails;
	}
	public void setTransactionDetails(List<VirtualTransactionDetails> transactionDetails) {
		this.transactionDetails = transactionDetails;
	}
	
	
}
