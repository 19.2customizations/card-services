package com.ecobank.digx.cz.extxface.mule.dto;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "totalCharge", "deliveryTime", "cardProduct", "cardProductDesc", "chargeCurrency", "issuanceFee",
		"deliveryCost" })
public class CardChargeInfo {

	@JsonProperty("totalCharge")
	private String totalCharge;
	@JsonProperty("deliveryTime")
	private String deliveryTime;
	@JsonProperty("cardProduct")
	private String cardProduct;
	@JsonProperty("cardProductDesc")
	private String cardProductDesc;
	@JsonProperty("chargeCurrency")
	private String chargeCurrency;
	@JsonProperty("issuanceFee")
	private String issuanceFee;
	@JsonProperty("deliveryCost")
	private String deliveryCost;

	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	public String getTotalCharge() {
		return totalCharge;
	}

	public void setTotalCharge(String totalCharge) {
		this.totalCharge = totalCharge;
	}

	public String getDeliveryTime() {
		return deliveryTime;
	}

	public void setDeliveryTime(String deliveryTime) {
		this.deliveryTime = deliveryTime;
	}

	public String getCardProduct() {
		return cardProduct;
	}

	public void setCardProduct(String cardProduct) {
		this.cardProduct = cardProduct;
	}

	public String getCardProductDesc() {
		return cardProductDesc;
	}

	public void setCardProductDesc(String cardProductDesc) {
		this.cardProductDesc = cardProductDesc;
	}

	public String getChargeCurrency() {
		return chargeCurrency;
	}

	public void setChargeCurrency(String chargeCurrency) {
		this.chargeCurrency = chargeCurrency;
	}

	public String getIssuanceFee() {
		return issuanceFee;
	}

	public void setIssuanceFee(String issuanceFee) {
		this.issuanceFee = issuanceFee;
	}

	public String getDeliveryCost() {
		return deliveryCost;
	}

	public void setDeliveryCost(String deliveryCost) {
		this.deliveryCost = deliveryCost;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}