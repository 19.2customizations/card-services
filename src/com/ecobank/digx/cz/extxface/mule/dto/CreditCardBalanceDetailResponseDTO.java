package com.ecobank.digx.cz.extxface.mule.dto;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "accountNo", "availableAmount", "balance", "bankC", "cardAcct", "crd", "hostHeaderInfo",
		"minPayable", "otb" })
public class CreditCardBalanceDetailResponseDTO {

	@JsonProperty("accountNo")
	private String accountNo;
	@JsonProperty("availableAmount")
	private Integer availableAmount;
	@JsonProperty("balance")
	private Integer balance;
	@JsonProperty("bankC")
	private String bankC;
	@JsonProperty("cardAcct")
	private String cardAcct;
	@JsonProperty("crd")
	private Integer crd;
	@JsonProperty("hostHeaderInfo")
	private HostHeaderInfo hostHeaderInfo;
	@JsonProperty("minPayable")
	private Integer minPayable;
	@JsonProperty("otb")
	private String otb;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("accountNo")
	public String getAccountNo() {
		return accountNo;
	}

	@JsonProperty("accountNo")
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	@JsonProperty("availableAmount")
	public Integer getAvailableAmount() {
		return availableAmount;
	}

	@JsonProperty("availableAmount")
	public void setAvailableAmount(Integer availableAmount) {
		this.availableAmount = availableAmount;
	}

	@JsonProperty("balance")
	public Integer getBalance() {
		return balance;
	}

	@JsonProperty("balance")
	public void setBalance(Integer balance) {
		this.balance = balance;
	}

	@JsonProperty("bankC")
	public String getBankC() {
		return bankC;
	}

	@JsonProperty("bankC")
	public void setBankC(String bankC) {
		this.bankC = bankC;
	}

	@JsonProperty("cardAcct")
	public String getCardAcct() {
		return cardAcct;
	}

	@JsonProperty("cardAcct")
	public void setCardAcct(String cardAcct) {
		this.cardAcct = cardAcct;
	}

	@JsonProperty("crd")
	public Integer getCrd() {
		return crd;
	}

	@JsonProperty("crd")
	public void setCrd(Integer crd) {
		this.crd = crd;
	}

	@JsonProperty("hostHeaderInfo")
	public HostHeaderInfo getHostHeaderInfo() {
		return hostHeaderInfo;
	}

	@JsonProperty("hostHeaderInfo")
	public void setHostHeaderInfo(HostHeaderInfo hostHeaderInfo) {
		this.hostHeaderInfo = hostHeaderInfo;
	}

	@JsonProperty("minPayable")
	public Integer getMinPayable() {
		return minPayable;
	}

	@JsonProperty("minPayable")
	public void setMinPayable(Integer minPayable) {
		this.minPayable = minPayable;
	}

	@JsonProperty("otb")
	public String getOtb() {
		return otb;
	}

	@JsonProperty("otb")
	public void setOtb(String otb) {
		this.otb = otb;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}