package com.ecobank.digx.cz.extxface.mule.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "hostHeaderInfo",  "amount", "charge", "tax", "totalCharge",
	"exchRate", "sourceAccountCcy", "udf1", "sourceAccount","sourceAccountName",
	"sourceAccountStatus" })
public class FetchVirtualCardFeeResponseDTO {
	
	

	@JsonProperty("hostHeaderInfo")
	private HostHeaderInfo hostHeaderInfo;
	@JsonProperty("transactionType")
	private String transactionType;
	
	@JsonProperty("amount")
	private double amount;
	
	@JsonProperty("charge")
	private double charge;
	
	@JsonProperty("tax")
	private double tax;
	
	@JsonProperty("totalCharge")
	private double totalCharge;
	
	@JsonProperty("exchRate")
	private double exchRate;
	
	@JsonProperty("sourceAccountCcy")
	private String sourceAccountCcy;
	
	@JsonProperty("udf1")
	private String udf1;
	
	@JsonProperty("sourceAccountName")
	private String sourceAccountName;
	
	@JsonProperty("sourceAccountStatus")
	private String sourceAccountStatus;

	public HostHeaderInfo getHostHeaderInfo() {
		return hostHeaderInfo;
	}

	public void setHostHeaderInfo(HostHeaderInfo hostHeaderInfo) {
		this.hostHeaderInfo = hostHeaderInfo;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public double getCharge() {
		return charge;
	}

	public void setCharge(double charge) {
		this.charge = charge;
	}

	public double getTax() {
		return tax;
	}

	public void setTax(double tax) {
		this.tax = tax;
	}

	public double getTotalCharge() {
		return totalCharge;
	}

	public void setTotalCharge(double totalCharge) {
		this.totalCharge = totalCharge;
	}

	public double getExchRate() {
		return exchRate;
	}

	public void setExchRate(double exchRate) {
		this.exchRate = exchRate;
	}

	public String getSourceAccountCcy() {
		return sourceAccountCcy;
	}

	public void setSourceAccountCcy(String sourceAccountCcy) {
		this.sourceAccountCcy = sourceAccountCcy;
	}

	public String getUdf1() {
		return udf1;
	}

	public void setUdf1(String udf1) {
		this.udf1 = udf1;
	}

	public String getSourceAccountName() {
		return sourceAccountName;
	}

	public void setSourceAccountName(String sourceAccountName) {
		this.sourceAccountName = sourceAccountName;
	}

	public String getSourceAccountStatus() {
		return sourceAccountStatus;
	}

	public void setSourceAccountStatus(String sourceAccountStatus) {
		this.sourceAccountStatus = sourceAccountStatus;
	}
	
	
	
	
	
	
	

}
