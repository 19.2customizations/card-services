package com.ecobank.digx.cz.extxface.mule.dto;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"SERVICE",
"EMCERT_ID",
"APP_VERSION",
"DEVICE_TYPE",
"MSISDN",
"LANG_ID",
"AFFILIATE_COUNTRY",
"UUID"
})
public class FetchVirtualCardRequestDTO {

@JsonProperty("SERVICE")
private String SERVICE;
@JsonProperty("EMCERT_ID")
private String EMCERT_ID;
@JsonProperty("APP_VERSION")
private String APP_VERSION;
@JsonProperty("DEVICE_TYPE")
private String DEVICE_TYPE;
@JsonProperty("MSISDN")
private String MSISDN;
@JsonProperty("LANG_ID")
private String LANG_ID;
@JsonProperty("AFFILIATE_COUNTRY")
private String AFFILIATE_COUNTRY;
@JsonProperty("UUID")
private String UUID;

public String getSERVICE() {
	return SERVICE;
}
public void setSERVICE(String sERVICE) {
	SERVICE = sERVICE;
}
public String getEMCERT_ID() {
	return EMCERT_ID;
}
public void setEMCERT_ID(String eMCERT_ID) {
	EMCERT_ID = eMCERT_ID;
}
public String getAPP_VERSION() {
	return APP_VERSION;
}
public void setAPP_VERSION(String aPP_VERSION) {
	APP_VERSION = aPP_VERSION;
}
public String getDEVICE_TYPE() {
	return DEVICE_TYPE;
}
public void setDEVICE_TYPE(String dEVICE_TYPE) {
	DEVICE_TYPE = dEVICE_TYPE;
}
public String getMSISDN() {
	return MSISDN;
}
public void setMSISDN(String mSISDN) {
	MSISDN = mSISDN;
}
public String getLANG_ID() {
	return LANG_ID;
}
public void setLANG_ID(String lANG_ID) {
	LANG_ID = lANG_ID;
}
public String getAFFILIATE_COUNTRY() {
	return AFFILIATE_COUNTRY;
}
public void setAFFILIATE_COUNTRY(String aFFILIATE_COUNTRY) {
	AFFILIATE_COUNTRY = aFFILIATE_COUNTRY;
}
public String getUUID() {
	return UUID;
}
public void setUUID(String uUID) {
	UUID = uUID;
}






}