package com.ecobank.digx.cz.extxface.mule.dto;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import com.ecobank.digx.cz.app.cardservices.dto.CardServiceResponseDTO;
import com.ecobank.digx.cz.extxface.mule.adapter.client.JEncrypt;
import com.google.gson.Gson;
import com.ofss.digx.framework.rest.client.RESTClientRequestConfig;
import com.ofss.digx.infra.exceptions.Exception;
import com.thoughtworks.xstream.XStream;

public class ObdxSSL {

	public static void main(String[] args) throws Exception, IOException //throws KeyManagementException, NoSuchAlgorithmException, IOException, Exception 
	{
		ObdxSSL obdxSSL = new ObdxSSL();
		//obdxSSL.generateCard("D29", "C50");
		//obdxSSL.fetchdebitcardlimit();
		for (VirtualTransactionDetails transactionDetails : obdxSSL.miniStatement()) {
			System.out.println("******************************************");
			System.out.println("balance    : " + transactionDetails.balance);
			System.out.println("balance    : " + transactionDetails.getBalance());
			System.out.println("baseAmount : " + transactionDetails.baseAmount);
			System.out.println("baseAmount : " + transactionDetails.fee);
			System.out.println("baseAmount : " + transactionDetails.narration);
			System.out.println("baseAmount : " + transactionDetails.referenceInfo);
			System.out.println("baseAmount : " + transactionDetails.totalAmount);
			System.out.println("baseAmount : " + transactionDetails.transDate);
		}
		//System.out.println("" + obdxSSL.miniStatement());

	}
	
	
	public List<VirtualTransactionDetails> miniStatement() throws Exception, IOException {
		System.out.println("::::: ok ::::: ");
		//CardActivityDTO response = new CardActivityDTO();
		CardServiceResponseDTO responsex = new CardServiceResponseDTO();
		responsex.setResponseCode("E04");
		responsex.setResponseMessage("Failed at interface level");
		//AdapterInteraction.begin();

//		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
//		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESB_VIRTUAL_CARD_MINISTATEMENT");
//		RESTClientFactory clientFactory = RESTClientFactory.getInstance();
//		IRESTClient restClient = clientFactory.getRESTClientInstance();

		
		
		
//		String requestId = GetRefNumber("BC", 12);
//		String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
//		String pwd = digx_consulting.get("ESB_OBDX_PWD", "");
//		String requestToken = JEncrypt.GenerateTransactionToken(sourceCode, requestId, pwd);
//		String ipx = digx_consulting.get("ESB_OBDX_IP", "");
		
		
		String requestId = "220303033000";
		String sourceCode = "OBDX";
		String pwd = "22222222";
		String requestToken = JEncrypt.GenerateTransactionToken(sourceCode, requestId, pwd);
		String ipx = "127.0.0.1";
		
		//String genRef = GetRefNumber("BC", 12);
		String genRef = "120203034030330";
		ESBCardRequestDTO esbRequestDTO = new ESBCardRequestDTO();
		HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
		hostHeaderInfo.setAffiliateCode("EGH"); //temporarily hardcoded
		hostHeaderInfo.setIpAddress(ipx);
		hostHeaderInfo.setRequestId(genRef);
		hostHeaderInfo.setRequestToken(requestToken);
		hostHeaderInfo.setRequestType("MINISTATEMENT");
		hostHeaderInfo.setSourceChannelId("MOBILE");
		hostHeaderInfo.setSourceCode(sourceCode);
		esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);
		esbRequestDTO.setCustomerNo("11802466");
		esbRequestDTO.setStartDate("23-JAN-2021");
		esbRequestDTO.setEndDate("28-FEB-2021");
		esbRequestDTO.setNumberOfTrans("10");

		VirtualCardStatementResponseDTO cardStatementResponseDTO = null;
		StringBuffer response = new StringBuffer();
		Gson gson = new Gson();
		
		try {
			XStream xs = new XStream();
			System.out.println("CARDSERVICES-Request : " + xs.toXML(esbRequestDTO));

			URL obj = new URL("http://192.168.31.89:8083/enterprise-integration/s-cards/virtual-cards/ministatement");
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setRequestMethod("POST");
			con.setRequestProperty("Content-Type", "application/json");

			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(gson.toJson(esbRequestDTO));
			wr.flush();
			wr.close();
			String responseStatus = con.getResponseMessage();
			System.out.println(responseStatus);
			int responseCode = con.getResponseCode();
			System.out.println("responseCode " + responseCode);

			String inputLine;

			if (responseCode == 200) {
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();
				System.out.println("response:" + response.toString());
			} else {
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getErrorStream()));
				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();
				System.out.println("response 2" + response.toString());
			}

			xs.processAnnotations(FetchCardDebitLimitResponseDTO.class);
			
			XStream.setupDefaultSecurity(xs);
			xs.allowTypeHierarchy(FetchCardDebitLimitResponseDTO.class);
			
			
			cardStatementResponseDTO = gson.fromJson(response.toString(), VirtualCardStatementResponseDTO.class);
			
			if(cardStatementResponseDTO.getTransactionDetails().size() > 0) {
				System.out.println(">>> responseMsg   ::: " + cardStatementResponseDTO.getHostHeaderInfo().getResponseCode());
				System.out.println(">>> responsecode  ::: " + cardStatementResponseDTO.getHostHeaderInfo().getResponseMessage());
				System.out.println(">>> size          ::: " + cardStatementResponseDTO.getTransactionDetails().get(0).getBalance());
				//System.out.println(">>> responsecode   ::: " + fetchCardDebitLimitResponseDTO.getHostHeaderInfo().get(0).getResponseCode());
			}

		} finally {
		}
		return cardStatementResponseDTO.getTransactionDetails();
	}
	
	
	public FetchCardDebitLimitResponseDTO fetchdebitcardlimit()
			throws NoSuchAlgorithmException, KeyManagementException, IOException {
		StringBuffer response = new StringBuffer();
		CardServiceResponseDTO responsex = new CardServiceResponseDTO();
		responsex.setResponseCode("E04");
		responsex.setResponseMessage("Failed at interface level");
		
		RESTClientRequestConfig prop = new RESTClientRequestConfig();

		prop.setServiceURL("https://10.4.139.50:7503/unifiedapiesc/services/createcardrequest");
		prop.setConsumes("application/json");
		prop.setProduces("application/json");
		prop.setContextURL("https://10.4.139.50:7503/unifiedapiesc/services/createcardrequest");

		
		enableSSL();
		
		String sourceCode = "SMARTTELLER";
		String ipx = "127.0.0.1";

		String requestId = "112202020303030303";

		// String pwd = digx_consulting.get("ESB_OBDX_PWD", "");
		String pwd = "";
		String requestToken = JEncrypt.GenerateTransactionToken(sourceCode, requestId, pwd);

		FetchCardDebitLimitRequestDTO requestDTO = new FetchCardDebitLimitRequestDTO();
		HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
		hostHeaderInfo.setAffiliateCode("ENG");
		hostHeaderInfo.setIpAddress(ipx);
		hostHeaderInfo.setRequestId(requestId);
		hostHeaderInfo.setRequestToken(requestToken);
		hostHeaderInfo.setRequestType("MPAY");
		hostHeaderInfo.setSourceChannelId("WEB");
		hostHeaderInfo.setSourceCode(sourceCode);
		requestDTO.setHostHeaderInfo(hostHeaderInfo);
		requestDTO.setPan("15061181661290020046");

		FetchCardDebitLimitResponseDTO fetchCardDebitLimitResponseDTO = null;

		try {
			XStream xs = new XStream();
			System.out.println("CARDSERVICES-Request : " + xs.toXML(requestDTO));

			URL obj = new URL("https://10.4.139.50:7503/unifiedapisgaf/services/fetchdebitcardlimit");
			HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
			con.setRequestMethod("POST");
			con.setRequestProperty("Content-Type", "application/xml");

			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(xs.toXML(requestDTO));
			wr.flush();
			wr.close();
			String responseStatus = con.getResponseMessage();
			System.out.println(responseStatus);
			int responseCode = con.getResponseCode();
			System.out.println("responseCode " + responseCode);

			String inputLine;

			if (responseCode == 200) {
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();
				System.out.println("response:" + response.toString());
			} else {
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getErrorStream()));
				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();
				System.out.println("response 2" + response.toString());
			}

			xs.processAnnotations(FetchCardDebitLimitResponseDTO.class);
			
			XStream.setupDefaultSecurity(xs);
			xs.allowTypeHierarchy(FetchCardDebitLimitResponseDTO.class);
			
			fetchCardDebitLimitResponseDTO = (FetchCardDebitLimitResponseDTO) xs.fromXML(response.toString());
			
			if(fetchCardDebitLimitResponseDTO.getHostHeaderInfo().size() > 0) {
				System.out.println(">>> purchaseLimit  ::: " + fetchCardDebitLimitResponseDTO.getPurchaseLimit());
				System.out.println(">>> transferLimit  ::: " + fetchCardDebitLimitResponseDTO.getPaymentTransferLimit());
				System.out.println(">>> responseMsg    ::: " + fetchCardDebitLimitResponseDTO.getHostHeaderInfo().get(0).getResponseMessage());
				System.out.println(">>> responsecode   ::: " + fetchCardDebitLimitResponseDTO.getHostHeaderInfo().get(0).getResponseCode());
			}

		} finally {
		}
		return fetchCardDebitLimitResponseDTO;
	}
	

	public ESBCardResponseDTO generateCard(String branchCode, String deliveryBranch)
			throws NoSuchAlgorithmException, KeyManagementException, IOException {
		StringBuffer response = new StringBuffer();
		CardServiceResponseDTO responsex = new CardServiceResponseDTO();
		responsex.setResponseCode("E04");
		responsex.setResponseMessage("Failed at interface level");
		// AdapterInteraction.begin();

		// RESTClientRequestBuilder clientBuilder =
		// RESTClientRequestBuilder.getInstance();
		// RESTClientRequestConfig prop =
		// clientBuilder.loadConfig("ESB_DEBIT_CARD_CREATE");
		RESTClientRequestConfig prop = new RESTClientRequestConfig();

		prop.setServiceURL("https://10.4.139.50:7503/unifiedapiesc/services/createcardrequest");
		prop.setConsumes("application/json");
		prop.setProduces("application/json");
		prop.setContextURL("https://10.4.139.50:7503/unifiedapiesc/services/createcardrequest");

		
		enableSSL();
		
		String sourceCode = "SMARTTELLER";
		String ipx = "127.0.0.1";

		String requestId = "112202020303030303";

		// String pwd = digx_consulting.get("ESB_OBDX_PWD", "");
		String pwd = "";
		String requestToken = JEncrypt.GenerateTransactionToken(sourceCode, requestId, pwd);

		SimpleDateFormat dff4 = new SimpleDateFormat("yyyy-MM-dd");
		java.util.Date dt = new java.util.Date();
		String dateCurrent = dff4.format(dt);

		ESBCardRequestDTO esbRequestDTO = new ESBCardRequestDTO();
		HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
		hostHeaderInfo.setAffiliateCode("ENG");
		hostHeaderInfo.setIpAddress(ipx);
		hostHeaderInfo.setRequestId(requestId);
		hostHeaderInfo.setRequestToken(requestToken);
		hostHeaderInfo.setRequestType("MPAY");
		hostHeaderInfo.setSourceChannelId("WEB");
		hostHeaderInfo.setSourceCode(sourceCode);
		esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);

		esbRequestDTO.setAccountNo("0048740936");
		esbRequestDTO.setCardScheme("VERVE");
		esbRequestDTO.setCardProduct("1");
		esbRequestDTO.setRequestBranch(branchCode);
		esbRequestDTO.setCollectionBranch(deliveryBranch);
		esbRequestDTO.setPersonalized("Y");
		esbRequestDTO.setCreatedBy("OBDX");
		esbRequestDTO.setCreatedDate(dateCurrent);
		esbRequestDTO.setVerifiedBy("OBDX");
		esbRequestDTO.setVerifiedDate(dateCurrent);
		esbRequestDTO.setAbridgedCardNo("");

		ESBCardResponseDTO esbCardResponseDTO = null;

		try {
			XStream xs = new XStream();
			System.out.println("CARDSERVICES-Request : " + xs.toXML(esbRequestDTO));

			Gson gson = new Gson();

			URL obj = new URL("https://10.4.139.50:7503/unifiedapiesc/services/createcardrequest");
			HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
			con.setRequestMethod("POST");
			con.setRequestProperty("Content-Type", "application/json");

			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(gson.toJson(esbRequestDTO));
			wr.flush();
			wr.close();
			String responseStatus = con.getResponseMessage();
			System.out.println(responseStatus);
			int responseCode = con.getResponseCode();
			System.out.println("responseCode " + responseCode);

			String inputLine;

			if (responseCode == 200) {
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();
				System.out.println("response:" + response.toString());
			} else {
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getErrorStream()));
				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();
				System.out.println("response 2" + response.toString());
			}

			//Gson g = new Gson();
			esbCardResponseDTO = gson.fromJson(response.toString(), ESBCardResponseDTO.class);
			System.out.println(">>> responseMsg  ::: " + esbCardResponseDTO.getHostHeaderInfo().getResponseMessage());
			System.out.println(">>> responsecode ::: " + esbCardResponseDTO.getHostHeaderInfo().getResponseCode());

		} finally {
		}
		// AdapterInteraction.close();
		return esbCardResponseDTO;
	}

	public static void enableSSL() throws NoSuchAlgorithmException, KeyManagementException {

		// Create a trust manager that does not validate certificate chains
		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			public void checkClientTrusted(X509Certificate[] certs, String authType) {
			}

			public void checkServerTrusted(X509Certificate[] certs, String authType) {
			}
		} };

		// Install the all-trusting trust manager
		SSLContext sc = SSLContext.getInstance("SSL");
		sc.init(null, trustAllCerts, new java.security.SecureRandom());
		HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

		// Create all-trusting host name verifier
		HostnameVerifier allHostsValid = new HostnameVerifier() {
			public boolean verify(String hostname, SSLSession session) {
				return true;
			}
		};

		// Install the all-trusting host verifier
		HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
	}

}
