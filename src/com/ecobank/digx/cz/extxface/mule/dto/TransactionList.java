package com.ecobank.digx.cz.extxface.mule.dto;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"transactionId",
"transactionDate",
"transactionTime",
"amount",
"fee",
"totalAmount",
"transactionDesc",
"referenceInfo"
})
public class TransactionList {

@JsonProperty("transactionId")
private String transactionId;
@JsonProperty("transactionDate")
private String transactionDate;
@JsonProperty("transactionTime")
private String transactionTime;
@JsonProperty("amount")
private String amount;
@JsonProperty("fee")
private String fee;
@JsonProperty("totalAmount")
private String totalAmount;
@JsonProperty("transactionDesc")
private String transactionDesc;
@JsonProperty("referenceInfo")
private String referenceInfo;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

@JsonProperty("transactionId")
public String getTransactionId() {
return transactionId;
}

@JsonProperty("transactionId")
public void setTransactionId(String transactionId) {
this.transactionId = transactionId;
}

@JsonProperty("transactionDate")
public String getTransactionDate() {
return transactionDate;
}

@JsonProperty("transactionDate")
public void setTransactionDate(String transactionDate) {
this.transactionDate = transactionDate;
}

@JsonProperty("transactionTime")
public String getTransactionTime() {
return transactionTime;
}

@JsonProperty("transactionTime")
public void setTransactionTime(String transactionTime) {
this.transactionTime = transactionTime;
}

@JsonProperty("amount")
public String getAmount() {
return amount;
}

@JsonProperty("amount")
public void setAmount(String amount) {
this.amount = amount;
}

@JsonProperty("fee")
public String getFee() {
return fee;
}

@JsonProperty("fee")
public void setFee(String fee) {
this.fee = fee;
}

@JsonProperty("totalAmount")
public String getTotalAmount() {
return totalAmount;
}

@JsonProperty("totalAmount")
public void setTotalAmount(String totalAmount) {
this.totalAmount = totalAmount;
}

@JsonProperty("transactionDesc")
public String getTransactionDesc() {
return transactionDesc;
}

@JsonProperty("transactionDesc")
public void setTransactionDesc(String transactionDesc) {
this.transactionDesc = transactionDesc;
}

@JsonProperty("referenceInfo")
public String getReferenceInfo() {
return referenceInfo;
}

@JsonProperty("referenceInfo")
public void setReferenceInfo(String referenceInfo) {
this.referenceInfo = referenceInfo;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}