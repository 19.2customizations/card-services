package com.ecobank.digx.cz.extxface.mule.dto;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "hostHeaderInfo", "charges" })
public class CardChargeResponseDTO {

	@JsonProperty("hostHeaderInfo")
	private HostHeaderInfo hostHeaderInfo;
	@JsonProperty("charges")
	private List<CardChargeInfo> charges = null;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("hostHeaderInfo")
	public HostHeaderInfo getHostHeaderInfo() {
		return hostHeaderInfo;
	}

	@JsonProperty("hostHeaderInfo")
	public void setHostHeaderInfo(HostHeaderInfo hostHeaderInfo) {
		this.hostHeaderInfo = hostHeaderInfo;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

	public List<CardChargeInfo> getCharges() {
		return charges;
	}

	public void setCharges(List<CardChargeInfo> charges) {
		this.charges = charges;
	}

}