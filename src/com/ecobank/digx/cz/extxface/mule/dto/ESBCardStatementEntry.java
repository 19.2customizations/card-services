package com.ecobank.digx.cz.extxface.mule.dto;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({"cardAccount", "accountNo","card",  "currency", "amount", "tranDate", "recDate", 
	"terminal", "merchant", "country", "city", "internalNo","desc","additionalInfo"})
public class ESBCardStatementEntry {
	
	
	@JsonProperty("cardAccount")
	private String cardAccount;
	@JsonProperty("accountNo")
	private String accountNo;
	@JsonProperty("card")
	private String card;
	
	@JsonProperty("currency")
	private String currency;
	
	@JsonProperty("amount")
	private double amount;
	@JsonProperty("tranDate")
	private String tranDate;
	@JsonProperty("internalNo")
	private String internalNo;
	@JsonProperty("additionalInfo")
	private String additionalInfo;
	
	@JsonProperty("desc")
	private String desc;
	
	@JsonProperty("terminal")
	private String terminal;
	
	@JsonProperty("merchant")
	private String merchant;
	
	@JsonProperty("country")
	private String country;
	
	@JsonProperty("city")
	private String city;

	public String getCardAccount() {
		return cardAccount;
	}

	public void setCardAccount(String cardAccount) {
		this.cardAccount = cardAccount;
	}

	public String getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public String getCard() {
		return card;
	}

	public void setCard(String card) {
		this.card = card;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getTranDate() {
		return tranDate;
	}

	public void setTranDate(String tranDate) {
		this.tranDate = tranDate;
	}

	public String getInternalNo() {
		return internalNo;
	}

	public void setInternalNo(String internalNo) {
		this.internalNo = internalNo;
	}

	public String getAdditionalInfo() {
		return additionalInfo;
	}

	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getTerminal() {
		return terminal;
	}

	public void setTerminal(String terminal) {
		this.terminal = terminal;
	}

	public String getMerchant() {
		return merchant;
	}

	public void setMerchant(String merchant) {
		this.merchant = merchant;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
	
	
	
	

}
