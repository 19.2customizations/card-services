package com.ecobank.digx.cz.extxface.mule.dto;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("FetchCardDebitLimitResponse")
public class FetchCardDebitLimitResponseDTO {
	
	public int cardNotPresentLimit;
	public int cashWithdrawalLimit;
	
	@XStreamImplicit
	private List<HostHeaderInfo> hostHeaderInfo;
	
	public int paymentTransferLimit;
	public int purchaseLimit;
	
	public int getCardNotPresentLimit() {
		return cardNotPresentLimit;
	}
	public void setCardNotPresentLimit(int cardNotPresentLimit) {
		this.cardNotPresentLimit = cardNotPresentLimit;
	}
	public int getCashWithdrawalLimit() {
		return cashWithdrawalLimit;
	}
	public void setCashWithdrawalLimit(int cashWithdrawalLimit) {
		this.cashWithdrawalLimit = cashWithdrawalLimit;
	}
//	public HostHeaderInfoDTO getHostHeaderInfo() {
//		return hostHeaderInfo;
//	}
//	public void setHostHeaderInfo(HostHeaderInfoDTO hostHeaderInfo) {
//		this.hostHeaderInfo = hostHeaderInfo;
//	}
	public int getPaymentTransferLimit() {
		return paymentTransferLimit;
	}
	public void setPaymentTransferLimit(int paymentTransferLimit) {
		this.paymentTransferLimit = paymentTransferLimit;
	}
	public int getPurchaseLimit() {
		return purchaseLimit;
	}
	public void setPurchaseLimit(int purchaseLimit) {
		this.purchaseLimit = purchaseLimit;
	}
	public List<HostHeaderInfo> getHostHeaderInfo() {
		return hostHeaderInfo;
	}
	public void setHostHeaderInfo(List<HostHeaderInfo> hostHeaderInfo) {
		this.hostHeaderInfo = hostHeaderInfo;
	}
	
	
	
	

}


