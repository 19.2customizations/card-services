package com.ecobank.digx.cz.extxface.mule.dto;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "hostHeaderInfo", "mobileNo", "travelDestination", "cardAccounts", "returnDate", "travelDate",
		"purposeOfTravel", "accountAlias" })
public class TravelNotificationRequestDTO {

	@JsonProperty("hostHeaderInfo")
	private HostHeaderInfo hostHeaderInfo;
	@JsonProperty("mobileNo")
	private String mobileNo;
	@JsonProperty("travelDestination")
	private String travelDestination;
	@JsonProperty("cardAccounts")
	private String cardAccounts;
	@JsonProperty("returnDate")
	private String returnDate;
	@JsonProperty("travelDate")
	private String travelDate;
	@JsonProperty("purposeOfTravel")
	private String purposeOfTravel;
	@JsonProperty("accountAlias")
	private String accountAlias;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("hostHeaderInfo")
	public HostHeaderInfo getHostHeaderInfo() {
		return hostHeaderInfo;
	}

	@JsonProperty("hostHeaderInfo")
	public void setHostHeaderInfo(HostHeaderInfo hostHeaderInfo) {
		this.hostHeaderInfo = hostHeaderInfo;
	}

	@JsonProperty("mobileNo")
	public String getMobileNo() {
		return mobileNo;
	}

	@JsonProperty("mobileNo")
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	@JsonProperty("travelDestination")
	public String getTravelDestination() {
		return travelDestination;
	}

	@JsonProperty("travelDestination")
	public void setTravelDestination(String travelDestination) {
		this.travelDestination = travelDestination;
	}

	@JsonProperty("cardAccounts")
	public String getCardAccounts() {
		return cardAccounts;
	}

	@JsonProperty("cardAccounts")
	public void setCardAccounts(String cardAccounts) {
		this.cardAccounts = cardAccounts;
	}

	@JsonProperty("returnDate")
	public String getReturnDate() {
		return returnDate;
	}

	@JsonProperty("returnDate")
	public void setReturnDate(String returnDate) {
		this.returnDate = returnDate;
	}

	@JsonProperty("travelDate")
	public String getTravelDate() {
		return travelDate;
	}

	@JsonProperty("travelDate")
	public void setTravelDate(String travelDate) {
		this.travelDate = travelDate;
	}

	@JsonProperty("purposeOfTravel")
	public String getPurposeOfTravel() {
		return purposeOfTravel;
	}

	@JsonProperty("purposeOfTravel")
	public void setPurposeOfTravel(String purposeOfTravel) {
		this.purposeOfTravel = purposeOfTravel;
	}

	@JsonProperty("accountAlias")
	public String getAccountAlias() {
		return accountAlias;
	}

	@JsonProperty("accountAlias")
	public void setAccountAlias(String accountAlias) {
		this.accountAlias = accountAlias;
	}

}