package com.ecobank.digx.cz.extxface.mule.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "hostHeaderInfo", "transactionType", "amount", "sourceAccount",
	"sourceAccountType", "sourceAccountCcy", "udf1", "udf2", "udf3" })
public class FetchVirtualCardFeeRequestDTO {
	
	
	@JsonProperty("hostHeaderInfo")
	private HostHeaderInfo hostHeaderInfo;
	@JsonProperty("transactionType")
	private String transactionType;
	
	@JsonProperty("amount")
	private String amount;
	
	@JsonProperty("sourceAccount")
	private String sourceAccount;
	
	@JsonProperty("sourceAccountType")
	private String sourceAccountType;
	
	@JsonProperty("sourceAccountCcy")
	private String sourceAccountCcy;
	
	@JsonProperty("udf1")
	private String udf1;
	@JsonProperty("udf2")
	private String udf2;
	@JsonProperty("udf3")
	private String udf3;
	
	
	public HostHeaderInfo getHostHeaderInfo() {
		return hostHeaderInfo;
	}
	public void setHostHeaderInfo(HostHeaderInfo hostHeaderInfo) {
		this.hostHeaderInfo = hostHeaderInfo;
	}
	public String getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getSourceAccount() {
		return sourceAccount;
	}
	public void setSourceAccount(String sourceAccount) {
		this.sourceAccount = sourceAccount;
	}
	public String getSourceAccountType() {
		return sourceAccountType;
	}
	public void setSourceAccountType(String sourceAccountType) {
		this.sourceAccountType = sourceAccountType;
	}
	public String getSourceAccountCcy() {
		return sourceAccountCcy;
	}
	public void setSourceAccountCcy(String sourceAccountCcy) {
		this.sourceAccountCcy = sourceAccountCcy;
	}
	public String getUdf1() {
		return udf1;
	}
	public void setUdf1(String udf1) {
		this.udf1 = udf1;
	}
	public String getUdf2() {
		return udf2;
	}
	public void setUdf2(String udf2) {
		this.udf2 = udf2;
	}
	public String getUdf3() {
		return udf3;
	}
	public void setUdf3(String udf3) {
		this.udf3 = udf3;
	}
	
	
	

}
