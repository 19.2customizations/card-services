package com.ecobank.digx.cz.extxface.mule.dto;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "expiryDate", "hotlistStatus", "nameOnCard", "cardNumber", "status" })
public class CardAccountInfo {
	

	@JsonProperty("expiryDate")
	private String expiryDate;
	
	@JsonProperty("hotlistStatus")
	private String hotlistStatus;
	@JsonProperty("nameOnCard")
	private String nameOnCard;
	@JsonProperty("cardNumber")
	private String cardNumber;
	@JsonProperty("status")
	private String status;
	public String getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}
	public String getHotlistStatus() {
		return hotlistStatus;
	}
	public void setHotlistStatus(String hotlistStatus) {
		this.hotlistStatus = hotlistStatus;
	}
	public String getNameOnCard() {
		return nameOnCard;
	}
	public void setNameOnCard(String nameOnCard) {
		this.nameOnCard = nameOnCard;
	}
	public String getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	

}
