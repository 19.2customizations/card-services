package com.ecobank.digx.cz.extxface.mule.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"ADDRESS",
"AFFILIATE_COUNTRY",
"AMOUNT",
"APP_VERSION",
"CHARGE_AMOUNT",
"CITY",
"CURRENCY_CODE",
"CUSTOMER_ID",
"DEVICE_TYPE",
"DOB",
"EMAIL",
"EMCERT_ID",
"FIRST_NAME",
"LANG_ID",
"LAST_NAME",
"MSISDN",
"OTP_NUMBER",
"SCHEME_TYPE",
"SERVICE",
"SOURCE_ACCOUNT",
"TOTAL_AMOUNT",
"UUID",
"VCARD_TYPE"
})
public class GenerateVirtualCardRequestDTO {

@JsonProperty("ADDRESS")
private String address;
@JsonProperty("AFFILIATE_COUNTRY")
private String affiliateCountry;
@JsonProperty("AMOUNT")
private String amount;
@JsonProperty("APP_VERSION")
private String appVersion;
@JsonProperty("CHARGE_AMOUNT")
private String chargeAmount;
@JsonProperty("CITY")
private String city;
@JsonProperty("CURRENCY_CODE")
private String currencyCode;
@JsonProperty("CUSTOMER_ID")
private String customerId;
@JsonProperty("DEVICE_TYPE")
private String deviceType;
@JsonProperty("DOB")
private String dob;
@JsonProperty("EMAIL")
private String email;
@JsonProperty("EMCERT_ID")
private String emcertId;
@JsonProperty("FIRST_NAME")
private String firstName;
@JsonProperty("LANG_ID")
private String langId;
@JsonProperty("LAST_NAME")
private String lastName;
@JsonProperty("MSISDN")
private String msisdn;
@JsonProperty("OTP_NUMBER")
private String optNumber;
@JsonProperty("SCHEME_TYPE")
private String schemType;
@JsonProperty("SERVICE")
private String service;
@JsonProperty("SOURCE_ACCOUNT")
private String sourceAccount;
@JsonProperty("TOTAL_AMOUNT")
private String totalAmount;
@JsonProperty("UUID")
private String uuid;
@JsonProperty("VCARD_TYPE")
private String vcardType;


@JsonProperty("ADDRESS")
public String getAddress() {
	return address;
}

@JsonProperty("ADDRESS")
public void setAddress(String address) {
	this.address = address;
}


@JsonProperty("AFFILIATE_COUNTRY")
public String getAffiliateCountry() {
	return affiliateCountry;
}

@JsonProperty("AFFILIATE_COUNTRY")
public void setAffiliateCountry(String affiliateCountry) {
	this.affiliateCountry = affiliateCountry;
}

@JsonProperty("AMOUNT")
public String getAmount() {
	return amount;
}

@JsonProperty("AMOUNT")
public void setAmount(String amount) {
	this.amount = amount;
}

@JsonProperty("APP_VERSION")
public String getAppVersion() {
	return appVersion;
}

@JsonProperty("APP_VERSION")
public void setAppVersion(String appVersion) {
	this.appVersion = appVersion;
}

@JsonProperty("CHARGE_AMOUNT")
public String getChargeAmount() {
	return chargeAmount;
}

@JsonProperty("CHARGE_AMOUNT")
public void setChargeAmount(String chargeAmount) {
	this.chargeAmount = chargeAmount;
}

@JsonProperty("CITY")
public String getCity() {
	return city;
}

@JsonProperty("CITY")
public void setCity(String city) {
	this.city = city;
}

@JsonProperty("CURRENCY_CODE")
public String getCurrencyCode() {
	return currencyCode;
}

@JsonProperty("CURRENCY_CODE")
public void setCurrencyCode(String currencyCode) {
	this.currencyCode = currencyCode;
}

@JsonProperty("CUSTOMER_ID")
public String getCustomerId() {
	return customerId;
}

@JsonProperty("CUSTOMER_ID")
public void setCustomerId(String customerId) {
	this.customerId = customerId;
}

@JsonProperty("DEVICE_TYPE")
public String getDeviceType() {
	return deviceType;
}

@JsonProperty("DEVICE_TYPE")
public void setDeviceType(String deviceType) {
	this.deviceType = deviceType;
}

@JsonProperty("DOB")
public String getDob() {
	return dob;
}

@JsonProperty("DOB")
public void setDob(String dob) {
	this.dob = dob;
}

@JsonProperty("EMAIL")
public String getEmail() {
	return email;
}

@JsonProperty("EMAIL")
public void setEmail(String email) {
	this.email = email;
}

@JsonProperty("EMCERT_ID")
public String getEmcertId() {
	return emcertId;
}

@JsonProperty("EMCERT_ID")
public void setEmcertId(String emcertId) {
	this.emcertId = emcertId;
}

@JsonProperty("FIRST_NAME")
public String getFirstName() {
	return firstName;
}

@JsonProperty("FIRST_NAME")
public void setFirstName(String firstName) {
	this.firstName = firstName;
}

@JsonProperty("LANG_ID")
public String getLangId() {
	return langId;
}

@JsonProperty("LANG_ID")
public void setLangId(String langId) {
	this.langId = langId;
}

@JsonProperty("LAST_NAME")
public String getLastName() {
	return lastName;
}

@JsonProperty("LAST_NAME")
public void setLastName(String lastName) {
	this.lastName = lastName;
}

@JsonProperty("MSISDN")
public String getMsisdn() {
	return msisdn;
}

@JsonProperty("MSISDN")
public void setMsisdn(String msisdn) {
	this.msisdn = msisdn;
}

@JsonProperty("OTP_NUMBER")
public String getOptNumber() {
	return optNumber;
}

@JsonProperty("OTP_NUMBER")
public void setOptNumber(String optNumber) {
	this.optNumber = optNumber;
}

@JsonProperty("SCHEME_TYPE")
public String getSchemType() {
	return schemType;
}

@JsonProperty("SCHEME_TYPE")
public void setSchemType(String schemType) {
	this.schemType = schemType;
}

@JsonProperty("SERVICE")
public String getService() {
	return service;
}

@JsonProperty("SERVICE")
public void setService(String service) {
	this.service = service;
}

@JsonProperty("SOURCE_ACCOUNT")
public String getSourceAccount() {
	return sourceAccount;
}

@JsonProperty("SOURCE_ACCOUNT")
public void setSourceAccount(String sourceAccount) {
	this.sourceAccount = sourceAccount;
}

@JsonProperty("TOTAL_AMOUNT")
public String getTotalAmount() {
	return totalAmount;
}

@JsonProperty("TOTAL_AMOUNT")
public void setTotalAmount(String totalAmount) {
	this.totalAmount = totalAmount;
}

@JsonProperty("UUID")
public String getUuid() {
	return uuid;
}

@JsonProperty("UUID")
public void setUuid(String uuid) {
	this.uuid = uuid;
}

@JsonProperty("VCARD_TYPE")
public String getVcardType() {
	return vcardType;
}

@JsonProperty("VCARD_TYPE")
public void setVcardType(String vcardType) {
	this.vcardType = vcardType;
}
}