package com.ecobank.digx.cz.extxface.mule.dto;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"SERVICE",
"EMCERT_ID",
"APP_VERSION",
"DEVICE_TYPE",
"MSISDN",
"LANG_ID",
"AFFILIATE_COUNTRY",
"UUID"
})
public class FetchVirtualCardRequestDTO {

@JsonProperty("SERVICE")
private String sERVICE;
@JsonProperty("EMCERT_ID")
private String eMCERTID;
@JsonProperty("APP_VERSION")
private String aPPVERSION;
@JsonProperty("DEVICE_TYPE")
private String dEVICETYPE;
@JsonProperty("MSISDN")
private String mSISDN;
@JsonProperty("LANG_ID")
private String lANGID;
@JsonProperty("AFFILIATE_COUNTRY")
private String aFFILIATECOUNTRY;
@JsonProperty("UUID")
private String uUID;

@JsonProperty("SERVICE")
public String getSERVICE() {
return sERVICE;
}

@JsonProperty("SERVICE")
public void setSERVICE(String sERVICE) {
this.sERVICE = sERVICE;
}

@JsonProperty("EMCERT_ID")
public String getEMCERTID() {
return eMCERTID;
}

@JsonProperty("EMCERT_ID")
public void setEMCERTID(String eMCERTID) {
this.eMCERTID = eMCERTID;
}

@JsonProperty("APP_VERSION")
public String getAPPVERSION() {
return aPPVERSION;
}

@JsonProperty("APP_VERSION")
public void setAPPVERSION(String aPPVERSION) {
this.aPPVERSION = aPPVERSION;
}

@JsonProperty("DEVICE_TYPE")
public String getDEVICETYPE() {
return dEVICETYPE;
}

@JsonProperty("DEVICE_TYPE")
public void setDEVICETYPE(String dEVICETYPE) {
this.dEVICETYPE = dEVICETYPE;
}

@JsonProperty("MSISDN")
public String getMSISDN() {
return mSISDN;
}

@JsonProperty("MSISDN")
public void setMSISDN(String mSISDN) {
this.mSISDN = mSISDN;
}

@JsonProperty("LANG_ID")
public String getLANGID() {
return lANGID;
}

@JsonProperty("LANG_ID")
public void setLANGID(String lANGID) {
this.lANGID = lANGID;
}

@JsonProperty("AFFILIATE_COUNTRY")
public String getAFFILIATECOUNTRY() {
return aFFILIATECOUNTRY;
}

@JsonProperty("AFFILIATE_COUNTRY")
public void setAFFILIATECOUNTRY(String aFFILIATECOUNTRY) {
this.aFFILIATECOUNTRY = aFFILIATECOUNTRY;
}

@JsonProperty("UUID")
public String getUUID() {
return uUID;
}

@JsonProperty("UUID")
public void setUUID(String uUID) {
this.uUID = uUID;
}

}