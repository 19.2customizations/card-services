package com.ecobank.digx.cz.extxface.mule.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "hostHeaderInfo", "cardNo", "expiryDate", "reasonCode" })
public class ESBCardBlockRequestDTO {

	@JsonProperty("hostHeaderInfo")
	private HostHeaderInfo hostHeaderInfo;
	@JsonProperty("cardNo")
	private String cardNo;
	@JsonProperty("expiryDate")
	private String expiryDate;
	
	@JsonProperty("reasonCode")
	private String reasonCode;

	@JsonProperty("hostHeaderInfo")
	public HostHeaderInfo getHostHeaderInfo() {
		return hostHeaderInfo;
	}

	@JsonProperty("hostHeaderInfo")
	public void setHostHeaderInfo(HostHeaderInfo hostHeaderInfo) {
		this.hostHeaderInfo = hostHeaderInfo;
	}

	
	@JsonProperty("reasonCode")
	public String getReasonCode() {
		return reasonCode;
	}

	@JsonProperty("reasonCode")
	public void setReasonCode(String reasonCode) {
		this.reasonCode = reasonCode;
	}

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

}