package com.ecobank.digx.cz.extxface.mule.dto;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"accountNumber",
"cardAlias",
"expiry",
"cvv",
"scheme",
"cardType",
"share",
"activated",
"blocked",
"balance",
"currencyCode",
"otpNumber",
"cardToken",
"CardName",
"custId"
})
public class AccountDetails {

@JsonProperty("accountNumber")
private String accountNumber;
@JsonProperty("cardAlias")
private String cardAlias;
@JsonProperty("expiry")
private String expiry;
@JsonProperty("cvv")
private String cvv;
@JsonProperty("scheme")
private String scheme;
@JsonProperty("cardType")
private String cardType;
@JsonProperty("share")
private Boolean share;
@JsonProperty("activated")
private Boolean activated;
@JsonProperty("blocked")
private Boolean blocked;
@JsonProperty("balance")
private String balance;
@JsonProperty("currencyCode")
private String currencyCode;
@JsonProperty("otpNumber")
private String otpNumber;
@JsonProperty("cardToken")
private String cardToken;
@JsonProperty("CardName")
private String cardName;
@JsonProperty("custId")
private String custId;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

@JsonProperty("accountNumber")
public String getAccountNumber() {
return accountNumber;
}

@JsonProperty("accountNumber")
public void setAccountNumber(String accountNumber) {
this.accountNumber = accountNumber;
}

@JsonProperty("cardAlias")
public String getCardAlias() {
return cardAlias;
}

@JsonProperty("cardAlias")
public void setCardAlias(String cardAlias) {
this.cardAlias = cardAlias;
}

@JsonProperty("expiry")
public String getExpiry() {
return expiry;
}

@JsonProperty("expiry")
public void setExpiry(String expiry) {
this.expiry = expiry;
}

@JsonProperty("cvv")
public String getCvv() {
return cvv;
}

@JsonProperty("cvv")
public void setCvv(String cvv) {
this.cvv = cvv;
}

@JsonProperty("scheme")
public String getScheme() {
return scheme;
}

@JsonProperty("scheme")
public void setScheme(String scheme) {
this.scheme = scheme;
}

@JsonProperty("cardType")
public String getCardType() {
return cardType;
}

@JsonProperty("cardType")
public void setCardType(String cardType) {
this.cardType = cardType;
}

@JsonProperty("share")
public Boolean getShare() {
return share;
}

@JsonProperty("share")
public void setShare(Boolean share) {
this.share = share;
}

@JsonProperty("activated")
public Boolean getActivated() {
return activated;
}

@JsonProperty("activated")
public void setActivated(Boolean activated) {
this.activated = activated;
}

@JsonProperty("blocked")
public Boolean getBlocked() {
return blocked;
}

@JsonProperty("blocked")
public void setBlocked(Boolean blocked) {
this.blocked = blocked;
}

@JsonProperty("balance")
public String getBalance() {
return balance;
}

@JsonProperty("balance")
public void setBalance(String balance) {
this.balance = balance;
}

@JsonProperty("currencyCode")
public String getCurrencyCode() {
return currencyCode;
}

@JsonProperty("currencyCode")
public void setCurrencyCode(String currencyCode) {
this.currencyCode = currencyCode;
}

@JsonProperty("otpNumber")
public String getOtpNumber() {
return otpNumber;
}

@JsonProperty("otpNumber")
public void setOtpNumber(String otpNumber) {
this.otpNumber = otpNumber;
}

@JsonProperty("cardToken")
public String getCardToken() {
return cardToken;
}

@JsonProperty("cardToken")
public void setCardToken(String cardToken) {
this.cardToken = cardToken;
}

@JsonProperty("CardName")
public String getCardName() {
return cardName;
}

@JsonProperty("CardName")
public void setCardName(String cardName) {
this.cardName = cardName;
}

@JsonProperty("custId")
public String getCustId() {
return custId;
}

@JsonProperty("custId")
public void setCustId(String custId) {
this.custId = custId;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}