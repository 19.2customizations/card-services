package com.ecobank.digx.cz.extxface.mule.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"STATUS_CODE",
"STATUS_MESSAGE",
"ACCOUNT_DETAILS"
})
public class FetchVirtualCardResponseDTO {

@JsonProperty("STATUS_CODE")
private String statusCode;
@JsonProperty("STATUS_MESSAGE")
private String statusMessage;
@JsonProperty("ACCOUNT_DETAILS")
private List<AccountDetails> accountDetails = null;

@JsonProperty("STATUS_CODE")
public String getStatusCode() {
	return statusCode;
}

@JsonProperty("STATUS_CODE")
public void setStatusCode(String statusCode) {
	this.statusCode = statusCode;
}

@JsonProperty("STATUS_MESSAGE")
public String getStatusMessage() {
	return statusMessage;
}

@JsonProperty("STATUS_MESSAGE")
public void setStatusMessage(String statusMessage) {
	this.statusMessage = statusMessage;
}

@JsonProperty("ACCOUNT_DETAILS")
public List<AccountDetails> getAccountDetails() {
	return accountDetails;
}

@JsonProperty("ACCOUNT_DETAILS")
public void setAccountDetails(List<AccountDetails> accountDetails) {
	this.accountDetails = accountDetails;
}

}