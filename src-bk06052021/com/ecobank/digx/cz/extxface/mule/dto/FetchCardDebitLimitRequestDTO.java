package com.ecobank.digx.cz.extxface.mule.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "hostHeaderInfo", "pan" })
public class FetchCardDebitLimitRequestDTO {
	@JsonProperty("hostHeaderInfo")
	private HostHeaderInfo hostHeaderInfo;
	@JsonProperty("pan")
	private String pan;
	@JsonProperty("cardNotPresentLimit")
	private String cardNotPresentLimit;
	@JsonProperty("cashWithdrawalLimit")
	private String cashWithdrawalLimit;
	@JsonProperty("paymentTransferLimit")
	private String paymentTransferLimit;
	@JsonProperty("purchaseLimit")
	private String purchaseLimit;

	@JsonProperty("hostHeaderInfo")
	public HostHeaderInfo getHostHeaderInfo() {
		return hostHeaderInfo;
	}

	@JsonProperty("hostHeaderInfo")
	public void setHostHeaderInfo(HostHeaderInfo hostHeaderInfo) {
		this.hostHeaderInfo = hostHeaderInfo;
	}

	@JsonProperty("pan")
	public String getPan() {
		return pan;
	}

	@JsonProperty("pan")
	public void setPan(String pan) {
		this.pan = pan;
	}

	@JsonProperty("cardNotPresentLimit")
	public String getCardNotPresentLimit() {
		return cardNotPresentLimit;
	}

	@JsonProperty("cardNotPresentLimit")
	public void setCardNotPresentLimit(String cardNotPresentLimit) {
		this.cardNotPresentLimit = cardNotPresentLimit;
	}

	@JsonProperty("cashWithdrawalLimit")
	public String getCashWithdrawalLimit() {
		return cashWithdrawalLimit;
	}

	@JsonProperty("cashWithdrawalLimit")
	public void setCashWithdrawalLimit(String cashWithdrawalLimit) {
		this.cashWithdrawalLimit = cashWithdrawalLimit;
	}

	@JsonProperty("paymentTransferLimit")
	public String getPaymentTransferLimit() {
		return paymentTransferLimit;
	}
	
	@JsonProperty("paymentTransferLimit")
	public void setPaymentTransferLimit(String paymentTransferLimit) {
		this.paymentTransferLimit = paymentTransferLimit;
	}

	@JsonProperty("purchaseLimit")
	public String getPurchaseLimit() {
		return purchaseLimit;
	}

	@JsonProperty("purchaseLimit")
	public void setPurchaseLimit(String purchaseLimit) {
		this.purchaseLimit = purchaseLimit;
	}
	
	

}
