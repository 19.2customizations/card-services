package com.ecobank.digx.cz.extxface.mule.adapter.client;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by Michael Muriuki Waihura on 20/07/2016. This class does e
 *
 * @email: michael.muriuki@cellulant.com
 */
public class AESEncrypterOriginal {

    /**
     * This generates the secret key to use for the encryption process
     *
     * @param keyValue
     * @return a secret key
     * @throws NoSuchAlgorithmException
     */
    public static Key getEncryptionKey(String keyValue) throws NoSuchAlgorithmException {
        return new SecretKeySpec(keyValue.getBytes(), "AES");
    }

    /**
     * This function encrypts the data and encodes it in base64
     *
     * @param data the data we need to encypt
     * @param secretKey the secretKey to use in the encryption
     * @return base64Encoded encrypted data
     * @throws NoSuchPaddingException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     * @throws BadPaddingException
     * @throws IllegalBlockSizeException
     */
    public static String encrypt(String data, Key secretKey) throws NoSuchPaddingException,
            NoSuchAlgorithmException, InvalidKeyException, BadPaddingException,
            IllegalBlockSizeException {
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.ENCRYPT_MODE, secretKey);
        return Base64.getEncoder().encodeToString(cipher.doFinal(data.getBytes()));
    }

    /**
     * This function decrypts the data using the secret Key.We start by decoding
     * the base64 encoded string then decrypt
     *
     * @param encryptedData the encrypted data
     * @param secretKey the shared secret key used to encrypt the data
     * @return the string that was encrypted
     * @throws NoSuchPaddingException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     * @throws BadPaddingException
     * @throws IllegalBlockSizeException
     * @throws java.io.IOException
     */
    public static String decrypt(String encryptedData, Key secretKey) throws NoSuchPaddingException,
            NoSuchAlgorithmException, InvalidKeyException, BadPaddingException,
            IllegalBlockSizeException, IOException {
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.DECRYPT_MODE, secretKey);
        byte[] decodedValue = Base64.getDecoder().decode(encryptedData);
        return new String(cipher.doFinal(decodedValue));
    }
}
