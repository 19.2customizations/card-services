package com.ecobank.digx.cz.extxface.mule.adapter.client;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Random;
import java.util.logging.Logger;
import java.util.prefs.Preferences;

import com.ecobank.digx.cz.app.cardservices.dto.CardBalanceResponse;
import com.ecobank.digx.cz.app.cardservices.dto.CardDetailDTO;
import com.ecobank.digx.cz.app.cardservices.dto.CardListResponse;
import com.ecobank.digx.cz.app.cardservices.dto.CardServiceResponseDTO;
import com.ecobank.digx.cz.app.cardservices.dto.CardStatementDTO;
import com.ecobank.digx.cz.app.cardservices.dto.CardStatementListResponse;
import com.ecobank.digx.cz.app.cardservices.dto.CardStatementRequestDTO;
import com.ecobank.digx.cz.app.domain.cardservices.entity.CardActivityInfo;
import com.ecobank.digx.cz.app.domain.cardservices.entity.CardRequest;
import com.ecobank.digx.cz.extxface.mule.assembler.CardRemoteServiceAssembler;
import com.ecobank.digx.cz.extxface.mule.dto.AccountDetails;
import com.ecobank.digx.cz.extxface.mule.dto.ESBCardRequestDTO;
import com.ecobank.digx.cz.extxface.mule.dto.ESBCardResponseDTO;
import com.ecobank.digx.cz.extxface.mule.dto.FetchVirtualCardRequestDTO;
import com.ecobank.digx.cz.extxface.mule.dto.FetchVirtualCardResponseDTO;
import com.ecobank.digx.cz.extxface.mule.dto.GenerateVirtualCardRequestDTO;
import com.ecobank.digx.cz.extxface.mule.dto.GenerateVirtualCardResponseDTO;
import com.ecobank.digx.cz.extxface.mule.dto.HostHeaderInfo;
import com.ecobank.digx.cz.extxface.mule.dto.TransactionList;
import com.ecobank.digx.cz.extxface.mule.dto.VirtualMiniStatementResponseDTO;
import com.ofss.digx.app.core.AdapterInteraction;
import com.ofss.digx.extxface.response.AbstractResponseHandler;
import com.ofss.digx.framework.rest.client.RESTClientFactory;
import com.ofss.digx.framework.rest.client.RESTClientRequestBuilder;
import com.ofss.digx.framework.rest.client.RESTClientRequestConfig;
import com.ofss.digx.framework.rest.client.impl.IRESTClient;
import com.ofss.digx.infra.exceptions.Exception;
import com.ofss.fc.infra.config.ConfigurationFactory;
import com.ofss.fc.infra.log.impl.MultiEntityLogger;
import com.thoughtworks.xstream.XStream;

public class VirtualCardServicesRepositoryAdapter extends AbstractResponseHandler{
	
	private static VirtualCardServicesRepositoryAdapter singletonInstance;

	private Preferences digx_consulting = ConfigurationFactory.getInstance().getConfigurations("DIGXCONSULTING");

	private static final String THIS_COMPONENT_NAME = VirtualCardServicesRepositoryAdapter.class.getName();
	private static transient Logger logger = MultiEntityLogger.getUniqueInstance().getLogger(THIS_COMPONENT_NAME);
	
	public static VirtualCardServicesRepositoryAdapter getInstance() {
		if (singletonInstance == null)
			synchronized (VirtualCardServicesRepositoryAdapter.class) {
				if (singletonInstance == null)
					singletonInstance = new VirtualCardServicesRepositoryAdapter();
			}
		return singletonInstance;
	}
	
	
	public CardServiceResponseDTO generateCard(CardRequest cardRequest, String[] personalData) throws Exception
	{
		CardServiceResponseDTO response = new CardServiceResponseDTO();
		response.setResponseCode("E04");
		response.setResponseMessage("Failed at interface level");
		AdapterInteraction.begin();
		
		String address = personalData[0];
		String city = personalData[1];
		String country = personalData[2];
		String dob = personalData[3];
		
		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESC_VIRTUAL_CARD_CREATE");
		RESTClientFactory clientFactory = RESTClientFactory.getInstance();
		IRESTClient restClient = clientFactory.getRESTClientInstance();
		
		String url = digx_consulting.get("ESB_VIRTUAL_CARD_URL", "");
		String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String affCode = cardRequest.getAffiliateCode();
		String ccyCode = digx_consulting.get(affCode + "_LCY", "");
		
		
		String fullName = cardRequest.getNameOnCard();
		String firstName = fullName.split(" ")[0];
		logger.info("firstName: " + firstName);
		int fName = fullName.split(" ")[0].length();
		String lastName = fullName.substring(fName).isEmpty() ? firstName : fullName.substring(fName);
		logger.info("lastName: " + lastName);
		
		String addr = address;
		if(addr != null && addr.length() > 50)
			addr = address.substring(0, 50);
		
		
		GenerateVirtualCardRequestDTO generateVirtualCardRequestDTO = new GenerateVirtualCardRequestDTO();
		generateVirtualCardRequestDTO.setAddress(address);
		generateVirtualCardRequestDTO.setAffiliateCountry(cardRequest.getAffiliateCode());
		generateVirtualCardRequestDTO.setAmount(""+cardRequest.getAmount());
		generateVirtualCardRequestDTO.setAppVersion("4.0.0.1");
		generateVirtualCardRequestDTO.setChargeAmount("10");
		generateVirtualCardRequestDTO.setCity(city);
		generateVirtualCardRequestDTO.setCurrencyCode("GHS"); //ccyCode
		generateVirtualCardRequestDTO.setCustomerId(cardRequest.getCustomerNo());
		generateVirtualCardRequestDTO.setDeviceType("");
		generateVirtualCardRequestDTO.setDob(dob);
		generateVirtualCardRequestDTO.setEmail(cardRequest.getEmail());
		generateVirtualCardRequestDTO.setEmcertId("");
		generateVirtualCardRequestDTO.setFirstName(firstName);
		generateVirtualCardRequestDTO.setLangId("en");
		generateVirtualCardRequestDTO.setLastName(lastName);
		generateVirtualCardRequestDTO.setMsisdn(cardRequest.getMobileNo());
		generateVirtualCardRequestDTO.setOptNumber(cardRequest.getMobileNo());
		generateVirtualCardRequestDTO.setSchemType("Visa");
		generateVirtualCardRequestDTO.setService("CREATE-VCARD");
		generateVirtualCardRequestDTO.setSourceAccount(cardRequest.getSourceAccountNo());
		generateVirtualCardRequestDTO.setTotalAmount(Double.toString(cardRequest.getAmount()));
		generateVirtualCardRequestDTO.setUuid("");
		generateVirtualCardRequestDTO.setVcardType("SHOPVCARD");
		
		GenerateVirtualCardResponseDTO resp = null;
		
		try {
			XStream xs = new XStream();
			logger.info("Generate Virtual Card Request: " + xs.toXML(generateVirtualCardRequestDTO));
			logger.info("CARDSERVICES-URL     : " + url);
			logger.info("CARDSERVICES-Source Code : " + sourceCode);
			
			resp = (GenerateVirtualCardResponseDTO) restClient.post(prop, GenerateVirtualCardResponseDTO.class, generateVirtualCardRequestDTO);
			
			logger.info(":: status Code    : " + resp.getStatusCode());
			
			logger.info(":: status Msg     : " + resp.getStatusMessage());
			logger.info(":: accountNo      : " + resp.getAccountDetails().get(0).getAccountNumber());
			logger.info(":: balance        : " + resp.getAccountDetails().get(0).getBalance());
			logger.info(":: card token     : " + resp.getAccountDetails().get(0).getCardToken());
			
			if(resp != null && resp.getStatusCode().equals("000"))
			{
				response.setResponseCode("000");
				response.setResponseMessage(resp.getStatusMessage());
				response.setTransactionRefNo(resp.getAccountDetails().get(0).getCardAlias());
				response.setCustomerNo(resp.getAccountDetails().get(0).getCustId());
				
				String panx = resp.getAccountDetails().get(0).getCardAlias();
				String maskedPan = panx.substring(0,6) + "****" + panx.substring(panx.length() - 4);
				String expDate = resp.getAccountDetails().get(0).getExpiry();
				
				response.setMaskedPan(maskedPan);
				response.setPseudoPAN(resp.getAccountDetails().get(0).getCardToken());
				response.setLast4Digits(panx.substring(panx.length() - 4));
				response.setCvv(resp.getAccountDetails().get(0).getCvv());
				response.setFullPan(panx);
			
				
				response.setNameOnCard(resp.getAccountDetails().get(0).getCardName());
				
				SimpleDateFormat dff4 = new SimpleDateFormat("dd-MMM-yyyy");
				//java.util.Date dtx = new java.util.Date();
				java.util.Date dtx = dff4.parse(expDate);
				SimpleDateFormat dff5 = new SimpleDateFormat("MM/YY");
				String expiryDate = dff5.format(dtx);
				response.setExpiryDate(expiryDate);
				
				String cardData = panx + "|" + resp.getAccountDetails().get(0).getCardToken(); // + "|" + resp.getAccountDetails().get(0).getCustId()
				response.setCardData(cardData);
				
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
		}
		AdapterInteraction.close();
	    return response;
	}
	
	
	public CardServiceResponseDTO fundCard(CardActivityInfo caInfo,String branchCode) throws Exception {
		CardServiceResponseDTO response = new CardServiceResponseDTO();
		response.setResponseCode("E04");
		response.setResponseMessage("Failed at interface level");
		AdapterInteraction.begin();

		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESC_VIRTUAL_CARD_FUND");
		RESTClientFactory clientFactory = RESTClientFactory.getInstance();
		IRESTClient restClient = clientFactory.getRESTClientInstance();

		/*String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String ip = digx_consulting.get("ESB_OBDX_IP", "");
		String pwd = digx_consulting.get("ESB_OBDX_PWD", "");
		String requestId = GetRefNumber("FC", 12);
		String requestToken = JEncrypt.GenerateTransactionToken(sourceCode, requestId, pwd);*/
		
		XStream xs = new XStream();
		System.out.println("Fund Virtual Card Request : " + xs.toXML(caInfo));
		
		CardRemoteServiceAssembler assembler = new CardRemoteServiceAssembler();
		
		ESBCardRequestDTO esbRequestDTO = assembler.fromDomainObjectToRemoteActivityFundVirtual(caInfo, branchCode);
		
		
		ESBCardResponseDTO resp = null;

		try {
			//XStream xs = new XStream();
			System.out.println("Fund Virtual Card Request : " + xs.toXML(esbRequestDTO));
			//logger.info("Fund Card Source Code : " + sourceCode);

			/*resp = (ESBCardResponseDTO) restClient.post(prop, ESBCardResponseDTO.class, esbRequestDTO);
			//logger.info("Generate CustomerNo     : " + resp.getCustomerNo());
			logger.info("Fund Virtual Card-Response  : " + xs.toXML(resp));
			logger.info("getResponseCode         : " + resp.getHostHeaderInfo().getResponseCode());
			logger.info("getResponseMessage      : " + resp.getHostHeaderInfo().getResponseMessage());
			response.setResponseCode(resp.getHostHeaderInfo().getResponseCode());
			response.setResponseMessage(resp.getHostHeaderInfo().getResponseMessage());
			
			if (resp != null && resp.getHostHeaderInfo().getResponseCode().equals("000")) {
				//response.setTransactionRefNo(resp.getTransRefNo());
				response.setTransactionRefNo(resp.getHostHeaderInfo().getRequestId());
			}*/
			
			//Mocking
			response.setResponseCode("000");
			response.setResponseMessage("SUCCESS");
			response.setTransactionRefNo("9999999999999999");
			
		} finally {
		}
		AdapterInteraction.close();

		return response;
	}
	
	
	public CardBalanceResponse balanceCheck(String cardCustomerId, String mobileNo) throws Exception {
		CardBalanceResponse response = new CardBalanceResponse();
		response.setResponseCode("E04");
		response.setResponseMessage("Failed at interface level");
		AdapterInteraction.begin();

		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESC_VIRTUAL_CARD_BALANCE");
		RESTClientFactory clientFactory = RESTClientFactory.getInstance();
		IRESTClient restClient = clientFactory.getRESTClientInstance();

		String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String ip = digx_consulting.get("ESB_OBDX_IP", "");
		String pwd = digx_consulting.get("ESB_OBDX_PWD", "");
		String requestId = GetRefNumber("BC", 12);
		String requestToken = JEncrypt.GenerateTransactionToken(sourceCode, requestId, pwd);
		
		String genRef = GetRefNumber("BC", 12);
		ESBCardRequestDTO esbRequestDTO = new ESBCardRequestDTO();
		HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
		hostHeaderInfo.setAffiliateCode("EGH"); //temporarily hardcoded
		hostHeaderInfo.setIpAddress(ip);
		hostHeaderInfo.setRequestId(genRef);
		hostHeaderInfo.setRequestToken(requestToken);
		hostHeaderInfo.setRequestType("GETBAL");
		hostHeaderInfo.setSourceChannelId("MOBILE");
		hostHeaderInfo.setSourceCode(sourceCode);
		esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);
		esbRequestDTO.setCardNo(""); //require a pan
		esbRequestDTO.setMsisdn(mobileNo);

		ESBCardResponseDTO resp = null;

		try {
			XStream xs = new XStream();
			logger.info("Card Balance Request : " + xs.toXML(esbRequestDTO));
			logger.info("Card Balance Source Code : " + sourceCode);

			resp = (ESBCardResponseDTO) restClient.post(prop, ESBCardResponseDTO.class, esbRequestDTO);
			logger.info("Generate CustomerNo     : " + resp.getCustomerNo());
			logger.info("Generate Card-Response  : " + xs.toXML(resp));
			logger.info("getResponseCode         : " + resp.getHostHeaderInfo().getResponseCode());
			logger.info("getResponseMessage      : " + resp.getHostHeaderInfo().getResponseMessage());
			logger.info("getBalance Check        : " + resp.getBalance());
			logger.info("isBlocked               : " + resp.getBlocked());
			
			if (resp != null && resp.getHostHeaderInfo().getResponseCode().equals("000")) {
				response.setResponseCode("000");
				response.setResponseMessage(resp.getHostHeaderInfo().getResponseMessage());
				response.setBalance(new BigDecimal(resp.getBalance()));
				response.setCardType("VIRTUAL");
			}
		} finally {
		}
		AdapterInteraction.close();

		return response;
	}
	
	
	
	public CardServiceResponseDTO blockCard(String pan,  String mobileNo) throws Exception {
		CardServiceResponseDTO response = new CardServiceResponseDTO();
		response.setResponseCode("E04");
		response.setResponseMessage("Failed at interface level");
		AdapterInteraction.begin();

		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESC_VIRTUAL_CARD_BLOCK");
		RESTClientFactory clientFactory = RESTClientFactory.getInstance();
		IRESTClient restClient = clientFactory.getRESTClientInstance();
		
		
		String requestId = GetRefNumber("BC", 12);
		String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String pwd = digx_consulting.get("ESB_OBDX_PWD", "");
		String requestToken = JEncrypt.GenerateTransactionToken(sourceCode, requestId, pwd);
		String ipx = digx_consulting.get("ESB_OBDX_IP", "");
		
		ESBCardRequestDTO esbRequestDTO = new ESBCardRequestDTO();
		HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
		hostHeaderInfo.setAffiliateCode("EGH"); //temporarily hardcoded
		hostHeaderInfo.setIpAddress(ipx);
		hostHeaderInfo.setRequestId(requestId);
		hostHeaderInfo.setRequestToken(requestToken);
		hostHeaderInfo.setRequestType("BLOCKVCARD");
		hostHeaderInfo.setSourceChannelId("MOBILE");
		hostHeaderInfo.setSourceCode(sourceCode);
		esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);
		esbRequestDTO.setCardNo(pan);
		esbRequestDTO.setMsisdn(mobileNo);

		ESBCardResponseDTO resp = null;

		try {
			XStream xs = new XStream();
			logger.info("Card Block Request : " + xs.toXML(esbRequestDTO));
			logger.info("Card Block Source Code : " + sourceCode);

			resp = (ESBCardResponseDTO) restClient.post(prop, ESBCardResponseDTO.class, esbRequestDTO);
			logger.info("Generate Card-Response  : " + xs.toXML(resp));
			logger.info("getResponseCode         : " + resp.getHostHeaderInfo().getResponseCode());
			logger.info("getResponseMessage      : " + resp.getHostHeaderInfo().getResponseMessage());
			if (resp != null && resp.getHostHeaderInfo().getResponseCode().equals("000")) {
				response.setResponseCode("000");
				response.setResponseMessage(resp.getHostHeaderInfo().getResponseMessage());
			}
		} finally {
		}
		AdapterInteraction.close();

		return response;
	}
	
	public CardServiceResponseDTO closeOrDeleteCard(String pan,  String mobileNo, String sourceAcct, String ccy) throws Exception {
		CardServiceResponseDTO response = new CardServiceResponseDTO();
		response.setResponseCode("E04");
		response.setResponseMessage("Failed at interface level");
		AdapterInteraction.begin();
		
		XStream xs = new XStream();

		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESC_VIRTUAL_DELETE_CARD");
		RESTClientFactory clientFactory = RESTClientFactory.getInstance();
		IRESTClient restClient = clientFactory.getRESTClientInstance();
		
		
		String requestId = GetRefNumber("BC", 12);
		String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String pwd = digx_consulting.get("ESB_OBDX_PWD", "");
		String requestToken = JEncrypt.GenerateTransactionToken(sourceCode, requestId, pwd);
		String ipx = digx_consulting.get("ESB_OBDX_IP", "");
		
		ESBCardRequestDTO esbRequestDTO = new ESBCardRequestDTO();
		HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
		hostHeaderInfo.setAffiliateCode("EGH"); //temporarily hardcoded
		hostHeaderInfo.setIpAddress(ipx);
		hostHeaderInfo.setRequestId(requestId);
		hostHeaderInfo.setRequestToken(requestToken);
		hostHeaderInfo.setRequestType("CLOSEVCARD");
		hostHeaderInfo.setSourceChannelId("MOBILE");
		hostHeaderInfo.setSourceCode(sourceCode);
		esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);
		esbRequestDTO.setCardNo(pan);
		esbRequestDTO.setMsisdn(mobileNo);
		esbRequestDTO.setSourceAccount(sourceAcct);
		esbRequestDTO.setSourceAccountCcy(ccy);

		ESBCardResponseDTO resp = null;

		try {
			
			System.out.println("Card Block Request : " + xs.toXML(esbRequestDTO));
			logger.info("Card Block Source Code : " + sourceCode);
			
			
			resp = (ESBCardResponseDTO) restClient.post(prop, ESBCardResponseDTO.class, esbRequestDTO);
			logger.info("Generate Card-Response  : " + xs.toXML(resp));
			logger.info("getResponseCode         : " + resp.getHostHeaderInfo().getResponseCode());
			logger.info("getResponseMessage      : " + resp.getHostHeaderInfo().getResponseMessage());
			if (resp != null && resp.getHostHeaderInfo().getResponseCode().equals("000")) {
				response.setResponseCode("000");
				response.setResponseMessage(resp.getHostHeaderInfo().getResponseMessage());
			}
			
			
			
		} finally {
		}
		AdapterInteraction.close();

		return response;
	}

	
	public CardServiceResponseDTO unblockCard(String pan,  String mobileNo) throws Exception {
		CardServiceResponseDTO response = new CardServiceResponseDTO();
		response.setResponseCode("E04");
		response.setResponseMessage("Failed at interface level");
		AdapterInteraction.begin();

		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESC_VIRTUAL_CARD_UNBLOCK");
		RESTClientFactory clientFactory = RESTClientFactory.getInstance();
		IRESTClient restClient = clientFactory.getRESTClientInstance();
		
		
		String requestId = GetRefNumber("BC", 12);
		String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String pwd = digx_consulting.get("ESB_OBDX_PWD", "");
		String requestToken = JEncrypt.GenerateTransactionToken(sourceCode, requestId, pwd);
		String ipx = digx_consulting.get("ESB_OBDX_IP", "");
		
		ESBCardRequestDTO esbRequestDTO = new ESBCardRequestDTO();
		HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
		hostHeaderInfo.setAffiliateCode("EGH"); //temporarily hardcoded
		hostHeaderInfo.setIpAddress(ipx);
		hostHeaderInfo.setRequestId(requestId);
		hostHeaderInfo.setRequestToken(requestToken);
		hostHeaderInfo.setRequestType("UNBLOCKVCARD");
		hostHeaderInfo.setSourceChannelId("MOBILE");
		hostHeaderInfo.setSourceCode(sourceCode);
		esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);
		esbRequestDTO.setCardNo(pan);
		esbRequestDTO.setMsisdn(mobileNo);

		ESBCardResponseDTO resp = null;

		try {
			XStream xs = new XStream();
			logger.info("Card UnBlock Request : " + xs.toXML(esbRequestDTO));
			logger.info("Card UnBlock Source Code : " + sourceCode);

			resp = (ESBCardResponseDTO) restClient.post(prop, ESBCardResponseDTO.class, esbRequestDTO);
			logger.info("Generate Card-Response  : " + xs.toXML(resp));
			logger.info("getResponseCode         : " + resp.getHostHeaderInfo().getResponseCode());
			logger.info("getResponseMessage      : " + resp.getHostHeaderInfo().getResponseMessage());
			if (resp != null && resp.getHostHeaderInfo().getResponseCode().equals("000")) {
				response.setResponseCode("000");
				response.setResponseMessage(resp.getHostHeaderInfo().getResponseMessage());
			}
		} finally {
		}
		AdapterInteraction.close();

		return response;
	}
	
	
	
	public CardStatementListResponse getStatement(CardStatementRequestDTO cardStatementRequestDTO) throws Exception {
		CardStatementListResponse cardStatementListResponse = new CardStatementListResponse();
		cardStatementListResponse.setResponseCode("E04");
		cardStatementListResponse.setResponseMessage("Failed at interface level");
		AdapterInteraction.begin();

		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESC_VIRTUAL_CARD_STATEMENT");
		RESTClientFactory clientFactory = RESTClientFactory.getInstance();
		IRESTClient restClient = clientFactory.getRESTClientInstance();

		String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String ipx = digx_consulting.get("ESB_OBDX_IP", "");
		String pwd = digx_consulting.get("ESB_OBDX_PWD", "");
		
		String genRef = GetRefNumber("CS", 12);
		String requestToken = JEncrypt.GenerateTransactionToken(sourceCode, genRef, pwd);
		ESBCardRequestDTO esbRequestDTO = new ESBCardRequestDTO();
		HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
		hostHeaderInfo.setAffiliateCode(cardStatementRequestDTO.getAffCode());
		hostHeaderInfo.setIpAddress(ipx);
		hostHeaderInfo.setRequestId(genRef);
		hostHeaderInfo.setRequestToken(requestToken);
		hostHeaderInfo.setRequestType("MINISTATEMENT");
		hostHeaderInfo.setSourceChannelId("MOBILE");
		hostHeaderInfo.setSourceCode(sourceCode);
		esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);
		
		//NB:
		//cardNo & Msisdn required
		//temporarily empty
		
		esbRequestDTO.setCardNo("");
		esbRequestDTO.setMsisdn("");
		esbRequestDTO.setStartDate(cardStatementRequestDTO.getStartDate());
		esbRequestDTO.setEndDate(cardStatementRequestDTO.getEndDate());

		VirtualMiniStatementResponseDTO virtualMiniStatementResponseDTO = null;
		try {
			XStream xs = new XStream();
			logger.info("Fund Card Request : " + xs.toXML(esbRequestDTO));
			logger.info("Fund Card Source Code : " + sourceCode);

			virtualMiniStatementResponseDTO = (VirtualMiniStatementResponseDTO) restClient.post(prop, VirtualMiniStatementResponseDTO.class, esbRequestDTO);
			logger.info("getResponseCode         : " + virtualMiniStatementResponseDTO.getHostHeaderInfo().getResponseCode());
			logger.info("getResponseMessage      : " + virtualMiniStatementResponseDTO.getHostHeaderInfo().getResponseMessage());
			
			
			if (virtualMiniStatementResponseDTO.getHostHeaderInfo().getResponseCode().equals("000")) {

				if (virtualMiniStatementResponseDTO.getTransactionList().size() > 0) {

					for (TransactionList transactionDetails : virtualMiniStatementResponseDTO.getTransactionList()) {

						CardStatementDTO cardStatementDTO = new CardStatementDTO();
						cardStatementDTO.setBalance("");
						//Balance Not Available
						cardStatementDTO.setBalance("");
						cardStatementDTO.setDescription(transactionDetails.getTransactionDesc());
						cardStatementDTO.setFee(new BigDecimal(transactionDetails.getFee()));
						cardStatementDTO.setPaymentAmount(new BigDecimal(transactionDetails.getTotalAmount()));
						cardStatementDTO.setReferenceNo(transactionDetails.getTransactionId());
						cardStatementDTO.setTransDate(transactionDetails.getTransactionDate());

						cardStatementListResponse.getCardStatement().add(cardStatementDTO);
						logger.info("******************************************");
						logger.info("fee        : " + transactionDetails.getFee());
						logger.info("narration  : " + transactionDetails.getTransactionDesc());
						logger.info("refInfo    : " + transactionDetails.getReferenceInfo());
						logger.info("totalAmount: " + transactionDetails.getTotalAmount());
						logger.info("transDate  : " + transactionDetails.getTransactionDate());
					}
				}
			}
			
		} finally {
		}
		return cardStatementListResponse;
	}

	
	public CardServiceResponseDTO updateOTPNumber(String pan, String customerNo, String mobileNo, String otp, String email) throws Exception {
		CardServiceResponseDTO response = new CardServiceResponseDTO();
		response.setResponseCode("E04");
		response.setResponseMessage("Failed at interface level");
		AdapterInteraction.begin();

		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESC_VIRTUAL_CARD_OTP");
		RESTClientFactory clientFactory = RESTClientFactory.getInstance();
		IRESTClient restClient = clientFactory.getRESTClientInstance();
		
		
		String requestId = GetRefNumber("BC", 12);
		String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String pwd = digx_consulting.get("ESB_OBDX_PWD", "");
		String requestToken = JEncrypt.GenerateTransactionToken(sourceCode, requestId, pwd);
		String ipx = digx_consulting.get("ESB_OBDX_IP", "");
		
		ESBCardRequestDTO esbRequestDTO = new ESBCardRequestDTO();
		HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
		hostHeaderInfo.setAffiliateCode("EGH"); //temporarily hardcoded
		hostHeaderInfo.setIpAddress(ipx);
		hostHeaderInfo.setRequestId(requestId);
		hostHeaderInfo.setRequestToken(requestToken);
		hostHeaderInfo.setRequestType("BLOCKVCARD");
		hostHeaderInfo.setSourceChannelId("MOBILE");
		hostHeaderInfo.setSourceCode(sourceCode);
		esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);
		esbRequestDTO.setCardNo(pan);
		esbRequestDTO.setMsisdn(mobileNo);
		esbRequestDTO.setOtp(otp);
		esbRequestDTO.setEmail(email);

		ESBCardResponseDTO resp = null;

		try {
			XStream xs = new XStream();
			logger.info("Card OTP Number Update Request : " + xs.toXML(esbRequestDTO));
			logger.info("Card OTP Number Update Source Code : " + sourceCode);

			resp = (ESBCardResponseDTO) restClient.post(prop, ESBCardResponseDTO.class, esbRequestDTO);
			logger.info("Generate Card-Response  : " + xs.toXML(resp));
			logger.info("getResponseCode         : " + resp.getHostHeaderInfo().getResponseCode());
			logger.info("getResponseMessage      : " + resp.getHostHeaderInfo().getResponseMessage());
			if (resp != null && resp.getHostHeaderInfo().getResponseCode().equals("000")) {
				response.setResponseCode("000");
				response.setResponseMessage(resp.getHostHeaderInfo().getResponseMessage());
			}
		} finally {
		}
		AdapterInteraction.close();

		return response;
	}
	
	
	public CardListResponse fetchVCardByMsisdn(String mobileNo, String referenceNo) throws Exception {
		CardListResponse cardListResponse = new CardListResponse();
		cardListResponse.setResponseCode("E04");
		cardListResponse.setResponseMessage("Failed at interface level");
		AdapterInteraction.begin();

		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESC_VIRTUAL_CARD_OTP");
		RESTClientFactory clientFactory = RESTClientFactory.getInstance();
		IRESTClient restClient = clientFactory.getRESTClientInstance();
		
		FetchVirtualCardRequestDTO fetchVirtualCardRequestDTO = new FetchVirtualCardRequestDTO();
		fetchVirtualCardRequestDTO.setAFFILIATECOUNTRY("EGH");
		fetchVirtualCardRequestDTO.setAPPVERSION("3.5.0");
		fetchVirtualCardRequestDTO.setDEVICETYPE("");
		fetchVirtualCardRequestDTO.setEMCERTID("1233185769");
		fetchVirtualCardRequestDTO.setLANGID("1");
		fetchVirtualCardRequestDTO.setMSISDN(mobileNo);
		fetchVirtualCardRequestDTO.setSERVICE("FETCH-VCARDS");
		fetchVirtualCardRequestDTO.setUUID(referenceNo);
		
		FetchVirtualCardResponseDTO fetchVirtualCardResponseDTO = null;

		try {
			XStream xs = new XStream();
			logger.info("Fetch Card by MSISDN Request : " + xs.toXML(fetchVirtualCardRequestDTO));

			fetchVirtualCardResponseDTO = (FetchVirtualCardResponseDTO) restClient.post(prop, FetchVirtualCardResponseDTO.class, fetchVirtualCardRequestDTO);
			logger.info("Fetch Virtual Card-Response  : " + xs.toXML(fetchVirtualCardResponseDTO));
			logger.info("getResponseCode         : " + fetchVirtualCardResponseDTO.getStatusCode());
			logger.info("getResponseMessage      : " + fetchVirtualCardResponseDTO.getStatusMessage());
			
			
			if (fetchVirtualCardResponseDTO.getStatusCode().equals("000")) {

				if (fetchVirtualCardResponseDTO.getAccountDetails().size() > 0) {

					for (AccountDetails transactionDetails : fetchVirtualCardResponseDTO.getAccountDetails()) {
						
						CardDetailDTO cardDetailDTO = new CardDetailDTO();
						cardDetailDTO.setBalance(new BigDecimal(transactionDetails.getBalance()));
						cardDetailDTO.setCardAccount(transactionDetails.getAccountNumber());
						cardDetailDTO.setCardCategory("VIRTUAL");
						cardDetailDTO.setExpiryDate(transactionDetails.getExpiry());
						cardDetailDTO.setMaskedPan(transactionDetails.getCardToken());
						cardDetailDTO.setMobileNo(transactionDetails.getOtpNumber());
						cardListResponse.getCards().add(cardDetailDTO);
						
						logger.info("******************************************");
						logger.info("Expiry Date   : " + cardDetailDTO.getExpiryDate());
						logger.info("Card Status   : " + cardDetailDTO.getCardStatus());
						logger.info("Name-On-Card  : " + cardDetailDTO.getNameOnCard());
						logger.info("Masked Pan    : " + cardDetailDTO.getMaskedPan());
					}
				}
			}
		} finally {
		}
		AdapterInteraction.close();

		return cardListResponse;
	}
	
	
	public  String GetRefNumber(String type, int len) {

        String finalString = "";
        int x = 0;
        char[] stringChars = new char[len];
        for (int i = 0; i < len; i++) //4
        {
            Random random = new Random();
            x = random.nextInt(9);

            stringChars[i] = Integer.toString(x).toCharArray()[0];
        }


        finalString = new String(stringChars);
        finalString = type + finalString;
        return finalString.trim();
    }

}
