 package com.ecobank.digx.cz.extxface.mule.adapter.client;

import java.math.BigDecimal;
import java.util.Random;
import java.util.logging.Logger;
import java.util.prefs.Preferences;

import com.ecobank.digx.cz.app.cardservices.dto.CardBalanceResponse;
import com.ecobank.digx.cz.app.cardservices.dto.CardDetailDTO;
import com.ecobank.digx.cz.app.cardservices.dto.CardListResponse;
import com.ecobank.digx.cz.app.cardservices.dto.CardServiceResponseDTO;
import com.ecobank.digx.cz.app.cardservices.dto.CardStatementListResponse;
import com.ecobank.digx.cz.app.cardservices.dto.CardStatementRequestDTO;
import com.ecobank.digx.cz.app.domain.cardservices.entity.CardActivityInfo;
import com.ecobank.digx.cz.app.domain.cardservices.entity.CardRequest;
import com.ecobank.digx.cz.extxface.mule.assembler.CardRemoteServiceAssembler;
import com.ecobank.digx.cz.extxface.mule.dto.ESBCardBlockRequestDTO;
import com.ecobank.digx.cz.extxface.mule.dto.ESBCardRequestDTO;
import com.ecobank.digx.cz.extxface.mule.dto.ESBCardResponseDTO;
import com.ecobank.digx.cz.extxface.mule.dto.ESBCardTransactionHistoryResponseDTO;
import com.ecobank.digx.cz.extxface.mule.dto.HostHeaderInfo;
import com.ofss.digx.app.core.AdapterInteraction;
import com.ofss.digx.framework.rest.client.RESTClientFactory;
import com.ofss.digx.framework.rest.client.RESTClientRequestBuilder;
import com.ofss.digx.framework.rest.client.RESTClientRequestConfig;
import com.ofss.digx.framework.rest.client.impl.IRESTClient;
import com.ofss.digx.infra.exceptions.Exception;
import com.ofss.fc.infra.config.ConfigurationFactory;
import com.ofss.fc.infra.log.impl.MultiEntityLogger;
import com.thoughtworks.xstream.XStream;

public class CreditCardServicesRepositoryAdapter {
	private static CreditCardServicesRepositoryAdapter singletonInstance;

	private Preferences digx_consulting = ConfigurationFactory.getInstance().getConfigurations("DIGXCONSULTING");
	

	private static final String THIS_COMPONENT_NAME = CreditCardServicesRepositoryAdapter.class.getName();
	private static transient Logger logger = MultiEntityLogger.getUniqueInstance().getLogger(THIS_COMPONENT_NAME);
	
	public static CreditCardServicesRepositoryAdapter getInstance() {
		if (singletonInstance == null)
			synchronized (CreditCardServicesRepositoryAdapter.class) {
				if (singletonInstance == null)
					singletonInstance = new CreditCardServicesRepositoryAdapter();
			}
		return singletonInstance;
	}
	
	public CardBalanceResponse balanceCheck(String cardId,String affCode) throws Exception {
		CardBalanceResponse response = new CardBalanceResponse();
		response.setResponseCode("E04");
		response.setResponseMessage("Failed at interface level");
		AdapterInteraction.begin();

		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		//RESTClientRequestConfig prop = clientBuilder.loadConfig("ESB_CREDIT_CARD_NAME");
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESB_CREDIT_CARD_DETAIL_FETCH");
		RESTClientFactory clientFactory = RESTClientFactory.getInstance();
		IRESTClient restClient = clientFactory.getRESTClientInstance();

		String requestId = GetRefNumber("BC", 12);
		String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String pwd = digx_consulting.get("ESB_OBDX_PWD", "");
		String requestToken = JEncrypt.GenerateTransactionToken(sourceCode, requestId, pwd);
		String ipx = digx_consulting.get("ESB_OBDX_IP", "");
		
		
		ESBCardRequestDTO esbRequestDTO = new ESBCardRequestDTO();
		HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
		hostHeaderInfo.setAffiliateCode("EGH"); //temporarily hardcoded
		hostHeaderInfo.setIpAddress(ipx);
		hostHeaderInfo.setRequestId(requestId);
		hostHeaderInfo.setRequestToken(requestToken);
		hostHeaderInfo.setRequestType("GETCARDBALANCE");
		hostHeaderInfo.setSourceChannelId("WEB");
		hostHeaderInfo.setSourceCode(sourceCode);
		esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);
		//esbRequestDTO.setCardNo(cardId);
		esbRequestDTO.setCardAcct(cardId);
		
		ESBCardResponseDTO resp = null;

		try {
			XStream xs = new XStream();
			logger.info("Card Balance Request : " + xs.toXML(esbRequestDTO));
			logger.info("Card Balance Source Code : " + sourceCode);

			resp = (ESBCardResponseDTO) restClient.post(prop, ESBCardResponseDTO.class, esbRequestDTO);
			logger.info("Generate Name           : " + resp.getName());
			logger.info("Generate Card-Response  : " + xs.toXML(resp));
			logger.info("getResponseCode         : " + resp.getHostHeaderInfo().getResponseCode());
			logger.info("getResponseMessage      : " + resp.getHostHeaderInfo().getResponseMessage());
			
			if (resp != null && resp.getHostHeaderInfo().getResponseCode().equals("000")) {
				
				response.setCcyCode(resp.getCcy());
				String pan = resp.getPsuedoPan();
                response.setPseudoPAN(pan);
				
				CardBalanceResponse cardBalanceResponse = getCardBalance(cardId);
				logger.info("credit card balance : " + cardBalanceResponse.getBalance());
				logger.info("credit Pseudo card account : " + cardBalanceResponse.getCardAccount());
				
				
				response.setResponseCode("000");
				response.setResponseMessage(resp.getHostHeaderInfo().getResponseMessage());
				response.setBalance(cardBalanceResponse.getBalance());
				
				System.out.println("Fetch Pseudo PAN-Bal::: " + cardBalanceResponse.getExpiry());
				if(cardBalanceResponse.getExpiry() != null)
				{
				  String exp = cardBalanceResponse.getExpiry();
				  if(exp.length() >= 10)
				  {
					  String expx = exp.substring(5,7) + "/"  + exp.substring(2,4);
					  response.setExpiry(expx);
				  }
				  else
					  response.setExpiry(cardBalanceResponse.getExpiry());
					 
				}
				else
					response.setExpiry("");
				
				//String ccyCode = digx_consulting.get(affCode + "_LCY", "");
				
				//System.out.println("Fetch Pseudo PAN::: " + ccyCode);
				
				//if( resp.getCcy() != null)
				 //  response.setCcyCode(resp.getCcy());
				//else
					
				
				response.setNameOnCard(resp.getName());
				response.setCardAccount(cardBalanceResponse.getCardAccount());
				
				//To be replaced with Pseudo PAN or Masked PAN in next release
				String maskedPan = pan.substring(0,6) + "****" + pan.substring(pan.length() - 4);
				
				response.setMaskedPan(maskedPan);
				response.setSourceAccount(cardBalanceResponse.getSourceAccount());
				response.setCardType("CREDIT");
				
				System.out.println("Fetch Pseudo PAN::: " + cardId);
				/*CardBalanceResponse respPsuedo = getCardPseudoPAN(cardId, affCode);
				if(respPsuedo != null && respPsuedo.getResponseCode().equals("000"))
				{
					cardBalanceResponse.setPseudoPAN(respPsuedo.getPseudoPAN());
					cardBalanceResponse.setCcyCode(respPsuedo.getCcyCode());
					pan = respPsuedo.getPseudoPAN();
					maskedPan = pan.substring(0,6) + "****" + pan.substring(pan.length() - 4);
					response.setMaskedPan(maskedPan);
				}*/
				
			}
		} finally {
		}
		AdapterInteraction.close();

		return response;
	}
	
	
	public CardBalanceResponse getCardBalance(String cardId) throws Exception {
		CardBalanceResponse cardBalanceResponse = new CardBalanceResponse();
		AdapterInteraction.begin();

		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESB_CREDIT_CARD_BALANCE");
		RESTClientFactory clientFactory = RESTClientFactory.getInstance();
		IRESTClient restClient = clientFactory.getRESTClientInstance();

		String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String ipx = digx_consulting.get("ESB_OBDX_IP", "");
		
		String requestId = GetRefNumber("BC", 12);
		String pwd = digx_consulting.get("ESB_OBDX_PWD", "");
		String requestToken = JEncrypt.GenerateTransactionToken(sourceCode, requestId, pwd);
		
		
		ESBCardRequestDTO esbRequestDTO = new ESBCardRequestDTO();
		HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
		hostHeaderInfo.setAffiliateCode("EGH"); //temporarily hardcoded
		hostHeaderInfo.setIpAddress(ipx);
		hostHeaderInfo.setRequestId(requestId);
		hostHeaderInfo.setRequestToken(requestToken);
		hostHeaderInfo.setRequestType("POSTBILL");
		hostHeaderInfo.setSourceChannelId("WEB");
		hostHeaderInfo.setSourceCode(sourceCode);
		esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);
		esbRequestDTO.setCardAccount(cardId);

		ESBCardResponseDTO resp = null;

		try {
			XStream xs = new XStream();
			logger.info("Card Balance Request : " + xs.toXML(esbRequestDTO));
			logger.info("Card Balance Source Code : " + sourceCode);

			resp = (ESBCardResponseDTO) restClient.post(prop, ESBCardResponseDTO.class, esbRequestDTO);
			logger.info("Available Balance            : " + resp.getAvailableAmount());
			logger.info("Generate Card-Response  : " + xs.toXML(resp));
			logger.info("getResponseCode         : " + resp.getHostHeaderInfo().getResponseCode());
			logger.info("getResponseMessage      : " + resp.getHostHeaderInfo().getResponseMessage());
			if (resp != null && resp.getHostHeaderInfo().getResponseCode().equals("000")) {
				//balance = resp.getAvailableAmount();
				cardBalanceResponse.setBalance(new BigDecimal(resp.getAvailableAmount()));
				cardBalanceResponse.setCardAccount(resp.getCardAccount());
				cardBalanceResponse.setExpiry(resp.getExpiry());
				cardBalanceResponse.setSourceAccount(resp.getAccountNo());
			}
		} finally {
		}
		AdapterInteraction.close();

		return cardBalanceResponse;
	}
	
	
	public CardBalanceResponse getCardPseudoPAN(String cardId,String affCode) throws Exception {
		CardBalanceResponse cardBalanceResponse = new CardBalanceResponse();
		AdapterInteraction.begin();
		
		System.out.println("Entering Pseudo balance get: " );

		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESB_CREDIT_CARD_DETAIL_FETCH");
		RESTClientFactory clientFactory = RESTClientFactory.getInstance();
		IRESTClient restClient = clientFactory.getRESTClientInstance();

		String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String ipx = digx_consulting.get("ESB_OBDX_IP", "");
		
		String requestId = GetRefNumber("BC", 12);
		String pwd = digx_consulting.get("ESB_OBDX_PWD", "");
		String requestToken = JEncrypt.GenerateTransactionToken(sourceCode, requestId, pwd);
		
		
		ESBCardRequestDTO esbRequestDTO = new ESBCardRequestDTO();
		HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
		hostHeaderInfo.setAffiliateCode("EGH"); //temporarily hardcoded
		hostHeaderInfo.setIpAddress(ipx);
		hostHeaderInfo.setRequestId(requestId);
		hostHeaderInfo.setRequestToken(requestToken);
		hostHeaderInfo.setRequestType("POSTBILL");
		hostHeaderInfo.setSourceChannelId("WEB");
		hostHeaderInfo.setSourceCode(sourceCode);
		esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);
		esbRequestDTO.setCardAcct(cardId);
		
		System.out.println("Pseudo balance get: " + prop.getServiceURL() + " " + prop.getContextURL() + " " + cardId);

		ESBCardResponseDTO resp = null;

		try {
			XStream xs = new XStream();
			logger.info("Card Balance Request : " + xs.toXML(esbRequestDTO));
			logger.info("Card Balance Source Code : " + sourceCode);

			resp = (ESBCardResponseDTO) restClient.post(prop, ESBCardResponseDTO.class, esbRequestDTO);
			logger.info("Available Balance            : " + resp.getAvailableAmount());
			logger.info("Generate Card-Response  : " + xs.toXML(resp));
			logger.info("getResponseCode         : " + resp.getHostHeaderInfo().getResponseCode());
			logger.info("getResponseMessage      : " + resp.getHostHeaderInfo().getResponseMessage());
			if (resp != null && resp.getHostHeaderInfo().getResponseCode().equals("000")) {
				//balance = resp.getAvailableAmount();
				cardBalanceResponse.setPseudoPAN(resp.getPsuedoPan());
				cardBalanceResponse.setCcyCode(resp.getCcy());
				cardBalanceResponse.setResponseCode("000");
				cardBalanceResponse.setResponseMessage("SUCCESS");
				
			}
			else
			{
				cardBalanceResponse.setResponseCode(resp.getHostHeaderInfo().getResponseCode());
				cardBalanceResponse.setResponseMessage(resp.getHostHeaderInfo().getResponseMessage());
			}
		} finally {
		}
		AdapterInteraction.close();

		return cardBalanceResponse;
	}
	
	
	public CardStatementListResponse getStatement(CardStatementRequestDTO cardStatementRequestDTO) throws Exception {
		
		CardStatementListResponse cardStatementListResponse = new CardStatementListResponse();
		cardStatementListResponse.setResponseCode("E04");
		cardStatementListResponse.setResponseMessage("Failed at interface level");
		
		AdapterInteraction.begin();

		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESB_CREDIT_CARD_STATEMENT");
		RESTClientFactory clientFactory = RESTClientFactory.getInstance();
		IRESTClient restClient = clientFactory.getRESTClientInstance();

		String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String ipx = digx_consulting.get("ESB_OBDX_IP", "");
		
		String requestId = GetRefNumber("BC", 12);
		String pwd = digx_consulting.get("ESB_OBDX_PWD", "");
		String requestToken = JEncrypt.GenerateTransactionToken(sourceCode, requestId, pwd);
		
		
		ESBCardRequestDTO esbRequestDTO = new ESBCardRequestDTO();
		HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
		hostHeaderInfo.setAffiliateCode("EGH"); //temporarily hardcoded
		hostHeaderInfo.setIpAddress(ipx);
		hostHeaderInfo.setRequestId(requestId);
		hostHeaderInfo.setRequestToken(requestToken);
		hostHeaderInfo.setRequestType("GETCREDITCARDHIST");
		hostHeaderInfo.setSourceChannelId("WEB");
		hostHeaderInfo.setSourceCode(sourceCode);
		esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);
		esbRequestDTO.setCardAccount(cardStatementRequestDTO.getCustomerNo());
		esbRequestDTO.setStartDate(cardStatementRequestDTO.getStartDate());
		esbRequestDTO.setEndDate(cardStatementRequestDTO.getEndDate());

		ESBCardTransactionHistoryResponseDTO resp = null;
		CardRemoteServiceAssembler assembler = new CardRemoteServiceAssembler();

		try {
			XStream xs = new XStream();
			logger.info("Card Statement Request     : " + xs.toXML(esbRequestDTO));
			logger.info("Card Statement Source Code : " + sourceCode);

			resp = (ESBCardTransactionHistoryResponseDTO) restClient.post(prop, ESBCardTransactionHistoryResponseDTO.class, esbRequestDTO);
			
			System.out.println("Generate Card-Response  : " + xs.toXML(resp));
			//logger.info("getResponseCode         : " + resp.getHostHeaderInfo().getResponseCode());
			//logger.info("getResponseMessage      : " + resp.getHostHeaderInfo().getResponseMessage());
			if (resp != null && resp.getHostHeaderInfo().getResponseCode().equals("000")) {
				cardStatementListResponse  = assembler.fromRemoteResponseCardStatementtoDTO(resp);
				
			}
		} finally {
		}
		AdapterInteraction.close();

		return cardStatementListResponse;
	}
	
	
	public CardListResponse getCardByAccount(CardStatementRequestDTO cardStatementRequestDTO) throws Exception {
		CardListResponse cardListResponse = new CardListResponse();
		cardListResponse.setResponseCode("E04");
		cardListResponse.setResponseMessage("Failed at interface level");
		AdapterInteraction.begin();

		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESB_CARD_BY_ACCOUNT");
		RESTClientFactory clientFactory = RESTClientFactory.getInstance();
		IRESTClient restClient = clientFactory.getRESTClientInstance();

		String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String ipx = digx_consulting.get("ESB_OBDX_IP", "");
		String pwd = digx_consulting.get("ESB_OBDX_PWD", "");
		
		String genRef = GetRefNumber("CS", 12);
		String requestToken = JEncrypt.GenerateTransactionToken(sourceCode, genRef, pwd);
		ESBCardRequestDTO esbRequestDTO = new ESBCardRequestDTO();
		HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
		hostHeaderInfo.setAffiliateCode(cardStatementRequestDTO.getAffCode());
		hostHeaderInfo.setIpAddress(ipx);
		hostHeaderInfo.setRequestId(genRef);
		hostHeaderInfo.setRequestToken(requestToken);
		hostHeaderInfo.setRequestType("MPAY");
		hostHeaderInfo.setSourceChannelId("WEB");
		hostHeaderInfo.setSourceCode(sourceCode);
		esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);
		esbRequestDTO.setAccountNo(cardStatementRequestDTO.getCustomerNo());
		esbRequestDTO.setCardScheme("");

		try {
			XStream xs = new XStream();
			logger.info("GET Card By Account Request : " + xs.toXML(esbRequestDTO));
			logger.info("Source Code : " + sourceCode);

			cardListResponse = (CardListResponse) restClient.post(prop, CardListResponse.class, esbRequestDTO);
			logger.info("getResponseCode         : " + cardListResponse.getHostHeaderInfo().getResponseCode());
			logger.info("getResponseMessage      : " + cardListResponse.getHostHeaderInfo().getResponseMessage());
			
			
			if (cardListResponse.getHostHeaderInfo().getResponseCode().equals("000")) {

				if (cardListResponse.getCards().size() > 0) {

					for (CardDetailDTO cardDetailDTO : cardListResponse.getCards()) {
						
						cardListResponse.getCards().add(cardDetailDTO);
						logger.info("******************************************");
						logger.info("Expiry Date   : " + cardDetailDTO.getExpiryDate());
						logger.info("Card Status   : " + cardDetailDTO.getCardStatus());
						logger.info("Name-On-Card  : " + cardDetailDTO.getNameOnCard());
						logger.info("Masked Pan    : " + cardDetailDTO.getMaskedPan());
					}
				}
			}
			
		} finally {
		}
		return cardListResponse;
	}

	
	//credit card funding
	public CardServiceResponseDTO fundCard(CardActivityInfo cardActivityInfo,String branchCode, String pan, String nameOnCard) throws Exception {
		CardServiceResponseDTO response = new CardServiceResponseDTO();
		response.setResponseCode("E04");
		response.setResponseMessage("Failed at interface level");
		AdapterInteraction.begin();
		
		CardRemoteServiceAssembler assembler = new CardRemoteServiceAssembler();

		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESB_CREDIT_CARD_FUND");
		RESTClientFactory clientFactory = RESTClientFactory.getInstance();
		IRESTClient restClient = clientFactory.getRESTClientInstance();

		//String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		
		
		//String genRef = requestAct.getExternalRefNo(); // GetRefNumber("BC", 12);
		ESBCardRequestDTO esbRequestDTO = assembler.fromDomainObjectToRemoteActivityFundCreditCard(cardActivityInfo, branchCode);
		
		ESBCardResponseDTO resp = null;

		try {
			XStream xs = new XStream();
			logger.info("Fund Credit Card Request : " + xs.toXML(esbRequestDTO));

			resp = (ESBCardResponseDTO) restClient.post(prop, ESBCardResponseDTO.class, esbRequestDTO);
			logger.info("Generate CustomerNo     : " + resp.getCustomerNo());
			logger.info("cc Card-Response           : " + xs.toXML(resp));
			logger.info("cc getResponseCode         : " + resp.getHostHeaderInfo().getResponseCode());
			logger.info("cc getResponseMessage      : " + resp.getHostHeaderInfo().getResponseMessage());
			logger.info("cc internalRefNo           : " + resp.getInternalRefNo());
			if (resp != null && resp.getHostHeaderInfo().getResponseCode().equals("000")) {
				response.setResponseCode("000");
				response.setResponseMessage(resp.getHostHeaderInfo().getResponseMessage());
				response.setTransactionRefNo(resp.getTransRefNo());
			}
		} finally {
		}
		AdapterInteraction.close();

		return response;
	}
	
	
	 
		public CardServiceResponseDTO blockCard(String cardNo, String expiryDate, String reason ) throws Exception {
		CardServiceResponseDTO response = new CardServiceResponseDTO();
		response.setResponseCode("E04");
		response.setResponseMessage("Failed at interface level");
		AdapterInteraction.begin();
		
		System.out.println("Card Block-Credit :" + cardNo + " " + expiryDate + " " + reason);

		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESB_CREDIT_CARD_BLOCK");
		RESTClientFactory clientFactory = RESTClientFactory.getInstance();
		IRESTClient restClient = clientFactory.getRESTClientInstance();

		String requestId = GetRefNumber("BC", 12);
		String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String pwd = digx_consulting.get("ESB_OBDX_PWD", "");
		String requestToken = JEncrypt.GenerateTransactionToken(sourceCode, requestId, pwd);
		String ipx = digx_consulting.get("ESB_OBDX_IP", "");
		
		ESBCardBlockRequestDTO esbRequestDTO = new ESBCardBlockRequestDTO();
		HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
		hostHeaderInfo.setAffiliateCode("EGH"); //temporarily hardcoded
		hostHeaderInfo.setIpAddress(ipx);
		hostHeaderInfo.setRequestId(requestId);
		hostHeaderInfo.setRequestToken(requestToken);
		hostHeaderInfo.setRequestType("BLOCKCARD");
		hostHeaderInfo.setSourceChannelId("MOBILE");
		hostHeaderInfo.setSourceCode(sourceCode);
		esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);
		esbRequestDTO.setReasonCode(reason);
		esbRequestDTO.setCardNo(cardNo); 
		//MM/YY
		String expDate = expiryDate.substring(3,5) + expiryDate.substring(0,2);
		esbRequestDTO.setExpiryDate(expDate);

		ESBCardResponseDTO resp = null;

		try {
			XStream xs = new XStream();
			logger.info("Card Block Request : " + xs.toXML(esbRequestDTO));
			logger.info("Card Block Source Code : " + sourceCode);

			resp = (ESBCardResponseDTO) restClient.post(prop, ESBCardResponseDTO.class, esbRequestDTO);
			logger.info("Generate CustomerNo     : " + resp.getCustomerNo());
			logger.info("Generate Card-Response  : " + xs.toXML(resp));
			logger.info("getResponseCode         : " + resp.getHostHeaderInfo().getResponseCode());
			logger.info("getResponseMessage      : " + resp.getHostHeaderInfo().getResponseMessage());
			if (resp != null && resp.getHostHeaderInfo().getResponseCode().equals("000")) {
				response.setResponseCode("000");
				response.setResponseMessage(resp.getHostHeaderInfo().getResponseMessage());
				//response.setActivityDescription("BLOCK CARD");
				//response.setCardId("");
				//response.setRefId(genRef);
			}
		} finally {
		}
		AdapterInteraction.close();

		return response;
	}
	
	
	public CardServiceResponseDTO unblockCard(String cardNo, String expiryDate, String reason ) throws Exception {
		CardServiceResponseDTO response = new CardServiceResponseDTO();
		response.setResponseCode("E04");
		response.setResponseMessage("Failed at interface level");
		AdapterInteraction.begin();

		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESB_CREDIT_CARD_UNBLOCK");
		RESTClientFactory clientFactory = RESTClientFactory.getInstance();
		IRESTClient restClient = clientFactory.getRESTClientInstance();

		String requestId = GetRefNumber("BC", 12);
		String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String pwd = digx_consulting.get("ESB_OBDX_PWD", "");
		String requestToken = JEncrypt.GenerateTransactionToken(sourceCode, requestId, pwd);
		String ipx = digx_consulting.get("ESB_OBDX_IP", "");
		
		ESBCardBlockRequestDTO esbRequestDTO = new ESBCardBlockRequestDTO();
		HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
		hostHeaderInfo.setAffiliateCode("EGH"); //temporarily hardcoded
		hostHeaderInfo.setIpAddress(ipx);
		hostHeaderInfo.setRequestId(requestId);
		hostHeaderInfo.setRequestToken(requestToken);
		hostHeaderInfo.setRequestType("BLOCKCARD");
		hostHeaderInfo.setSourceChannelId("MOBILE");
		hostHeaderInfo.setSourceCode(sourceCode);
		esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);
		esbRequestDTO.setReasonCode(reason);
		esbRequestDTO.setCardNo(cardNo); 
		//MM/YY
		String expDate = expiryDate.substring(3,5) + expiryDate.substring(0,2);
		esbRequestDTO.setExpiryDate(expDate);

		ESBCardResponseDTO resp = null;

		try {
			XStream xs = new XStream();
			logger.info("Card UnBlock Request : " + xs.toXML(esbRequestDTO));
			logger.info("Card UnBlock Source Code : " + sourceCode);

			resp = (ESBCardResponseDTO) restClient.post(prop, ESBCardResponseDTO.class, esbRequestDTO);
			logger.info("Generate CustomerNo     : " + resp.getCustomerNo());
			logger.info("Generate Card-Response  : " + xs.toXML(resp));
			logger.info("getResponseCode         : " + resp.getHostHeaderInfo().getResponseCode());
			logger.info("getResponseMessage      : " + resp.getHostHeaderInfo().getResponseMessage());
			if (resp != null && resp.getHostHeaderInfo().getResponseCode().equals("000")) {
				response.setResponseCode("000");
				response.setResponseMessage(resp.getHostHeaderInfo().getResponseMessage());
				//response.setActivityDescription("UNBLOCK CARD");
				//response.setCardId("");
				//response.setRefId(genRef);
			}
		} finally {
		}
		AdapterInteraction.close();

		return response;
	}
	
	
	
	public  String GetRefNumber(String type, int len) {

        String finalString = "";
        int x = 0;
        char[] stringChars = new char[len];
        for (int i = 0; i < len; i++) //4
        {
            Random random = new Random();
            x = random.nextInt(9);

            stringChars[i] = Integer.toString(x).toCharArray()[0];
        }


        finalString = new String(stringChars);
        finalString = type + finalString;
        return finalString.trim();
    }
}
