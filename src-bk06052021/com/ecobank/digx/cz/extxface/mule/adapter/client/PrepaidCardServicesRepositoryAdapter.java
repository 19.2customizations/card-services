package com.ecobank.digx.cz.extxface.mule.adapter.client;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.security.InvalidKeyException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.Random;
import java.util.logging.Logger;
import java.util.prefs.Preferences;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import com.ecobank.digx.cz.app.cardservices.dto.CardBalanceDTO;
import com.ecobank.digx.cz.app.cardservices.dto.CardBalanceRequestDTO;
import com.ecobank.digx.cz.app.cardservices.dto.CardBalanceResponse;
import com.ecobank.digx.cz.app.cardservices.dto.CardServiceResponseDTO;
import com.ecobank.digx.cz.app.cardservices.dto.CardStatementListResponse;
import com.ecobank.digx.cz.app.cardservices.dto.CardStatementRequestDTO;
import com.ecobank.digx.cz.app.domain.cardservices.entity.CardActivityInfo;
import com.ecobank.digx.cz.extxface.mule.dto.ESBCardRequestDTO;
import com.ecobank.digx.cz.extxface.mule.dto.ESBCardResponseDTO;
import com.ecobank.digx.cz.extxface.mule.dto.ESBPrepaidCardDetailResponseDTO;
import com.ecobank.digx.cz.extxface.mule.dto.HostHeaderInfo;
import com.google.gson.Gson;
import com.ofss.digx.app.core.AdapterInteraction;
import com.ofss.digx.extxface.response.AbstractResponseHandler;
import com.ofss.digx.framework.rest.client.RESTClientRequestBuilder;
import com.ofss.digx.framework.rest.client.RESTClientRequestConfig;
import com.ofss.digx.infra.exceptions.Exception;
import com.ofss.fc.infra.config.ConfigurationFactory;
import com.ofss.fc.infra.log.impl.MultiEntityLogger;
import com.thoughtworks.xstream.XStream;

public class PrepaidCardServicesRepositoryAdapter extends AbstractResponseHandler{
	
	private static PrepaidCardServicesRepositoryAdapter singletonInstance;

	private Preferences digx_consulting = ConfigurationFactory.getInstance().getConfigurations("DIGXCONSULTING");

	private static final String THIS_COMPONENT_NAME = PrepaidCardServicesRepositoryAdapter.class.getName();
	private static transient Logger logger = MultiEntityLogger.getUniqueInstance().getLogger(THIS_COMPONENT_NAME);
	
	public static PrepaidCardServicesRepositoryAdapter getInstance() {
		if (singletonInstance == null)
			synchronized (PrepaidCardServicesRepositoryAdapter.class) {
				if (singletonInstance == null)
					singletonInstance = new PrepaidCardServicesRepositoryAdapter();
			}
		return singletonInstance;
	}	
	
	
	public CardServiceResponseDTO fundCard(CardActivityInfo caInfo,String branchCode) throws Exception {
		CardServiceResponseDTO response = new CardServiceResponseDTO();
		response.setResponseCode("E04");
		response.setResponseMessage("Failed at interface level");
		AdapterInteraction.begin();

		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESB_PREPAID_CARD_FUND");
		try {
			enableSSL();
		} catch (KeyManagementException | NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		
		String requestId = GetRefNumber("BC", 12);
		String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String pwd = digx_consulting.get("ESB_OBDX_PWD", "");
		String requestToken = JEncrypt.GenerateTransactionToken(sourceCode, requestId, pwd);
		String ip = digx_consulting.get("ESB_OBDX_IP", "");
		String key = digx_consulting.get("ESB_OBDX_AES_KEY", "");
		
		ESBCardResponseDTO esbCardResponseDTO = null;

		try {
			ESBCardRequestDTO esbRequestDTO = new ESBCardRequestDTO();
			HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
			hostHeaderInfo.setAffiliateCode("EGH"); //temporarily hardcoded
			hostHeaderInfo.setIpAddress(ip);
			hostHeaderInfo.setRequestId(caInfo.getRefKey().getRefId());
			hostHeaderInfo.setRequestToken(requestToken);
			hostHeaderInfo.setRequestType("MPAY");
			hostHeaderInfo.setSourceChannelId("MOBILE");
			hostHeaderInfo.setSourceCode(sourceCode);
			esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);
			String expiryDate = "";
			String name = caInfo.getName();
			String cvv ="";
			
			esbRequestDTO.setFirstName(AESEncrypterOriginal.encrypt(name, AESEncrypterOriginal.getEncryptionKey(key)));
			esbRequestDTO.setLastName(AESEncrypterOriginal.encrypt(name, AESEncrypterOriginal.getEncryptionKey(key)));
			esbRequestDTO.setCvv(AESEncrypterOriginal.encrypt(cvv, AESEncrypterOriginal.getEncryptionKey(key)));
			esbRequestDTO.setBalance(AESEncrypterOriginal.encrypt(""+caInfo.getAmount(), AESEncrypterOriginal.getEncryptionKey(key)));
			esbRequestDTO.setAccountid(AESEncrypterOriginal.encrypt(caInfo.getSourceAccount(), AESEncrypterOriginal.getEncryptionKey(key)));
			esbRequestDTO.setAccountBrn(AESEncrypterOriginal.encrypt(branchCode, AESEncrypterOriginal.getEncryptionKey(key)));
			esbRequestDTO.setCurrency(AESEncrypterOriginal.encrypt(caInfo.getCcy(), AESEncrypterOriginal.getEncryptionKey(key)));
			esbRequestDTO.setCustomerID(AESEncrypterOriginal.encrypt(caInfo.getCardCustomerId(), AESEncrypterOriginal.getEncryptionKey(key)));
			esbRequestDTO.setMobileNo(AESEncrypterOriginal.encrypt(caInfo.getMobileNo(), AESEncrypterOriginal.getEncryptionKey(key)));
			esbRequestDTO.setExpiryDate(AESEncrypterOriginal.encrypt(expiryDate, AESEncrypterOriginal.getEncryptionKey(key)));
			esbRequestDTO.setGtpCustID(AESEncrypterOriginal.encrypt(caInfo.getCardCustomerId(), AESEncrypterOriginal.getEncryptionKey(key)));
			esbRequestDTO.setCardType(AESEncrypterOriginal.encrypt("SHOPVCARD", AESEncrypterOriginal.getEncryptionKey(key)));
			StringBuffer resp = new StringBuffer();
			XStream xs = new XStream();
			logger.info("Fund Prepaid Card Request : " + xs.toXML(esbRequestDTO));
			logger.info("Fund Prepaid Card Source Code : " + sourceCode);
			
			logger.info("Prepaid Endpoint 1 : " + prop.getServiceURL());
			logger.info("Prepaid Endpoint 2 : " + prop.getContextURL());

			Gson gson = new Gson();

			URL obj = null;
			try {
				obj = new URL(prop.getContextURL() + "/" +prop.getServiceURL());
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
			
			HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
			try {
				con.setRequestMethod("POST");
			} catch (ProtocolException e) {
				e.printStackTrace();
			}
			con.setRequestProperty("Content-Type", "application/json");

			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(gson.toJson(esbRequestDTO));
			wr.flush();
			wr.close();
			String responseStatus = con.getResponseMessage();
			logger.info(responseStatus);
			int responseCode = con.getResponseCode();
			logger.info("responseCode " + responseCode);

			String inputLine;

			if (responseCode == 200) {
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				while ((inputLine = in.readLine()) != null) {
					resp.append(inputLine);
				}
				try {
					in.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				logger.info("response:" + resp.toString());
			} else {
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getErrorStream()));
				while ((inputLine = in.readLine()) != null) {
					resp.append(inputLine);
				}
				in.close();
				logger.info("response 2" + resp.toString());
			}

			esbCardResponseDTO = gson.fromJson(resp.toString(), ESBCardResponseDTO.class);
			logger.info("Fund Prepaid Card Response        : " + xs.toXML(resp));
			logger.info("Fund Prepaid Card getResponseCode : " + esbCardResponseDTO.getHostHeaderInfo().getResponseCode());
			logger.info("Fund Prepaid Card getResponseMsg  : " + esbCardResponseDTO.getHostHeaderInfo().getResponseMessage());
			response.setResponseCode(esbCardResponseDTO.getHostHeaderInfo().getResponseCode());
			response.setResponseMessage(esbCardResponseDTO.getHostHeaderInfo().getResponseMessage());
			
			if (esbCardResponseDTO != null && esbCardResponseDTO.getHostHeaderInfo().getResponseCode().equals("000")) {
				response.setTransactionRefNo(esbCardResponseDTO.getHostHeaderInfo().getRequestId());
			}
		} catch (InvalidKeyException | NoSuchPaddingException | NoSuchAlgorithmException | BadPaddingException
				| IllegalBlockSizeException | IOException e1) {
			e1.printStackTrace();
		}
		finally {
		}
		AdapterInteraction.close();

		return response;
	}
	
	
	public CardBalanceResponse balanceCheck(CardBalanceDTO requestDTO) throws Exception {
		CardBalanceResponse response = new CardBalanceResponse();
		response.setResponseCode("E04");
		response.setResponseMessage("Failed at interface level");
		AdapterInteraction.begin();

		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESC_PREPAID_CARD_BALANCE");
		try {
			enableSSL();
		} catch (KeyManagementException | NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		
		String requestId = GetRefNumber("BC", 12);
		String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String pwd = digx_consulting.get("ESB_OBDX_PWD", "");
		String requestToken = JEncrypt.GenerateTransactionToken(sourceCode, requestId, pwd);
		String ip = digx_consulting.get("ESB_OBDX_IP", "");
		String key = digx_consulting.get("ESB_OBDX_AES_KEY", "");

		ESBCardResponseDTO esbCardResponseDTO = null;

		try {
			
		 //String cardCustomerId, String mobileNo, String last4Digit
			String expiryDate = requestDTO.getExpiryDate();
			String pan = requestDTO.getMaskedPan();
			String last4 = pan;
			if(last4.length() > 4)
				last4 = pan.substring(pan.length() - 4);
			
			ESBCardRequestDTO esbRequestDTO = new ESBCardRequestDTO();
			HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
			hostHeaderInfo.setAffiliateCode("EGH"); //temporarily hardcoded
			hostHeaderInfo.setIpAddress(ip);
			hostHeaderInfo.setRequestId(requestId);
			hostHeaderInfo.setRequestToken(requestToken);
			hostHeaderInfo.setRequestType("MPAY");
			hostHeaderInfo.setSourceChannelId("MOBILE");
			hostHeaderInfo.setSourceCode(sourceCode);
			esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);
			esbRequestDTO.setLast4Digits(AESEncrypterOriginal.encrypt(last4, AESEncrypterOriginal.getEncryptionKey(key)));
			esbRequestDTO.setCvv(AESEncrypterOriginal.encrypt(requestDTO.getCvv(), AESEncrypterOriginal.getEncryptionKey(key)));
			esbRequestDTO.setExpiryDate(AESEncrypterOriginal.encrypt(expiryDate, AESEncrypterOriginal.getEncryptionKey(key)));
			esbRequestDTO.setGtpCustID(AESEncrypterOriginal.encrypt(requestDTO.getCardCustomerId(), AESEncrypterOriginal.getEncryptionKey(key)));
			esbRequestDTO.setCardType(AESEncrypterOriginal.encrypt("SHOPVCARD", AESEncrypterOriginal.getEncryptionKey(key)));
			
			StringBuffer resp = new StringBuffer();
			XStream xs = new XStream();
			logger.info("Fund Prepaid Card Request : " + xs.toXML(esbRequestDTO));
			logger.info("Fund Prepaid Card Source Code : " + sourceCode);
			
			logger.info("Prepaid Endpoint 1 : " + prop.getServiceURL());
			logger.info("Prepaid Endpoint 2 : " + prop.getContextURL());

			Gson gson = new Gson();

			URL obj = null;
			try {
				obj = new URL(prop.getContextURL() + "/" +prop.getServiceURL());
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
			
			HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
			try {
				con.setRequestMethod("POST");
			} catch (ProtocolException e) {
				e.printStackTrace();
			}
			con.setRequestProperty("Content-Type", "application/json");

			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(gson.toJson(esbRequestDTO));
			wr.flush();
			wr.close();
			String responseStatus = con.getResponseMessage();
			logger.info(responseStatus);
			int responseCode = con.getResponseCode();
			logger.info("responseCode " + responseCode);

			String inputLine;

			if (responseCode == 200) {
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				while ((inputLine = in.readLine()) != null) {
					resp.append(inputLine);
				}
				try {
					in.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				logger.info("response:" + resp.toString());
			} else {
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getErrorStream()));
				while ((inputLine = in.readLine()) != null) {
					resp.append(inputLine);
				}
				in.close();
				logger.info("response 2" + resp.toString());
			}

			esbCardResponseDTO = gson.fromJson(resp.toString(), ESBCardResponseDTO.class);
			logger.info("Fund Prepaid Card Response        : " + xs.toXML(resp));
			logger.info("Fund Prepaid Card getResponseCode : " + esbCardResponseDTO.getHostHeaderInfo().getResponseCode());
			logger.info("Fund Prepaid Card getResponseMsg  : " + esbCardResponseDTO.getHostHeaderInfo().getResponseMessage());
			response.setResponseCode(esbCardResponseDTO.getHostHeaderInfo().getResponseCode());
			response.setResponseMessage(esbCardResponseDTO.getHostHeaderInfo().getResponseMessage());
			
			if (esbCardResponseDTO != null && esbCardResponseDTO.getHostHeaderInfo().getResponseCode().equals("000")) {
				response.setBalance(new BigDecimal(AESEncrypterOriginal.decrypt((esbCardResponseDTO.getBalance()), AESEncrypterOriginal.getEncryptionKey(key))));  
				response.setNameOnCard(AESEncrypterOriginal.decrypt((esbCardResponseDTO.getName()), AESEncrypterOriginal.getEncryptionKey(key)));  
				response.setCcyCode(AESEncrypterOriginal.decrypt((esbCardResponseDTO.getCcy()), AESEncrypterOriginal.getEncryptionKey(key)));  
				response.setPseudoPAN(AESEncrypterOriginal.decrypt((esbCardResponseDTO.getPsuedoPan()), AESEncrypterOriginal.getEncryptionKey(key)));  
				response.setExpiry(expiryDate);
				//response.setCardAccount(AESEncrypterOriginal.decrypt((esbCardResponseDTO.), AESEncrypterOriginal.getEncryptionKey(key)));  
				//response.setCcyCode(esbCardResponseDTO.getCcy());
				response.setCardType("PREPAID");
				
				CardBalanceResponse resp3 = this.fetchCardDetail(requestDTO);
			}
		} catch (InvalidKeyException | NoSuchPaddingException | NoSuchAlgorithmException | BadPaddingException
				| IllegalBlockSizeException | IOException e1) {
			e1.printStackTrace();
		}
		finally {
		}
		AdapterInteraction.close();

		return response;
	}
	
	public CardBalanceResponse fetchCardDetail(CardBalanceDTO requestDTO) throws Exception {
		CardBalanceResponse response = new CardBalanceResponse();
		response.setResponseCode("E04");
		response.setResponseMessage("Failed at interface level");
		AdapterInteraction.begin();

		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESC_PREPAID_CARD_DETAIL");
		try {
			enableSSL();
		} catch (KeyManagementException | NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		
		String requestId = GetRefNumber("BC", 12);
		String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String pwd = digx_consulting.get("ESB_OBDX_PWD", "");
		String requestToken = JEncrypt.GenerateTransactionToken(sourceCode, requestId, pwd);
		String ip = digx_consulting.get("ESB_OBDX_IP", "");
		String key = digx_consulting.get("ESB_OBDX_AES_KEY", "");

		ESBPrepaidCardDetailResponseDTO esbCardResponseDTO = null;

		try {
			
		 //String cardCustomerId, String mobileNo, String last4Digit
			//String expiryDate = requestDTO.getExpiryDate();
			
			ESBCardRequestDTO esbRequestDTO = new ESBCardRequestDTO();
			HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
			hostHeaderInfo.setAffiliateCode("EGH"); //temporarily hardcoded
			hostHeaderInfo.setIpAddress(ip);
			hostHeaderInfo.setRequestId(requestId);
			hostHeaderInfo.setRequestToken(requestToken);
			hostHeaderInfo.setRequestType("MPAY");
			hostHeaderInfo.setSourceChannelId("MOBILE");
			hostHeaderInfo.setSourceCode(sourceCode);
			esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);
			esbRequestDTO.setGtpCustID(AESEncrypterOriginal.encrypt(requestDTO.getCardCustomerId(), AESEncrypterOriginal.getEncryptionKey(key)));
			esbRequestDTO.setCardType(AESEncrypterOriginal.encrypt("SHOPVCARD", AESEncrypterOriginal.getEncryptionKey(key)));
			
			StringBuffer resp = new StringBuffer();
			XStream xs = new XStream();
			logger.info("Fund Prepaid Card Request : " + xs.toXML(esbRequestDTO));
			logger.info("Fund Prepaid Card Source Code : " + sourceCode);
			
			logger.info("Prepaid Endpoint 1 : " + prop.getServiceURL());
			logger.info("Prepaid Endpoint 2 : " + prop.getContextURL());

			Gson gson = new Gson();

			URL obj = null;
			try {
				obj = new URL(prop.getContextURL() + "/" +prop.getServiceURL());
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
			
			HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
			try {
				con.setRequestMethod("POST");
			} catch (ProtocolException e) {
				e.printStackTrace();
			}
			con.setRequestProperty("Content-Type", "application/json");

			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(gson.toJson(esbRequestDTO));
			wr.flush();
			wr.close();
			String responseStatus = con.getResponseMessage();
			logger.info(responseStatus);
			int responseCode = con.getResponseCode();
			logger.info("responseCode " + responseCode);

			String inputLine;

			if (responseCode == 200) {
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				while ((inputLine = in.readLine()) != null) {
					resp.append(inputLine);
				}
				try {
					in.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				logger.info("response:" + resp.toString());
			} else {
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getErrorStream()));
				while ((inputLine = in.readLine()) != null) {
					resp.append(inputLine);
				}
				in.close();
				logger.info("response 2" + resp.toString());
			}

			esbCardResponseDTO = gson.fromJson(resp.toString(), ESBPrepaidCardDetailResponseDTO.class);
			logger.info("Fund Prepaid Card Response        : " + xs.toXML(resp));
			logger.info("Fund Prepaid Card getResponseCode : " + esbCardResponseDTO.getHostHeaderInfo().getResponseCode());
			logger.info("Fund Prepaid Card getResponseMsg  : " + esbCardResponseDTO.getHostHeaderInfo().getResponseMessage());
			response.setResponseCode(esbCardResponseDTO.getHostHeaderInfo().getResponseCode());
			response.setResponseMessage(esbCardResponseDTO.getHostHeaderInfo().getResponseMessage());
			
			if (esbCardResponseDTO != null && esbCardResponseDTO.getHostHeaderInfo().getResponseCode().equals("000")) {
				//response.setBalance(new BigDecimal(AESEncrypterOriginal.decrypt((esbCardResponseDTO.getBalance()), AESEncrypterOriginal.getEncryptionKey(key))));  
				response.setNameOnCard(AESEncrypterOriginal.decrypt((esbCardResponseDTO.getPreferredName()), AESEncrypterOriginal.getEncryptionKey(key)));  
				response.setEmail(AESEncrypterOriginal.decrypt((esbCardResponseDTO.getEmail()), AESEncrypterOriginal.getEncryptionKey(key)));  
				response.setMobileNo(AESEncrypterOriginal.decrypt((esbCardResponseDTO.getMobileNo()), AESEncrypterOriginal.getEncryptionKey(key)));  
				
				
				//response.setCardAccount(AESEncrypterOriginal.decrypt((esbCardResponseDTO.), AESEncrypterOriginal.getEncryptionKey(key)));  
				//response.setCcyCode(esbCardResponseDTO.getCcy());
				response.setCardType("PREPAID");
			}
		} catch (InvalidKeyException | NoSuchPaddingException | NoSuchAlgorithmException | BadPaddingException
				| IllegalBlockSizeException | IOException e1) {
			e1.printStackTrace();
		}
		finally {
		}
		AdapterInteraction.close();

		return response;
	}

	
	
	
	public CardServiceResponseDTO blockCard(String cardNo, String mobileNo) throws Exception {
		CardServiceResponseDTO response = new CardServiceResponseDTO();
		response.setResponseCode("E04");
		response.setResponseMessage("Failed at interface level");
		AdapterInteraction.begin();

		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESB_PREPAID_CARD_BLOCK");
		try {
			enableSSL();
		} catch (KeyManagementException | NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		
		String requestId = GetRefNumber("BC", 12);
		String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String pwd = digx_consulting.get("ESB_OBDX_PWD", "");
		String requestToken = JEncrypt.GenerateTransactionToken(sourceCode, requestId, pwd);
		String ip = digx_consulting.get("ESB_OBDX_IP", "");
		String key = digx_consulting.get("ESB_OBDX_AES_KEY", "");
		
		ESBCardResponseDTO esbCardResponseDTO = null;

		try {
			ESBCardRequestDTO esbRequestDTO = new ESBCardRequestDTO();
			HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
			hostHeaderInfo.setAffiliateCode("EGH"); //temporarily hardcoded
			hostHeaderInfo.setIpAddress(ip);
			hostHeaderInfo.setRequestId(requestId);
			hostHeaderInfo.setRequestToken(requestToken);
			hostHeaderInfo.setRequestType("BLOCKVCARD");
			hostHeaderInfo.setSourceChannelId("MOBILE");
			hostHeaderInfo.setSourceCode(sourceCode);
			esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);
			
			esbRequestDTO.setCardNo(AESEncrypterOriginal.encrypt(cardNo, AESEncrypterOriginal.getEncryptionKey(key)));
			esbRequestDTO.setMsisdn(AESEncrypterOriginal.encrypt(mobileNo, AESEncrypterOriginal.getEncryptionKey(key)));
			
			StringBuffer resp = new StringBuffer();
			XStream xs = new XStream();
			logger.info("Block Prepaid Card Request : " + xs.toXML(esbRequestDTO));
			logger.info("Block Prepaid Card Source Code : " + sourceCode);
			
			logger.info("Block Prepaid Endpoint 1 : " + prop.getServiceURL());
			logger.info("Block Prepaid Endpoint 2 : " + prop.getContextURL());

			Gson gson = new Gson();

			URL obj = null;
			try {
				obj = new URL(prop.getContextURL() + "/" +prop.getServiceURL());
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
			
			HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
			try {
				con.setRequestMethod("POST");
			} catch (ProtocolException e) {
				e.printStackTrace();
			}
			con.setRequestProperty("Content-Type", "application/json");

			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(gson.toJson(esbRequestDTO));
			wr.flush();
			wr.close();
			String responseStatus = con.getResponseMessage();
			logger.info(responseStatus);
			int responseCode = con.getResponseCode();
			logger.info("responseCode " + responseCode);

			String inputLine;

			if (responseCode == 200) {
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				while ((inputLine = in.readLine()) != null) {
					resp.append(inputLine);
				}
				try {
					in.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				logger.info("response:" + resp.toString());
			} else {
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getErrorStream()));
				while ((inputLine = in.readLine()) != null) {
					resp.append(inputLine);
				}
				in.close();
				logger.info("response 2" + resp.toString());
			}

			esbCardResponseDTO = gson.fromJson(resp.toString(), ESBCardResponseDTO.class);
			logger.info("Block Prepaid Card Response        : " + xs.toXML(resp));
			logger.info("Block Prepaid Card getResponseCode : " + esbCardResponseDTO.getHostHeaderInfo().getResponseCode());
			logger.info("Block Prepaid Card getResponseMsg  : " + esbCardResponseDTO.getHostHeaderInfo().getResponseMessage());
			response.setResponseCode(esbCardResponseDTO.getHostHeaderInfo().getResponseCode());
			response.setResponseMessage(esbCardResponseDTO.getHostHeaderInfo().getResponseMessage());
			
			if (esbCardResponseDTO != null && esbCardResponseDTO.getHostHeaderInfo().getResponseCode().equals("000")) {
				response.setTransactionRefNo(esbCardResponseDTO.getHostHeaderInfo().getRequestId());
			}
		} catch (InvalidKeyException | NoSuchPaddingException | NoSuchAlgorithmException | BadPaddingException
				| IllegalBlockSizeException | IOException e1) {
			e1.printStackTrace();
		}
		finally {
		}
		AdapterInteraction.close();

		return response;
	}

	
	public CardServiceResponseDTO unblockCard(String cardNo, String mobileNo) throws Exception {
		CardServiceResponseDTO response = new CardServiceResponseDTO();
		response.setResponseCode("E04");
		response.setResponseMessage("Failed at interface level");
		AdapterInteraction.begin();

		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESB_PREPAID_CARD_UNBLOCK");
		try {
			enableSSL();
		} catch (KeyManagementException | NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		
		String requestId = GetRefNumber("BC", 12);
		String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String pwd = digx_consulting.get("ESB_OBDX_PWD", "");
		String requestToken = JEncrypt.GenerateTransactionToken(sourceCode, requestId, pwd);
		String ip = digx_consulting.get("ESB_OBDX_IP", "");
		String key = digx_consulting.get("ESB_OBDX_AES_KEY", "");
		
		ESBCardResponseDTO esbCardResponseDTO = null;

		try {
			ESBCardRequestDTO esbRequestDTO = new ESBCardRequestDTO();
			HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
			hostHeaderInfo.setAffiliateCode("EGH"); //temporarily hardcoded
			hostHeaderInfo.setIpAddress(ip);
			hostHeaderInfo.setRequestId(requestId);
			hostHeaderInfo.setRequestToken(requestToken);
			hostHeaderInfo.setRequestType("MPAY");
			hostHeaderInfo.setSourceChannelId("MOBILE");
			hostHeaderInfo.setSourceCode(sourceCode);
			esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);
			
			esbRequestDTO.setCardNo(AESEncrypterOriginal.encrypt(cardNo, AESEncrypterOriginal.getEncryptionKey(key)));
			esbRequestDTO.setMsisdn(AESEncrypterOriginal.encrypt(mobileNo, AESEncrypterOriginal.getEncryptionKey(key)));
			
			StringBuffer resp = new StringBuffer();
			XStream xs = new XStream();
			logger.info("UNBlock Prepaid Card Request : " + xs.toXML(esbRequestDTO));
			logger.info("UNBlock Prepaid Card Source Code : " + sourceCode);
			
			logger.info("UNBlock Prepaid Endpoint 1 : " + prop.getServiceURL());
			logger.info("UNBlock Prepaid Endpoint 2 : " + prop.getContextURL());

			Gson gson = new Gson();

			URL obj = null;
			try {
				obj = new URL(prop.getContextURL() + "/" +prop.getServiceURL());
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
			
			HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
			try {
				con.setRequestMethod("POST");
			} catch (ProtocolException e) {
				e.printStackTrace();
			}
			con.setRequestProperty("Content-Type", "application/json");

			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(gson.toJson(esbRequestDTO));
			wr.flush();
			wr.close();
			String responseStatus = con.getResponseMessage();
			logger.info(responseStatus);
			int responseCode = con.getResponseCode();
			logger.info("responseCode " + responseCode);

			String inputLine;

			if (responseCode == 200) {
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				while ((inputLine = in.readLine()) != null) {
					resp.append(inputLine);
				}
				try {
					in.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				logger.info("response:" + resp.toString());
			} else {
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getErrorStream()));
				while ((inputLine = in.readLine()) != null) {
					resp.append(inputLine);
				}
				in.close();
				logger.info("response 2" + resp.toString());
			}

			esbCardResponseDTO = gson.fromJson(resp.toString(), ESBCardResponseDTO.class);
			logger.info("UNBlock Prepaid Card Response        : " + xs.toXML(resp));
			logger.info("UNBlock Prepaid Card getResponseCode : " + esbCardResponseDTO.getHostHeaderInfo().getResponseCode());
			logger.info("UNBlock Prepaid Card getResponseMsg  : " + esbCardResponseDTO.getHostHeaderInfo().getResponseMessage());
			response.setResponseCode(esbCardResponseDTO.getHostHeaderInfo().getResponseCode());
			response.setResponseMessage(esbCardResponseDTO.getHostHeaderInfo().getResponseMessage());
			
			if (esbCardResponseDTO != null && esbCardResponseDTO.getHostHeaderInfo().getResponseCode().equals("000")) {
				response.setTransactionRefNo(esbCardResponseDTO.getHostHeaderInfo().getRequestId());
			}
		} catch (InvalidKeyException | NoSuchPaddingException | NoSuchAlgorithmException | BadPaddingException
				| IllegalBlockSizeException | IOException e1) {
			e1.printStackTrace();
		}
		finally {
		}
		AdapterInteraction.close();

		return response;
	}
	
	
	
	public CardStatementListResponse getStatement(CardStatementRequestDTO cardStatementRequestDTO) throws Exception {
		CardStatementListResponse cardStatementListResponse = new CardStatementListResponse();
		cardStatementListResponse.setResponseCode("E04");
		cardStatementListResponse.setResponseMessage("Failed at interface level");
		AdapterInteraction.begin();

		RESTClientRequestBuilder clientBuilder = RESTClientRequestBuilder.getInstance();
		RESTClientRequestConfig prop = clientBuilder.loadConfig("ESB_PREAPAID_CARD_STATEMENT");
		
		try {
			enableSSL();
		} catch (KeyManagementException | NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		
		String requestId = GetRefNumber("ST", 12);
		String sourceCode = digx_consulting.get("ESB_OBDX_SOURCE_CODE", "");
		String pwd = digx_consulting.get("ESB_OBDX_PWD", "");
		String requestToken = JEncrypt.GenerateTransactionToken(sourceCode, requestId, pwd);
		String ip = digx_consulting.get("ESB_OBDX_IP", "");
		String key = digx_consulting.get("ESB_OBDX_AES_KEY", "");
		
		ESBCardResponseDTO esbCardResponseDTO = null;

		try {
			ESBCardRequestDTO esbRequestDTO = new ESBCardRequestDTO();
			HostHeaderInfo hostHeaderInfo = new HostHeaderInfo();
			hostHeaderInfo.setAffiliateCode("EGH"); //temporarily hardcoded
			hostHeaderInfo.setIpAddress(ip);
			hostHeaderInfo.setRequestId(requestId);
			hostHeaderInfo.setRequestToken(requestToken);
			hostHeaderInfo.setRequestType("MPAY");
			hostHeaderInfo.setSourceChannelId("MOBILE");
			hostHeaderInfo.setSourceCode(sourceCode);
			esbRequestDTO.setHostHeaderInfo(hostHeaderInfo);
			
			esbRequestDTO.setLast4Digits(AESEncrypterOriginal.encrypt("", AESEncrypterOriginal.getEncryptionKey(key)));
			esbRequestDTO.setGtpCustID(AESEncrypterOriginal.encrypt("", AESEncrypterOriginal.getEncryptionKey(key)));
			esbRequestDTO.setCardType(AESEncrypterOriginal.encrypt("SHOPVCARD", AESEncrypterOriginal.getEncryptionKey(key)));
			
			//date format : 01-JUL-2019 : DD-MON-YYYY
			esbRequestDTO.setStrRange(AESEncrypterOriginal.encrypt(cardStatementRequestDTO.getStartDate(), AESEncrypterOriginal.getEncryptionKey(key)));
			esbRequestDTO.setEndRange(AESEncrypterOriginal.encrypt(cardStatementRequestDTO.getEndDate(), AESEncrypterOriginal.getEncryptionKey(key)));
			
			esbRequestDTO.setNumOfTrans(AESEncrypterOriginal.encrypt(""+cardStatementRequestDTO.getTransCount(), AESEncrypterOriginal.getEncryptionKey(key)));
			
			StringBuffer resp = new StringBuffer();
			XStream xs = new XStream();
			logger.info("Prepaid Card Statement Request : " + xs.toXML(esbRequestDTO));
			logger.info("Prepaid Card Statement Source Code : " + sourceCode);
			
			logger.info("Prepaid Card  Endpoint 1 : " + prop.getServiceURL());
			logger.info("Prepaid Card  Endpoint 2 : " + prop.getContextURL());

			Gson gson = new Gson();

			URL obj = null;
			try {
				obj = new URL(prop.getContextURL() + "/" +prop.getServiceURL());
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
			
			HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
			try {
				con.setRequestMethod("POST");
			} catch (ProtocolException e) {
				e.printStackTrace();
			}
			con.setRequestProperty("Content-Type", "application/json");

			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(gson.toJson(esbRequestDTO));
			wr.flush();
			wr.close();
			String responseStatus = con.getResponseMessage();
			logger.info(responseStatus);
			int responseCode = con.getResponseCode();
			logger.info("responseCode " + responseCode);

			String inputLine;

			if (responseCode == 200) {
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				while ((inputLine = in.readLine()) != null) {
					resp.append(inputLine);
				}
				try {
					in.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				logger.info("response:" + resp.toString());
			} else {
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getErrorStream()));
				while ((inputLine = in.readLine()) != null) {
					resp.append(inputLine);
				}
				in.close();
				logger.info("response 2" + resp.toString());
			}

			esbCardResponseDTO = gson.fromJson(resp.toString(), ESBCardResponseDTO.class);
			logger.info("Prepaid Card Statement Response        : " + xs.toXML(resp));
			logger.info("Prepaid Card Statement ResponseCode : " + esbCardResponseDTO.getHostHeaderInfo().getResponseCode());
			logger.info("Prepaid Card Statement ResponseMsg  : " + esbCardResponseDTO.getHostHeaderInfo().getResponseMessage());
			//response.setResponseCode(esbCardResponseDTO.getHostHeaderInfo().getResponseCode());
			//response.setResponseMessage(esbCardResponseDTO.getHostHeaderInfo().getResponseMessage());
			
			//if (esbCardResponseDTO != null && esbCardResponseDTO.getHostHeaderInfo().getResponseCode().equals("000")) {
			//	response.setTransactionRefNo(esbCardResponseDTO.getHostHeaderInfo().getRequestId());
			//}
		} catch (InvalidKeyException | NoSuchPaddingException | NoSuchAlgorithmException | BadPaddingException
				| IllegalBlockSizeException | IOException e1) {
			e1.printStackTrace();
		}
		finally {
		}
		AdapterInteraction.close();

		return null;
	}

	
	
	
	
	public  String GetRefNumber(String type, int len) {

        String finalString = "";
        int x = 0;
        char[] stringChars = new char[len];
        for (int i = 0; i < len; i++) //4
        {
            Random random = new Random();
            x = random.nextInt(9);

            stringChars[i] = Integer.toString(x).toCharArray()[0];
        }


        finalString = new String(stringChars);
        finalString = type + finalString;
        return finalString.trim();
    }
	
	
	public static void enableSSL() throws NoSuchAlgorithmException, KeyManagementException {

		// Create a trust manager that does not validate certificate chains
		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			public void checkClientTrusted(X509Certificate[] certs, String authType) {
			}

			public void checkServerTrusted(X509Certificate[] certs, String authType) {
			}
		} };

		// Install the all-trusting trust manager
		SSLContext sc = SSLContext.getInstance("SSL");
		sc.init(null, trustAllCerts, new java.security.SecureRandom());
		HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

		// Create all-trusting host name verifier
		HostnameVerifier allHostsValid = new HostnameVerifier() {
			public boolean verify(String hostname, SSLSession session) {
				return true;
			}
		};

		// Install the all-trusting host verifier
		HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
	}

}
