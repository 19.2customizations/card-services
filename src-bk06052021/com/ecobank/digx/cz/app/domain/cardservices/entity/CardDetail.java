package com.ecobank.digx.cz.app.domain.cardservices.entity;

import java.util.List;

import com.ecobank.digx.cz.app.domain.cardservices.entity.repository.CardDetailRepository;
import com.ofss.digx.infra.exceptions.Exception;
import com.ofss.fc.datatype.Date;
import com.ofss.fc.framework.domain.IPersistenceObject;
import com.ofss.fc.infra.das.exception.PersistenceException;

import io.swagger.v3.oas.annotations.media.Schema;

public class CardDetail implements IPersistenceObject{

	private static final long serialVersionUID = -6750856467006872305L;
	
	
	private String maskedPan;
	private String cardType;  //
	private String schemeType;
	private String cardCategory;  //DEBIT, CREDIT, VIRTUAL, PREPAID
	private String customerId;
	private String mobileNo;
	private String linkAccounts;
	private String sourceAccount;
	private String nameOnCard;
	private String cardAccount;
	private String expiryDate;
	private String cardStatus;
	private String cardCcy;
	private String cardCustomerId;
	private Date createdDate;
	private String createdBy;
	private String modifiedBy;
	private Date modifiedDate;
	private String fullPan;
	
	private String userId;
	@Schema(description = "CardLimit must be greater than zero. ", type = "Double", required = true)
	private double cardLimit;
	private CardDetailKey refKey;
	
	public void create(CardDetail cardDetail) throws Exception {
		CardDetailRepository.getInstance().create(cardDetail);
	}
	
	public void update(CardDetail cardDetail) throws Exception {
		CardDetailRepository.getInstance().update(cardDetail);
	}

	public CardDetail read(CardDetailKey key) throws Exception {
		System.out.println("refID ::: " + key.getCardId());
		return CardDetailRepository.getInstance().read(key);
	}
	
	public List<CardDetail> listCardsByCustomerId(String customerId, String cardCategory) throws Exception 
	{
		return CardDetailRepository.getInstance().listCardsByCustomerId(customerId, cardCategory);
	}
	
	@Override
	public boolean isEntityReadOnly() {
		return false;
	}

	@Override
	public void markReadOnly(boolean arg0) throws PersistenceException {
	}

	
	

	public String getMaskedPan() {
		return maskedPan;
	}

	public void setMaskedPan(String maskedPan) {
		this.maskedPan = maskedPan;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public String getSchemeType() {
		return schemeType;
	}

	public void setSchemeType(String schemeType) {
		this.schemeType = schemeType;
	}

	public String getCardCategory() {
		return cardCategory;
	}

	public void setCardCategory(String cardCategory) {
		this.cardCategory = cardCategory;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getLinkAccounts() {
		return linkAccounts;
	}

	public void setLinkAccounts(String linkAccounts) {
		this.linkAccounts = linkAccounts;
	}

	public String getSourceAccount() {
		return sourceAccount;
	}

	public void setSourceAccount(String sourceAccount) {
		this.sourceAccount = sourceAccount;
	}

	public String getNameOnCard() {
		return nameOnCard;
	}

	public void setNameOnCard(String nameOnCard) {
		this.nameOnCard = nameOnCard;
	}

	public String getCardAccount() {
		return cardAccount;
	}

	public void setCardAccount(String cardAccount) {
		this.cardAccount = cardAccount;
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getCardStatus() {
		return cardStatus;
	}

	public void setCardStatus(String cardStatus) {
		this.cardStatus = cardStatus;
	}

	public String getCardCcy() {
		return cardCcy;
	}

	public void setCardCcy(String cardCcy) {
		this.cardCcy = cardCcy;
	}

	public String getCardCustomerId() {
		return cardCustomerId;
	}

	public void setCardCustomerId(String cardCustomerId) {
		this.cardCustomerId = cardCustomerId;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public CardDetailKey getRefKey() {
		return refKey;
	}

	public void setRefKey(CardDetailKey refKey) {
		this.refKey = refKey;
	}

	public double getCardLimit() {
		return cardLimit;
	}

	public void setCardLimit(double cardLimit) {
		this.cardLimit = cardLimit;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getFullPan() {
		return fullPan;
	}

	public void setFullPan(String fullPan) {
		this.fullPan = fullPan;
	}

	
	
}