package com.ecobank.digx.cz.app.domain.cardservices.entity;

import java.math.BigDecimal;
import java.util.List;

import com.ecobank.digx.cz.app.domain.cardservices.entity.repository.CardActivityInfoRepository;
import com.ofss.digx.infra.exceptions.Exception;
import com.ofss.fc.datatype.Date;
import com.ofss.fc.framework.domain.IPersistenceObject;
import com.ofss.fc.infra.das.exception.PersistenceException;

public class CardActivityInfo implements IPersistenceObject{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//private static final long serialVersionUID = 222786759021659990L;
	
	private String cardId;
	private Date activityDate;
	private String activityType;
	private String activityDescription;
	private String status;
	private String responseCode;
	private String responseMessage;
	private String tranRefNo;
	private BigDecimal fee;
	private String userId;
	private CardActivityInfoKey refKey;
	
	private String affiliateCode;
	private String sourceAccount;
	private String cbaRspCode;
	private String cbaRspMsg;
	private String cbaReferenceNo;
	
	private String cardCategory;
	private String cardAccount;
	private String maskedPan;
	private String cardDat;
	private String udf1;
	private String udf2;
	private String udf3;
	
	
	private BigDecimal amount;
	private String ccy;
	private String cardCustomerId;
	private String branchCode;
	private String name;
	private String cardType;
	private String mobileNo;
	private String email;
	
	
	public void create(CardActivityInfo cardActivityInfo) throws Exception {
		System.out.println("create refID ::: " + cardActivityInfo.getRefKey().getRefId());
		CardActivityInfoRepository.getInstance().create(cardActivityInfo);
	}
	
	public void update(CardActivityInfo cardActivityInfo) throws Exception {
		CardActivityInfoRepository.getInstance().update(cardActivityInfo);
	}
	
	public CardActivityInfo read(CardActivityInfoKey key) throws Exception {
		System.out.println("refID ::: " + key.getRefId());
		return CardActivityInfoRepository.getInstance().read(key);
	}
	
	public List<CardActivityInfo> listCardActivityById(String cardId ) throws Exception {
		System.out.println("refID ::: " + cardId);
		return CardActivityInfoRepository.getInstance().listCardActivityById(cardId);
	}
	
	@Override
	public boolean isEntityReadOnly() {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public void markReadOnly(boolean arg0) throws PersistenceException {
		// TODO Auto-generated method stub
		
	}

	

	public String getCardId() {
		return cardId;
	}

	public void setCardId(String cardId) {
		this.cardId = cardId;
	}

	public Date getActivityDate() {
		return activityDate;
	}

	public void setActivityDate(Date activityDate) {
		this.activityDate = activityDate;
	}

	public String getActivityType() {
		return activityType;
	}

	public void setActivityType(String activityType) {
		this.activityType = activityType;
	}

	public String getActivityDescription() {
		return activityDescription;
	}

	public void setActivityDescription(String activityDescription) {
		this.activityDescription = activityDescription;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public String getTranRefNo() {
		return tranRefNo;
	}

	public void setTranRefNo(String tranRefNo) {
		this.tranRefNo = tranRefNo;
	}

	

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public CardActivityInfoKey getRefKey() {
		return refKey;
	}

	public void setRefKey(CardActivityInfoKey refKey) {
		this.refKey = refKey;
	}

	public String getAffiliateCode() {
		return affiliateCode;
	}

	public void setAffiliateCode(String affiliateCode) {
		this.affiliateCode = affiliateCode;
	}

	public String getSourceAccount() {
		return sourceAccount;
	}

	public void setSourceAccount(String sourceAccount) {
		this.sourceAccount = sourceAccount;
	}

	public String getCbaRspCode() {
		return cbaRspCode;
	}

	public void setCbaRspCode(String cbaRspCode) {
		this.cbaRspCode = cbaRspCode;
	}

	public String getCbaRspMsg() {
		return cbaRspMsg;
	}

	public void setCbaRspMsg(String cbaRspMsg) {
		this.cbaRspMsg = cbaRspMsg;
	}

	public String getCbaReferenceNo() {
		return cbaReferenceNo;
	}

	public void setCbaReferenceNo(String cbaReferenceNo) {
		this.cbaReferenceNo = cbaReferenceNo;
	}

	public String getCardCategory() {
		return cardCategory;
	}

	public void setCardCategory(String cardCategory) {
		this.cardCategory = cardCategory;
	}

	public String getCardAccount() {
		return cardAccount;
	}

	public void setCardAccount(String cardAccount) {
		this.cardAccount = cardAccount;
	}

	public String getMaskedPan() {
		return maskedPan;
	}

	public void setMaskedPan(String maskedPan) {
		this.maskedPan = maskedPan;
	}

	

	public String getUdf1() {
		return udf1;
	}

	public void setUdf1(String udf1) {
		this.udf1 = udf1;
	}

	public String getUdf2() {
		return udf2;
	}

	public void setUdf2(String udf2) {
		this.udf2 = udf2;
	}

	public String getUdf3() {
		return udf3;
	}

	public void setUdf3(String udf3) {
		this.udf3 = udf3;
	}

	public String getCardDat() {
		return cardDat;
	}

	public void setCardDat(String cardDat) {
		this.cardDat = cardDat;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public String getCardCustomerId() {
		return cardCustomerId;
	}

	public void setCardCustomerId(String cardCustomerId) {
		this.cardCustomerId = cardCustomerId;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getFee() {
		return fee;
	}

	public void setFee(BigDecimal fee) {
		this.fee = fee;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	
	
}
