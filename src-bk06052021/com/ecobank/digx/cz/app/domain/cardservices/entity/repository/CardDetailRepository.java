package com.ecobank.digx.cz.app.domain.cardservices.entity.repository;

import java.util.List;

import com.ecobank.digx.cz.app.domain.cardservices.entity.CardDetail;
import com.ecobank.digx.cz.app.domain.cardservices.entity.CardDetailKey;
import com.ecobank.digx.cz.app.domain.cardservices.entity.repository.adapter.ICardDetailRepositoryAdapter;
import com.ofss.digx.framework.domain.repository.AbstractDomainObjectRepository;
import com.ofss.digx.framework.domain.repository.RepositoryAdapterFactory;
import com.ofss.digx.infra.exceptions.Exception;

public class CardDetailRepository extends AbstractDomainObjectRepository<CardDetail, CardDetailKey> {
	private static CardDetailRepository singletonInstance;

	public static CardDetailRepository getInstance() {
		if (singletonInstance == null) {
			synchronized (CardDetailRepository.class) {
				if (singletonInstance == null) {
					singletonInstance = new CardDetailRepository();
				}
			}
		}
		return singletonInstance;
	}

	public void create(CardDetail object) throws Exception {
		ICardDetailRepositoryAdapter repositoryAdapter = (ICardDetailRepositoryAdapter) 
				RepositoryAdapterFactory.getInstance().getRepositoryAdapter("CARD_DETAIL_REPOSITORY_ADAPTER");
		repositoryAdapter.create(object);
	}
	
	@Override
	public void update(CardDetail object) throws Exception {
		ICardDetailRepositoryAdapter repositoryAdapter = (ICardDetailRepositoryAdapter) RepositoryAdapterFactory
				.getInstance().getRepositoryAdapter("CARD_DETAIL_REPOSITORY_ADAPTER");
		repositoryAdapter.update(object);
		
	}


	@Override
	public void delete(CardDetail arg0) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public CardDetail read(CardDetailKey key) throws Exception {
		System.out.println("[[KEY]]   ::: "+ key.getCardId());
		ICardDetailRepositoryAdapter repositoryAdapter = (ICardDetailRepositoryAdapter) RepositoryAdapterFactory
				.getInstance().getRepositoryAdapter("CARD_DETAIL_REPOSITORY_ADAPTER");
		return repositoryAdapter.read(key);
	}
	
	
	public List<CardDetail> listCardsByCustomerId(String customerId, String cardCategory) throws Exception {
		System.out.println("[[KEY-CUSTOMEr-CARD]]   ::: "+ customerId);
		ICardDetailRepositoryAdapter repositoryAdapter = (ICardDetailRepositoryAdapter) RepositoryAdapterFactory
				.getInstance().getRepositoryAdapter("CARD_DETAIL_REPOSITORY_ADAPTER");
		return repositoryAdapter.listCardsByCustomerId(customerId, cardCategory);
	}
	
	public CardDetail getCardDetailByMaskedPANandExpiryDate(String customerId,String cardType, String maskedPAN, String expiryDate) throws Exception  {
		System.out.println("[[KEY-CUSTOMEr-CARD]]   ::: "+ customerId);
		ICardDetailRepositoryAdapter repositoryAdapter = (ICardDetailRepositoryAdapter) RepositoryAdapterFactory
				.getInstance().getRepositoryAdapter("CARD_DETAIL_REPOSITORY_ADAPTER");
		return repositoryAdapter.getCardDetailByMaskedPANandExpiryDate(customerId, cardType, maskedPAN, expiryDate);
	}
}
