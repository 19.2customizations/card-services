package com.ecobank.digx.cz.app.domain.cardservices.entity;

import java.util.List;


import com.ecobank.digx.cz.app.domain.cardservices.entity.repository.CardServiceRepository;
import com.ofss.digx.infra.exceptions.Exception;

import com.ofss.fc.datatype.Date;
import com.ofss.fc.framework.domain.IPersistenceObject;
import com.ofss.fc.infra.das.exception.PersistenceException;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * @author kofidan
 *
 */
public class CardRequest implements IPersistenceObject {

	private static final long serialVersionUID = -4960596856991625778L;

	private String cardType;
	private String affiliateCode;
	private String customerNo;
	private String last4Digits;
	private String schemeType;
	@Schema(description = "Amount must be greater than zero. ", type = "Double", required = true)
	private double amount;
	private String typeOfCard;
	private String mobileNo;
	private String userId;
	private String email;
	private String note;
	private String nameOnCard;
	private String expiryDate;
	private String status;
	private String maskedCardNo;
	private String cardAccountNo;
	@Schema(description = "Source Account", type = "String", required = true)
	private String sourceAccountNo;
	private String responseCode;
	private String responseMessage;
	private String ccy;
	private Date requestDate;
	private Date postedDate;
	private String cbaReferenceNo;
	private CardRequestKey refKey;
	
	  private String deliveryLocationType;
	  private String city;
	  private String address;
	  private String branchCode;
	  
	  
	  public String getDeliveryLocationType() {
		return deliveryLocationType;
	}

	public void setDeliveryLocationType(String deliveryLocationType) {
		this.deliveryLocationType = deliveryLocationType;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	

	

	public void create(CardRequest cardService) throws Exception {
		CardServiceRepository.getInstance().create(cardService);
	}
	
	public void update(CardRequest cardService) throws Exception {
		CardServiceRepository.getInstance().update(cardService);
	}

	public CardRequest read(CardRequestKey key) throws Exception {
		System.out.println("externalRefNo ::: " + key.getExternalRefNo());
		return CardServiceRepository.getInstance().read(key);
	}
    public List<CardRequest> listCardRequestByCustomerId(String customerId, String cardCategory) throws Exception {
		
		return CardServiceRepository.getInstance().listCardRequestByCustomerId(customerId, cardCategory);
	}
	
	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public String getAffiliateCode() {
		return affiliateCode;
	}

	public void setAffiliateCode(String affiliateCode) {
		this.affiliateCode = affiliateCode;
	}

	public String getSchemeType() {
		return schemeType;
	}

	public void setSchemeType(String schemeType) {
		this.schemeType = schemeType;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getTypeOfCard() {
		return typeOfCard;
	}

	public void setTypeOfCard(String typeOfCard) {
		this.typeOfCard = typeOfCard;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getNameOnCard() {
		return nameOnCard;
	}

	public void setNameOnCard(String nameOnCard) {
		this.nameOnCard = nameOnCard;
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMaskedCardNo() {
		return maskedCardNo;
	}

	public void setMaskedCardNo(String maskedCardNo) {
		this.maskedCardNo = maskedCardNo;
	}
	
	public String getCardAccountNo() {
		return cardAccountNo;
	}

	public void setCardAccountNo(String cardAccountNo) {
		this.cardAccountNo = cardAccountNo;
	}

	public String getSourceAccountNo() {
		return sourceAccountNo;
	}

	public void setSourceAccountNo(String sourceAccountNo) {
		this.sourceAccountNo = sourceAccountNo;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public Date getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}

	public Date getPostedDate() {
		return postedDate;
	}

	public void setPostedDate(Date postedDate) {
		this.postedDate = postedDate;
	}

	public String getCbaReferenceNo() {
		return cbaReferenceNo;
	}

	public void setCbaReferenceNo(String cbaReferenceNo) {
		this.cbaReferenceNo = cbaReferenceNo;
	}

	public CardRequestKey getRefKey() {
		return refKey;
	}

	public void setRefKey(CardRequestKey refKey) {
		this.refKey = refKey;
	}

	@Override
	public boolean isEntityReadOnly() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void markReadOnly(boolean arg0) throws PersistenceException {
		// TODO Auto-generated method stub

	}

	

	public String getCustomerNo() {
		return customerNo;
	}

	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}

	public String getLast4Digits() {
		return last4Digits;
	}

	public void setLast4Digits(String last4Digits) {
		this.last4Digits = last4Digits;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

}
