package com.ecobank.digx.cz.app.cardservices.service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;

import com.ecobank.digx.cz.app.cardservices.assembler.CardDetailAssembler;
import com.ecobank.digx.cz.app.cardservices.assembler.CardServiceAssembler;
import com.ecobank.digx.cz.app.cardservices.dto.CardActivityDTO;
import com.ecobank.digx.cz.app.cardservices.dto.CardActivityListResponse;
import com.ecobank.digx.cz.app.cardservices.dto.CardBalanceDTO;
import com.ecobank.digx.cz.app.cardservices.dto.CardBalanceResponse;
import com.ecobank.digx.cz.app.cardservices.dto.CardDetailCreateRequestDTO;
import com.ecobank.digx.cz.app.cardservices.dto.CardDetailDTO;
import com.ecobank.digx.cz.app.cardservices.dto.CardLimitResponseDTO;
import com.ecobank.digx.cz.app.cardservices.dto.CardListResponse;
import com.ecobank.digx.cz.app.cardservices.dto.CardRequestListResponse;
import com.ecobank.digx.cz.app.cardservices.dto.CardServiceCreateRequestDTO;
import com.ecobank.digx.cz.app.cardservices.dto.CardServiceCreateResponseDTO;
import com.ecobank.digx.cz.app.cardservices.dto.CardServiceResponseDTO;
import com.ecobank.digx.cz.app.cardservices.dto.CardServiceUpdateRequestDTO;
import com.ecobank.digx.cz.app.cardservices.dto.CardStatementListResponse;
import com.ecobank.digx.cz.app.cardservices.dto.CardStatementRequestDTO;
import com.ecobank.digx.cz.app.domain.cardservices.entity.CardActivityInfo;
import com.ecobank.digx.cz.app.domain.cardservices.entity.CardActivityInfoKey;
import com.ecobank.digx.cz.app.domain.cardservices.entity.CardDetail;
import com.ecobank.digx.cz.app.domain.cardservices.entity.CardDetailKey;
import com.ecobank.digx.cz.app.domain.cardservices.entity.CardRequest;
import com.ecobank.digx.cz.app.domain.cardservices.entity.CardRequestKey;
import com.ecobank.digx.cz.app.domain.cardservices.entity.repository.CardActivityInfoRepository;
import com.ecobank.digx.cz.app.domain.cardservices.entity.repository.CardDetailRepository;
import com.ecobank.digx.cz.extxface.fc12if.adapter.ubs124.dto.CustomerUDFInquiryResponseDTO;
import com.ecobank.digx.cz.extxface.fc12if.adapter.ubs124.impl.RemoteFlexIFPostingAdapter;
import com.ecobank.digx.cz.extxface.mule.adapter.client.CreditCardServicesRepositoryAdapter;
import com.ecobank.digx.cz.extxface.mule.adapter.client.DebitCardServicesRepositoryAdapter;
import com.ecobank.digx.cz.extxface.mule.adapter.client.JEncrypt;
import com.ecobank.digx.cz.extxface.mule.adapter.client.PrepaidCardServicesRepositoryAdapter;
import com.ecobank.digx.cz.extxface.mule.adapter.client.RemoteCardServicesRepositoryAdapter;
import com.ecobank.digx.cz.extxface.mule.adapter.client.VirtualCardServicesRepositoryAdapter;
import com.ecobank.digx.cz.extxface.mule.dto.ESBCardResponseDTO;
import com.ofss.digx.app.AbstractApplication;
import com.ofss.digx.app.Interaction;
import com.ofss.digx.app.exception.RunTimeException;
import com.ofss.digx.app.party.dto.PartyAddressDTO;
import com.ofss.digx.app.party.dto.PartyContactDTO;
import com.ofss.digx.app.user.service.UserPartyResponse;
import com.ofss.digx.datatype.complex.Party;
import com.ofss.digx.domain.account.entity.core.AccountKey;
import com.ofss.digx.domain.config.entity.ConfigVarBDomain;
import com.ofss.digx.domain.dda.entity.DemandDepositAccount;
import com.ofss.digx.framework.determinant.DeterminantResolver;
import com.ofss.fc.app.context.SessionContext;
import com.ofss.fc.datatype.Date;
import com.ofss.fc.infra.config.ConfigurationFactory;
import com.ofss.fc.infra.log.impl.MultiEntityLogger;
import com.ofss.fc.service.response.TransactionStatus;
import com.thoughtworks.xstream.XStream;

public class CardServices extends AbstractApplication implements ICardServices {

	private static final String THIS_COMPONENT_NAME = CardServices.class.getName();
	private MultiEntityLogger FORMATTER = MultiEntityLogger.getUniqueInstance();
	private transient Logger logger = this.FORMATTER.getLogger(THIS_COMPONENT_NAME);
	protected static final Preferences preferences = ConfigurationFactory.getInstance().getConfigurations("DIGXCONSULTING");

	@Override
	public CardServiceCreateResponseDTO read(SessionContext sessionContext,
			CardServiceCreateRequestDTO cardServiceCreateRequestDTO) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CardServiceCreateResponseDTO create(SessionContext sessionContext,
			CardServiceCreateRequestDTO cardServiceCreateRequestDTO) throws Exception {
		System.out.println("CardServiceResponseDTO [AFFCODE]: "
				+ cardServiceCreateRequestDTO.getCardServiceDetails().getAffiliateCode());

		if (logger.isLoggable(Level.FINE)) {
			logger.log(Level.FINE, FORMATTER.formatMessage(
					"Entered into create method of Card services  Input: CardServiceReadRequestDTO: %s in class '%s'",
					new Object[] { cardServiceCreateRequestDTO, THIS_COMPONENT_NAME }));
		}

		super.canonicalizeInput(cardServiceCreateRequestDTO);
		Interaction.begin(sessionContext);
		System.out.println("CardServiceResponseDTO method ::: ");
		TransactionStatus status = fetchTransactionStatus();
		CardServiceCreateResponseDTO cardServiceCreateResponseDTO = new CardServiceCreateResponseDTO();
		CardServiceAssembler cardServiceAssembler = new CardServiceAssembler();

		try {
			System.out.println("Before Validate ::: ");
			cardServiceCreateResponseDTO.validate(sessionContext);
			System.out.println(
					"After Validate [CCY] ::: " + cardServiceCreateRequestDTO.getCardServiceDetails().getCcy());

			// SessionContext sessionContext2 = (SessionContext)ThreadAttribute.get("CTX");
			//String affCode = "ENG"; // sessionContext2.getTargetUnit();
			String affCode = preferences.get("AFFILIATE_CODE", DeterminantResolver.getInstance().fetchDeterminantValue(ConfigVarBDomain.class.getName()));
			System.out.println("Before Validate ::: " + affCode);
			System.out.println("AFFCODE--- ::: " + affCode + " " + sessionContext.getTargetUnit());

			cardServiceCreateRequestDTO.getCardServiceDetails().setAffiliateCode(affCode);

			DemandDepositAccount ddAccount = new DemandDepositAccount();
			AccountKey key = new AccountKey();
			key.setAccountId(cardServiceCreateRequestDTO.getCardServiceDetails().getSourceAccountNo());
			DemandDepositAccount sendAccountInfo = ddAccount.read(key);

			String customerId = sendAccountInfo.getPartyId().getValue();
			String acctName = sendAccountInfo.getDisplayName();

			cardServiceCreateRequestDTO.getCardServiceDetails().setCcy(sendAccountInfo.getCurrency());

			System.out.println("CustomerId2222 ::: " + customerId);
			System.out.println("Cust Name 222 ::: " + acctName);

			Party partyId = new Party();
			partyId.setValue(customerId);

			com.ofss.digx.app.user.service.User user = new com.ofss.digx.app.user.service.User();
			UserPartyResponse userPartyResponse = user.fetchPartyInformation(sessionContext, partyId);
			XStream xs = new XStream();
			System.out.println("Cust Name 222 :::" + xs.toXML(userPartyResponse));
			String email = "", mobile = "";
			
			

			String custName = userPartyResponse.getParty().getPersonalDetails().getFullName();
			// String partyIdx = userPartyResponse.getParty().getId().getValue();
			String typeOfCard = cardServiceCreateRequestDTO.getCardServiceDetails().getTypeOfCard();
			System.out.println("Type of Card  ::: " + typeOfCard);
			if (typeOfCard.equals("VIRTUAL")) {
				cardServiceCreateRequestDTO.getCardServiceDetails().setNameOnCard(custName);
				cardServiceCreateRequestDTO.getCardServiceDetails().setDeliveryLocationType("EMAIL");
				String cardType = cardServiceCreateRequestDTO.getCardServiceDetails().getCardType();
				cardServiceCreateRequestDTO.getCardServiceDetails().setExpiryDate(this.GetExpiryDate(cardType));

			} else {

				if (userPartyResponse.getParty().getContacts() != null
						&& userPartyResponse.getParty().getContacts().size() > 0) {
					for (PartyContactDTO contact : userPartyResponse.getParty().getContacts()) {
						if (contact.getContactType().name().contains("EMAIL")) {
							if (email.equals(""))
								email = contact.getEmail();
						} else if (contact.getContactType().name().contains("PHONE")) {
							if (contact.getPhone() != null && mobile.equals(""))
								mobile = contact.getPhone().getNumber();
						}
					}

					// mobile =
					// userPartyResponse.getParty().getContacts().get(0).getPhone().getNumber();

				}

				cardServiceCreateRequestDTO.getCardServiceDetails().setEmail(email);
				cardServiceCreateRequestDTO.getCardServiceDetails().setMobileNo(mobile);

			}
			cardServiceCreateRequestDTO.getCardServiceDetails().setRequestDate(new Date());
			cardServiceCreateRequestDTO.getCardServiceDetails().setStatus("PP");
			cardServiceCreateRequestDTO.getCardServiceDetails().setCustomerNo(customerId);

			//

			CardRequest cardRequest = cardServiceAssembler.toCardRequestDomainObjectCreate(cardServiceCreateRequestDTO);
			cardRequest.create(cardRequest);
			System.out.println("GetExternalRefNo [REFKEY]  ::: " + cardRequest.getRefKey().getExternalRefNo());

			cardServiceCreateResponseDTO.setStatus(buildStatus(status));
			cardServiceCreateResponseDTO.setCcy(cardServiceCreateRequestDTO.getCardServiceDetails().getCcy());
			cardServiceCreateResponseDTO.setExternalRefNo(cardRequest.getRefKey().getExternalRefNo());

		} catch (com.ofss.digx.infra.exceptions.Exception e) {
			e.printStackTrace();
			fillTransactionStatus(status, e);
			this.logger.log(Level.SEVERE,
					this.FORMATTER.formatMessage("Fatal Exception from create for Generate Card Service  Request '%s'",
							new Object[] { cardServiceCreateRequestDTO }),
					e);
		} catch (RunTimeException rte) {
			rte.printStackTrace();
			fillTransactionStatus(status, rte);
			this.logger.log(Level.SEVERE,
					this.FORMATTER.formatMessage("RunTimeException from create for Generate Card Service Request '%s'",
							new Object[] { cardServiceCreateRequestDTO }),
					rte);
		} catch (Exception e) {
			e.printStackTrace();
			fillTransactionStatus(status, e);
			this.logger.log(Level.SEVERE,
					this.FORMATTER.formatMessage(
							"Exception encountered while generating the systemReferenceId in create of service  %s",
							new Object[] { CardServices.class

									.getName() }),
					e);
		} finally {
			Interaction.close();
		}
		super.encodeOutput(cardServiceCreateResponseDTO);
		super.checkResponsePolicy(sessionContext, cardServiceCreateResponseDTO);
		if (this.logger.isLoggable(Level.FINE)) {
			this.logger.log(Level.FINE, this.FORMATTER.formatMessage(
					"Exiting create of Self Transfer Service, SessionContext: %s, CardServiceCreateResponse: %s ",
					new Object[] { sessionContext, cardServiceCreateResponseDTO, THIS_COMPONENT_NAME }));
		}
		return cardServiceCreateResponseDTO;

	}

//	@Override
//	public CardServiceCreateResponseDTO saveCardDetails(SessionContext sessionContext,
//			CardServiceCreateRequestDTO requestDTO) throws Exception {
//		// TODO Auto-generated method stub
//		return null;
//	}


	public CardServiceResponseDTO proocessCardRequest(SessionContext sessionContext,
			CardServiceUpdateRequestDTO cardServiceUpdateRequestDTO) throws com.ofss.digx.infra.exceptions.Exception {
		System.out.println("Procees action Card Request::: " +  cardServiceUpdateRequestDTO.getActionCode() + "_" + cardServiceUpdateRequestDTO.getPaymentId());
		super.checkAccessPolicy("com.ecobank.digx.cz.app.cardservices.service.CardServices.proocessCardRequest",
				new Object[] { sessionContext, cardServiceUpdateRequestDTO });

		super.canonicalizeInput(cardServiceUpdateRequestDTO);
		
		if (logger.isLoggable(Level.FINE)) {
			logger.log(Level.FINE, FORMATTER.formatMessage(
					"Entered into update method of Card Service Input: CardServiceUpdateRequestDTO: %s in class '%s'",
					new Object[] { cardServiceUpdateRequestDTO, THIS_COMPONENT_NAME }));
		}

		
		Interaction.begin(sessionContext);
		TransactionStatus transactionStatus = fetchTransactionStatus();
		CardServiceResponseDTO cardServiceResponse = new CardServiceResponseDTO();
		cardServiceResponse.setStatus(fetchStatus());
		try {
			cardServiceUpdateRequestDTO.validate(sessionContext);

			String actionCode = cardServiceUpdateRequestDTO.getActionCode();
			String paymentId = cardServiceUpdateRequestDTO.getPaymentId();
			
			String affCode = preferences.get("AFFILIATE_CODE", DeterminantResolver.getInstance().fetchDeterminantValue(ConfigVarBDomain.class.getName()));
			System.out.println("Update Card Action Validate ::: " + affCode);

			if (actionCode.contains("CARD_REQUEST"))
			{
				//process card request or virtual card generation;
				//cardServiceResponse = this.completeCardRequest(sessionContext, actionCode, paymentId);
				
				com.ecobank.digx.cz.app.domain.cardservices.entity.CardRequest cardRequestDomain = new com.ecobank.digx.cz.app.domain.cardservices.entity.CardRequest();
				CardDetailAssembler cardDetailAssembler = new CardDetailAssembler();

				CardRequestKey cardRequestKey = new CardRequestKey();
				System.out.println("card-request-paymentId    ::: " + paymentId);
				cardRequestKey.setExternalRefNo(paymentId);

				CardRequest cardRequest2 = cardRequestDomain.read(cardRequestKey);

				// String externalRefNo = cardServiceUpdateRequestDTO.getPaymentId();
				// cardRequest2.setExternalRefNo(externalRefNo);

				System.out.println("external Ref ::: " + cardRequest2.getRefKey().getExternalRefNo());
				System.out.println("name         ::: " + cardRequest2.getNameOnCard());
				System.out.println("Customer Id         ::: " + cardRequest2.getCustomerNo());

				// String partyIdx = userPartyResponse.getParty().getId().getValue();
				String typeOfCard = cardRequest2.getTypeOfCard();
				System.out.println("Type of Card  ::: " + typeOfCard);
				if (typeOfCard.equals("VIRTUAL") || typeOfCard.equals("PREPAID")) {
					// Generate virtual card
					System.out.println("Generate virtual card ...");

					Party partyId = new Party();
					partyId.setValue(cardRequest2.getCustomerNo());

					com.ofss.digx.app.user.service.User user = new com.ofss.digx.app.user.service.User();
					UserPartyResponse userPartyResponse = user.fetchPartyInformation(sessionContext, partyId);
					XStream xs = new XStream();
					System.out.println("Cust Name 222 :::" + xs.toXML(userPartyResponse));
					String email = "", mobile = "";
					String country = "", city = "";

					String custName = userPartyResponse.getParty().getPersonalDetails().getFullName();
					Date dob = userPartyResponse.getParty().getPersonalDetails().getBirthDate();

					String address = "";
					if (userPartyResponse.getParty().getAddresses() != null
							&& userPartyResponse.getParty().getAddresses().size() > 0) {
						for (PartyAddressDTO addr : userPartyResponse.getParty().getAddresses()) {
							if (addr.getPostalAddress() != null) {

								if (addr.getPostalAddress().getLine1() != null)
									address = addr.getPostalAddress().getLine1() + " " + addr.getPostalAddress().getLine2();

								if (addr.getPostalAddress().getCity() != null)
									city = addr.getPostalAddress().getCity();

								if (addr.getPostalAddress().getCountry() != null)
									country = addr.getPostalAddress().getCountry();

								System.out.println("ADDRESS:: " + address + " " + city);
								if (city != null && !city.equals(""))
									break;
							}
						}
					}
					SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
					String dobs = "";
					try {
						dobs = formatter.format(dob.fetchJavaDate());
					} catch (Exception ey) {
						ey.printStackTrace();
					}

					String[] personalData = new String[5];
					personalData[0] = address;
					personalData[1] = city;
					personalData[2] = country;
					personalData[3] = dobs;
					
					System.out.println("ADDRESS:: " + personalData[0]);

					String maskedPan = "";
					//RemoteCardServicesRepositoryAdapter adapter = RemoteCardServicesRepositoryAdapter.getInstance();
					CardServiceResponseDTO resp = null; //adapter.generateCard(cardRequest2, personalData);
					
					if(typeOfCard.equals("VIRTUAL"))
					{
						VirtualCardServicesRepositoryAdapter adapter1 = new VirtualCardServicesRepositoryAdapter();
						cardServiceResponse = adapter1.generateCard(cardRequest2, personalData);
						if(cardServiceResponse.getResponseCode().equals("000"))
							this.SavePrepaidVirtualCardInformation(cardServiceResponse, sessionContext, cardRequest2.getSourceAccountNo(), "VIRTUAL");
					}
					

					System.out.println("Card Response Generate: " + cardServiceResponse.getResponseCode());

					String cardIdGen = "";
					if (cardServiceResponse.getResponseCode().equalsIgnoreCase("000")) {
						
						/*cardServiceResponse.setResponseCode(resp.getResponseCode());
					cardServiceResponse.setResponseMessage(resp.getResponseMessage());
					cardServiceResponse.setLast4Digits(maskedPan);
					cardServiceResponse.setCustomerNo(resp.getCustomerNo());
					cardServiceResponse.setCardId(cardIdGen);*/

						//RemoteCardServicesRepositoryAdapter adapterInquiry = RemoteCardServicesRepositoryAdapter
						//		.getInstance();
						//ESBCardResponseDTO esbCardResponseDTO = adapterInquiry.inquiry(cardRequest2, resp);

						//String pan = esbCardResponseDTO.getPan();
						//maskedPan = pan.substring(0, 6) + "****" + pan.substring(pan.length() - 4);

						//System.out.println("Masked PAN 24: " + maskedPan);

						//System.out.println("Card Response Generate 25: " + resp.getResponseCode());
						/*
						 * cardRequest2.setResponseCode(resp.getResponseCode());
						 * cardRequest2.setResponseMessage(resp.getResponseMessage());
						 * cardRequest2.setPostedDate(new Date());
						 * cardRequest2.setMaskedCardNo(maskedPan); cardRequest2.setStatus("SUCCESS");
						 * cardRequestDomain.update(cardRequest2);
						 * System.out.println("Update Card Response Generate 22: " );
						 */

						/*CardRequest cardRequestDomain3 = new CardRequest();
						CardRequest cardRequest3 = cardRequestDomain3.read(cardRequestKey);

						cardRequest3.setResponseCode(resp.getResponseCode());
						cardRequest3.setResponseMessage(resp.getResponseMessage());
						cardRequest3.setPostedDate(new Date());
						cardRequest3.setMaskedCardNo(maskedPan);
						cardRequest3.setStatus("SUCCESS");
						cardRequestDomain3.update(cardRequest3);
						System.out.println("Update Card XXXX Response Generate 22: ");

						CardDetailDTO cardDetailDTO = cardDetailAssembler.fromCardRequestDTOToDomainCardDetail(cardRequest2,
								esbCardResponseDTO, resp.getCustomerNo(), maskedPan);
						CardDetailCreateRequestDTO cardDetailCreateRequestDTO = new CardDetailCreateRequestDTO();
						cardDetailCreateRequestDTO.setCardDetailDTO(cardDetailDTO);

						System.out.println("Card-Update-Here ");

						// CardDetailCreateRequestDTO cardDetailCreateRequestDTO =
						// cardDetailCreateRequestDTO.setCardDetailDTO(cardDetailDTO);
						CardDetail cardDetailDomain = new CardDetail();
						CardDetail cardDetail = cardDetailAssembler
								.toCardDetailDomainObjectCreate(cardDetailCreateRequestDTO);
						cardDetailDomain.create(cardDetail);
						cardIdGen = cardDetail.getRefKey().getCardId();*/

						//System.out.println(
						//		"Card-Detail-Create- GetExternalRefNo [REFKEY]  ::: " + cardDetail.getRefKey().getCardId());

					}

					
				} else if (typeOfCard.equals("DEBIT")) {
					// Update Debit or Credit card request Status
					// Send email

					// Call Unified API

					System.out.println("-DEBIT-SOURCE-ACCOUNT: " + cardRequest2.getSourceAccountNo());

					DemandDepositAccount ddAccount = new DemandDepositAccount();
					AccountKey key = new AccountKey();
					key.setAccountId(cardRequest2.getSourceAccountNo());
					DemandDepositAccount sendAccountInfo = ddAccount.read(key);
					String branchCode = sendAccountInfo.getBranchId();

					System.out.println("-DEBIT-SOURCE-BRANCHCODE: " + branchCode);

					DebitCardServicesRepositoryAdapter repository = new DebitCardServicesRepositoryAdapter();
					CardServiceResponseDTO responseDTO = repository.createCardRequest(cardRequest2, branchCode,
							cardRequest2.getBranchCode());

					System.out.println("service-responseCode    : " + responseDTO.getResponseCode());
					System.out.println("service-responseMessage : " + responseDTO.getResponseMessage());
					System.out.println("service-getExternalRefNo: " + responseDTO.getExternalRefNo());

					cardRequest2.setStatus("Submitted");
					cardRequest2.setResponseCode(responseDTO.getResponseCode());
					cardRequest2.setResponseMessage(responseDTO.getResponseMessage());

					cardServiceResponse.setResponseCode(responseDTO.getResponseCode());
					cardServiceResponse.setResponseMessage(responseDTO.getResponseMessage());
					cardServiceResponse.setExternalRefNo(cardRequest2.getRefKey().getExternalRefNo());

				}
				
				cardServiceResponse.setStatus(buildStatus(transactionStatus));
				
			}
			else if (actionCode.equals("TOPUP_CARD")) 
			{
				
				CardServiceResponseDTO respx = null;
				String externalRefNo = cardServiceUpdateRequestDTO.getPaymentId();
				CardActivityInfo cardActivityInfo = new CardActivityInfo();
				CardActivityInfoKey key = new CardActivityInfoKey();
				key.setRefId(externalRefNo);
				CardActivityInfo cardActivity = cardActivityInfo.read(key);
				// String debitAccount = cardActivity.getSourceAccount();
				
				
				CardDetail cardDetailDomain = new CardDetail();
				CardDetailKey keyCard = new CardDetailKey();
				keyCard.setCardId(cardActivity.getCardId());
				CardDetail cardInfo = cardDetailDomain.read(keyCard);
				String maskedPan = cardInfo.getMaskedPan();
				String pan = JEncrypt.decrypt(cardInfo.getFullPan());
				//String expiryDate = cardInfo.getExpiryDate();
				//String note = cardActivity.getActivityDescription();
				//String mobileNo = cardInfo.getMobileNo();
				//String customerNo = cardInfo.getCardCustomerId();
				String name = cardInfo.getNameOnCard();
				
				String cardCategory = cardInfo.getCardCategory();
				
				
				if(cardCategory.equals("CREDIT"))
				{
					DemandDepositAccount ddAccount = new DemandDepositAccount();
					AccountKey keyAcc = new AccountKey();
					keyAcc.setAccountId(cardActivity.getSourceAccount());
					DemandDepositAccount sendAccountInfo = ddAccount.read(keyAcc);
					String branchCode = sendAccountInfo.getBranchId();
					
					CreditCardServicesRepositoryAdapter crAdapter = new CreditCardServicesRepositoryAdapter();
					respx = crAdapter.fundCard(cardActivity, pan, name, branchCode);
					
					cardServiceResponse.setCbaReferenceNo(respx.getCbaReferenceNo());
					cardServiceResponse.setResponseCode(respx.getResponseCode());
					cardServiceResponse.setResponseMessage(respx.getResponseMessage());
					cardServiceResponse.setExternalRefNo(respx.getExternalRefNo());
					cardServiceResponse.setTransactionRefNo(respx.getTransactionRefNo());
					cardServiceResponse.setStatus(buildStatus(transactionStatus));
				}
				else if(cardCategory.equals("VIRTUAL"))
				{
					DemandDepositAccount ddAccount = new DemandDepositAccount();
					AccountKey keyAcc = new AccountKey();
					keyAcc.setAccountId(cardActivity.getSourceAccount());
					DemandDepositAccount sendAccountInfo = ddAccount.read(keyAcc);
					String branchCode = sendAccountInfo.getBranchId();
					
					VirtualCardServicesRepositoryAdapter crAdapter = new VirtualCardServicesRepositoryAdapter();
					respx = crAdapter.fundCard(cardActivity, branchCode);
					
					cardServiceResponse.setCbaReferenceNo(respx.getCbaReferenceNo());
					cardServiceResponse.setResponseCode(respx.getResponseCode());
					cardServiceResponse.setResponseMessage(respx.getResponseMessage());
					cardServiceResponse.setExternalRefNo(respx.getExternalRefNo());
					cardServiceResponse.setTransactionRefNo(respx.getTransactionRefNo());
					cardServiceResponse.setStatus(buildStatus(transactionStatus));
				}
				else if(cardCategory.equals("PREPAID"))
				{
					DemandDepositAccount ddAccount = new DemandDepositAccount();
					AccountKey keyAcc = new AccountKey();
					keyAcc.setAccountId(cardActivity.getSourceAccount());
					DemandDepositAccount sendAccountInfo = ddAccount.read(keyAcc);
					String branchCode = sendAccountInfo.getBranchId();
					
					PrepaidCardServicesRepositoryAdapter crAdapter = new PrepaidCardServicesRepositoryAdapter();
					respx = crAdapter.fundCard(cardActivity, branchCode);
					
					cardServiceResponse.setCbaReferenceNo(respx.getCbaReferenceNo());
					cardServiceResponse.setResponseCode(respx.getResponseCode());
					cardServiceResponse.setResponseMessage(respx.getResponseMessage());
					cardServiceResponse.setExternalRefNo(respx.getExternalRefNo());
					cardServiceResponse.setTransactionRefNo(respx.getTransactionRefNo());
					cardServiceResponse.setStatus(buildStatus(transactionStatus));
				}
				else
				{
					System.out.println("Source Account   : " + cardActivity.getSourceAccount());
	
					/*CardDetail cardDetailDomain = new CardDetail();
					CardDetailKey keyCard = new CardDetailKey();
					keyCard.setCardId(cardActivity.getCardId());
					CardDetail cardInfo = cardDetailDomain.read(keyCard);
					String maskedPan = cardInfo.getMaskedPan();*/
	
					System.out.println("Masked PAN   : " + maskedPan);
	
					// System.out.println("customerId: " + cardDetail2.getCustomerId());
					
	
					 //respx = CardActivityInfoRepository.getInstance().processTopupCard(affCode,
						//	cardActivity, cardInfo);
					cardServiceResponse.setCbaReferenceNo(respx.getCbaReferenceNo());
					cardServiceResponse.setResponseCode(respx.getResponseCode());
					cardServiceResponse.setResponseMessage(respx.getResponseMessage());
					cardServiceResponse.setExternalRefNo(respx.getExternalRefNo());
					cardServiceResponse.setTransactionRefNo(respx.getTransactionRefNo());
					cardServiceResponse.setStatus(buildStatus(transactionStatus));
				}

				return cardServiceResponse;
			} else if (actionCode.equals("BLOCK_CARD") || actionCode.equals("HOTLIST") || actionCode.equals("PIN_RESET")
					|| actionCode.equals("UNBLOCK_CARD") || actionCode.equals("CHANGE_PIN") || actionCode.equals("UPDATE_OTP_MOBILE") || 
					actionCode.equals("DELETE_CARD") || actionCode.equals("UPDATE_OTP_MOBILE") ) {

				// START
				// Need to be reviewed
				// block and unblock
				//com.ecobank.digx.cz.app.domain.cardservices.entity.CardRequest cardRequestDomain = new com.ecobank.digx.cz.app.domain.cardservices.entity.CardRequest();

				System.out.println("paymentId    ::: " + cardServiceUpdateRequestDTO.getPaymentId());
				CardActivityInfo cardActivityInfo = new CardActivityInfo();
				CardActivityInfoKey key = new CardActivityInfoKey();
				key.setRefId(cardServiceUpdateRequestDTO.getPaymentId());
				CardActivityInfo cardActivity = cardActivityInfo.read(key);
				
				//CardRequest cardRequest2 = cardRequestDomain.read(cardRequestKey);
				CardServiceResponseDTO resp = null;

				 //.getTypeOfCard();
				
				
				CardDetail cardDetailDomain = new CardDetail();
				CardDetailKey keyCard = new CardDetailKey();
				keyCard.setCardId(cardActivity.getCardId());
				CardDetail cardInfo = cardDetailDomain.read(keyCard);
				String maskedPan = cardInfo.getMaskedPan();
				String pan = JEncrypt.decrypt(cardInfo.getFullPan());
				String expiryDate = cardInfo.getExpiryDate();
				String note = cardActivity.getActivityDescription();
				String mobileNo = cardInfo.getMobileNo();
				String customerNo = cardInfo.getCardCustomerId();
				String name = cardInfo.getNameOnCard();
				
				String typeOfCard = cardInfo.getCardCategory();
				System.out.println("Card Type    ::: " + typeOfCard );

				System.out.println("Masked PAN   : " + maskedPan + " " + cardInfo.getCardCategory());


				if (typeOfCard.equalsIgnoreCase("CREDIT")) {
					CreditCardServicesRepositoryAdapter cardServicesRepositoryAdapter = new CreditCardServicesRepositoryAdapter();

					if (actionCode.equalsIgnoreCase("BLOCK_CARD")) {

						
						resp = cardServicesRepositoryAdapter.blockCard(pan,expiryDate, note);
					}
					else if (actionCode.equalsIgnoreCase("UNBLOCK_CARD")) {
						resp = cardServicesRepositoryAdapter.unblockCard(pan,expiryDate, note);
					}

				} else if (typeOfCard.equalsIgnoreCase("PREPAID")) {
					PrepaidCardServicesRepositoryAdapter cardServicesRepositoryAdapter = new PrepaidCardServicesRepositoryAdapter();

					if (actionCode.equalsIgnoreCase("BLOCK_CARD")) {

						
						resp = cardServicesRepositoryAdapter.blockCard(pan, mobileNo);
					}
					else if (actionCode.equalsIgnoreCase("UNBLOCK_CARD")) {
						resp = cardServicesRepositoryAdapter.unblockCard(pan, mobileNo);
					}

				}  else if (typeOfCard.equalsIgnoreCase("DEBIT")) {

					DebitCardServicesRepositoryAdapter debitCardServicesRepositoryAdapter = new DebitCardServicesRepositoryAdapter();
					if (actionCode.equalsIgnoreCase("BLOCK_CARD")) {

						resp = debitCardServicesRepositoryAdapter.blockCard(pan,expiryDate, note);
					}
					else if (actionCode.equalsIgnoreCase("UNBLOCK_CARD")) {
						resp = debitCardServicesRepositoryAdapter.unblockCard(pan,expiryDate, note);
					}

					// NB:
					// setDebitCardLimit will require we extend the digx_cz_card_request table to
					// include below columns
					// CardNotPresentLimit
					// CashWithdrawalLimit
					// PaymentTransferLimit
					// PurchaseLimit
					//debitCardServicesRepositoryAdapter.setDebitCardLimit(cardRequest2);

				} else if (typeOfCard.equalsIgnoreCase("VIRTUAL") ) {
					VirtualCardServicesRepositoryAdapter remoteCardServicesRepositoryAdapter = new VirtualCardServicesRepositoryAdapter();

					if (actionCode.equals("BLOCK_CARD")) {
						resp = remoteCardServicesRepositoryAdapter.blockCard(pan,  mobileNo);
					}
					else if (actionCode.equals("UNBLOCK_CARD")) {
						resp = remoteCardServicesRepositoryAdapter.unblockCard(pan,  mobileNo);
					}
					else if (actionCode.equals("UPDATE_OTP_MOBILE")) {
						resp = remoteCardServicesRepositoryAdapter.updateOTPNumber(pan, customerNo, mobileNo, cardActivity.getMobileNo(), cardActivity.getEmail());
					}
					else if (actionCode.equals("DELETE_CARD")) {
						
						DemandDepositAccount ddAccount = new DemandDepositAccount();
						AccountKey keyAcc = new AccountKey();
						keyAcc.setAccountId(cardActivity.getSourceAccount());
						DemandDepositAccount sendAccountInfo = ddAccount.read(keyAcc);
						//String branchCode = sendAccountInfo.getBranchId();
						
						resp = remoteCardServicesRepositoryAdapter.closeOrDeleteCard(pan, mobileNo, cardActivity.getSourceAccount(), sendAccountInfo.getCurrency());
					}
					
					

				}
				cardServiceResponse.setCbaReferenceNo(resp.getCbaReferenceNo());
				cardServiceResponse.setResponseCode(resp.getResponseCode());
				cardServiceResponse.setResponseMessage(resp.getResponseMessage());
				cardServiceResponse.setExternalRefNo(resp.getExternalRefNo());
				cardServiceResponse.setTransactionRefNo(resp.getTransactionRefNo());
				cardServiceResponse.setStatus(buildStatus(transactionStatus));
				//
				//
				// END

				System.out.println("Card Activity Action   : " + actionCode);
				String externalRefNo = cardServiceUpdateRequestDTO.getPaymentId();
				//CardActivityInfo cardActivityInfo = new CardActivityInfo();
				//CardActivityInfoKey key = new CardActivityInfoKey();
				//key.setRefId(externalRefNo);
				//CardActivityInfo cardActivity = cardActivityInfo.read(key);
				// String debitAccount = cardActivity.getSourceAccount();

				System.out.println("Source Account   : " + cardActivity.getActivityDescription());

				cardServiceResponse.setCbaReferenceNo(externalRefNo);
				cardServiceResponse.setResponseCode("000");
				cardServiceResponse.setResponseMessage("Successful");
				cardServiceResponse.setExternalRefNo(externalRefNo);
				cardServiceResponse.setTransactionRefNo(externalRefNo);
				cardServiceResponse.setStatus(buildStatus(transactionStatus));

				cardActivity.setCbaRspCode("NAP");
				cardActivity.setCbaRspMsg("NAP");
				cardActivity.setResponseCode("000");
				cardActivity.setResponseMessage("Successfully Blocked");
				cardActivity.setTranRefNo(externalRefNo);
				cardActivityInfo.update(cardActivity);

			}

			cardServiceResponse.setStatus(buildStatus(transactionStatus));

		} catch (com.ofss.digx.infra.exceptions.Exception e) {
			e.printStackTrace();
			fillTransactionStatus(transactionStatus, e);

		} catch (RunTimeException rte) {
			rte.printStackTrace();
			fillTransactionStatus(transactionStatus, rte);

		} catch (Exception e) {
			e.printStackTrace();
			fillTransactionStatus(transactionStatus, e);

		} finally {
			Interaction.close();
		}
		super.encodeOutput(cardServiceResponse);
		super.checkResponsePolicy(sessionContext, cardServiceResponse);
		if (this.logger.isLoggable(Level.FINE)) {
			this.logger.log(Level.FINE,
					this.FORMATTER.formatMessage(
							"Process Card Service Request and Activity, SessionContext: %s, CardServiceResponse: %s ",
							new Object[] { sessionContext, cardServiceResponse, THIS_COMPONENT_NAME }));
		}

		return cardServiceResponse;
	}

	@Override
	public CardServiceResponseDTO updateCardRequest(SessionContext sessionContext, CardServiceUpdateRequestDTO cardServiceUpdateRequestDTO )
			 throws com.ofss.digx.infra.exceptions.Exception {
		if (logger.isLoggable(Level.FINE)) {
			logger.log(Level.FINE,
					FORMATTER.formatMessage(
							"Entered into updateCardRequest Input: CardServiceUpdateRequestDTO: %s in class '%s'",
							new Object[] { cardServiceUpdateRequestDTO, THIS_COMPONENT_NAME }));
		}

		super.checkAccessPolicy("com.ecobank.digx.cz.app.cardservices.service.CardServices.updateCardRequest",
				new Object[] { sessionContext, cardServiceUpdateRequestDTO });

		super.canonicalizeInput(cardServiceUpdateRequestDTO);
		Interaction.begin(sessionContext);
		TransactionStatus transactionStatus = fetchTransactionStatus();
		CardServiceResponseDTO cardServiceResponse = new CardServiceResponseDTO();
		cardServiceResponse.setStatus(fetchStatus());
		try {
			cardServiceUpdateRequestDTO.validate(sessionContext);

			String actionCode = cardServiceUpdateRequestDTO.getActionCode();
			String paymentId = cardServiceUpdateRequestDTO.getPaymentId();
			
			cardServiceResponse = this.completeCardRequest(sessionContext, actionCode, paymentId);
			cardServiceResponse.setStatus(buildStatus(transactionStatus));
			
			
			

		} catch (com.ofss.digx.infra.exceptions.Exception e) {
			e.printStackTrace();
			fillTransactionStatus(transactionStatus, e);

		} catch (RunTimeException rte) {
			rte.printStackTrace();
			fillTransactionStatus(transactionStatus, rte);

		} catch (Exception e) {
			e.printStackTrace();
			fillTransactionStatus(transactionStatus, e);

		} finally {
			Interaction.close();
		}
		super.encodeOutput(cardServiceResponse);
		super.checkResponsePolicy(sessionContext, cardServiceResponse);
		if (this.logger.isLoggable(Level.FINE)) {
			this.logger.log(Level.FINE,
					this.FORMATTER.formatMessage(
							"Process Card Service Request and Activity, SessionContext: %s, CardServiceResponse: %s ",
							new Object[] { sessionContext, cardServiceResponse, THIS_COMPONENT_NAME }));
		}

		return cardServiceResponse;
	}

	@Override
	public CardLimitResponseDTO fetchCardLimit(SessionContext sessionContext, CardActivityDTO cardActivityDTO)
			throws Exception {
		Interaction.begin(sessionContext);
		TransactionStatus transactionStatus = fetchTransactionStatus();
		com.ecobank.digx.cz.app.domain.cardservices.entity.CardDetail cardDetailDomain = new com.ecobank.digx.cz.app.domain.cardservices.entity.CardDetail();
		CardLimitResponseDTO cardLimitResponseDTO = new CardLimitResponseDTO();
		cardLimitResponseDTO.setStatus(fetchStatus());
		try {

			XStream xs = new XStream();
			logger.info("Card Limit: " + xs.toXML(cardActivityDTO));
			String cardId = cardActivityDTO.getCardId();
			String cardRef = cardActivityDTO.getCardAccount();
			String cardCategory = cardActivityDTO.getCardType();
			
			/*CardRemoteServiceAssembler assembler = new CardRemoteServiceAssembler();
			cardLimitResponseDTO = assembler.fetchCardLimitMocking();
			cardLimitResponseDTO.setStatus(buildStatus(transactionStatus));*/
			
			logger.info("Card Limit response: " + xs.toXML(cardLimitResponseDTO));

			if (cardActivityDTO.getCardType().equalsIgnoreCase("DEBIT")) {
				DebitCardServicesRepositoryAdapter repository = new DebitCardServicesRepositoryAdapter();
				cardLimitResponseDTO = repository.fetchDebitCardLimit(cardCategory,cardRef);
			}
			
			

			//
			// fetch card limit currently not available for CREDIT, VIRTUAL & PREPAID CARDS
			//
		} /*catch (com.ofss.digx.infra.exceptions.Exception e) {
			e.printStackTrace();
			fillTransactionStatus(transactionStatus, e);

		} */catch (RunTimeException rte) {
			rte.printStackTrace();
			fillTransactionStatus(transactionStatus, rte);

		} catch (Exception e) {
			e.printStackTrace();
			fillTransactionStatus(transactionStatus, e);

		} finally {
			Interaction.close();
		}
		super.encodeOutput(cardLimitResponseDTO);
		super.checkResponsePolicy(sessionContext, cardLimitResponseDTO);
		if (this.logger.isLoggable(Level.FINE)) {
			this.logger.log(Level.FINE, this.FORMATTER.formatMessage(
					"Exiting create of Self Transfer Service, SessionContext: %s, SelfTransferCreateResponse: %s ",
					new Object[] { sessionContext, cardLimitResponseDTO, THIS_COMPONENT_NAME }));
		}
		return cardLimitResponseDTO;
	}

//	@Override
//	public CardServiceResponseDTO blockCard(SessionContext sessionContext, String cardId)
//			throws com.ofss.digx.infra.exceptions.Exception {
//		logger.info("Get cardId:" + cardId);
//
//		Interaction.begin(sessionContext);
//		TransactionStatus transactionStatus = fetchTransactionStatus();
//		com.ecobank.digx.cz.app.domain.cardservices.entity.CardDetail cardDetailDomain = new com.ecobank.digx.cz.app.domain.cardservices.entity.CardDetail();
//		CardServiceResponseDTO cardServiceResponse = new CardServiceResponseDTO();
//		cardServiceResponse.setStatus(fetchStatus());
//		try {
//
//			CardDetailKey cardDetailKey = new CardDetailKey();
//			cardDetailKey.setCardId(cardId);
//			CardDetail cardDetail2 = cardDetailDomain.read(cardDetailKey);
//
//			logger.info("card Id   : " + cardDetail2.getRefKey().getCardId());
//
//			logger.info("customerId: " + cardDetail2.getCustomerId());
//
//			RemoteCardServicesRepositoryAdapter adapter = RemoteCardServicesRepositoryAdapter.getInstance();
//			CardActivityDTO cardActivityDTO = adapter.blockCard(cardDetail2);
//
//			CardActivityInfoKey cardActivityInfoKey = new CardActivityInfoKey();
//			if (cardActivityDTO.getResponseCode().equalsIgnoreCase("000")) {
//				CardActivityInfo cardActivityInfo = new CardActivityInfo();
//				cardActivityInfo.setActivityDate(new com.ofss.fc.datatype.Date());
//				cardActivityInfo.setActivityDescription(cardActivityDTO.getActivityDescription());
//				cardActivityInfo.setActivityType("BLOCKCARD");
//				cardActivityInfo.setStatus("BLOCKED");
//				String refGen = GetRefNumber("CD", 12);
//				cardActivityInfo.setCardId(refGen);
//				cardActivityInfo.setTranRefNo(refGen);
//				cardActivityInfo.setFee("0");
//				cardActivityInfoKey.setRefId(cardActivityDTO.getRefId());
//				// cardActivityInfo.setRefId(cardActivityDTO.getRefId());
//				cardActivityInfo.setRefKey(cardActivityInfoKey);
//				cardActivityInfo.setResponseCode(cardActivityDTO.getResponseCode());
//				cardActivityInfo.setResponseMessage(cardActivityDTO.getResponseMessage());
//				cardActivityInfo.setUserId(sessionContext.getUserId());
//
//				cardActivityInfo.create(cardActivityInfo);
//				System.out.println("GetExternalRefNo [REFKEY]  ::: " + cardActivityInfo.getRefKey().getRefId());
//
//				cardDetail2.setCardStatus("BLOCKED");
//				cardDetailDomain.update(cardDetail2);
//
//			}
//
//			cardServiceResponse.setResponseCode(cardActivityDTO.getResponseCode());
//			cardServiceResponse.setResponseMessage(cardActivityDTO.getResponseCode());
//			cardServiceResponse.setStatus(buildStatus(transactionStatus));
//
//		} catch (com.ofss.digx.infra.exceptions.Exception e) {
//			e.printStackTrace();
//			fillTransactionStatus(transactionStatus, e);
//
//		} catch (RunTimeException rte) {
//			rte.printStackTrace();
//			fillTransactionStatus(transactionStatus, rte);
//
//		} catch (Exception e) {
//			e.printStackTrace();
//			fillTransactionStatus(transactionStatus, e);
//
//		} finally {
//			Interaction.close();
//		}
//		super.encodeOutput(cardServiceResponse);
//		super.checkResponsePolicy(sessionContext, cardServiceResponse);
//		if (this.logger.isLoggable(Level.FINE)) {
//			this.logger.log(Level.FINE, this.FORMATTER.formatMessage(
//					"Exiting create of Self Transfer Service, SessionContext: %s, SelfTransferCreateResponse: %s ",
//					new Object[] { sessionContext, cardServiceResponse, THIS_COMPONENT_NAME }));
//		}
//		return cardServiceResponse;
//	}

	@Override
	public CardServiceResponseDTO createCardActivity(SessionContext sessionContext, CardActivityDTO cardActivity)
			throws com.ofss.digx.infra.exceptions.Exception {
		// logger.info("Get cardId:" + cardId);

		Interaction.begin(sessionContext);
		TransactionStatus transactionStatus = fetchTransactionStatus();
		com.ecobank.digx.cz.app.domain.cardservices.entity.CardDetail cardDetailDomain = new com.ecobank.digx.cz.app.domain.cardservices.entity.CardDetail();
		CardServiceResponseDTO cardServiceResponse = new CardServiceResponseDTO();
		cardServiceResponse.setStatus(fetchStatus());
		try {

			// String cardId

			XStream xs = new XStream();
			System.out.println("Card Activity 22: " + xs.toXML(cardActivity));
			//String affCode = "ENG";
			String affCode = preferences.get("AFFILIATE_CODE", DeterminantResolver.getInstance().fetchDeterminantValue(ConfigVarBDomain.class.getName()));
			System.out.println("Before Validate ::: " + affCode);

			String cardId = cardActivity.getCardId();

			System.out.println("Card Id 22: " + cardId);

			CardDetailKey cardDetailKey = new CardDetailKey();
			cardDetailKey.setCardId(cardId);
			CardDetail cardDetail2 = cardDetailDomain.read(cardDetailKey);

			System.out.println("card Id   : " + cardDetail2.getRefKey().getCardId());

			System.out.println("customerId: " + cardDetail2.getCustomerId());

			// RemoteCardServicesRepositoryAdapter adapter =
			// RemoteCardServicesRepositoryAdapter.getInstance();
			// CardActivityDTO cardActivityDTO = adapter.blockCard(cardDetail2);

			String userId = sessionContext.getUserId();
			CardDetailAssembler assembler = new CardDetailAssembler();

			// CardActivityInfo domain = new CardActivityInfo();

			CardActivityInfo cardActivityInfo = assembler.fromCardActivityDTOToDomainObject(cardActivity, userId,
					affCode);
			System.out.println("Card Activity Entity: " + xs.toXML(cardActivityInfo));
			cardActivityInfo.create(cardActivityInfo);

			System.out.println("Successfully Logged RefKey  ::: " + cardActivityInfo.getRefKey().getRefId());
			// String externalRefNo = cardActivityInfo.getRefKey().getRefId();

			// String debitAccount = cardActivity.getSourceAccountNo();
			// String pan = cardDetail2.getMaskedPan();

			// String maskedPan = pan.substring(pan.length() - 4);

			cardDetail2.setCardStatus(cardActivityInfo.getActivityType());
			cardDetailDomain.update(cardDetail2);

			cardServiceResponse.setResponseCode("000");
			cardServiceResponse.setResponseMessage("SUCCESS");
			cardServiceResponse.setExternalRefNo(cardActivityInfo.getRefKey().getRefId());

			String actionCode = cardActivityInfo.getActivityType();
			if (actionCode.equals("TOPUP_CARD")) {

				//CardActivityInfoRepository cardActivityInfoRepository = new CardActivityInfoRepository();

				//cardServiceResponse = cardActivityInfoRepository.processTopupCard(affCode, cardActivityInfo,
						//cardDetail2);

				// cardServiceResponse =
				// CardActivityInfoRepository.getInstance().processTopupCard(externalRefNo,
				// affCode, debitAccount, maskedPan);
			}

			cardServiceResponse.setStatus(buildStatus(transactionStatus));

		} catch (com.ofss.digx.infra.exceptions.Exception e) {
			e.printStackTrace();
			fillTransactionStatus(transactionStatus, e);

		} catch (RunTimeException rte) {
			rte.printStackTrace();
			fillTransactionStatus(transactionStatus, rte);

		} catch (Exception e) {
			e.printStackTrace();
			fillTransactionStatus(transactionStatus, e);

		} finally {
			Interaction.close();
		}
		super.encodeOutput(cardServiceResponse);
		super.checkResponsePolicy(sessionContext, cardServiceResponse);
		if (this.logger.isLoggable(Level.FINE)) {
			this.logger.log(Level.FINE,
					this.FORMATTER.formatMessage(
							"Exiting Card Activity Create, SessionContext: %s, CardServiceResponseDTO: %s ",
							new Object[] { sessionContext, cardServiceResponse, THIS_COMPONENT_NAME }));
		}
		return cardServiceResponse;
	}

//	@Override
//	public CardServiceResponseDTO unblockCard(SessionContext sessionContext, String cardId)
//			throws com.ofss.digx.infra.exceptions.Exception {
//		logger.info("Get cardId:" + cardId);
//
//		Interaction.begin(sessionContext);
//		TransactionStatus transactionStatus = fetchTransactionStatus();
//		com.ecobank.digx.cz.app.domain.cardservices.entity.CardDetail cardDetailDomain = new com.ecobank.digx.cz.app.domain.cardservices.entity.CardDetail();
//		CardServiceResponseDTO cardServiceResponse = new CardServiceResponseDTO();
//		cardServiceResponse.setStatus(fetchStatus());
//		try {
//
//			CardDetailKey cardDetailKey = new CardDetailKey();
//			cardDetailKey.setCardId(cardId);
//			CardDetail cardDetail2 = cardDetailDomain.read(cardDetailKey);
//
//			logger.info("card Id   : " + cardDetail2.getRefKey().getCardId());
//
//			logger.info("customerId: " + cardDetail2.getCustomerId());
//
//			RemoteCardServicesRepositoryAdapter adapter = RemoteCardServicesRepositoryAdapter.getInstance();
//			CardActivityDTO cardActivityDTO = adapter.unblockCard(cardDetail2);
//
//			CardActivityInfoKey cardActivityInfoKey = new CardActivityInfoKey();
//			if (cardActivityDTO.getResponseCode().equalsIgnoreCase("000")) {
//				CardActivityInfo cardActivityInfo = new CardActivityInfo();
//				cardActivityInfo.setActivityDate(new com.ofss.fc.datatype.Date());
//				cardActivityInfo.setActivityDescription(cardActivityDTO.getActivityDescription());
//				cardActivityInfo.setActivityType("UNBLOCKCARD");
//				cardActivityInfo.setStatus("UNBLOCKED");
//				String refGen = GetRefNumber("CD", 12);
//				cardActivityInfo.setCardId(refGen);
//				cardActivityInfo.setTranRefNo(refGen);
//				cardActivityInfo.setFee("0");
//				cardActivityInfoKey.setRefId(cardActivityDTO.getRefId());
//				// cardActivityInfo.setRefId(cardActivityDTO.getRefId());
//				cardActivityInfo.setRefKey(cardActivityInfoKey);
//				cardActivityInfo.setResponseCode(cardActivityDTO.getResponseCode());
//				cardActivityInfo.setResponseMessage(cardActivityDTO.getResponseMessage());
//				cardActivityInfo.setUserId(sessionContext.getUserId());
//
//				cardActivityInfo.create(cardActivityInfo);
//				System.out.println("GetExternalRefNo [REFKEY]  ::: " + cardActivityInfo.getRefKey().getRefId());
//
//			}
//
//			cardServiceResponse.setResponseCode(cardActivityDTO.getResponseCode());
//			cardServiceResponse.setResponseMessage(cardActivityDTO.getResponseCode());
//			cardServiceResponse.setStatus(buildStatus(transactionStatus));
//
//		} catch (com.ofss.digx.infra.exceptions.Exception e) {
//			e.printStackTrace();
//			fillTransactionStatus(transactionStatus, e);
//
//		} catch (RunTimeException rte) {
//			rte.printStackTrace();
//			fillTransactionStatus(transactionStatus, rte);
//
//		} catch (Exception e) {
//			e.printStackTrace();
//			fillTransactionStatus(transactionStatus, e);
//
//		} finally {
//			Interaction.close();
//		}
//		super.encodeOutput(cardServiceResponse);
//		super.checkResponsePolicy(sessionContext, cardServiceResponse);
//		if (this.logger.isLoggable(Level.FINE)) {
//			this.logger.log(Level.FINE, this.FORMATTER.formatMessage(
//					"Exiting create of Self Transfer Service, SessionContext: %s, SelfTransferCreateResponse: %s ",
//					new Object[] { sessionContext, cardServiceResponse, THIS_COMPONENT_NAME }));
//		}
//		return cardServiceResponse;
//	}

	@Override
	public CardServiceCreateResponseDTO fundCard(SessionContext sessionContext, CardServiceCreateRequestDTO requestDTO)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CardBalanceResponse balanceCheck(SessionContext sessionContext, CardBalanceDTO requestDTO)
			throws com.ofss.digx.infra.exceptions.Exception {
		Interaction.begin(sessionContext);
		TransactionStatus transactionStatus = fetchTransactionStatus();
		com.ecobank.digx.cz.app.domain.cardservices.entity.CardDetail cardDetailDomain = new com.ecobank.digx.cz.app.domain.cardservices.entity.CardDetail();
		CardBalanceResponse cardBalanceResponse = new CardBalanceResponse();

		try {
			
			String affCode = preferences.get("AFFILIATE_CODE", DeterminantResolver.getInstance().fetchDeterminantValue(ConfigVarBDomain.class.getName()));
			System.out.println("Before Validate ::: " + affCode);

			System.out.println("inside card services cardCategory :: " + requestDTO.getCardCategory());
			if (requestDTO.getCardCategory().equals("VIRTUAL") ) {
				cardBalanceResponse.setStatus(fetchStatus());

				CardDetailKey cardDetailKey = new CardDetailKey();
				cardDetailKey.setCardId(requestDTO.getCardId());
				CardDetail cardDetail2 = cardDetailDomain.read(cardDetailKey);

				System.out.println("card Id   : " + cardDetail2.getRefKey().getCardId());

				System.out.println("customerId: " + cardDetail2.getCardCustomerId());

				

				RemoteCardServicesRepositoryAdapter adapter = RemoteCardServicesRepositoryAdapter.getInstance();
				cardBalanceResponse = adapter.balanceCheck(cardDetail2.getCardCustomerId(), cardDetail2.getMobileNo());
				if (cardBalanceResponse.getResponseCode().equals("000")) {
					cardBalanceResponse.setCardId(cardDetail2.getRefKey().getCardId());
					cardBalanceResponse.setCardAccount(cardDetail2.getCardCustomerId());
					cardBalanceResponse.setMaskedPan(cardDetail2.getMaskedPan());
					cardBalanceResponse.setNameOnCard(cardDetail2.getNameOnCard());

				}
				cardBalanceResponse.setStatus(buildStatus(transactionStatus));

			} else if (requestDTO.getCardCategory().equals("CREDIT")) {
				logger.info(":: Credit Card Processing [1] ::");
				CreditCardServicesRepositoryAdapter creditCardServicesRepositoryAdapter = new CreditCardServicesRepositoryAdapter();
				cardBalanceResponse = creditCardServicesRepositoryAdapter.balanceCheck(requestDTO.getCardId(), affCode);
				logger.info(":: NameOnCard :: " + cardBalanceResponse.getNameOnCard());
				logger.info(":: Balance    :: " + cardBalanceResponse.getBalance());

				if (cardBalanceResponse.getResponseCode().equals("000")) {
					// check if card information exist else save credit card info to DIGX_CZ_CARD
					
					
					String idx = this.SaveCardInformation(cardBalanceResponse, sessionContext,
							requestDTO.getCardId());
					cardBalanceResponse.setCardId(idx);
				}
				cardBalanceResponse.setStatus(buildStatus(transactionStatus));
			}
			else if (requestDTO.getCardCategory().equals("PREPAID")) {
				logger.info(":: Prepaid Card Processing [1] ::");
				PrepaidCardServicesRepositoryAdapter creditCardServicesRepositoryAdapter = new PrepaidCardServicesRepositoryAdapter();
				cardBalanceResponse = creditCardServicesRepositoryAdapter.balanceCheck(requestDTO);
				logger.info(":: NameOnCard :: " + cardBalanceResponse.getNameOnCard());
				logger.info(":: Balance    :: " + cardBalanceResponse.getBalance());

				if (cardBalanceResponse.getResponseCode().equals("000")) {
					// check if card information exist else save credit card info to DIGX_CZ_CARD
					
					String cardAccount ="";
					String idx = this.SaveCardInformation(cardBalanceResponse, sessionContext, cardAccount);
							
							
					cardBalanceResponse.setCardId(idx);
				}
				cardBalanceResponse.setStatus(buildStatus(transactionStatus));
			}

		} catch (com.ofss.digx.infra.exceptions.Exception e) {
			e.printStackTrace();
			fillTransactionStatus(transactionStatus, e);

		} catch (RunTimeException rte) {
			rte.printStackTrace();
			fillTransactionStatus(transactionStatus, rte);

		} catch (Exception e) {
			e.printStackTrace();
			fillTransactionStatus(transactionStatus, e);

		} finally {
			Interaction.close();
		}
		super.encodeOutput(cardBalanceResponse);
		super.checkResponsePolicy(sessionContext, cardBalanceResponse);
		if (this.logger.isLoggable(Level.FINE)) {
			this.logger.log(Level.FINE, this.FORMATTER.formatMessage(
					"Exiting create of Self Transfer Service, SessionContext: %s, SelfTransferCreateResponse: %s ",
					new Object[] { sessionContext, cardBalanceResponse, THIS_COMPONENT_NAME }));
		}
		return cardBalanceResponse;
	}

	@Override
	public CardServiceCreateResponseDTO miniStatement(SessionContext sessionContext,
			CardServiceCreateRequestDTO requestDTO) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CardServiceCreateResponseDTO inquiry(SessionContext sessionContext, CardServiceCreateRequestDTO requestDTO)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public CardListResponse listCardsByCustomerId(SessionContext sessionContext, String customerId, String cardCategory)
			throws com.ofss.digx.infra.exceptions.Exception {
		System.out.println("list card by customer id " + customerId);

		if (logger.isLoggable(Level.FINE)) {
			logger.log(Level.FINE, FORMATTER.formatMessage(
					"Entered into create method of Card services  Input: listCardsByCustomerId : %s in class '%s'",
					new Object[] { customerId, THIS_COMPONENT_NAME }));
		}

		// super.canonicalizeInput(cardServiceCreateRequestDTO);
		Interaction.begin(sessionContext);
		System.out.println("CardServiceResponseDTO method ::: ");
		TransactionStatus status = fetchTransactionStatus();
		CardListResponse response = new CardListResponse();
		CardDetailAssembler assembler = new CardDetailAssembler();

		try {
			
			// cardServiceCreateResponseDTO.validate(sessionContext);
			
			String affCode = preferences.get("AFFILIATE_CODE", DeterminantResolver.getInstance().fetchDeterminantValue(ConfigVarBDomain.class.getName()));
			System.out.println("Before Validate ::: " + affCode);
			
			CardDetail cardDetail = new CardDetail();
			response = assembler.fromCardDetailDomainToDTOList(cardDetail.listCardsByCustomerId(customerId, cardCategory));

			System.out.println("CustomerId-Response ::: ");
			
			System.out.println("List card response :: ");
			if(response == null || response.getCards() == null || response.getCards().size() <= 0 )
			{
				System.out.println("List card response :: null " + cardCategory);
				if(cardCategory.equals("DEBIT"))
				{
					DebitCardServicesRepositoryAdapter adapterD = new DebitCardServicesRepositoryAdapter();
					String accountNo ="";
					
					response = adapterD.getCardByAccount(accountNo, affCode);
					if(response != null && response.getCards() != null && response.getCards().size() > 0)
					{
						for(CardDetailDTO cardDetailDTO : response.getCards())
						{
							this.SaveLinkedDebitCardInformation(cardDetailDTO, sessionContext, accountNo);
						}
					}
				}
			}

			response.setStatus(buildStatus(status));

		} catch (com.ofss.digx.infra.exceptions.Exception e) {
			e.printStackTrace();
			fillTransactionStatus(status, e);
			this.logger.log(Level.SEVERE,
					this.FORMATTER.formatMessage(
							"Fatal Exception from create for listCardsByCustomerId Card Service  Request '%s'",
							new Object[] { customerId }),
					e);
		} catch (RunTimeException rte) {
			rte.printStackTrace();
			fillTransactionStatus(status, rte);
			this.logger.log(Level.SEVERE,
					this.FORMATTER.formatMessage(
							"RunTimeException from create for listCardsByCustomerId Card Service Request '%s'",
							new Object[] { customerId }),
					rte);
		} catch (Exception e) {
			e.printStackTrace();
			fillTransactionStatus(status, e);
			this.logger.log(Level.SEVERE, this.FORMATTER.formatMessage(
					"Exception encountered while generating the systemReferenceId in listCardsByCustomerId of service  %s",
					new Object[] { CardServices.class

							.getName() }),
					e);
		} finally {
			Interaction.close();
		}
		super.encodeOutput(response);
		super.checkResponsePolicy(sessionContext, response);
		if (this.logger.isLoggable(Level.FINE)) {
			this.logger.log(Level.FINE,
					this.FORMATTER.formatMessage(
							"Exiting create of listCardsByCustomerId, SessionContext: %s, CardListResponse: %s ",
							new Object[] { sessionContext, response, THIS_COMPONENT_NAME }));
		}
		return response;

	}

	public CardRequestListResponse listCardRequestsByCustomerId(SessionContext sessionContext, String customerId,
			String cardCategory) throws com.ofss.digx.infra.exceptions.Exception {
		System.out.println("list card request by 22 customer id " + customerId);

		if (logger.isLoggable(Level.FINE)) {
			logger.log(Level.FINE, FORMATTER.formatMessage(
					"Entered into create method of Card services  Input: listCardRequestsByCustomerId : %s in class '%s'",
					new Object[] { customerId, THIS_COMPONENT_NAME }));
		}

		// super.canonicalizeInput(cardServiceCreateRequestDTO);
		Interaction.begin(sessionContext);
		System.out.println("CardServiceResponseDTO method ::: ");
		TransactionStatus status = fetchTransactionStatus();
		CardRequestListResponse response = new CardRequestListResponse();
		CardServiceAssembler assembler = new CardServiceAssembler();

		try {
			System.out.println("Before Validate ::: ");
			// cardServiceCreateResponseDTO.validate(sessionContext);

			CardRequest cardRequest = new CardRequest();
			response = assembler.fromCardRequestDomainToDTOList(cardRequest.listCardRequestByCustomerId(customerId, cardCategory));

			System.out.println("CustomerId-Response ::: " + response.getRequests().size());

			response.setStatus(buildStatus(status));

		} catch (com.ofss.digx.infra.exceptions.Exception e) {
			e.printStackTrace();
			fillTransactionStatus(status, e);
			this.logger.log(Level.SEVERE,
					this.FORMATTER.formatMessage(
							"Fatal Exception from create for listCardRequestsByCustomerId Card Service  Request '%s'",
							new Object[] { customerId }),
					e);
		} catch (RunTimeException rte) {
			rte.printStackTrace();
			fillTransactionStatus(status, rte);
			this.logger.log(Level.SEVERE,
					this.FORMATTER.formatMessage(
							"RunTimeException from create for listCardRequestsByCustomerId Card Service Request '%s'",
							new Object[] { customerId }),
					rte);
		} catch (Exception e) {
			e.printStackTrace();
			fillTransactionStatus(status, e);
			this.logger.log(Level.SEVERE, this.FORMATTER.formatMessage(
					"Exception encountered while generating the systemReferenceId in listCardRequestsByCustomerId of service  %s",
					new Object[] { CardServices.class

							.getName() }),
					e);
		} finally {
			Interaction.close();
		}
		super.encodeOutput(response);
		super.checkResponsePolicy(sessionContext, response);
		if (this.logger.isLoggable(Level.FINE)) {
			this.logger.log(Level.FINE,
					this.FORMATTER.formatMessage(
							"Exiting create of listCardsByCustomerId, SessionContext: %s, CardListResponse: %s ",
							new Object[] { sessionContext, response, THIS_COMPONENT_NAME }));
		}
		return response;

	}

	public CardActivityListResponse listCardActivityById(SessionContext sessionContext, String cardId)
			throws com.ofss.digx.infra.exceptions.Exception {
		System.out.println("list card activity by card id " + cardId);

		if (logger.isLoggable(Level.FINE)) {
			logger.log(Level.FINE, FORMATTER.formatMessage(
					"Entered into create method of Card services  Input: listCardsByCustomerId : %s in class '%s'",
					new Object[] { cardId, THIS_COMPONENT_NAME }));
		}

		// super.canonicalizeInput(cardServiceCreateRequestDTO);
		Interaction.begin(sessionContext);
		System.out.println("CardServiceResponseDTO method ::: ");
		TransactionStatus status = fetchTransactionStatus();
		CardActivityListResponse response = new CardActivityListResponse();
		CardDetailAssembler assembler = new CardDetailAssembler();

		try {
			System.out.println("Before Validate ::: ");
			// cardServiceCreateResponseDTO.validate(sessionContext);

			CardActivityInfo cardDetail = new CardActivityInfo();
			response = assembler.fromCardActivityDomainToDTOList(cardDetail.listCardActivityById(cardId));

			System.out.println("CustomerId-Response ::: ");

			response.setStatus(buildStatus(status));

		} catch (com.ofss.digx.infra.exceptions.Exception e) {
			e.printStackTrace();
			fillTransactionStatus(status, e);
			this.logger.log(Level.SEVERE,
					this.FORMATTER.formatMessage(
							"Fatal Exception from create for listCardActivityById Card Service  Request '%s'",
							new Object[] { cardId }),
					e);
		} catch (RunTimeException rte) {
			rte.printStackTrace();
			fillTransactionStatus(status, rte);
			this.logger.log(Level.SEVERE,
					this.FORMATTER.formatMessage(
							"RunTimeException from create for listCardActivityById Card Service Request '%s'",
							new Object[] { cardId }),
					rte);
		} catch (Exception e) {
			e.printStackTrace();
			fillTransactionStatus(status, e);
			this.logger.log(Level.SEVERE, this.FORMATTER.formatMessage(
					"Exception encountered while generating the systemReferenceId in listCardActivityById of service  %s",
					new Object[] { CardServices.class

							.getName() }),
					e);
		} finally {
			Interaction.close();
		}
		super.encodeOutput(response);
		super.checkResponsePolicy(sessionContext, response);
		if (this.logger.isLoggable(Level.FINE)) {
			this.logger.log(Level.FINE,
					this.FORMATTER.formatMessage(
							"Exiting create of listCardsByCustomerId, SessionContext: %s, CardListResponse: %s ",
							new Object[] { sessionContext, response, THIS_COMPONENT_NAME }));
		}
		return response;

	}

	@Override
	public CardStatementListResponse fetchStatement(SessionContext sessionContext,
			CardStatementRequestDTO cardStatementRequestDTO) throws Exception {

		Interaction.begin(sessionContext);
		TransactionStatus transactionStatus = fetchTransactionStatus();
		//com.ecobank.digx.cz.app.domain.cardservices.entity.CardDetail cardDetailDomain = new com.ecobank.digx.cz.app.domain.cardservices.entity.CardDetail();
		CardStatementListResponse cardStatementListResponse = new CardStatementListResponse();

		try {
			
			//CardRemoteServiceAssembler assembler = new CardRemoteServiceAssembler();
			//cardStatementListResponse = assembler.fromRemoteResponseCardStatementtoDTOMocking("NGN");
			//cardStatementListResponse.setStatus(buildStatus(transactionStatus));
			if (cardStatementRequestDTO.getCardCategory().equals("VIRTUAL") || cardStatementRequestDTO.getCardCategory().equals("PREPAID")) {
				RemoteCardServicesRepositoryAdapter adapter = RemoteCardServicesRepositoryAdapter.getInstance();
				cardStatementListResponse = adapter.getStatement(cardStatementRequestDTO);
				cardStatementListResponse.setStatus(buildStatus(transactionStatus));
			} else if (cardStatementRequestDTO.getCardCategory().equals("CREDIT")) {
				
				CreditCardServicesRepositoryAdapter adapter = CreditCardServicesRepositoryAdapter.getInstance();
				cardStatementListResponse = adapter.getStatement(cardStatementRequestDTO);
				cardStatementListResponse.setStatus(buildStatus(transactionStatus));

			}

		} /*catch (com.ofss.digx.infra.exceptions.Exception e) {
			e.printStackTrace();
			fillTransactionStatus(transactionStatus, e);

		} */catch (RunTimeException rte) {
			rte.printStackTrace();
			fillTransactionStatus(transactionStatus, rte);

		} catch (Exception e) {
			e.printStackTrace();
			fillTransactionStatus(transactionStatus, e);

		} finally {
			Interaction.close();
		}
		super.encodeOutput(cardStatementListResponse);
		super.checkResponsePolicy(sessionContext, cardStatementListResponse);
		if (this.logger.isLoggable(Level.FINE)) {
			this.logger.log(Level.FINE, this.FORMATTER.formatMessage(
					"Exiting create of Self Transfer Service, SessionContext: %s, SelfTransferCreateResponse: %s ",
					new Object[] { sessionContext, cardStatementListResponse, THIS_COMPONENT_NAME }));
		}
		return cardStatementListResponse;
	}
	
	
	@Override
	public CardListResponse fetchCardByAccount(SessionContext sessionContext, CardStatementRequestDTO cardStatementRequestDTO) throws Exception {
		Interaction.begin(sessionContext);
		TransactionStatus transactionStatus = fetchTransactionStatus();
		com.ecobank.digx.cz.app.domain.cardservices.entity.CardDetail cardDetailDomain = new com.ecobank.digx.cz.app.domain.cardservices.entity.CardDetail();
		CardListResponse cardListResponse = new CardListResponse();

		try {
			CardDetailKey cardDetailKey = new CardDetailKey();
			cardDetailKey.setCardId(cardStatementRequestDTO.getCardId());
			CardDetail cardDetail = cardDetailDomain.read(cardDetailKey);
			logger.info("card Id   : " + cardDetail.getRefKey().getCardId());

			// start commenting
			logger.info("customerId: " + cardDetail.getCardCustomerId());
			logger.info("maskedPan : " + cardDetail.getMaskedPan());

			RemoteFlexIFPostingAdapter repositoryAdapter = new RemoteFlexIFPostingAdapter();
			logger.info("customerId-Flex: " + cardDetail.getCustomerId());
			CustomerUDFInquiryResponseDTO custResponse = repositoryAdapter.fetchCustomerDetail("ENG",
					cardDetail.getCustomerId());
			logger.info("customerId-Flex-result: " + custResponse.getCustomerId());
			// end commenting

			if (cardStatementRequestDTO.getCardCategory().equals("CREDIT") || cardStatementRequestDTO.getCardCategory().equals("DEBIT")) {

				//cardStatementRequestDTO.(cardDetail.getCardAccount());

				CreditCardServicesRepositoryAdapter adapter = CreditCardServicesRepositoryAdapter.getInstance();
				cardListResponse = adapter.getCardByAccount(cardStatementRequestDTO);
				cardListResponse.setStatus(buildStatus(transactionStatus));
			} else if (cardStatementRequestDTO.getCardCategory().equals("CREDIT")) {
				// credit card card statement endpoint is currently not working

			}

		} catch (com.ofss.digx.infra.exceptions.Exception e) {
			e.printStackTrace();
			fillTransactionStatus(transactionStatus, e);

		} catch (RunTimeException rte) {
			rte.printStackTrace();
			fillTransactionStatus(transactionStatus, rte);

		} catch (Exception e) {
			e.printStackTrace();
			fillTransactionStatus(transactionStatus, e);

		} finally {
			Interaction.close();
		}
		super.encodeOutput(cardListResponse);
		super.checkResponsePolicy(sessionContext, cardListResponse);
		if (this.logger.isLoggable(Level.FINE)) {
			this.logger.log(Level.FINE, this.FORMATTER.formatMessage(
					"Exiting create of Self Transfer Service, SessionContext: %s, SelfTransferCreateResponse: %s ",
					new Object[] { sessionContext, cardListResponse, THIS_COMPONENT_NAME }));
		}
		return cardListResponse;
	}

	public String GetExpiryDate(String cardType) {
		SimpleDateFormat formatter = new SimpleDateFormat("MM/YY");

		String expDate = "";
		int m = 3;
		try {
			Calendar c = Calendar.getInstance();
			c.setTime(new java.util.Date()); // Now use today date.
			c.add(Calendar.MONTH, m);

			expDate = formatter.format(c.getTime());
		} catch (Exception ey) {
			ey.printStackTrace();
		}

		return expDate;
	}

	public String GetRefNumber(String type, int len) {

		String finalString = "";
		int x = 0;
		char[] stringChars = new char[len];
		for (int i = 0; i < len; i++) // 4
		{
			Random random = new Random();
			x = random.nextInt(9);

			stringChars[i] = Integer.toString(x).toCharArray()[0];
		}

		finalString = new String(stringChars);
		finalString = type + finalString;
		return finalString.trim();
	}

	private String SaveCardInformation(CardBalanceResponse cardBalanceResponse, SessionContext sessionContext,
			String cardAccount) throws com.ofss.digx.infra.exceptions.Exception {
		String username = sessionContext.getUserId();
		String customerId = sessionContext.getTransactingPartyCode();
		String expiryDate = cardBalanceResponse.getExpiry();
		String nameOnCard = cardBalanceResponse.getNameOnCard();
		String maskedPAN = cardBalanceResponse.getMaskedPan();
		String status = "";
		String ccy = cardBalanceResponse.getCcyCode();
		String cardType = cardBalanceResponse.getCardType();
		String fullPan = cardBalanceResponse.getPseudoPAN();
		String cardId = "";

		// check if card record is available
		// insert into card details table.

		CardDetailRepository repository = CardDetailRepository.getInstance();
		CardDetail cardDetailDomain2 = repository.getCardDetailByMaskedPANandExpiryDate(customerId, cardType, maskedPAN,
				expiryDate);
		if (cardDetailDomain2 == null) {
			CardDetailAssembler assembler = new CardDetailAssembler();
			CardDetail cardDetail = assembler.toCardDetailDomainObject(cardType, cardType,
					cardBalanceResponse.getMaskedPan(), cardAccount, customerId, expiryDate, nameOnCard, status, ccy,username,fullPan,cardAccount,"");
			cardDetailDomain2 = new CardDetail();
			cardDetailDomain2.create(cardDetail);
			cardBalanceResponse.setCardId(cardDetail.getRefKey().getCardId());
			cardId = cardDetail.getRefKey().getCardId();

		} else {
			cardBalanceResponse.setCardId(cardDetailDomain2.getRefKey().getCardId());
			cardId = cardDetailDomain2.getRefKey().getCardId();

		}

		return cardId;
	}
	
	private String SavePrepaidVirtualCardInformation(CardServiceResponseDTO cardBalanceResponse, SessionContext sessionContext,
			String sourceAccount, String cardType) throws com.ofss.digx.infra.exceptions.Exception {
		String username = sessionContext.getUserId();
		String customerId = sessionContext.getTransactingPartyCode();
		String expiryDate = cardBalanceResponse.getExpiryDate();
		String nameOnCard = cardBalanceResponse.getNameOnCard();
		String maskedPAN = cardBalanceResponse.getMaskedPan();
		String status = "";
		String ccy = cardBalanceResponse.getCcyCode();
		//String cardType = cardBalanceResponse.getCardType();
		String fullPan = cardBalanceResponse.getPseudoPAN();
		String cardId = "";
		String cardCustomerId = cardBalanceResponse.getCustomerNo();
		String cardAccount = cardBalanceResponse.getCardData();

		// check if card record is available
		// insert into card details table.

		CardDetailRepository repository = CardDetailRepository.getInstance();
		CardDetail cardDetailDomain2 = repository.getCardDetailByMaskedPANandExpiryDate(customerId, cardType, maskedPAN,
				expiryDate);
		if (cardDetailDomain2 == null) {
			CardDetailAssembler assembler = new CardDetailAssembler();
			CardDetail cardDetail = assembler.toCardDetailDomainObject(cardType, cardType,
					cardBalanceResponse.getMaskedPan(), sourceAccount, customerId, expiryDate, nameOnCard, status, ccy,username,fullPan,cardCustomerId,cardAccount);
			cardDetailDomain2 = new CardDetail();
			cardDetailDomain2.create(cardDetail);
			cardBalanceResponse.setCardId(cardDetail.getRefKey().getCardId());
			cardId = cardDetail.getRefKey().getCardId();

		} else {
			cardBalanceResponse.setCardId(cardDetailDomain2.getRefKey().getCardId());
			cardId = cardDetailDomain2.getRefKey().getCardId();

		}

		return cardId;
	}
	
	private String SaveLinkedDebitCardInformation(CardDetailDTO cardInfo, SessionContext sessionContext,
			String cardAccount) throws com.ofss.digx.infra.exceptions.Exception {
		String username = sessionContext.getUserId();
		String customerId = sessionContext.getTransactingPartyCode();
		String expiryDate = cardInfo.getExpiryDate();
		String nameOnCard = cardInfo.getNameOnCard();
		String maskedPAN = cardInfo.getMaskedPan();
		String status = "";
		String ccy = cardInfo.getCardCcy();
		String cardType = cardInfo.getCardType();
		String cardId = "";
		String fullPan ="";

		// check if card record is available
		// insert into card details table.

		CardDetailRepository repository = CardDetailRepository.getInstance();
		CardDetail cardDetailDomain2 = repository.getCardDetailByMaskedPANandExpiryDate(customerId, cardType, maskedPAN,
				expiryDate);
		if (cardDetailDomain2 == null) {
			CardDetailAssembler assembler = new CardDetailAssembler();
			CardDetail cardDetail = assembler.toCardDetailDomainObject(cardType, cardType,
					maskedPAN, cardAccount, customerId, expiryDate, nameOnCard, status, ccy,username,fullPan,cardAccount,"");
			cardDetailDomain2 = new CardDetail();
			cardDetailDomain2.create(cardDetail);
			//cardBalanceResponse.setCardId(cardDetail.getRefKey().getCardId());
			cardId = cardDetail.getRefKey().getCardId();

		} else {
			//cardBalanceResponse.setCardId(cardDetailDomain2.getRefKey().getCardId());
			cardId = cardDetailDomain2.getRefKey().getCardId();

		}

		return cardId;
	}
	
	

	
	public CardServiceResponseDTO completeCardRequest(SessionContext sessionContext,String actionCode, String paymentId) throws com.ofss.digx.infra.exceptions.Exception
	{
		CardServiceResponseDTO cardServiceResponse = new CardServiceResponseDTO();
		try
		{
			
			//String actionCode = cardServiceUpdateRequestDTO.getActionCode();
			System.out.println("card-request-action-code    ::: " + actionCode);

			com.ecobank.digx.cz.app.domain.cardservices.entity.CardRequest cardRequestDomain = new com.ecobank.digx.cz.app.domain.cardservices.entity.CardRequest();
			CardDetailAssembler cardDetailAssembler = new CardDetailAssembler();

			CardRequestKey cardRequestKey = new CardRequestKey();
			System.out.println("card-request-paymentId    ::: " + paymentId);
			cardRequestKey.setExternalRefNo(paymentId);

			CardRequest cardRequest2 = cardRequestDomain.read(cardRequestKey);

			// String externalRefNo = cardServiceUpdateRequestDTO.getPaymentId();
			// cardRequest2.setExternalRefNo(externalRefNo);

			System.out.println("external Ref ::: " + cardRequest2.getRefKey().getExternalRefNo());
			System.out.println("name         ::: " + cardRequest2.getNameOnCard());
			System.out.println("Customer Id         ::: " + cardRequest2.getCustomerNo());

			// String partyIdx = userPartyResponse.getParty().getId().getValue();
			String typeOfCard = cardRequest2.getTypeOfCard();
			System.out.println("Type of Card  ::: " + typeOfCard);
			if (typeOfCard.equals("VIRTUAL") || typeOfCard.equals("PREPAID")) {
				// Generate virtual card
				System.out.println("Generate virtual card ...");

				Party partyId = new Party();
				partyId.setValue(cardRequest2.getCustomerNo());

				com.ofss.digx.app.user.service.User user = new com.ofss.digx.app.user.service.User();
				UserPartyResponse userPartyResponse = user.fetchPartyInformation(sessionContext, partyId);
				XStream xs = new XStream();
				System.out.println("Cust Name 222 :::" + xs.toXML(userPartyResponse));
				String email = "", mobile = "";
				String country = "", city = "";

				String custName = userPartyResponse.getParty().getPersonalDetails().getFullName();
				Date dob = userPartyResponse.getParty().getPersonalDetails().getBirthDate();

				String address = "";
				if (userPartyResponse.getParty().getAddresses() != null
						&& userPartyResponse.getParty().getAddresses().size() > 0) {
					for (PartyAddressDTO addr : userPartyResponse.getParty().getAddresses()) {
						if (addr.getPostalAddress() != null) {

							if (addr.getPostalAddress().getLine1() != null)
								address = addr.getPostalAddress().getLine1() + " " + addr.getPostalAddress().getLine2();

							if (addr.getPostalAddress().getCity() != null)
								city = addr.getPostalAddress().getCity();

							if (addr.getPostalAddress().getCountry() != null)
								country = addr.getPostalAddress().getCountry();

							System.out.println("ADDRESS:: " + address + " " + city);
							if (city != null && !city.equals(""))
								break;
						}
					}
				}
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
				String dobs = "";
				try {
					dobs = formatter.format(dob.fetchJavaDate());
				} catch (Exception ey) {
					ey.printStackTrace();
				}

				String[] personalData = new String[5];
				personalData[0] = address;
				personalData[1] = city;
				personalData[2] = country;
				personalData[3] = dobs;

				String maskedPan = "";
				RemoteCardServicesRepositoryAdapter adapter = RemoteCardServicesRepositoryAdapter.getInstance();
				CardServiceResponseDTO resp = adapter.generateCard(cardRequest2, personalData);

				System.out.println("Card Response Generate: " + resp.getResponseCode());

				String cardIdGen = "";
				if (resp.getResponseCode().equalsIgnoreCase("000")) {

					RemoteCardServicesRepositoryAdapter adapterInquiry = RemoteCardServicesRepositoryAdapter
							.getInstance();
					ESBCardResponseDTO esbCardResponseDTO = adapterInquiry.inquiry(cardRequest2, resp);

					String pan = esbCardResponseDTO.getPan();
					maskedPan = pan.substring(0, 6) + "****" + pan.substring(pan.length() - 4);

					System.out.println("Masked PAN 24: " + maskedPan);

					System.out.println("Card Response Generate 25: " + resp.getResponseCode());
					/*
					 * cardRequest2.setResponseCode(resp.getResponseCode());
					 * cardRequest2.setResponseMessage(resp.getResponseMessage());
					 * cardRequest2.setPostedDate(new Date());
					 * cardRequest2.setMaskedCardNo(maskedPan); cardRequest2.setStatus("SUCCESS");
					 * cardRequestDomain.update(cardRequest2);
					 * System.out.println("Update Card Response Generate 22: " );
					 */

					CardRequest cardRequestDomain3 = new CardRequest();
					CardRequest cardRequest3 = cardRequestDomain3.read(cardRequestKey);

					cardRequest3.setResponseCode(resp.getResponseCode());
					cardRequest3.setResponseMessage(resp.getResponseMessage());
					cardRequest3.setPostedDate(new Date());
					cardRequest3.setMaskedCardNo(maskedPan);
					cardRequest3.setStatus("SUCCESS");
					cardRequestDomain3.update(cardRequest3);
					System.out.println("Update Card XXXX Response Generate 22: ");

					CardDetailDTO cardDetailDTO = cardDetailAssembler.fromCardRequestDTOToDomainCardDetail(cardRequest2,
							esbCardResponseDTO, resp.getCustomerNo(), maskedPan);
					CardDetailCreateRequestDTO cardDetailCreateRequestDTO = new CardDetailCreateRequestDTO();
					cardDetailCreateRequestDTO.setCardDetailDTO(cardDetailDTO);

					System.out.println("Card-Update-Here ");

					// CardDetailCreateRequestDTO cardDetailCreateRequestDTO =
					// cardDetailCreateRequestDTO.setCardDetailDTO(cardDetailDTO);
					CardDetail cardDetailDomain = new CardDetail();
					CardDetail cardDetail = cardDetailAssembler
							.toCardDetailDomainObjectCreate(cardDetailCreateRequestDTO);
					cardDetailDomain.create(cardDetail);
					cardIdGen = cardDetail.getRefKey().getCardId();

					System.out.println(
							"Card-Detail-Create- GetExternalRefNo [REFKEY]  ::: " + cardDetail.getRefKey().getCardId());

				}

				cardServiceResponse.setResponseCode(resp.getResponseCode());
				cardServiceResponse.setResponseMessage(resp.getResponseMessage());
				cardServiceResponse.setLast4Digits(maskedPan);
				cardServiceResponse.setCustomerNo(resp.getCustomerNo());
				cardServiceResponse.setCardId(cardIdGen);
			} else if (typeOfCard.equals("DEBIT")) {
				// Update Debit or Credit card request Status
				// Send email

				// Call Unified API

				System.out.println("-DEBIT-SOURCE-ACCOUNT: " + cardRequest2.getSourceAccountNo());

				DemandDepositAccount ddAccount = new DemandDepositAccount();
				AccountKey key = new AccountKey();
				key.setAccountId(cardRequest2.getSourceAccountNo());
				DemandDepositAccount sendAccountInfo = ddAccount.read(key);
				String branchCode = sendAccountInfo.getBranchId();
				
				

				System.out.println("-DEBIT-SOURCE-BRANCHCODE: " + branchCode);

				DebitCardServicesRepositoryAdapter repository = new DebitCardServicesRepositoryAdapter();
				CardServiceResponseDTO responseDTO = repository.createCardRequest(cardRequest2, branchCode,
						cardRequest2.getBranchCode());

				System.out.println("service-responseCode    : " + responseDTO.getResponseCode());
				System.out.println("service-responseMessage : " + responseDTO.getResponseMessage());
				System.out.println("service-getExternalRefNo: " + responseDTO.getExternalRefNo());

				cardRequest2.setStatus("Submitted");
				cardRequest2.setResponseCode(responseDTO.getResponseCode());
				cardRequest2.setResponseMessage(responseDTO.getResponseMessage());

				cardServiceResponse.setResponseCode(responseDTO.getResponseCode());
				cardServiceResponse.setResponseMessage(responseDTO.getResponseMessage());
				cardServiceResponse.setExternalRefNo(cardRequest2.getRefKey().getExternalRefNo());

			}
			
		}
		catch(com.ofss.digx.infra.exceptions.Exception ex)
		{
			ex.printStackTrace();
		}
		
		return cardServiceResponse;
	}

}
