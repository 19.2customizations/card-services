package com.ecobank.digx.cz.app.cardservices.dto;

import java.util.logging.Logger;

import com.ofss.digx.app.common.dto.DataTransferObject;
import com.ofss.fc.infra.log.impl.MultiEntityLogger;

public class CardServiceCreateRequestDTO extends DataTransferObject {
	
	private static final long serialVersionUID = 1L;
	private static final String THIS_COMPONENT_NAME = CardServiceCreateRequestDTO.class.getName();
	private transient MultiEntityLogger formatter;

	public CardServiceCreateRequestDTO() {
		formatter = MultiEntityLogger.getUniqueInstance();
	}
	
	private transient Logger logger = MultiEntityLogger.getUniqueInstance().getLogger(THIS_COMPONENT_NAME);
	//@Mandatory(errorCode = "DIGX_PY_0155")
	//@Schema(description = "  Holds the CardServiceDTO. It has the value for card Type, scheme Type, Currency Code etc. ", required = true)
	private CardServiceDTO cardServiceDetails;
	private String externalRefNo;

	public CardServiceDTO getCardServiceDetails() {
		return cardServiceDetails;
	}

	public void setCardServiceDetails(CardServiceDTO cardServiceDetails) {
		this.cardServiceDetails = cardServiceDetails;
	}

	public String getExternalRefNo() {
		return externalRefNo;
	}

	public void setExternalRefNo(String externalRefNo) {
		this.externalRefNo = externalRefNo;
	}

	public String toString() {
		return "SelfTransferCreateRequestDTO [transfer Details=" + cardServiceDetails + " ]";
	}
}