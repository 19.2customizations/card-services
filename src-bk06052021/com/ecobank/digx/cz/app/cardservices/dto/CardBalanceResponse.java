package com.ecobank.digx.cz.app.cardservices.dto;

import java.math.BigDecimal;

import com.ofss.digx.service.response.BaseResponseObject;
import com.ofss.fc.infra.log.impl.MultiEntityLogger;

public class CardBalanceResponse extends BaseResponseObject {

	private static final long serialVersionUID = 1L;
	private static final String THIS_COMPONENT_NAME = CardBalanceResponse.class.getName();
	private transient MultiEntityLogger formatter;

	public CardBalanceResponse() {
		formatter = MultiEntityLogger.getUniqueInstance();
	}

	
	private String cardId;
	private String maskedPan;
	private String cardAccount;
	private String expiry;
	private String nameOnCard;
	private String cardType;
	private BigDecimal balance;
	private String ccyCode;
	private String sourceAccount;
	
	private BigDecimal cardLimit;
	
	private String pseudoPAN;
	
	private String  email;
	private String  mobileNo;
	private String  cardToken;
	



	private String responseCode;
	private String responseMessage;
	
	
	public String getPseudoPAN() {
		return pseudoPAN;
	}
	public void setPseudoPAN(String pseudoPAN) {
		this.pseudoPAN = pseudoPAN;
	}

	public String getCardId() {
		return cardId;
	}
	public void setCardId(String cardId) {
		this.cardId = cardId;
	}
	public String getMaskedPan() {
		return maskedPan;
	}
	public void setMaskedPan(String maskedPan) {
		this.maskedPan = maskedPan;
	}
	public String getCardAccount() {
		return cardAccount;
	}
	public void setCardAccount(String cardAccount) {
		this.cardAccount = cardAccount;
	}
	public String getNameOnCard() {
		return nameOnCard;
	}
	public void setNameOnCard(String nameOnCard) {
		this.nameOnCard = nameOnCard;
	}
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	
	public String getCcyCode() {
		return ccyCode;
	}
	public void setCcyCode(String ccyCode) {
		this.ccyCode = ccyCode;
	}
	public String getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	public String getResponseMessage() {
		return responseMessage;
	}
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}
	public BigDecimal getBalance() {
		return balance;
	}
	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}
	public String getExpiry() {
		return expiry;
	}
	public void setExpiry(String expiry) {
		this.expiry = expiry;
	}
	public String getSourceAccount() {
		return sourceAccount;
	}
	public void setSourceAccount(String sourceAccount) {
		this.sourceAccount = sourceAccount;
	}
	public BigDecimal getCardLimit() {
		return cardLimit;
	}
	public void setCardLimit(BigDecimal cardLimit) {
		this.cardLimit = cardLimit;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getCardToken() {
		return cardToken;
	}
	public void setCardToken(String cardToken) {
		this.cardToken = cardToken;
	}
	
}