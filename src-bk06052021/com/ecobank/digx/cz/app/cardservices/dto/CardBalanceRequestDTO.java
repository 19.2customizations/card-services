package com.ecobank.digx.cz.app.cardservices.dto;



import com.ofss.digx.app.common.dto.DataTransferObject;
import com.ofss.digx.datatype.complex.Account;
import com.ofss.fc.datatype.Date;
import com.ofss.fc.infra.log.impl.MultiEntityLogger;

import io.swagger.v3.oas.annotations.media.Schema;

public class CardBalanceRequestDTO extends DataTransferObject {

	private static final long serialVersionUID = 1L;
	private static final String THIS_COMPONENT_NAME = CardBalanceRequestDTO.class.getName();
	private transient MultiEntityLogger formatter;

	public CardBalanceRequestDTO() {
		formatter = MultiEntityLogger.getUniqueInstance();
	}

	private String affiliateCode;
	private String ccyCode;
	private String maskedPan;
	private String cardAccount;
	private String fullPan;
	private String cardType;
	private String mobileNo;
	private String cardCustomerId;
	private String expiryDate;
	private String cvv;
	private String last4Digit;

	public MultiEntityLogger getFormatter() {
		return formatter;
	}
	public void setFormatter(MultiEntityLogger formatter) {
		this.formatter = formatter;
	}
	public String getAffiliateCode() {
		return affiliateCode;
	}
	public void setAffiliateCode(String affiliateCode) {
		this.affiliateCode = affiliateCode;
	}
	public String getCcyCode() {
		return ccyCode;
	}
	public void setCcyCode(String ccyCode) {
		this.ccyCode = ccyCode;
	}
	public String getMaskedPan() {
		return maskedPan;
	}
	public void setMaskedPan(String maskedPan) {
		this.maskedPan = maskedPan;
	}
	public String getCardAccount() {
		return cardAccount;
	}
	public void setCardAccount(String cardAccount) {
		this.cardAccount = cardAccount;
	}
	public String getFullPan() {
		return fullPan;
	}
	public void setFullPan(String fullPan) {
		this.fullPan = fullPan;
	}
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getCardCustomerId() {
		return cardCustomerId;
	}
	public void setCardCustomerId(String cardCustomerId) {
		this.cardCustomerId = cardCustomerId;
	}
	public String getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}
	public String getCvv() {
		return cvv;
	}
	public void setCvv(String cvv) {
		this.cvv = cvv;
	}
	public String getLast4Digit() {
		return last4Digit;
	}
	public void setLast4Digit(String last4Digit) {
		this.last4Digit = last4Digit;
	}
	
	


	
		
}