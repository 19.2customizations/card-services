package com.ecobank.digx.cz.app.cardservices.dto;

import com.ofss.digx.service.response.BaseResponseObject;

public class CardServiceCreateResponseDTO extends BaseResponseObject{
	
	private static final long serialVersionUID = 1L;
	 private String responseCode;
	 private String responseMesssage;
	 private String customerNo;
	 private String last4Digits;
	 private String ccy;
	 private String externalRefNo;
	 
	public String getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	public String getResponseMesssage() {
		return responseMesssage;
	}
	public void setResponseMesssage(String responseMesssage) {
		this.responseMesssage = responseMesssage;
	}
	public String getCustomerNo() {
		return customerNo;
	}
	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}
	public String getLast4Digits() {
		return last4Digits;
	}
	public void setLast4Digits(String last4Digits) {
		this.last4Digits = last4Digits;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	public String getExternalRefNo() {
		return externalRefNo;
	}
	public void setExternalRefNo(String externalRefNo) {
		this.externalRefNo = externalRefNo;
	}
	
	public String toString() {
		return "CardServiceCreateResponse [externalRefNo=" + externalRefNo + " ]";
	}
	
	
	
}
