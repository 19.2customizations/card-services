package com.ecobank.digx.cz.app.cardservices.dto;

import java.util.logging.Logger;

import com.ecobank.digx.cz.app.domain.cardservices.entity.CardActivityInfo;
import com.ofss.digx.app.common.dto.DataTransferObject;
import com.ofss.fc.infra.log.impl.MultiEntityLogger;

public class CardActivityRequestDTO extends DataTransferObject {
	
	private static final long serialVersionUID = 1L;
	private static final String THIS_COMPONENT_NAME = CardActivityRequestDTO.class.getName();
	private transient MultiEntityLogger formatter;

	public CardActivityRequestDTO() {
		formatter = MultiEntityLogger.getUniqueInstance();
	}
	
	private transient Logger logger = MultiEntityLogger.getUniqueInstance().getLogger(THIS_COMPONENT_NAME);
	private CardActivityInfo cardActivityInfo;
	private String refId;

	
	
	public CardActivityInfo getCardActivityInfo() {
		return cardActivityInfo;
	}


	public void setCardActivityInfo(CardActivityInfo cardActivityInfo) {
		this.cardActivityInfo = cardActivityInfo;
	}


	public String getRefId() {
		return refId;
	}



	public void setRefId(String refId) {
		this.refId = refId;
	}



	public String toString() {
		return "CardActivityDTO [Card Activity =" + cardActivityInfo.getCardId() + " ]";
	}
}