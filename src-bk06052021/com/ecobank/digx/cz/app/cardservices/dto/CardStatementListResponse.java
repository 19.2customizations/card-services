package com.ecobank.digx.cz.app.cardservices.dto;

import java.util.List;

import com.ofss.digx.service.response.BaseResponseObject;

public class CardStatementListResponse extends BaseResponseObject{
	
private static final long serialVersionUID = 1L;

	
	private String responseCode;
	private String responseMessage;
	private List<CardStatementDTO> cardStatement;

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public List<CardStatementDTO> getCardStatement() {
		return cardStatement;
	}

	public void setCardStatement(List<CardStatementDTO> cardStatement) {
		this.cardStatement = cardStatement;
	}
}
